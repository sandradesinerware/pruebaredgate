CREATE OR REPLACE TRIGGER snw_auth."MIGRLOG_TRG" BEFORE INSERT OR UPDATE ON snw_auth.MIGRLOG
FOR EACH ROW
BEGIN
  if inserting and :new.id is null then
        :new.id := MD_META.get_next_id;
    end if;
END;
/