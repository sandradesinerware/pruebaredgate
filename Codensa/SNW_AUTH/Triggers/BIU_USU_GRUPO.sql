CREATE OR REPLACE TRIGGER snw_auth."BIU_USU_GRUPO" 
  BEFORE INSERT OR UPDATE ON snw_auth."USU_GRUPO"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "USU_GRUPO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;
/