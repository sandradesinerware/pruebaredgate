CREATE OR REPLACE TRIGGER snw_auth."MD_CONSTRAINTS_TRG" BEFORE INSERT OR UPDATE ON snw_auth.MD_CONSTRAINTS
FOR EACH ROW
BEGIN
  if inserting and :new.id is null then
        :new.id := MD_META.get_next_id;
    end if;
END;
/