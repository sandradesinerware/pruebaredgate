CREATE OR REPLACE TRIGGER snw_auth."BIU_USUARIO" 
  BEFORE INSERT OR UPDATE ON snw_auth."USUARIO"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "USUARIO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;
/