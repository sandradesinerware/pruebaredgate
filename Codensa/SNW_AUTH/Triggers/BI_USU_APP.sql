CREATE OR REPLACE TRIGGER snw_auth."BI_USU_APP" 
  BEFORE INSERT ON snw_auth."USU_APP"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "USU_APP_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/