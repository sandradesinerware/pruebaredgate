CREATE OR REPLACE TRIGGER snw_auth."BIU_APLICACION" 
  BEFORE INSERT OR UPDATE ON snw_auth."APLICACION"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "APLICACION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;
/