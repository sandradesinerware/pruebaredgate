CREATE OR REPLACE TRIGGER snw_auth."BIU_GRUPO_FUNCION" 
  BEFORE INSERT OR UPDATE ON snw_auth."GRUPO_FUNCION"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "GRUPO_FUNCION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;
/