CREATE OR REPLACE PACKAGE snw_auth."AUTORIZACION" 
AS

---- Autorizacion paquete ----
  FUNCTION AUTH(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION AUTH_ALUMNOS(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION cambiar_contrasena(
      i_usuario    IN VARCHAR2,
      i_contrasena IN VARCHAR2)
    RETURN BOOLEAN;


  FUNCTION verificar_grupo(
      i_grupo      IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN RESULT_CACHE;
  FUNCTION verificar_grupo(
      i_grupo   IN NUMBER,
      i_fecha   IN DATE,
      i_usuario IN VARCHAR2)
    RETURN BOOLEAN RESULT_CACHE;
  FUNCTION verificar_privilegio(
      i_privilegio IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION verificar_privilegio_usuario(
      i_privilegio IN NUMBER,
      i_fecha      IN DATE,
      i_usuario    IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION wrap_verificar_grupo(
      i_grupo   IN NUMBER,
      i_fecha   IN DATE,
      i_usuario IN VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION wrap_verificar_grupo_agente(
      i_grupo      IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION AUTH_HASH(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN VARCHAR2 ;
  FUNCTION nombre_grupo(
      i_grupo IN grupo.id%type)
    RETURN VARCHAR2;
  FUNCTION existe_usuario(
      i_usuario VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION existe_usuario(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  PROCEDURE asociar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER);
  PROCEDURE quitar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER);
  FUNCTION obtener_nombre_usuario(
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION cambiar_fecha_expira_usuario(
      i_id_usuario       IN NUMBER,
      i_fecha_expiracion IN DATE)
    RETURN BOOLEAN;
  PROCEDURE activar_usuario(
      i_id_usuario IN NUMBER);
  FUNCTION usuario_activo(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  PROCEDURE ELIMINAR_USUARIO(
      i_id_agente IN NUMBER);
  FUNCTION VERIFICAR_USUARIO_ACTIVO(
      i_nombre_usuario IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION OBTENER_CONTRASENA(
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2 ;
  FUNCTION VALIDAR_PRIV_ACCESO_DATOS(
      i_usuario IN VARCHAR2)
    RETURN BOOLEAN ;
  FUNCTION restablecer_contrasena(
      i_usuario             IN VARCHAR2, 
      i_passAutogenerada    IN VARCHAR2,
      i_passNueva           IN VARCHAR2,
      i_passNuevaConfirmada IN VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION obtener_agente_grupo(
      i_NivelAccesoProyecto IN NUMBER,
      i_GrupoAcceso         IN NUMBER,
      i_TipoAcceso          IN NUMBER)
      RETURN VARCHAR2;  

END AUTORIZACION;
/