CREATE OR REPLACE PACKAGE snw_auth."CONTROL_ACCESO_DATOS" 
AS
  FUNCTION validar_nivel_acceso(
      i_id_nivel_acceso IN NUMBER ,
      i_usuario         IN VARCHAR ,
      i_grupo           IN NUMBER )
    RETURN BOOLEAN;
  FUNCTION wrap_validar_nivel_acceso(
      i_id_nivel_acceso IN NUMBER ,
      i_usuario         IN VARCHAR ,
      i_grupo           IN NUMBER )
    RETURN VARCHAR2 RESULT_CACHE;
  FUNCTION insertar_nivel_acceso(
      i_nivel_sup         IN NUMBER,
      i_nombre            IN VARCHAR2,
      i_tipo_nivel_acceso IN NUMBER)
    RETURN NUMBER;
  PROCEDURE eliminar_nivel_acceso(
      i_nivel IN nivel_acceso.id%type);
  PROCEDURE conceder_permiso(
      i_nivel_acceso IN NUMBER,
      i_usuario      IN VARCHAR2,
      i_grupo        IN NUMBER,
      i_estado       IN NUMBER DEFAULT 123);
  PROCEDURE conceder_permiso_agente(
      i_nivel_acceso IN NUMBER,
      i_usuario      IN NUMBER,
      i_grupo        IN NUMBER,
      i_estado       IN NUMBER DEFAULT 123);
  PROCEDURE revocar_permiso_agente(
      i_nivel_acceso IN NUMBER,
      i_usuario      IN NUMBER,
      i_grupo        IN NUMBER );
  FUNCTION es_hijo(
      i_id_padre IN NUMBER,
      i_id_hijo  IN NUMBER)
    RETURN BOOLEAN ;
  FUNCTION wrap_es_hijo(
      i_id_padre IN NUMBER,
      i_id_hijo  IN NUMBER)
    RETURN VARCHAR2 ;
  FUNCTION nombre_art_nivel_acceso(
      i_nivel IN nivel_acceso.id%type)
    RETURN CLOB;
  FUNCTION tiene_permisos_asignados(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION wrap_tiene_permisos_asignados(
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2;
  PROCEDURE cambiar_estado_permisos(
      i_id_permiso_nivel_acceso IN NUMBER,
      i_estado                  IN NUMBER);
  FUNCTION val_estado_permisos(
      i_id_usuario IN NUMBER,
      i_rol        IN NUMBER,
      i_estado     IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION permisos_x_validar(
      i_id_agente IN NUMBER)
    RETURN BOOLEAN;
  PROCEDURE revocar_total_permiso_agente(
      i_usuario IN NUMBER );
END;

/