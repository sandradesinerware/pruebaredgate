CREATE OR REPLACE PACKAGE snw_auth."SNW_GESTION_USUARIOS" as

procedure snw_reset_password (  
                                i_numero_asignado in number ,
                                o_pass             out varchar2 );


procedure  snw_update_password(
                                p_username VARCHAR2,
                                p_password VARCHAR2);

--Procedimiento para crear un usuario. No retorna resultado si no se pudo crear.
PROCEDURE snw_create_user( p_NUMERO_ASIGNADO NUMBER );

PROCEDURE associate_usr_group( i_numero_grupo NUMBER );

PROCEDURE snw_create_user_contratista(
                                      p_cedula IN NUMBER );


FUNCTION snw_authentication(
                            p_username IN VARCHAR2,
                            p_password IN VARCHAR2 )
                            RETURN BOOLEAN;

FUNCTION snw_id_usuario(
                    i_user_name in VARCHAR2 )
                    RETURN number;                           


function snw_random_password return varchar2;

function  snw_validate_pass (i_usuario  in VARCHAR2 ,
                             i_password in VARCHAR2 )
                              return boolean;


Procedure snw_update_reset_password (i_usuario  in VARCHAR2 ,
                                 i_password in VARCHAR2 );

function snw_update_reset_password (i_cambio in VARCHAR2 ,
                                 i_password in VARCHAR2 )
                                    return varchar2;

 -- POSIBLES ENTRADAS DE I_TIPO_CRUD : 'ACTUALIZAR'  --  'ELIMINAR'
 PROCEDURE snw_administrar_usuario (i_usuario           in     USUARIO%ROWTYPE,
                                     i_tipo_crud        in     varchar2,
                                     o_id_usuario       out    varchar2);

 procedure cambiar_nro_id_trab (nro_id_viejo  in number,
                                nro_id_nuevo   in number);                                    
end;

/