CREATE TABLE snw_auth.usu_grupo (
  "ID" NUMBER NOT NULL,
  grupo NUMBER,
  usuario NUMBER,
  CONSTRAINT usu_grupo_pk PRIMARY KEY ("ID"),
  CONSTRAINT usu_grupo_fk FOREIGN KEY (grupo) REFERENCES snw_auth.grupo ("ID"),
  CONSTRAINT usu_grupo_fk1 FOREIGN KEY (usuario) REFERENCES snw_auth.usuario ("ID")
);