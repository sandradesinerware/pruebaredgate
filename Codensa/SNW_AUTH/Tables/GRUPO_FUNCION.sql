CREATE TABLE snw_auth.grupo_funcion (
  "ID" NUMBER NOT NULL,
  grupo NUMBER,
  funcion NUMBER,
  CONSTRAINT grupo_funcion_pk PRIMARY KEY ("ID"),
  CONSTRAINT grupo_funcion_uq UNIQUE (grupo,funcion),
  CONSTRAINT grupo_funcion_fk FOREIGN KEY (grupo) REFERENCES snw_auth.grupo ("ID"),
  CONSTRAINT grupo_funcion_fk1 FOREIGN KEY (funcion) REFERENCES snw_auth.funcion ("ID")
);