CREATE TABLE snw_auth.usuario (
  "ID" NUMBER NOT NULL,
  nombres VARCHAR2(50 BYTE),
  apellidos VARCHAR2(50 BYTE),
  numero_identificacion NUMBER,
  fecha_expiracion DATE,
  user_name VARCHAR2(60 BYTE) NOT NULL,
  "PASSWORD" VARCHAR2(60 BYTE) NOT NULL,
  first_time CHAR CONSTRAINT usuario_chk CHECK ( "FIRST_TIME" in ('Y','N')),
  cambio VARCHAR2(4000 BYTE),
  fin_cambio DATE,
  estado NUMBER,
  CONSTRAINT usuario_pk PRIMARY KEY ("ID"),
  CONSTRAINT usuario_uk1 UNIQUE (user_name)
);