CREATE TABLE snw_auth.nivel_acceso (
  "ID" NUMBER NOT NULL,
  nivel_superior NUMBER,
  nombre VARCHAR2(200 BYTE) NOT NULL,
  tipo_acceso NUMBER NOT NULL,
  aud_actualizado_por VARCHAR2(400 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_creado_por VARCHAR2(400 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_terminal_actualizacion VARCHAR2(100 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT nivel_acceso_chk CHECK ( "ID" not in (NIVEL_SUPERIOR)),
  CONSTRAINT nivel_acceso_pk PRIMARY KEY ("ID"),
  CONSTRAINT nivel_acceso_sup_fk FOREIGN KEY (nivel_superior) REFERENCES snw_auth.nivel_acceso ("ID")
)
ENABLE ROW MOVEMENT;