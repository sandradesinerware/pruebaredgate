CREATE TABLE snw_auth.md_repoversions (
  revision NUMBER NOT NULL
);
COMMENT ON TABLE snw_auth.md_repoversions IS 'This table is used to version this schema for future requirements.';