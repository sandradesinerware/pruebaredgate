CREATE TABLE snw_auth.aplicacion (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(20 BYTE),
  "LINK" VARCHAR2(100 BYTE),
  CONSTRAINT aplicacion_pk PRIMARY KEY ("ID")
);