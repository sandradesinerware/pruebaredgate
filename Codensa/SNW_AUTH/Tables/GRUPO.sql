CREATE TABLE snw_auth.grupo (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(100 BYTE),
  aplicacion NUMBER,
  CONSTRAINT grupo_pk PRIMARY KEY ("ID"),
  CONSTRAINT grupo_fk FOREIGN KEY (aplicacion) REFERENCES snw_auth.aplicacion ("ID")
);