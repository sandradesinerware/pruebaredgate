CREATE TABLE snw_auth.tipo (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(45 BYTE),
  descripcion VARCHAR2(200 BYTE),
  activo CHAR,
  super_tipo NUMBER,
  aud_actualizado_por VARCHAR2(400 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_creado_por VARCHAR2(400 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_terminal_actualizacion VARCHAR2(100 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT tipo_pk PRIMARY KEY ("ID"),
  CONSTRAINT tipo_super_tipo_fk FOREIGN KEY (super_tipo) REFERENCES snw_auth.tipo ("ID")
)
ENABLE ROW MOVEMENT;