CREATE TABLE snw_auth.usu_app (
  "ID" NUMBER NOT NULL,
  usuario NUMBER,
  aplicacion NUMBER,
  CONSTRAINT usu_app_pk PRIMARY KEY ("ID"),
  CONSTRAINT usu_app_con UNIQUE (usuario,aplicacion),
  CONSTRAINT usu_app_fk2 FOREIGN KEY (aplicacion) REFERENCES snw_auth.aplicacion ("ID")
);