CREATE TABLE snw_auth.permiso_nivel_acceso (
  "ID" NUMBER NOT NULL,
  id_nivel_acceso NUMBER,
  id_grupo NUMBER,
  id_agente NUMBER,
  estado NUMBER,
  aud_actualizado_por VARCHAR2(400 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_creado_por VARCHAR2(400 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_terminal_actualizacion VARCHAR2(100 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT permiso_nivel_acceso_pk PRIMARY KEY ("ID"),
  CONSTRAINT permiso_nivel_acceso_uk UNIQUE (id_nivel_acceso,id_grupo,id_agente,estado),
  CONSTRAINT permiso_nivel_acceso_estado FOREIGN KEY (estado) REFERENCES snw_auth.tipo ("ID"),
  CONSTRAINT permiso_nivel_acceso_fk1 FOREIGN KEY (id_nivel_acceso) REFERENCES snw_auth.nivel_acceso ("ID"),
  CONSTRAINT permiso_nivel_acceso_fk2 FOREIGN KEY (id_agente) REFERENCES snw_auth.usuario ("ID")
)
ENABLE ROW MOVEMENT;