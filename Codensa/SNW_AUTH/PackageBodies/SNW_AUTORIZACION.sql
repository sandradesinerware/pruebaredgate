CREATE OR REPLACE PACKAGE BODY snw_auth."SNW_AUTORIZACION" AS

    --------------------------------------------------------------------------------
    FUNCTION pertenece_usu_grupo (
        i_usuario NUMBER,
        i_grupo NUMBER
    ) RETURN BOOLEAN AS
        l_conteo   NUMBER;
    BEGIN
        SELECT
            COUNT(*)
        INTO
            l_conteo
        FROM
            usu_grupo
        WHERE
            usuario = i_usuario
            AND   grupo = i_grupo;

        RETURN l_conteo > 0;
    EXCEPTION
        WHEN OTHERS THEN
            RETURN false;
    END pertenece_usu_grupo;
    --------------------------------------------------------------------------------
    PROCEDURE ASIGNAR_GRUPO(I_USER_NRO_IDENTIFICACION NUMBER,I_GRUPO NUMBER) IS
        l_conteo number;
    BEGIN
        select count(*) into l_conteo from usu_grupo where Usuario = I_USER_NRO_IDENTIFICACION and grupo= i_grupo;
        if l_conteo = 0 then
            insert into usu_grupo (usuario,grupo) values(I_USER_NRO_IDENTIFICACION,i_grupo);
        end if;
    END ASIGNAR_GRUPO;
    --------------------------------------------------------------------------------
    PROCEDURE GESTION_PADRE_NIVEL_ACCESO is
    
    begin    
    
    -- Se recorren las gerencias en UNIDAD_O y se actualizan los padres en la tabla NIVEL_ACCESO  
    for gerencia in (
        
        select id,id_padre from SNW_CARGOS.UNIDAD_ORGANIZATIVA where TIPO_UNIDAD = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('GERENCIA')
    )
    
    LOOP   
        -- Se actualiza el padre en NIVEL_ACCESO de acuerdo a U_O       
        UPDATE snw_auth.NIVEL_ACCESO set nivel_superior = gerencia.id_padre where id = gerencia.id;    
    END LOOP;
    
    End GESTION_PADRE_NIVEL_ACCESO;
    --------------------------------------------------------------------------------
    PROCEDURE GESTION_REGISTRO_NIVEL_ACCESO is
    
    l_max_unidad number;
    l_conteo number;
    
    Begin
    
    -- (1) Se recorren las lineas BP en UNIDAD_O, insertando aquellas que no existan en NIVEL_ACCESO. Si ya existe, solo se actualiza el nombre.
    for linea in (
        
        select id,id_padre,nombre,tipo_unidad from SNW_CARGOS.UNIDAD_ORGANIZATIVA where TIPO_UNIDAD = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('LINEA_BP')
    )
    
    LOOP
        -- Se verifica la existencia de la → en NIVEL_ACCESO
        select count(*) into l_conteo from NIVEL_ACCESO where id = linea.id;
        
        if l_conteo = 0 then
        
            -- Se inserta en NIVEL_ACCESO la nueva →
            INSERT INTO NIVEL_ACCESO (id,nivel_superior,nombre,tipo_acceso) VALUES (linea.id,linea.id_padre,linea.nombre,linea.tipo_unidad);                    
            
        else 
        
            UPDATE NIVEL_ACCESO SET NOMBRE = linea.nombre where id = linea.id;
        
        end if;
                
    END LOOP;
    
    -- (2) Se recorren las lineas BP en NIVEL_ACCESO, eliminando aquellas que no existan en UNIDAD_O.
    for linea in (
        
        select id,nivel_superior,nombre,tipo_acceso from NIVEL_ACCESO where TIPO_ACCESO = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('LINEA_BP')
    )
    
    LOOP
        -- Se verifica la existencia de la → en UNIDAD_O
        select count(*) into l_conteo from SNW_CARGOS.UNIDAD_ORGANIZATIVA where id = linea.id;
        
        if l_conteo = 0 then
        
            -- Se elimina la → de NIVEL_ACCESO
            DELETE FROM NIVEL_ACCESO where id = linea.id;
            
        end if;
            
    END LOOP;
        
    End GESTION_REGISTRO_NIVEL_ACCESO;
        
END snw_autorizacion;
/