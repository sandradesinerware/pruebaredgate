CREATE OR REPLACE PACKAGE BODY snw_auth."CONTROL_ACCESO_DATOS" 
IS
  FUNCTION validar_nivel_acceso(
      i_id_nivel_acceso IN NUMBER ,
      i_usuario         IN VARCHAR ,
      i_grupo           IN NUMBER )
    RETURN BOOLEAN
  AS
    l_id_agente NUMBER;
  BEGIN
    --identificar agente
    BEGIN
      SELECT id
      INTO l_id_agente
      FROM usuario
      WHERE upper(user_name) = upper(i_usuario);
    EXCEPTION
    WHEN no_data_found THEN
      l_id_agente:=-1;
      NULL;
    END;
    --bucle que escala permisos de acuerdo a arbol de control de acceso.
    FOR permiso IN
    (SELECT level,
      initcap(lower(NOMBRE)) AS title,
      ID,
      tipo_acceso
    FROM NIVEL_ACCESO
      START WITH ID                   =i_id_nivel_acceso
      CONNECT BY prior NIVEL_SUPERIOR = ID

    )
    LOOP
      DECLARE
        --l_conteo_privilegios NUMBER:=0;
      BEGIN
        --Consultar sobre permisos asociados a nodo actual del arbol de control de acceso. el valor permiso.id , el grupo y el usuario en caso de ser nulos actuan como comodines genericos
        FOR permiso_encontrado IN
        (SELECT                 *
        FROM permiso_nivel_acceso
        WHERE NVL(ID_NIVEL_ACCESO, NVL(permiso.id,0) )= NVL(permiso.id,0)
        AND (NVL(ID_GRUPO, NVL(i_grupo,0) )           = NVL(i_grupo,0)
        OR i_grupo                                   IS NULL )
        AND NVL(ID_AGENTE,l_id_agente)                =l_id_agente
        AND estado                                    = 123
        )
        LOOP
          DECLARE
            l_aplica BOOLEAN:=true;
          BEGIN
            --  dbms_output.put_line('Nivel de acceso encontrado! ID:'||permiso.id);
            --Verfica que usuario cuente con el rol solicitado.
            IF (permiso_encontrado.ID_GRUPO IS NOT NULL) THEN
              IF(autorizacion.verificar_grupo(permiso_encontrado.ID_GRUPO, sysdate, i_id_usuario => l_id_agente)) THEN
                NULL;
              ELSE
                -- dbms_output.put_line('usuario no tiene grupo autorizado!');
                l_aplica:=false;
              END IF;
            END IF;
            IF (l_aplica) THEN
              RETURN true;
              --l_conteo_privilegios:=l_conteo_privilegios+1;
            END IF;
            NULL;
          END;
        END LOOP;
        --IF(l_conteo_privilegios>0) THEN
          -- dbms_output.put_line('Permiso concedido en nivel de acceso '||permiso.title||' con escalamiento de '||(permiso.level-1));
        --  RETURN true;
        --ELSE
          --si no tiene permiso_nivel_acceso verificar si escalando encuentra permiso.
          -- dbms_output.put_line('Escalando permisos!');
          --NULL;
        --END IF;
      END;
    END LOOP;
    RETURN false;
  END validar_nivel_acceso;
  FUNCTION wrap_validar_nivel_acceso(
      i_id_nivel_acceso IN NUMBER ,
      i_usuario         IN VARCHAR ,
      i_grupo           IN NUMBER )
    RETURN VARCHAR2 RESULT_CACHE RELIES_ON (permiso_nivel_acceso)
  IS
  BEGIN
  IF(i_id_nivel_acceso is null) then
    return 'FALSE';
  end if;

    IF(validar_nivel_acceso(i_id_nivel_acceso,i_usuario, i_grupo) ) THEN
      RETURN 'TRUE';
    END IF;
    RETURN 'FALSE';
  END;
  FUNCTION insertar_nivel_acceso(
      i_nivel_sup         IN NUMBER,
      i_nombre            IN VARCHAR2,
      i_tipo_nivel_acceso IN NUMBER)
    RETURN NUMBER
  IS
    o_nivel_acceso NUMBER;
  BEGIN
    INSERT
    INTO NIVEL_ACCESO
      (
        NIVEL_SUPERIOR,
        NOMBRE,
        TIPO_ACCESO
      )
      VALUES
      (
        i_nivel_sup,
        i_nombre,
        i_tipo_nivel_acceso
      )
    RETURNING ID
    INTO o_nivel_acceso ;
    RETURN o_nivel_acceso;
  END insertar_nivel_acceso;
--------------------------------------------------------------------------------
  PROCEDURE eliminar_nivel_acceso
    (
      i_nivel IN nivel_acceso.id%type
    )
  IS
  BEGIN
    DELETE permiso_nivel_acceso WHERE id_nivel_acceso=i_nivel;
    DELETE NIVEL_ACCESO WHERE ID = i_nivel;
  EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line(SQLERRM);
  END eliminar_nivel_acceso;
--------------------------------------------------------------------------------
  PROCEDURE conceder_permiso(
      i_nivel_acceso IN NUMBER,
      i_usuario      IN VARCHAR2,
      i_grupo        IN NUMBER,
      i_estado       IN NUMBER DEFAULT 123)
  IS
    l_id_agente    NUMBER;
  BEGIN
    BEGIN
      SELECT id
      INTO l_id_agente
      FROM usuario
      WHERE upper(user_name) LIKE upper(i_usuario);
      CONTROL_ACCESO_DATOS.conceder_permiso_agente(i_nivel_acceso,l_id_agente,i_grupo,i_estado);
    EXCEPTION
    WHEN no_data_found THEN
      NULL;
    END;
  END conceder_permiso;
--------------------------------------------------------------------------------
  PROCEDURE conceder_permiso_agente
    (
      i_nivel_acceso IN NUMBER,
      i_usuario      IN NUMBER,
      i_grupo        IN NUMBER,
      i_estado       IN NUMBER DEFAULT 123
    )
  IS
    l_contador_permiso NUMBER;
    l_conteo_grupo     NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_contador_permiso
    FROM PERMISO_NIVEL_ACCESO
    WHERE ID_NIVEL_ACCESO=i_nivel_acceso
    AND ID_AGENTE        =i_usuario
    AND ID_GRUPO         =i_grupo;

    SELECT COUNT(*)
    INTO l_conteo_grupo
    FROM usu_grupo
    WHERE usuario     = i_usuario
    AND grupo         = i_grupo;

    IF(l_contador_permiso=0 )THEN
      INSERT
      INTO PERMISO_NIVEL_ACCESO
        (
          ID_NIVEL_ACCESO,
          ID_AGENTE,
          ID_GRUPO,
          ESTADO
        )
        VALUES
        (
          i_nivel_acceso,
          i_usuario,
          i_grupo,
          i_estado
        );
    END IF;
    IF(l_conteo_grupo=0 AND i_grupo IS NOT NULL AND i_estado = 123) THEN
      INSERT INTO usu_grupo
        (usuario,grupo
        ) VALUES
        (i_usuario, i_grupo
        );
    END IF;
  END conceder_permiso_agente;
--------------------------------------------------------------------------------
  PROCEDURE revocar_permiso_agente
    (
      i_nivel_acceso IN NUMBER,
      i_usuario      IN NUMBER,
      i_grupo        IN NUMBER
    )
  IS
  l_conteo_permisos_extra NUMBER;
  BEGIN

  select count(*) into l_conteo_permisos_extra from permiso_nivel_acceso where id_agente = i_usuario and id_grupo= i_grupo;

    if(l_conteo_permisos_extra <= 1) then
        delete usu_grupo where usuario = i_usuario and grupo = i_grupo;
    end if;

    DELETE
    FROM PERMISO_NIVEL_ACCESO
    WHERE ID_NIVEL_ACCESO = i_nivel_acceso
    AND ID_AGENTE         = i_usuario
    AND ID_GRUPO          = i_grupo;
  END revocar_permiso_agente;
--------------------------------------------------------------------------------
  FUNCTION es_hijo(
      i_id_padre IN NUMBER,
      i_id_hijo  IN NUMBER)
    RETURN BOOLEAN
  IS
    l_conteo NUMBER;
  BEGIN
    IF (i_id_padre IS NULL OR i_id_hijo IS NULL) THEN
      RETURN false;
    elsif (i_id_padre=i_id_hijo) THEN
      RETURN false;
    END IF;
    SELECT COUNT(*)
    INTO l_conteo
    FROM
      (SELECT id
      FROM NIVEL_ACCESO
        START WITH ID                   = i_id_hijo
        CONNECT BY prior NIVEL_SUPERIOR = ID
      )
    WHERE id       =i_id_padre;
    RETURN l_conteo>0;
  END es_hijo;
--------------------------------------------------------------------------------
  FUNCTION wrap_es_hijo(
      i_id_padre IN NUMBER,
      i_id_hijo  IN NUMBER)
    RETURN VARCHAR2
  IS
  BEGIN
    IF(es_hijo(i_id_padre, i_id_hijo)) THEN
      RETURN 'TRUE';
    ELSE
      RETURN 'FALSE';
    END IF;
    RETURN '';
  END wrap_es_hijo;
--------------------------------------------------------------------------------
  FUNCTION nombre_art_nivel_acceso(
      i_nivel IN nivel_acceso.id%type)
    RETURN CLOB
  IS
    l_nivel_acceso nivel_acceso%rowtype;
    l_nombre CLOB;
    l_art VARCHAR2(20):=' ';
  BEGIN
    SELECT * INTO l_nivel_acceso FROM nivel_acceso WHERE id=i_nivel;
    SELECT nombre INTO l_nombre FROM tipo WHERE id=l_nivel_acceso.tipo_acceso;
    IF l_nombre IN ('ENTIDAD','SEDE','FACULTAD','UAB','ESCUELA') THEN
      l_art:='LA';
    elsif l_nombre IN ('DEPARTAMENTO','INSTITUTO','CENTRO DE EXTENSION','PROYECTO') THEN
      l_art:='EL';
    END IF;
    RETURN to_clob(trim(l_art||' '||l_nivel_acceso.nombre));
  END nombre_art_nivel_acceso;
--------------------------------------------------------------------------------
  FUNCTION tiene_permisos_asignados(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN
  IS
    l_existe_registro NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_existe_registro
    FROM PERMISO_NIVEL_ACCESO
    WHERE ID_AGENTE          = i_id_usuario;
    RETURN l_existe_registro > 0;
  END tiene_permisos_asignados;
--------------------------------------------------------------------------------
  FUNCTION wrap_tiene_permisos_asignados(
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2 
  IS
  BEGIN
    IF ( tiene_permisos_asignados( i_id_usuario ) ) THEN
      RETURN 'TRUE';
    ELSE
      RETURN 'FALSE';
    END IF;
  END wrap_tiene_permisos_asignados;
--------------------------------------------------------------------------------
  PROCEDURE cambiar_estado_permisos(
      i_id_permiso_nivel_acceso IN NUMBER,
      i_estado                  IN NUMBER)
  IS
    l_permiso_nivel_acceso permiso_nivel_acceso%rowtype;
    l_conteo NUMBER;
  BEGIN
    SELECT *
    INTO l_permiso_nivel_acceso
    FROM PERMISO_NIVEL_ACCESO
    WHERE id=i_id_permiso_nivel_acceso;
    SELECT COUNT(*)
    INTO l_conteo
    FROM usu_grupo
    WHERE grupo = l_permiso_nivel_acceso.id_grupo
    AND usuario =l_permiso_nivel_acceso.id_agente;
        --TODO: cambiar comportamiento en caso de estar aprobador y luego es rechazado,
    --para retirar el grupo del usuario en caso de ser el unico
    IF(l_conteo    =0 AND i_estado=123) THEN
      INSERT
      INTO usu_grupo
        (
          usuario,
          grupo
        )
        VALUES
        (
          l_permiso_nivel_acceso.id_agente,
          l_permiso_nivel_acceso.id_grupo
        );
    END IF;
    UPDATE PERMISO_NIVEL_ACCESO
    SET ESTADO = i_estado
    WHERE ID   = i_id_permiso_nivel_acceso;
  END cambiar_estado_permisos;
------------------------------------------------------------------------------
  FUNCTION val_estado_permisos(
      i_id_usuario IN NUMBER,
      i_rol        IN NUMBER,
      i_estado     IN NUMBER)
    RETURN BOOLEAN
  IS
    l_conteo NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_conteo
    FROM usuario
    LEFT JOIN permiso_nivel_acceso
    ON (permiso_nivel_acceso.ID_AGENTE =usuario.id)
    WHERE usuario.id                   =i_id_usuario
    AND permiso_nivel_acceso.estado    = i_estado
    AND id_grupo                       =i_rol;
    RETURN l_conteo                    >0;
  END val_estado_permisos;
--------------------------------------------------------------------------------
  FUNCTION permisos_x_validar(
      i_id_agente IN NUMBER)
    RETURN BOOLEAN
  IS
    l_conteo NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_conteo
    FROM permiso_nivel_acceso
    WHERE id_agente = i_id_agente
    AND estado      = 122;
    RETURN l_conteo > 0;
  END permisos_x_validar;
--------------------------------------------------------------------------------
  PROCEDURE revocar_total_permiso_agente(
      i_usuario IN NUMBER )
  IS
  BEGIN
    DELETE FROM PERMISO_NIVEL_ACCESO WHERE ID_AGENTE = i_usuario;
    DELETE FROM USU_GRUPO WHERE USUARIO = i_usuario ;
  END revocar_total_permiso_agente;

END "CONTROL_ACCESO_DATOS";

/