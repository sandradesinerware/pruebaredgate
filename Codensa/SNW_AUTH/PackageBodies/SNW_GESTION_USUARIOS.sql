CREATE OR REPLACE PACKAGE BODY snw_auth."SNW_GESTION_USUARIOS" as
    
  L_CRUD_DELETE     VARCHAR2(100):= 'ELIMINAR';
  L_CRUD_ACTUALIZAR VARCHAR2(100):= 'ACTUALIZAR';
--------------------------------------------------------------------------------

/*
-------------------------------------------------------------------------------
Procedimiento para restaurar una contraseña
-------------------------------------------------------------------------------
*/


procedure snw_reset_password (  
                                i_numero_asignado in number ,
                                o_pass             out varchar2 )

is
l_random_password VARCHAR2(8);
l_username VARCHAR2(60);
l_usuario   USUARIO%ROWTYPE;
Begin

l_random_password := snw_random_password;

select * into l_usuario   
from usuario where numero_identificacion=i_numero_asignado;

l_usuario.password:=rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(l_random_password||l_usuario.id||l_usuario.user_name), 2 ));
l_usuario.first_time  :='Y';

snw_administrar_usuario (l_usuario ,L_CRUD_ACTUALIZAR ,l_usuario.id);

  o_pass:=l_random_password;

end snw_reset_password;


/*
-------------------------------------------------------------------------------
Procedimiento para  actualizar la contraseña
-------------------------------------------------------------------------------
*/


PROCEDURE snw_update_password(
                                p_username VARCHAR2,
                                p_password VARCHAR2  )
IS
l_usuario   USUARIO%ROWTYPE;   
BEGIN

select * into l_usuario   
from usuario where upper(user_name) = upper(p_username) ;

l_usuario.password := rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password
    ||l_usuario.id
    ||l_usuario.user_name), 2 ));
l_usuario.first_time  := 'Y';

snw_administrar_usuario (l_usuario ,L_CRUD_ACTUALIZAR ,l_usuario.id);

END snw_update_password;


PROCEDURE snw_create_user(
    p_NUMERO_ASIGNADO NUMBER )
IS
  l_trabajador CODENSA_GTH.trabajador%rowtype;
  l_password VARCHAR2(60);
  l_usuario   USUARIO%ROWTYPE;   

BEGIN
   SELECT    *     INTO    l_trabajador
     FROM    CODENSA_GTH.TRABAJADOR    WHERE
     NUMERO_IDENTIFICACION        = p_NUMERO_ASIGNADO;
   l_usuario.NOMBRES:=l_trabajador.NOMBRES;
   l_usuario.APELLIDOS:=l_trabajador.APELLIDOS;
   l_usuario.NUMERO_IDENTIFICACION:=l_trabajador.NUMERO_IDENTIFICACION;
   l_usuario.FECHA_EXPIRACION:=to_date('2099-12-31','YYYY-MM-DD');
   l_usuario.USER_NAME :='CO'||l_trabajador.identificacion;
   l_usuario.PASSWORD:= 1;
   l_usuario.FIRST_TIME:='Y';
   -- ID de USUARIO se llena por el trigger
   INSERT INTO USUARIO VALUES l_usuario;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN --No encontró el trabajador, no se crea el usuario
      NULL;
    WHEN DUP_VAL_ON_INDEX THEN --El usuario ya existe con el id o el user_name
      NULL;

END snw_create_user;    

/*
-------------------------------------------------------------------------------
Procedimiento para ASOCIAR UN GRUPO A UN USUARIO
-------------------------------------------------------------------------------
*/

PROCEDURE associate_usr_group( i_numero_grupo NUMBER ) AS
BEGIN

    for i in (select numero_identificacion from usuario where numero_identificacion not in (select usuario from usu_grupo where grupo in (i_numero_grupo)) and numero_identificacion not in (select numero_identificacion from usuario group by NUMERO_IDENTIFICACION having count(*)>1))
    loop
        begin
            insert into usu_grupo (grupo,usuario) values (i_numero_grupo,i.numero_identificacion);
            exception when others then
                dbms_output.put_line('Error en usuario:' ||i.numero_identificacion||' - '||SQLERRM);
        end;
    end loop;

END;

 /*
-------------------------------------------------------------------------------
Procedimiento para  crear un contratista
-------------------------------------------------------------------------------
*/    

PROCEDURE snw_create_user_contratista(
    p_cedula IN NUMBER )
IS
  l_contratista CODENSA_GTH.contratista%rowtype;
  l_user USUARIO%rowtype;
  l_password VARCHAR2(60);
  l_id_grupo NUMBER;
  l_usuario   USUARIO%ROWTYPE;   

BEGIN
   SELECT * INTO l_contratista
     FROM    CODENSA_GTH.CONTRATISTA
    WHERE NIT        = p_cedula;

  BEGIN
   l_usuario.NUMERO_IDENTIFICACION:=l_contratista.nit;
   l_usuario.FECHA_EXPIRACION:=to_date('2099-12-31','YYYY-MM-DD');
   l_usuario.USER_NAME :=to_char(l_contratista.nit);
   l_usuario.PASSWORD:= 1;
   l_usuario.FIRST_TIME:='Y';
  -- ID de USUARIO se llena por el trigger
   snw_administrar_usuario (l_usuario ,L_CRUD_ACTUALIZAR ,l_usuario.id);

  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN --El usuario ya existe
   null;
  END;

  SELECT *
  INTO l_user
  FROM usuario
  WHERE upper(user_name) =  upper(to_char(l_contratista.nit));

  l_user.PASSWORD:=  rawtohex(   sys.dbms_crypto.hash (  sys.utl_raw.cast_to_raw( l_contratista.nit||l_user.id||upper(to_char(l_contratista.nit)) ), 2  )   );

  snw_administrar_usuario (l_user,L_CRUD_ACTUALIZAR ,l_user.id);  

  --Por omisión un contratista es usuario de tipo Evaluador de Cliente Interno
  select id into l_id_grupo from grupo where trim(upper(nombre)) = 'EVALUADORES DE SERVICIO';

  BEGIN
    insert into usu_grupo (grupo, usuario) values (l_id_grupo, l_user.numero_identificacion);
  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN --El usu_grupo ya existe
   null;
  END;

EXCEPTION
WHEN OTHERS THEN  
  dbms_output.put_line( dbms_utility.format_error_backtrace );
  raise_application_error(-20001, 'Error encontrado en crear usuario contratista - ' || SQLCODE || ' - ' || SQLERRM);

END snw_create_user_contratista;    

/*
-------------------------------------------------------------------------------
Función para autenticar un usuario
-------------------------------------------------------------------------------
*/    

 FUNCTION snw_authentication(
    p_username IN VARCHAR2,
    p_password IN VARCHAR2 )
  RETURN BOOLEAN
IS
  l_user usuario.user_name%type := upper(p_username);
  l_pwd usuario.password%type;
  l_id usuario.id%type;
BEGIN
  SELECT id ,
    password
  INTO l_id,
    l_pwd
  FROM usuario
  WHERE  upper(user_name) =  upper(l_user);
  RETURN l_pwd   = rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password||l_id||l_user), 2 ));
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN false;
END snw_authentication; 

/*
-------------------------------------------------------------------------------
Función para obtener el id de usuario a partir del user name
-------------------------------------------------------------------------------
*/    

 FUNCTION snw_id_usuario(
    i_user_name in VARCHAR2 )
  RETURN number
IS
    l_id number;
BEGIN

    select id into l_id from usuario where UPPER(user_name) = UPPER(i_user_name);
    return l_id;    

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
END snw_id_usuario; 


/*
-------------------------------------------------------------------------------
Función para obtener un password random
-------------------------------------------------------------------------------
*/

function snw_random_password
    return varchar2
    is
l_random_password varchar2(30);
begin
select dbms_random.string('x',6) || lpad(to_char(trunc(dbms_random.value(1,99))),2,'0') into l_random_password from dual;
return l_random_password;
end snw_random_password ;    

/*
-------------------------------------------------------------------------------
Función para validar una nueva contraseña
-------------------------------------------------------------------------------
*/    


function  snw_validate_pass (i_usuario  in varchar2 ,
                             i_password in varchar2 )
                              return boolean
is
l_coincide_patron number;
begin
if trim(i_usuario) like trim(i_password) then
    return false;
    end if;

/* Revisamos que contenga:

    1. Un Digito
    2. Una letra mayúscula
    3. Una letra Minúscula
    4. Debe tener entre 8 y 30 caracteres
    5. Un caracter especial
*/

select count(*) into l_coincide_patron
from dual WHERE
    REGEXP_LIKE(i_password , '^.*[0-9]', 'c')
and REGEXP_LIKE(i_password , '^.*[A-Z]', 'c')
and REGEXP_LIKE(i_password , '^.*[a-z]', 'c')
and REGEXP_LIKE(i_password , '^.*[!"#$%&()*+,-./:;<=>?@\^_|~]') /*no incluí corchetes cuadrados ni comilla simple*/
and REGEXP_LIKE(i_password , '^[a-zA-Z0-9!"#$%&()*+,-./:;<=>?@\^_|~]{8,30}$', 'c');

if l_coincide_patron > 0 then
    return true;
    end if;
return false;
end snw_validate_pass ;

/*
-------------------------------------------------------------------------------
Porcedimiento para actualizar o restaurar la contraseña
-------------------------------------------------------------------------------
*/   
Procedure snw_update_reset_password (i_usuario  in varchar2 ,
                                 i_password in varchar2 )
is
l_numero_asignado number;
l_usuario  USUARIO%ROWTYPE;
l_pass varchar2(4000) ;

begin
select * into l_usuario  from usuario
 where  upper(USER_NAME)=  upper(i_usuario);

if i_password  is not null then
snw_update_password(l_usuario.user_name ,i_password );
 select * into l_usuario  from usuario
 where  upper(USER_NAME)=  upper(i_usuario);
l_usuario.FIRST_TIME:='N';
snw_administrar_usuario (l_usuario ,L_CRUD_ACTUALIZAR ,l_usuario.id);

else
    select numero_identificacion into l_numero_asignado
    from usuario where user_name =l_usuario.user_name;

snw_reset_password (l_numero_asignado,l_pass );

    if( l_pass  is null) then
        RAISE_APPLICATION_ERROR(-20000,'Ocurrio un problema al realizar la actualización de la contraseña');

    end if;
end if;


end snw_update_reset_password ;
/*
-------------------------------------------------------------------------------
Porcedimiento para actualizar o restaurar la contraseña para procedimento de email
-------------------------------------------------------------------------------
*/   
function snw_update_reset_password (i_cambio in VARCHAR2 ,
                                 i_password in VARCHAR2 )
return varchar2
    is
l_numero_asignado number;
l_pass varchar2(4000) ;
l_usuario  USUARIO%ROWTYPE;   

begin
select * into l_usuario  from usuario where CAMBIO= i_cambio;

if i_password  is not null then
snw_update_password(l_usuario.user_name ,i_password );
select * into l_usuario  from usuario where CAMBIO= i_cambio;
l_usuario.FIRST_TIME:='N';
snw_administrar_usuario (l_usuario ,L_CRUD_ACTUALIZAR ,l_usuario.id);


snw_update_password(l_usuario.user_name ,i_password );

return null;
else
    select numero_identificacion into l_numero_asignado
    from usuario where upper(user_name) =upper(l_usuario.user_name);

snw_reset_password (l_numero_asignado,l_pass );

    if( l_pass  is null) then
        RAISE_APPLICATION_ERROR(-20000,'Ocurrio un problema al realizar la actualización de la contraseña');
     else
         return l_pass ;

    end if;
end if;

return null;
end snw_update_reset_password ;
/*
-------------------------------------------------------------------------------
Porcedimiento centralizado para CRUD de usuario  
-------------------------------------------------------------------------------
*/


 PROCEDURE snw_administrar_usuario (i_usuario           in     USUARIO%ROWTYPE,
                                     i_tipo_crud        in     varchar2,
                                     o_id_usuario       out    varchar2)
     is
     l_contador_usuario number;
     begin
     IF (TRIM(i_tipo_crud)=TRIM(L_CRUD_DELETE)) THEN
      -- ACTUALIZAMOS LA FECHA DE EXPIRACIÓN DEL USUARIO
       UPDATE USUARIO
      SET FECHA_EXPIRACION=SYSDATE
      WHERE ID         =i_usuario.ID;

     ELSIF (TRIM(i_tipo_crud)=TRIM(L_CRUD_ACTUALIZAR)) THEN
      --EXISTEN DOS CAMINOS ACTUALIZAR O CREAR
      select count(*) into  l_contador_usuario from USUARIO WHERE ID =i_usuario.ID;
        IF (l_contador_usuario =0) THEN
        INSERT INTO USUARIO VALUES i_usuario RETURNING id INTO o_id_usuario;
      ELSE

       dbms_output.put_line(' id '||i_usuario.password);

        UPDATE USUARIO
         SET ROW =i_usuario
        WHERE id=i_usuario.ID;
        o_id_usuario:=i_usuario.ID;
      END IF;      
    END IF;  
    EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'Error encontrado en crear usuario contratista - ' || SQLCODE || ' - ' || SQLERRM);
    end snw_administrar_usuario ;

/*
-------------------------------------------------------------------------------
Porcedimiento para actualizar nro identificación  
-------------------------------------------------------------------------------
*/  


  procedure cambiar_nro_id_trab (nro_id_viejo  in number,
                                nro_id_nuevo   in number) AS

p_table_name varchar2(500):='TRABAJADOR';
p_old_value number:=nro_id_viejo;
p_new_value number:=nro_id_nuevo;
l_column_name varchar2(500);

  BEGIN
  select
cc.column_name into l_column_name
from
user_cons_columns cc
join user_constraints c on (c.constraint_name = cc.constraint_name)
where
c.constraint_type = 'P' and c.table_name = p_table_name
;

for d in (
select
cc2.table_name, cc2.column_name,cc2.constraint_name
from
user_cons_columns cc
join user_constraints c on (c.constraint_name = cc.constraint_name)
join user_constraints c2 on (c.constraint_name = c2.r_constraint_name)
join user_cons_columns cc2 on (c2.constraint_name = cc2.constraint_name)
where
c.constraint_type = 'P' and c.table_name = p_table_name
and cc.column_name = l_column_name
)
loop
execute immediate 'ALTER TABLE ' || d.table_name || ' DISABLE CONSTRAINT ' || d.constraint_name;
end loop;

execute immediate 'set constraints all deferred';

execute immediate 'update "' || p_table_name || '" set "' || l_column_name || 
'" = :B1 where "' || l_column_name || '" = :B2' using p_new_value, p_old_value;

for c in (
select
cc2.table_name, cc2.column_name
from
user_cons_columns cc
join user_constraints c on (c.constraint_name = cc.constraint_name)
join user_constraints c2 on (c.constraint_name = c2.r_constraint_name)
join user_cons_columns cc2 on (c2.constraint_name = cc2.constraint_name)
where
c.constraint_type = 'P' and c.table_name = p_table_name
and cc.column_name = l_column_name
)
loop
execute immediate 'update "' || c.table_name || '" set "' || c.column_name || 
'" = :B1 where "' || c.column_name || '" = :B2' using p_new_value, p_old_value;

DBMS_OUTPUT.put_line('actualiza la tabla '||c.table_name||' La columna '||c.column_name);

end loop;

for d in (
select
cc2.table_name, cc2.column_name,cc2.constraint_name
from
user_cons_columns cc
join user_constraints c on (c.constraint_name = cc.constraint_name)
join user_constraints c2 on (c.constraint_name = c2.r_constraint_name)
join user_cons_columns cc2 on (c2.constraint_name = cc2.constraint_name)
where
c.constraint_type = 'P' and c.table_name = p_table_name
and cc.column_name = l_column_name
)
loop
execute immediate 'ALTER TABLE ' || d.table_name || ' ENABLE CONSTRAINT ' || d.constraint_name;
end loop; 

  END cambiar_nro_id_trab;

END SNW_GESTION_USUARIOS ;
/