CREATE OR REPLACE FUNCTION snw_auth."SNW_AUTH" 
     (p_username in VARCHAR2, p_password in VARCHAR2)
return BOOLEAN
is
  l_password varchar2(4000);
  l_stored_password varchar2(4000);
  l_expires_on date;
  l_count number;

 -- l_campo_validador varchar2(100);
 -- vreturn_val number;
  --vreturn_msj_val varchar2(500);

begin
  -- Verificar si el usuario está en base de datos
  SELECT count(*) into l_count
    from USUARIO
    where upper(USER_NAME) = upper(p_username);
  if l_count > 0 then --Usuario existe. Fetch password encriptada  date
    SELECT password, fecha_expiracion into l_stored_password, l_expires_on
      from USUARIO
      where upper(USER_NAME) = upper(p_username);
    --Si la fecha de expiración se superó, se cancela la autenticación
    if l_expires_on > sysdate or l_expires_on is null then --Se puede autenticar
      --Verificar contraseña
      l_password := SNW_HASH(p_username, p_password);
      if l_password = l_stored_password then
        RETURN TRUE;
      else
        RETURN FALSE;
      end if;
    else --El usuario ya expiró
      RETURN FALSE;
    end if;
  else --El usuario no existe en la tabla usuario
    RETURN FALSE;
  end if;
END;

/