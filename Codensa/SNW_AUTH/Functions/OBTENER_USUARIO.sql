CREATE OR REPLACE FUNCTION snw_auth."OBTENER_USUARIO" (APP_USR in varchar2)
return number
is 
id_agente number;
begin 
select id into id_agente from snw_auth.USUARIO where UPPER(snw_auth.USUARIO.user_name) = UPPER(APP_USR);
return id_agente;
exception when no_data_found then
return null;
end;

/