CREATE OR REPLACE FUNCTION snw_auth."AUTH_FUNCTION" (
    p_user       VARCHAR2,
    p_function   VARCHAR2,
    p_aplicacion VARCHAR2)
  RETURN BOOLEAN
IS
  TEMP NUMBER;
BEGIN
  BEGIN

  SELECT 1
    INTO TEMP
    FROM GRUPO G
    JOIN USU_GRUPO UG
    ON (UG.GRUPO=G.ID)
    JOIN USUARIO U
    ON (U.ID=UG.USUARIO)
    JOIN GRUPO_FUNCION GF
    ON(GF.GRUPO=G.ID)
    JOIN FUNCION F
    ON (GF.FUNCION=F.ID)
    JOIN APLICACION AP
    ON(AP.ID=G.APLICACION)
    WHERE TRIM(UPPER(U.USER_NAME))=TRIM(UPPER(p_user))
    AND TRIM(UPPER(F.NOMBRE)) = TRIM(UPPER(p_function))
    AND TRIM(UPPER(AP.NOMBRE) ) = TRIM(UPPER(p_aplicacion)) ;

  EXCEPTION
  
   WHEN NO_DATA_FOUND THEN    
     RETURN FALSE;
      
   WHEN TOO_MANY_ROWS THEN  
     RETURN TRUE;
     
   WHEN OTHERS THEN   
    RETURN FALSE;    
  END;      
  
    RETURN TRUE;
END;
/