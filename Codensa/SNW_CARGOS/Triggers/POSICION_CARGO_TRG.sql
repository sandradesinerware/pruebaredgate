CREATE OR REPLACE TRIGGER snw_cargos."POSICION_CARGO_TRG" 
BEFORE INSERT ON snw_cargos.POSICION_CARGO 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT POSICION_CARGO_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/