CREATE OR REPLACE TRIGGER snw_cargos."CONCEPTO_CARGO_TRG" 
BEFORE INSERT ON snw_cargos.CONCEPTO_CARGO 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT CONCEPTO_CARGO_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/