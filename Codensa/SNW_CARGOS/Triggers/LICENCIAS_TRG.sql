CREATE OR REPLACE TRIGGER snw_cargos."LICENCIAS_TRG" 
BEFORE INSERT ON snw_cargos.LICENCIA_MATRICULA 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT LICENCIAS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/