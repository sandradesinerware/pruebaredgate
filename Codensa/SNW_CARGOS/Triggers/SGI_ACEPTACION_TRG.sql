CREATE OR REPLACE TRIGGER snw_cargos."SGI_ACEPTACION_TRG" 
BEFORE INSERT ON snw_cargos.SGI_ACEPTACION 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT SGI_ACEPTACION_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/