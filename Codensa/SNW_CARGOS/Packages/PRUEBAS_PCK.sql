CREATE OR REPLACE PACKAGE snw_cargos."PRUEBAS_PCK" AS 
  
  --Esta función retorna el id, nombre y user_name de los BPs que pueden avalar una DC
  function bp_from_cargo(i_id_cargo CARGO.id%type) return VARCHAR2;

  --Este procedimiento asigna a un trabajador a una UO (mediante su posición vigente), creando la posición si es necesario
  procedure trabajador_a_unidad(i_id_trabajador number, i_id_unidad number);

  --función para ejecutar una eliminación de cargo una vez se valide que se puede eliminar
  function ejecutar_eliminacion_cargo(i_id_cargo CARGO.id%type) return BOOLEAN;

  --Este procedimiento elimina una versión de una descripción de cargo
  procedure eliminar_version_cargo(i_id_cargo CARGO.id%type);

END PRUEBAS_PCK;

/