CREATE OR REPLACE PACKAGE snw_cargos."ADMIN_VALIDACION_PCK" AS 

function completitud_identificacion(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function completitud_ubicacion(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  

function completitud_responsabilidades(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  
function completitud_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  

function completitud_otros(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 
    
--------------------------------------------------------------------  

function completitud_digital_skills(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  

function completitud_dimensiones(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

-------------------------------------------------------------------- 

function completitud_decisiones(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

-------------------------------------------------------------------- 

function completitud_contactos(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

-------------------------------------------------------------------- 

function existe_concepto_bp(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function existe_concepto_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function concepto_bp(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function concepto_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function cancela_dc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

END ADMIN_VALIDACION_PCK;
/