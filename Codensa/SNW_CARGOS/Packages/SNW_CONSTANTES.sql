CREATE OR REPLACE PACKAGE snw_cargos."SNW_CONSTANTES" as
function constante_tipo(i_nombre in varchar2) return number;
function generar_constante_tipo(i_tipo_nombre in varchar2) return varchar2;

--Esta función retorna el id del tipo que corresponde a un nombre de dominio y tipo de dominio particular
function get_id_tipo(i_nombre in varchar2, i_tipo_constante in varchar2) return number;

--Esta función retorna el nombre del id del tipo entregado en el schema CARGOS
function get_nombre_tipo(i_id_tipo in number) return varchar2;

end;

/