CREATE OR REPLACE FORCE VIEW snw_cargos.v_rama_unidad_id ("ID",id_gerencia,id_sub_gerencia,id_division,id_departamento) AS
select 
id,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('GERENCIA')) id_gerencia,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('SUBGERENCIA')) id_sub_gerencia,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('DIVISION')) id_division,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('DEPARTAMENTO')) id_departamento
from unidad_organizativa;