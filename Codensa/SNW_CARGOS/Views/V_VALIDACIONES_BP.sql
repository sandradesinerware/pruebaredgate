CREATE OR REPLACE FORCE VIEW snw_cargos.v_validaciones_bp (identificacion_bp,business_partner,linea_bp,numero_validaciones) AS
select tr.NUMERO_IDENTIFICACION identificacion_bp,
tr.NOMBRES || ' ' || tr.APELLIDOS business_partner,
uo.nombre linea_bp,
count(*) numero_validaciones
from usuario us
inner join concepto_cargo cc on (cc.USUARIO = us.USER_NAME)
inner join trabajador tr on (us.numero_identificacion = tr.NUMERO_IDENTIFICACION)
inner join BUSINESS_PARTNER bs on (tr.id = bs.ID_TRABAJADOR)
inner join UNIDAD_ORGANIZATIVA uo on (uo.id = bs.ID_UNIDAD_ORGANIZATIVA)
where us.id in (select usuario from SNW_AUTH.USU_GRUPO where grupo = 9) and
cc.tipo = SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_BP') and
cc.concepto = SNW_CONSTANTES.CONSTANTE_TIPO('APROBADO_BP') 
group by tr.NUMERO_IDENTIFICACION, tr.NOMBRES || ' ' || tr.APELLIDOS, uo.nombre;