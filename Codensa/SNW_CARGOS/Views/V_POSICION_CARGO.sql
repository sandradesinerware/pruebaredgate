CREATE OR REPLACE FORCE VIEW snw_cargos.v_posicion_cargo (id_cargo,id_posicion,denominacion,id_tipo_ubicacion,tipo_ubicacion,id_unidad_organizativa,gerencia,subgerencia,division,departamento,nombres,apellidos,numero_identificacion,fecha_inicio,fecha_fin,codigo,codigo_ocupacion,pos_res) AS
select 
cargo.id id_cargo, 
posicion.id id_posicion,
cargo.denominacion,
cargo.tipo_ubicacion id_tipo_ubicacion,
(select nombre from tipo where id = cargo.tipo_ubicacion) tipo_ubicacion,
posicion.id_unidad_organizativa id_unidad_organizativa, 
v_rama_unidad_nombre.gerencia gerencia,
v_rama_unidad_nombre.subgerencia subgerencia,
v_rama_unidad_nombre.division division,
v_rama_unidad_nombre.departamento departamento,
TRABAJADOR.nombres,
TRABAJADOR.apellidos,
TRABAJADOR.numero_identificacion,
posicion_trabajador.fecha_inicio fecha_inicio,
posicion_trabajador.fecha_fin fecha_fin,
posicion.codigo,
cargo.codigo_funcion codigo_ocupacion,
posicion.pos_res pos_res
from posicion_trabajador 
left join posicion_cargo on (posicion_cargo.id_posicion = posicion_trabajador.id_posicion and 
sysdate between posicion_cargo.fecha_inicio and posicion_cargo.fecha_fin or posicion_cargo.fecha_fin is null)
left join cargo on (cargo.id = posicion_cargo.id_cargo)
left join posicion on (posicion_trabajador.id_posicion = posicion.id)
left join TRABAJADOR on (TRABAJADOR.id = posicion_trabajador.id_trabajador)
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = posicion.id_unidad_organizativa)
where  (sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null);