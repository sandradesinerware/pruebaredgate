CREATE OR REPLACE FORCE VIEW snw_cargos.v_cargo_vigencia ("ID",estado,fecha_inicio,fecha_fin,codigo_funcion,"VERSION",denominacion,subgrupo_profesional,nombre_gerencia,nombre_subgerencia,nombre_division,nombre_departamento,solicitante) AS
select 
ID,
decode(vigencia,'S','Vigente','N','No Vigente') ESTADO, 
FECHA_INICIO, 
FECHA_FIN, 
CODIGO_FUNCION,
version_Cargo version,
DENOMINACION,
(select nombre from subgrupo_profesional where id = SUBGRUPO_PROFESIONAL) SUBGRUPO_PROFESIONAL,
(select GERENCIA from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_GERENCIA,
(select SUBGERENCIA from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_SUBGERENCIA,
(select DIVISION from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_DIVISION,
(select DEPARTAMENTO from V_RAMA_UNIDAD_NOMBRE where id = ID_UNIDAD_ORGANIZATIVA) NOMBRE_DEPARTAMENTO,
FORMULARIO.DENOMINACION_DE_USUARIO(elaboro) SOLICITANTE
from cargo
where (VIGENCIA = 'S' or VIGENCIA = 'N') and VERSION_CARGO is not null
order by fecha_inicio asc;