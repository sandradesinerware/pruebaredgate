CREATE OR REPLACE FORCE VIEW snw_cargos.v_posicion (id_cargo,id_posicion,id_unidad_organizativa,nombre_unidad,nombres,apellidos,numero_identificacion,fecha_inicio,fecha_fin,codigo_posicion,pos_res) AS
select 
VERSIONADOR_PCK.BUSCAR_CARGO_VIGENTE( ADMIN_UNIDAD_ORGANIZATIVA.BUSCAR_CARGO_TRABAJADOR(posicion_trabajador.id_trabajador)) id_cargo, 
posicion.id id_posicion,
posicion.id_unidad_organizativa id_unidad_organizativa, 
(select nombre from unidad_organizativa where id = posicion.id_unidad_organizativa) nombre_unidad,
trabajador.nombres,
trabajador.apellidos,
trabajador.numero_identificacion,
posicion_trabajador.fecha_inicio fecha_inicio,
posicion_trabajador.fecha_fin fecha_fin,
posicion.codigo codigo_posicion,
posicion.pos_res pos_res
from posicion  
left join posicion_trabajador on (posicion_trabajador.id_posicion = posicion.id)
left join TRABAJADOR on (TRABAJADOR.id = posicion_trabajador.id_trabajador)
where  (sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null);