CREATE OR REPLACE FORCE VIEW snw_cargos.v_competencias_cargo (id_cargo,tipo,skill,especificacion) AS
select 
id_cargo,
'Específica' tipo,
(select nombre from digital_skill where id = id_digital_skill) skill,
especificacion
 from CARGO_COMPETENCIA_ESPECIFICA
;