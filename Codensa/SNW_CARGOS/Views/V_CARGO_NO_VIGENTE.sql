CREATE OR REPLACE FORCE VIEW snw_cargos.v_cargo_no_vigente ("ID",id_unidad_organizativa,nombre_unidad,subgrupo_profesional,denominacion,codigo_funcion,gerencia,subgerencia,division,departamento,fecha_inicio,personas_impacto) AS
select 
cargo.id, 
cargo.id_unidad_organizativa, 
(select nombre from unidad_organizativa where cargo.id_unidad_organizativa = id) NOMBRE_UNIDAD,
cargo.subgrupo_profesional, 
cargo.denominacion, 
cargo.codigo_funcion,
v_rama_unidad_nombre.gerencia gerencia,
v_rama_unidad_nombre.subgerencia subgerencia,
v_rama_unidad_nombre.division division,
v_rama_unidad_nombre.departamento departamento,
fecha_inicio fecha_inicio,
ADMIN_UNIDAD_ORGANIZATIVA.PERSONAS_IMPACTO(cargo.id) personas_impacto
from cargo
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = cargo.id_unidad_organizativa)
where VERSIONADOR_PCK.CARGO_VIGENTE(cargo.id) = 'N';