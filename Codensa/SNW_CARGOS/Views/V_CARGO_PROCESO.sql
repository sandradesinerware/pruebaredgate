CREATE OR REPLACE FORCE VIEW snw_cargos.v_cargo_proceso ("ID",estado,fecha_actividad,tiempo_actividad,codigo_funcion,denominacion,nombre_gerencia,nombre_subgerencia,nombre_division,nombre_departamento,solicitante) AS
select 
ID,
ESTADO,
FECHA_ACTIVIDAD,
trunc(to_date(sysdate,'DD/MM/YYYY'))-trunc(to_date(FECHA_ACTIVIDAD,'DD/MM/YYYY')) as TIEMPO_ACTIVIDAD,
CODIGO_FUNCION,
DENOMINACION,
NOMBRE_GERENCIA,
NOMBRE_SUBGERENCIA,
NOMBRE_DIVISION,
NOMBRE_DEPARTAMENTO,
FORMULARIO.DENOMINACION_DE_USUARIO(elaboro) SOLICITANTE
from V_BANDEJA
WHERE ID_ACTIVIDAD_VIGENTE in (325, 345, 346)
order by fecha_actividad asc;