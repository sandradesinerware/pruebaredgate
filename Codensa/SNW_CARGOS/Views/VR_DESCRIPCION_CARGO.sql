CREATE OR REPLACE FORCE VIEW snw_cargos.vr_descripcion_cargo ("ID",id_unidad_organizativa,denominacion,subgrupo,tipo_ubicacion,codigo_funcion,subgrupo_profesional,category_enel,"ROLES",functional,homologacion,codigo_homologacion,fecha_creacion,fecha_actualizacion,"VERSION",gerencia,subgerencia,division,departamento,cargo_superior,personas_directas,personas_indirectas,recursos_empresas,recursos_personas,total,mision,basico_nivel,basico_especificacion,extra_nivel,complementario_especificacion,experiencia,ingles_nivel,ingles_especificacion,complementario_nivel,idioma_extra_especificacion,digital_transversal,digital_especifica,teletrabajo,detalles_teletrabajo,otros,elaboro,reviso,aprobo,software,licencias,skills,economicas,cualitativas,tomar,proponer,internas,externas,reportan,vigencia) AS
select 
id,
id_unidad_organizativa,
denominacion, 
subgrupo_profesional subgrupo,
tipo_ubicacion,
nvl(TO_CHAR(codigo_funcion),'-') codigo_funcion,
nvl((select nombre from SUBGRUPO_PROFESIONAL where id = SUBGRUPO_PROFESIONAL),'-') subgrupo_profesional, 
nvl((select nombre from tipo where id = category_enel),'-') category_enel, 
nvl((select nombre from tipo where id = roles),'-') roles,
nvl((select nombre from tipo where id = functional_area),'-') functional,
nvl(homologacion,'-') homologacion,
nvl(codigo_homologacion,'-') codigo_homologacion,
nvl(TO_CHAR(VERSIONADOR_PCK.FECHA_CREACION(id)),'-') fecha_creacion,
nvl(TO_CHAR(VERSIONADOR_PCK.FECHA_ACTUALIZACION(id)),'-') fecha_actualizacion,
nvl(TO_CHAR(version_cargo),'-') version,
nvl(nombre_gerencia,'-') gerencia,
nvl(nombre_subgerencia,'-') subgerencia,
nvl(nombre_division,'-') division,
nvl(nombre_departamento,'-') departamento,
nvl(cargo_superior_jerarquico,'No aplica') cargo_superior,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(personas_directas),'-') end personas_directas,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(personas_indirectas),'-') end personas_indirectas,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(recursos_empresas),'-') end recursos_empresas,
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(recursos_personas),'-') end recursos_personas, 
CASE  
when SUBGRUPO_PROFESIONAL not in(select id from subgrupo_profesional where jefe='S') or tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') then 'No aplica' 
else nvl(TO_CHAR(personas_directas + personas_indirectas),'-') end total,
nvl(mision,'-') mision,
(select nombre from tipo where id = basico_nivel) basico_nivel,
basico_Especificacion,
(select nombre from tipo where id = nivel_complementario) extra_nivel,
nvl(complementario_especificacion,'-') complementario_especificacion,
nvl(experiencia,'-') experiencia,
(select nombre from tipo where id = idioma_nivel) ingles_nivel,
(select nombre from tipo where id = idioma_especificacion) ingles_especificacion,
nvl((select nombre from tipo where id = complementario_nivel),'-') complementario_nivel,
nvl(IDIOMA_EXTRA_ESPECIFICACION,'-') IDIOMA_EXTRA_ESPECIFICACION,
digital_transversal,
nvl(digital_especifica,'-') digital_especifica,
nvl((select nombre from tipo where id = teletrabajo),'-') teletrabajo,
detalles_teletrabajo,
nvl(otros,'-') otros,
nombre_elaboro elaboro,
nombre_reviso reviso,
nombre_aprobo aprobo,
(SELECT count(*) FROM SOFTWARE WHERE ID_CARGO = cargo.id) software,
(SELECT COUNT(*) FROM LICENCIA_MATRICULA WHERE ID_CARGO = cargo.id) licencias,
(SELECT COUNT(*) FROM CARGO_COMPETENCIA_ESPECIFICA WHERE ID_CARGO = cargo.id) skills,
(SELECT count(*) FROM DIMENSION WHERE TIPO_VARIABLE = SNW_CONSTANTES.CONSTANTE_TIPO('ECONOMICA') AND ID_CARGO = cargo.id) economicas,
(SELECT count(*) FROM DIMENSION WHERE TIPO_VARIABLE = SNW_CONSTANTES.CONSTANTE_TIPO('CUALITATIVA') AND ID_CARGO = cargo.id) cualitativas,
(SELECT count(*) FROM DECISION WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('TOMAR') AND ID_CARGO = cargo.id) tomar,
(SELECT count(*) FROM DECISION WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('PROPONER') AND ID_CARGO = cargo.id) proponer,
(SELECT count(*) FROM RESPONSABILIDAD_CONTACTOS WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('INTERNAS') AND ID_CARGO = cargo.id) internas,
(SELECT count(*) FROM RESPONSABILIDAD_CONTACTOS WHERE TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('EXTERNAS') AND ID_CARGO = cargo.id) externas,
(SELECT COUNT(*) FROM CARGOS_REPORTAN WHERE ID_CARGO = cargo.id) reportan,
vigencia
from cargo;