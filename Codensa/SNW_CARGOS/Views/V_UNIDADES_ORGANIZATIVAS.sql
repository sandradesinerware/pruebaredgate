CREATE OR REPLACE FORCE VIEW snw_cargos.v_unidades_organizativas ("ID",nivel,nombre,tipo_unidad,tipo_unidad_nombre,raiz,codigo_codensa,codigo_emgesa) AS
select id, level nivel, 
  CONCAT ( LPAD (
         ' ',
         LEVEL*3-3
      ),
      nombre
   ) nombre, 
   tipo_unidad, (select nombre from tipo where id = tipo_unidad) tipo_unidad_nombre,
  SYS_CONNECT_BY_PATH(nombre,'|') raiz,
  CODIGO_CODENSA, CODIGO_EMGESA
from unidad_organizativa
start with id_padre is null
connect by prior id = id_padre;