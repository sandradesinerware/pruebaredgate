CREATE OR REPLACE FORCE VIEW snw_cargos.v_version_cargo ("ID",id_cargo_padre,version_cargo,fecha_inicio,fecha_fin,estado,denominacion,gerencia,subgerencia,division,departamento) AS
select 
cargo.id, 
cargo.id_cargo_padre id_cargo_padre,
cargo.version_cargo,
cargo.fecha_inicio fecha_inicio,
cargo.fecha_fin fecha_fin,
(select nombre from tipo where id = cargo.estado) estado,
cargo.denominacion, 
v_rama_unidad_nombre.gerencia gerencia,
v_rama_unidad_nombre.subgerencia subgerencia,
v_rama_unidad_nombre.division division,
v_rama_unidad_nombre.departamento departamento
from cargo
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = cargo.id_unidad_organizativa)
where VERSIONADOR_PCK.CARGO_VIGENTE(cargo.id) in ('N','S');