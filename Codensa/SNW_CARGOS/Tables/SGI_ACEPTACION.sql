CREATE TABLE snw_cargos.sgi_aceptacion (
  "ID" NUMBER NOT NULL,
  id_trabajador NUMBER,
  id_version NUMBER,
  aceptacion NUMBER,
  fecha_notificacion DATE DEFAULT SYSDATE,
  fecha_aceptacion DATE,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  comentarios VARCHAR2(4000 BYTE),
  CONSTRAINT sgi_aceptacion_pk PRIMARY KEY ("ID"),
  CONSTRAINT sgi_aceptacion_fk1 FOREIGN KEY (aceptacion) REFERENCES snw_cargos.tipo ("ID")
);