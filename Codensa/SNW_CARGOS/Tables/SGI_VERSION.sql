CREATE TABLE snw_cargos.sgi_version (
  "ID" NUMBER NOT NULL,
  tipo NUMBER,
  nombre NUMBER,
  fecha_actualizacion DATE DEFAULT SYSDATE,
  justificacion VARCHAR2(4000 BYTE),
  vigente CHAR,
  consecutivo NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  version_oficial VARCHAR2(400 BYTE),
  CONSTRAINT sgi_version_pk PRIMARY KEY ("ID")
);