CREATE TABLE snw_cargos.posicion (
  "ID" NUMBER NOT NULL,
  codigo NUMBER,
  nombre VARCHAR2(2000 BYTE),
  id_unidad_organizativa NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  pos_res CHAR,
  CONSTRAINT posicion_pk PRIMARY KEY ("ID"),
  CONSTRAINT posicion_fk1 FOREIGN KEY (id_unidad_organizativa) REFERENCES snw_cargos.unidad_organizativa ("ID")
);