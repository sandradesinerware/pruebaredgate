CREATE TABLE snw_cargos.unidad_organizativa (
  "ID" NUMBER NOT NULL,
  id_padre NUMBER,
  nombre VARCHAR2(80 BYTE),
  tipo_unidad NUMBER,
  estado CHAR,
  codigo_emgesa VARCHAR2(20 BYTE),
  codigo_codensa VARCHAR2(20 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT unidad_organizativa_pk PRIMARY KEY ("ID"),
  CONSTRAINT unidad_organizativa_fk1 FOREIGN KEY (id_padre) REFERENCES snw_cargos.unidad_organizativa ("ID"),
  CONSTRAINT unidad_organizativa_fk2 FOREIGN KEY (tipo_unidad) REFERENCES snw_cargos.tipo ("ID")
);