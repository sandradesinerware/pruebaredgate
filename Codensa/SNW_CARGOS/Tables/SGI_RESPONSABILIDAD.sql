CREATE TABLE snw_cargos.sgi_responsabilidad (
  "ID" NUMBER NOT NULL,
  id_version NUMBER,
  descripcion VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  clase_responsabilidades NUMBER,
  tipo_cambio NUMBER,
  CONSTRAINT sgi_responsabilidad_pk PRIMARY KEY ("ID"),
  CONSTRAINT fk_clase_responsabilidad FOREIGN KEY (clase_responsabilidades) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT fk_tipo_cambio FOREIGN KEY (tipo_cambio) REFERENCES snw_cargos.tipo ("ID")
);