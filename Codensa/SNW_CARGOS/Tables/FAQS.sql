CREATE TABLE snw_cargos.faqs (
  "ID" VARCHAR2(20 BYTE) NOT NULL,
  pregunta VARCHAR2(4000 BYTE),
  respuesta VARCHAR2(4000 BYTE),
  seccion VARCHAR2(2000 BYTE),
  CONSTRAINT faqs_pk PRIMARY KEY ("ID")
);