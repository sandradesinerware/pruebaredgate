CREATE TABLE snw_cargos.sgi_subgrupo_nivel (
  "ID" NUMBER NOT NULL,
  id_subgrupo NUMBER,
  id_nivel NUMBER,
  CONSTRAINT sgi_subgrupo_nivel_pk PRIMARY KEY ("ID"),
  CONSTRAINT sgi_subgrupo_nivel_fk1 FOREIGN KEY (id_subgrupo) REFERENCES snw_cargos.subgrupo_profesional ("ID"),
  CONSTRAINT sgi_subgrupo_nivel_fk2 FOREIGN KEY (id_nivel) REFERENCES snw_cargos.sgi_nombre ("ID") DISABLE NOVALIDATE
);