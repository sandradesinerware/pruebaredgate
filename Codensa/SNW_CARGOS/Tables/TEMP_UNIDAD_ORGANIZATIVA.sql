CREATE TABLE snw_cargos.temp_unidad_organizativa (
  "ID" NUMBER NOT NULL,
  cod_unidad NUMBER,
  nombre_unidad VARCHAR2(4000 BYTE),
  cod_gerencia NUMBER,
  nombre_gerencia VARCHAR2(4000 BYTE),
  cod_subgerencia NUMBER,
  nombre_subgerencia VARCHAR2(4000 BYTE),
  cod_division NUMBER,
  nombre_division VARCHAR2(4000 BYTE),
  cod_departamento NUMBER,
  nombre_departamento VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT temp_unidad_organizativa_pk PRIMARY KEY ("ID")
);