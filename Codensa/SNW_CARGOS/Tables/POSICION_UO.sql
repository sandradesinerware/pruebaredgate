CREATE TABLE snw_cargos.posicion_uo (
  "ID" NUMBER NOT NULL,
  id_posicion NUMBER,
  id_unidad_organizativa NUMBER,
  fecha_inicio DATE,
  fecha_fin DATE,
  nombre_unidad VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT posicion_uo_pk PRIMARY KEY ("ID"),
  CONSTRAINT posicion_uo_fk1 FOREIGN KEY (id_unidad_organizativa) REFERENCES snw_cargos.unidad_organizativa ("ID"),
  CONSTRAINT posicion_uo_fk2 FOREIGN KEY (id_posicion) REFERENCES snw_cargos.posicion ("ID")
);