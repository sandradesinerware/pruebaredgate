CREATE TABLE snw_cargos.concepto_cargo (
  "ID" NUMBER NOT NULL,
  id_cargo NUMBER,
  concepto NUMBER,
  tipo NUMBER,
  comentario VARCHAR2(2000 BYTE),
  usuario VARCHAR2(100 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT concepto_cargo_pk PRIMARY KEY ("ID"),
  CONSTRAINT concepto_cargo_fk1 FOREIGN KEY (concepto) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT concepto_cargo_fk2 FOREIGN KEY (tipo) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT concepto_cargo_fk3 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID")
);