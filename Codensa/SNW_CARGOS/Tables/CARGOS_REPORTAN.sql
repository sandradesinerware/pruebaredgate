CREATE TABLE snw_cargos.cargos_reportan (
  "ID" NUMBER NOT NULL,
  denominacion VARCHAR2(4000 BYTE),
  id_cargo NUMBER NOT NULL,
  CONSTRAINT cargos_reportan_pk PRIMARY KEY ("ID"),
  CONSTRAINT cargos_reportan_fk1 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID")
);