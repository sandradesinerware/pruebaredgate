CREATE TABLE snw_cargos.subgrupo_profesional (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(4000 BYTE),
  tipo NUMBER NOT NULL,
  tipo_complementario NUMBER,
  ingles NUMBER,
  ingles_especificacion NUMBER,
  jefe CHAR,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT subgrupo_profesional_pk PRIMARY KEY ("ID"),
  CONSTRAINT subgrupo_profesional_fk1 FOREIGN KEY (tipo) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT subgrupo_profesional_fk2 FOREIGN KEY (tipo_complementario) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT subgrupo_profesional_fk3 FOREIGN KEY (ingles) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT subgrupo_profesional_fk4 FOREIGN KEY (ingles_especificacion) REFERENCES snw_cargos.tipo ("ID")
);