CREATE TABLE snw_cargos.business_partner (
  "ID" NUMBER NOT NULL,
  id_unidad_organizativa NUMBER,
  id_trabajador NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT business_partner_pk PRIMARY KEY ("ID"),
  CONSTRAINT business_partner_fk1 FOREIGN KEY (id_unidad_organizativa) REFERENCES snw_cargos.unidad_organizativa ("ID")
);