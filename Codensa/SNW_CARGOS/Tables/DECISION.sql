CREATE TABLE snw_cargos.decision (
  "ID" NUMBER NOT NULL,
  descripcion VARCHAR2(600 BYTE),
  id_cargo NUMBER NOT NULL,
  tipo NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT desiciones_pk PRIMARY KEY ("ID"),
  CONSTRAINT decision_fk1 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID"),
  CONSTRAINT desiciones_fk2 FOREIGN KEY (tipo) REFERENCES snw_cargos.tipo ("ID")
);