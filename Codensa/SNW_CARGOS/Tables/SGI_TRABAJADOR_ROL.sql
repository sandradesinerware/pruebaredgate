CREATE TABLE snw_cargos.sgi_trabajador_rol (
  "ID" NUMBER NOT NULL,
  id_trabajador NUMBER,
  id_rol NUMBER,
  fecha_inicio DATE,
  fecha_fin DATE,
  CONSTRAINT sgi_trabajador_rol_pk PRIMARY KEY ("ID"),
  CONSTRAINT sgi_trabajador_rol_fk2 FOREIGN KEY (id_rol) REFERENCES snw_cargos.sgi_nombre ("ID")
);