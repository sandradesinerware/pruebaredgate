CREATE TABLE snw_cargos.md_file_artifacts (
  "ID" NUMBER NOT NULL CONSTRAINT md_app_file_art_nonull CHECK ("ID" IS NOT NULL),
  applicationfiles_id NUMBER NOT NULL CONSTRAINT md_appl_file_fk_nonull CHECK ("APPLICATIONFILES_ID" IS NOT NULL),
  "PATTERN" VARCHAR2(4000 BYTE),
  string_found VARCHAR2(4000 BYTE),
  string_replaced VARCHAR2(4000 BYTE),
  "TYPE" VARCHAR2(200 BYTE),
  status VARCHAR2(4000 BYTE),
  line NUMBER,
  pattern_start NUMBER,
  pattern_end NUMBER,
  due_date DATE,
  db_type VARCHAR2(100 BYTE),
  code_type VARCHAR2(1000 BYTE),
  description VARCHAR2(4000 BYTE),
  "PRIORITY" NUMBER(*,0),
  security_group_id VARCHAR2(20 BYTE) DEFAULT '0' NOT NULL,
  created_on DATE DEFAULT sysdate NOT NULL,
  created_by VARCHAR2(4000 BYTE),
  last_updated DATE,
  last_updated_by VARCHAR2(4000 BYTE),
  CONSTRAINT md_file_artifacts_pk PRIMARY KEY ("ID")
);
COMMENT ON TABLE snw_cargos.md_file_artifacts IS 'Holds a tuple for each interesting thing the scanner finds in a file';