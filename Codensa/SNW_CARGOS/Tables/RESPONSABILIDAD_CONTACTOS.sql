CREATE TABLE snw_cargos.responsabilidad_contactos (
  "ID" NUMBER NOT NULL,
  tipo NUMBER,
  interaccion VARCHAR2(1000 BYTE),
  proposito VARCHAR2(1000 BYTE),
  frecuencia NUMBER,
  id_cargo NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT responsabilidad_contactos_pk PRIMARY KEY ("ID"),
  CONSTRAINT responsabilidad_contactos_fk1 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID"),
  CONSTRAINT responsabilidad_contactos_fk2 FOREIGN KEY (tipo) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT responsabilidad_contactos_fk3 FOREIGN KEY (frecuencia) REFERENCES snw_cargos.tipo ("ID")
);