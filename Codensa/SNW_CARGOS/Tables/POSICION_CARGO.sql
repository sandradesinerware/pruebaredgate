CREATE TABLE snw_cargos.posicion_cargo (
  "ID" NUMBER NOT NULL,
  id_posicion NUMBER,
  id_cargo NUMBER,
  fecha_inicio DATE,
  fecha_fin DATE,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT posicion_cargo_pk PRIMARY KEY ("ID"),
  CONSTRAINT posicion_cargo_fk1 FOREIGN KEY (id_posicion) REFERENCES snw_cargos.posicion ("ID"),
  CONSTRAINT posicion_cargo_fk2 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID")
);