CREATE TABLE snw_cargos.tipo (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(2000 BYTE),
  tipo NUMBER,
  constante VARCHAR2(4000 BYTE),
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  CONSTRAINT tipo_pk PRIMARY KEY ("ID")
);