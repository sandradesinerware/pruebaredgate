CREATE TABLE snw_cargos."DIMENSION" (
  "ID" NUMBER NOT NULL,
  tipo_variable NUMBER,
  variable VARCHAR2(256 BYTE),
  impacto NUMBER,
  descripcion VARCHAR2(4000 BYTE),
  meta_anual NUMBER,
  id_cargo NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT dimension_pk PRIMARY KEY ("ID"),
  CONSTRAINT dimension_fk1 FOREIGN KEY (tipo_variable) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT dimension_fk2 FOREIGN KEY (impacto) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT dimension_fk3 FOREIGN KEY (meta_anual) REFERENCES snw_cargos.tipo ("ID"),
  CONSTRAINT dimension_fk4 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID")
);