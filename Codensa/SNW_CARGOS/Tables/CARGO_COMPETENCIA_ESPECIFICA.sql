CREATE TABLE snw_cargos.cargo_competencia_especifica (
  "ID" NUMBER NOT NULL,
  id_cargo NUMBER NOT NULL,
  id_digital_skill NUMBER NOT NULL,
  especificacion VARCHAR2(4000 BYTE),
  CONSTRAINT cargo_competencia_especifi_pk PRIMARY KEY ("ID"),
  CONSTRAINT cargo_competencia_especif_fk1 FOREIGN KEY (id_cargo) REFERENCES snw_cargos.cargo ("ID"),
  CONSTRAINT cargo_competencia_especif_fk2 FOREIGN KEY (id_digital_skill) REFERENCES snw_cargos.digital_skill ("ID")
);