CREATE OR REPLACE PACKAGE BODY snw_cargos."ADMIN_TAREAS_PCK" AS
---------------------------------------------------------------------------------------------------------
  PROCEDURE GENERAR_AUD_CARGO ( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 ) AS
  l_reviso varchar2(4000);
  l_aprobo varchar2(4000) ;
  BEGIN
    select usuario into l_reviso
    from concepto_cargo
    where id_cargo = i_number and tipo = snw_constantes.constante_tipo('CONCEPTO_BP');
    select usuario into l_aprobo
    from concepto_cargo
    where id_cargo = i_number and tipo = snw_constantes.constante_tipo('CONCEPTO_POC');
    UPDATE CARGO SET reviso = l_reviso, aprobo = l_aprobo WHERE ID = I_NUMBER;
  END GENERAR_AUD_CARGO;

---------------------------------------------------------------------------------------------------------

  PROCEDURE GENERAR_NUEVA_VERSION ( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 ) AS
  l_id_padre number;
  BEGIN
    VERSIONADOR_PCK.ACTUALIZAR_VIGENCIA(i_number);
    VERSIONADOR_PCK.ACTUALIZAR_VERSION(i_number);
    
    for i in (select id from cargo where id_cargo_padre in (select id_cargo_padre from cargo where id = i_number))
        loop
            ADMIN_TAREAS_PCK.ACTUALIZAR_CARGO(i.id,sysdate,null);
        end loop;
  END GENERAR_NUEVA_VERSION;

---------------------------------------------------------------------------------------------------------

  PROCEDURE ACTUALIZAR_CARGO ( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 ) AS
  l_conteo number;
  l_id_actividad_vigente number;
  l_bandera boolean := true;
  BEGIN
    select count(*) into l_conteo from cargo where version_cargo is not null and vigencia = 'S' and id = i_number;
    if l_conteo > 0 and l_bandera then
        update cargo set estado = snw_constantes.constante_tipo('CARGO_VIGENTE') where id = i_number;
        l_bandera := false;
    end if;
    select count(*) into l_conteo from cargo where version_cargo is not null and vigencia = 'N' and id = i_number;
    if l_conteo > 0 and l_bandera then
        update cargo set estado = snw_constantes.constante_tipo('CARGO_NO_VIGENTE') where id = i_number;
        l_bandera := false;
    end if;
    select MAX(id_actividad_vigente) into l_id_actividad_vigente 
    from cargo
    left join snw_flow.v_bandeja on (id_instancia_activa = snw_flow.flujo.determinar_instancia_activa(cargo.id_instancia_proceso))
    where cargo.id = i_number;
    if l_id_actividad_vigente = 366 and l_bandera then
        update cargo set estado = snw_constantes.constante_tipo('CARGO_ANULADO') where id = i_number;
    end if;
    if l_id_actividad_vigente = 325 and l_bandera then
        select count(*) into l_conteo from cargo where id_cargo_padre = i_number;
        if l_conteo > 0 then
            update cargo set estado = snw_constantes.constante_tipo('EN_CREACION') where id = i_number;
        else
            update cargo set estado = snw_constantes.constante_tipo('EN_ACTUALIZACION') where id = i_number;
        end if;
        l_bandera := false;
    end if;
    if l_id_actividad_vigente = 345 and l_bandera then
        update cargo set estado = snw_constantes.constante_tipo('EN_VALIDACION_BP') where id = i_number;
        l_bandera := false;
    end if; 
    if l_id_actividad_vigente = 346 and l_bandera then
        update cargo set estado = snw_constantes.constante_tipo('EN_VALIDACION_POC') where id = i_number;        
    end if;
  END ACTUALIZAR_CARGO;

---------------------------------------------------------------------------------------------------------
  PROCEDURE NOTIFICAR_GESTOR_BP( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 )AS
  BEGIN
    CODENSA_GTH.NOTIFICACION_PCK.NOTIFICAR_BP_DESDE_GESTOR(i_number);
  END NOTIFICAR_GESTOR_BP;
---------------------------------------------------------------------------------------------------------
  PROCEDURE NOTIFICAR_POC_BP( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 )AS
  BEGIN
    CODENSA_GTH.NOTIFICACION_PCK.NOTIFICAR_BP_DESDE_POC(i_number);
  END NOTIFICAR_POC_BP;
---------------------------------------------------------------------------------------------------------
  PROCEDURE NOTIFICAR_BP_GESTOR( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 )AS
  BEGIN
   CODENSA_GTH.NOTIFICACION_PCK.NOTIFICAR_GESTOR_DESDE_BP(i_number);
  END NOTIFICAR_BP_GESTOR;
---------------------------------------------------------------------------------------------------------
  PROCEDURE NOTIFICAR_BP_POC( i_number IN NUMBER, i_fecha IN DATE, i_texto IN VARCHAR2 )AS
  BEGIN
    CODENSA_GTH.NOTIFICACION_PCK.NOTIFICAR_POC_DESDE_BP(i_number);
  END NOTIFICAR_BP_POC;
---------------------------------------------------------------------------------------------------------
END ADMIN_TAREAS_PCK;
/