CREATE OR REPLACE PACKAGE BODY snw_cargos."ADMIN_UNIDAD_ORGANIZATIVA" AS

------------------------------------------------------------------------------------------------------------------------

  FUNCTION BUSCAR_UNIDAD(I_ID_UNIDAD NUMBER,I_TIPO NUMBER) RETURN NUMBER AS
  BEGIN
      for i in (select id,tipo_unidad
                    from unidad_organizativa
                    where tipo_unidad is not null
                    start with id = I_ID_UNIDAD
                    connect by prior id_padre  = id) loop
        if i.tipo_unidad = I_TIPO then
            return i.id;
        end if;
      end loop;
    RETURN NULL;
  END BUSCAR_UNIDAD;

------------------------------------------------------------------------------------------------------------------------

  FUNCTION NOMBRE_UNIDAD(I_ID_UNIDAD NUMBER) RETURN VARCHAR2 AS
  l_nombre varchar2(4000);
  BEGIN
    select nombre into l_nombre from unidad_organizativa where id = I_ID_UNIDAD;
    RETURN l_nombre;
    exception when others then
    return '';
  END NOMBRE_UNIDAD;

------------------------------------------------------------------------------------------------------------------------

  FUNCTION PERTENECE_UNIDAD(I_ID_UNIDAD_USUARIO NUMBER, I_ID_UNIDAD_CARGO NUMBER) RETURN CHAR AS
  BEGIN
    for i in (select id
                    from unidad_organizativa
                    where tipo_unidad is not null
                    start with id = I_ID_UNIDAD_USUARIO
                    connect by prior id  = id_padre) 
        loop
            if i.id = I_ID_UNIDAD_CARGO then
                return 'S';
            end if;
        end loop;
    RETURN 'N';
  END PERTENECE_UNIDAD;

------------------------------------------------------------------------------------------------------------------------

  FUNCTION PERSONAS_IMPACTO(I_ID_CARGO NUMBER) RETURN NUMBER AS
    l_conteo number;
    l_id_padre number;
  BEGIN
    select id_cargo_padre into l_id_padre from cargo where id = I_ID_CARGO;
    select count(*) into l_conteo from posicion_cargo where id_cargo = l_id_padre and 
    (sysdate between fecha_inicio and fecha_fin or fecha_fin is null);
    RETURN l_conteo;
  END PERSONAS_IMPACTO;

  ------------------------------------------------------------------------------------------------------------------------

  FUNCTION USUARIO_PERTENECE_UNIDAD(I_USER VARCHAR2) RETURN NUMBER AS
    l_id_unidad_usuario number;  
  BEGIN
  --TODO GCARRILLO 20190605: Hacer robusta para cuando haya más de uno vigente, o más de uno tenga fecha_fin null en posicion_trabajador
        
        -- Parte de un usuario, y busca la unidad organizativa de la posicion vigente del trabajador
        select id_unidad_organizativa into l_id_unidad_usuario 
        from posicion_uo where id_posicion = (select id_posicion from posicion_trabajador where id_trabajador =
        (select id from trabajador where numero_identificacion = 
        (select numero_identificacion from usuario where user_name = UPPER(I_USER)))
        and (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))
        and (sysdate between fecha_inicio and fecha_fin or fecha_fin is null);
        
        return l_id_unidad_usuario;

        exception when no_data_found then
        return null;
  END USUARIO_PERTENECE_UNIDAD;

    ------------------------------------------------------------------------------------------------------------------------

  FUNCTION deprecated_CAR_SUP_JER(I_CARGO NUMBER) RETURN VARCHAR2 AS
    l_cargo_superior varchar2(4000);
    l_unidad_perteneciente number;
    l_conteo number;
  BEGIN

        -- Esta función obsoleta, funciona igual que CARGO_SUPERIOR_JERARQUICO, excepto que no se basa de la estructura jerarquica
        -- de las posiciones, sino de la estructura de los cargos

        if I_CARGO IS NULL then

            return '- Seleccione una Unidad Organizativa -';

        end if;        

            select id_unidad_organizativa into l_unidad_perteneciente from cargo where id = I_CARGO;

              if l_unidad_perteneciente IS NULL then

                    return '- Seleccione una Unidad Organizativa -';

              end if; 

              select count(*) into l_conteo from V_CARGO_VIGENTE where ID_UNIDAD_ORGANIZATIVA = 
              (select id from UNIDAD_ORGANIZATIVA where ID = (select id_padre from UNIDAD_ORGANIZATIVA where id = l_unidad_perteneciente)) and
              SUBGRUPO_PROFESIONAL in (1,2,3,4);


              if l_conteo = 0 then

                    return 'No se encontró el cargo';

              end if; 


              select DENOMINACION into l_cargo_superior from V_CARGO_VIGENTE where ID_UNIDAD_ORGANIZATIVA = 
              (select id from UNIDAD_ORGANIZATIVA where ID = (select id_padre from UNIDAD_ORGANIZATIVA where id = l_unidad_perteneciente)) and
              SUBGRUPO_PROFESIONAL in (1,2,3,4);

              return l_cargo_superior;

  END deprecated_CAR_SUP_JER;

      ------------------------------------------------------------------------------------------------------------------------

  FUNCTION CARGO_SUPERIOR_JERARQUICO(I_CARGO NUMBER, I_SUBGRUPO NUMBER, I_UNIDAD NUMBER) RETURN VARCHAR2 AS

    l_cargo_superior varchar2(4000);
    l_unidad_perteneciente number;
    l_subgrupo number;
    l_conteo number;

  BEGIN            
         -- << En esta nueva version del cargo superior jerarquico, se va a calcular de dos formas: >>   

         -- 1. Denominacion de la DC de la posición responsable de la UO superior a la UO que se asocia directamente
         -- a la DC que se está creando o actualizando.  

         -- 2. La denominacion de la DC de la posición responsable de la misma UO a la que se asocia directamente a la DC que 
         -- se está creando o actualizando.

         if i_Cargo IS NULL then

            return 'No se visualizará el cargo superior jerárquico mientras la Descripción de Cargo está sin código DC (el cual es asignado por POC).';

         end if;        

         if i_unidad IS NULL then

            select id_unidad_organizativa into l_unidad_perteneciente from cargo where id = I_CARGO;

            if l_unidad_perteneciente is null then

                return 'No se visualizará el cargo superior jerárquico mientras la Descripción de Cargo está sin código DC (el cual es asignado por POC).';

            end if;

         else

            l_unidad_perteneciente := i_unidad;

         end if; 

         if I_SUBGRUPO IS NULL then

          select subgrupo_profesional into l_subgrupo from cargo where id = I_CARGO;

            if l_subgrupo is null or i_unidad is not null then                    

                return '- Seleccione un Subgrupo profesional -';

            end if;

         else

            l_subgrupo := i_subgrupo;

         end if;   



         -- < 1 > --
         if l_SUBGRUPO IN (1,2,3,4) then               

              select count(*) into l_conteo from posicion_cargo where id_posicion =               
              (select id from posicion where id in (
              (select id_posicion from posicion_uo where id_unidad_organizativa = 
              (select id_padre from UNIDAD_ORGANIZATIVA where id = l_unidad_perteneciente) and 
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))) and POS_RES = 'X') and
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null);              

              if l_conteo = 0 then

                    return 'No se encontró el cargo';

              end if; 

              select denominacion into l_cargo_superior from cargo where id = 
              (select id_cargo from posicion_cargo where id_posicion =               
              (select id from posicion where id in (
              (select id_posicion from posicion_uo where id_unidad_organizativa = 
              (select id_padre from UNIDAD_ORGANIZATIVA where id = l_unidad_perteneciente) and 
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))) and POS_RES = 'X') and
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null)); 

              return l_cargo_superior;


         -- < 2 > --
         ELSE

              select count(*) into l_conteo from posicion_cargo where id_posicion =               
              (select id from posicion where id in (
              (select id_posicion from posicion_uo where id_unidad_organizativa = l_unidad_perteneciente and 
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))) and POS_RES = 'X') and
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null);              

              if l_conteo = 0 then

                    return 'No se encontró el cargo';

              end if; 

              select denominacion into l_cargo_superior from cargo where id = 
              (select id_cargo from posicion_cargo where id_posicion =               
              (select id from posicion where id in (
              (select id_posicion from posicion_uo where id_unidad_organizativa = l_unidad_perteneciente and 
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))) and POS_RES = 'X') and
              (sysdate between fecha_inicio and fecha_fin or fecha_fin is null)); 

              return l_cargo_superior;

         end if;
         --TODO CRISTIAN 'Ver cual es la excepcion y darle un manejo apropiado'
         EXCEPTION WHEN OTHERS THEN
            RETURN '';


  END CARGO_SUPERIOR_JERARQUICO;

      ------------------------------------------------------------------------------------------------------------------------

  PROCEDURE GUARDAR_CARGO_SUPERIOR(I_CARGO NUMBER) AS

    l_unidad_perteneciente number;
    l_subgrupo number;
    l_cargo_superior varchar2(100);


  BEGIN            
        -- << Procedimiento que se llama al momento de versionar el cargo, para guardar en la columna este cargo superior jerarquico>>   

       select id_unidad_organizativa into l_unidad_perteneciente from cargo where id = I_CARGO;
       if l_unidad_perteneciente is not null then      
            select subgrupo_profesional into l_subgrupo from cargo where id = I_CARGO;       
            l_cargo_superior := CARGO_SUPERIOR_JERARQUICO(I_CARGO,l_subgrupo,l_unidad_perteneciente);                       
            UPDATE CARGO SET CARGO_SUPERIOR_JERARQUICO = l_cargo_superior WHERE ID = I_CARGO;                         
       end if;   
  END GUARDAR_CARGO_SUPERIOR;

  ------------------------------------------------------------------------------------------------------------------------

  PROCEDURE CARGOS_REPORTAN(I_CARGO NUMBER) AS

    l_unidad_perteneciente number;  
    l_subgrupo number;  
    l_conteo_unidad number;        
    l_conteo_subgrupo number;        

  BEGIN            
         -- << Este procedimiento se ejecuta cuando se versiona un cargo, de manera que se hace la consulta de
         -- cargos de plantilla directa que le reportan, y los cargos encontrados se guardan en la tabla cargos_reportan >>   

        select count(*) into l_conteo_unidad from cargo where ID_UNIDAD_ORGANIZATIVA is not null and id = I_CARGO;
        select count(*) into l_conteo_subgrupo from subgrupo_profesional where jefe = 'S' and id = (select subgrupo_profesional from cargo where id = I_CARGO);

        if l_conteo_unidad > 0 and l_conteo_subgrupo > 0 then

            select id_unidad_organizativa into l_unidad_perteneciente from cargo where id = I_CARGO;
            
            for i in (            

                select distinct nvl(denominacion, posicion.nombre) denominacion
                from cargo 
                left join POSICION_CARGO on (cargo.id = posicion_Cargo.id_Cargo)
                left join POSICION on (posicion.id = POSICION_CARGO.ID_POSICION)        
                where cargo.id in 
                -- Cargos de la misma unidad y NO son jefes
                (select id_cargo from posicion_Cargo where id_posicion in 
                (select id from posicion where pos_res is null and id in 
                (select id_posicion from posicion_uo where posicion_uo.id_unidad_organizativa = l_unidad_perteneciente and 
                (sysdate between fecha_inicio and fecha_fin or fecha_fin is null)) and
                (sysdate between fecha_inicio and fecha_fin or fecha_fin is null)))
                or cargo.id in 
                -- Cargos de unidades inferiores y SI son jefes
                (select id_cargo from posicion_Cargo where id_posicion in 
                (select id from posicion where pos_res = 'X' and id in 
                (select id_posicion from posicion_uo where posicion_uo.id_unidad_organizativa in 
                (select id from unidad_organizativa where id_padre = l_unidad_perteneciente) and 
                (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))) and 
                (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))
            ) 

            LOOP

                insert into CARGOS_REPORTAN values (null,i.denominacion,I_CARGO);

            end loop;                          

        end if;

  END CARGOS_REPORTAN;


  ------------------------------------------------------------------------------------------------------------------------

  FUNCTION BUSCAR_CARGO_TRABAJADOR(I_ID_TRABAJADOR NUMBER) RETURN NUMBER AS
    l_id_cargo number;
  BEGIN
    select id_cargo into l_id_cargo from posicion_cargo 
    where id_posicion in (select id_posicion from posicion_trabajador where id_trabajador = I_ID_TRABAJADOR and
    (sysdate between fecha_inicio and fecha_fin or fecha_fin is null)) and 
    (sysdate between fecha_inicio and fecha_fin or fecha_fin is null); 
    RETURN l_id_cargo;
  END;
    ------------------------------------------------------------------------------------------------------------------------

    PROCEDURE ASIGNACION_BP(I_ID_UNIDAD NUMBER) IS
    BEGIN
        for i in (select id from snw_auth.usuario where numero_identificacion in 
                        (select numero_identificacion from trabajador where id in (
                        select business_partner.id_trabajador
                        from business_partner where id_unidad_organizativa = I_ID_UNIDAD))) loop
            snw_auth.CONTROL_ACCESO_DATOS.conceder_permiso_agente(I_ID_UNIDAD,i.id,9);
            SNW_AUTH.autorizacion.asociar_usuario_grupo(9,i.id);
        end loop;
        for i in (select id_agente from snw_auth.permiso_nivel_acceso
                        where id_agente not in (select id from snw_auth.usuario where numero_identificacion in 
                        (select numero_identificacion from trabajador where id in (
                        select business_partner.id_trabajador
                        from business_partner where id_unidad_organizativa = I_ID_UNIDAD))) and id_nivel_Acceso = I_ID_UNIDAD
                        and id_grupo = 9) loop
            snw_auth.CONTROL_ACCESO_DATOS.revocar_permiso_agente(I_ID_UNIDAD,i.id_agente,9);
        end loop;
        for i in (select usuario from snw_auth.usu_grupo where grupo=9 and 
                        usuario not in (select id_agente from snw_auth.permiso_nivel_acceso where id_grupo=9)) loop
                SNW_AUTH.autorizacion.quitar_usuario_grupo(9,i.usuario);
        end loop;
    END ASIGNACION_BP;

    ------------------------------------------------------------------------------------------------------------------------
 FUNCTION TRABAJADOR_PERTENECE_UNIDAD(I_ID_TRABAJADOR NUMBER) RETURN NUMBER AS
    l_id_unidad_usuario number;  
  BEGIN
  --TODO GCARRILLO 20190605: Hacer robusta para cuando haya más de uno vigente, o más de uno tenga fecha_fin null en posicion_trabajador
  
  
        -- Parte de un trabajador, y busca la unidad organizativa de la posicion vigente del trabajador en cuestion
        select id_unidad_organizativa into l_id_unidad_usuario 
        from posicion_uo where id_posicion = (select id_posicion from posicion_trabajador where id_trabajador =
        i_id_trabajador and (sysdate between fecha_inicio and fecha_fin or fecha_fin is null))
        and (sysdate between fecha_inicio and fecha_fin or fecha_fin is null);
        
        return l_id_unidad_usuario;
        
        exception when no_data_found then
        return null;
  END TRABAJADOR_PERTENECE_UNIDAD;

 ---------------------------------------------------------------------------------------------------------------------------

END ADMIN_UNIDAD_ORGANIZATIVA;
/