CREATE OR REPLACE PACKAGE BODY snw_cargos."SGI" AS

  --PROCEDURE RESPONSABILIDAD_NUEVA_VERSION ( i_version IN NUMBER ) AS
  --BEGIN

  --  -- TODO: Implementation required for PROCEDURE SGI.RESPONSABILIDAD_NUEVA_VERSION
  --  NULL;
  --END RESPONSABILIDAD_NUEVA_VERSION;

-------------------------------------------------------------------------
  PROCEDURE ENVIO_CORREO_ELECTRONICO (i_destino IN NUMBER, i_version IN NUMBER, i_tipo NUMBER) AS

    l_correo_destino        VARCHAR2(4000);
    l_nivel                 VARCHAR2(4000);
    l_nombres               VARCHAR2(4000);
    l_asunto                VARCHAR2(4000);
    l_mensaje               VARCHAR2(4000);    

    l_count                 NUMBER;

    BEGIN

    select count(*) into l_count from MV_TRABAJADOR where id = i_destino and email is not null;

    -- SI el correo existe, procedemos con el envio
    if l_count > 0 then

        if i_tipo = SNW_CONSTANTES.CONSTANTE_TIPO('GENERALES') THEN

            l_asunto := 'Las responsabilidades del subgrupo asignado a su nivel se han actualizado'; 

            -- EL nombre del nivel de la version
            select nombre into l_nivel from SGI_NOMBRE where id = (select nombre from SGI_VERSION where id = i_version);

            -- Los nombres del destinatario
            select nombres||' ' ||apellidos into l_nombres from MV_TRABAJADOR where id = i_destino;

            -- El correo del destinatario
            select email into l_correo_destino from MV_TRABAJADOR where id = i_destino;              

            l_mensaje := 'Estimad@ ' || initcap(l_nombres) || '<br>' ||

              '<br>Por medio del presente la Unidad Personas & Organización Colombia le notifica las responsabilidades y Autoridades del nivel 
              profesional<b> '|| initcap(l_nivel) ||' </b>definido por las Unidades HSEQ. Por favor hacer click '||
              '<a href="' || CODENSA_GTH.DEPLOY_PCK.OBTENER_SERVER || CODENSA_GTH.DEPLOY_PCK.SERVER_ORDS_CONTEXT || APEX_UTIL.PREPARE_URL('f?p='||'101'||':208:'||V('SESSION')||'::NO::P208_VERSION,P208_TRABAJADOR:'|| i_version || ',' || i_destino) || '">Aquí</a>' 
              ||' para poder ver y aceptar dichas responsabilidades.<br><br>'||
            'Cordialmente,<br><br>' ||
            '<b>Enel</b><br><b>Codensa - Emgesa</b><br>Carrera 11 No. 82-76<br>Bogotá, D.C. - Colombia';

           -- Se puede usar este procedimiento para el envio DIRECTO, en caso de requerir pruebas
            -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje,sysdate);       
            CODENSA_GTH.NOTIFICACION_PCK.enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    

        ELSE

            l_asunto := 'Las responsabilidades asignadas a su rol se han actualizado'; 

            -- EL nombre del rol de la version
            select nombre into l_nivel from SGI_NOMBRE where id = (select nombre from SGI_VERSION where id = i_version);

            -- Los nombres del destinatario
            select nombres||' ' ||apellidos into l_nombres from MV_TRABAJADOR where id = i_destino;

            -- El correo del destinatario
            select email into l_correo_destino from MV_TRABAJADOR where id = i_destino;


            l_mensaje := 'Estimad@ ' || initcap(l_nombres) || '<br>' ||

              '<br>Por medio del presente la Unidad Personas & Organización Colombia le notifica el Rol Específico <b>' || initcap(l_nivel) ||
               ' </b>definido por las Unidades HSEQ. Por favor hacer click '||
               '<a href="' || CODENSA_GTH.DEPLOY_PCK.OBTENER_SERVER || CODENSA_GTH.DEPLOY_PCK.SERVER_ORDS_CONTEXT || APEX_UTIL.PREPARE_URL('f?p='||'101'||':208:'||V('SESSION')||'::NO::P208_VERSION,P208_TRABAJADOR:'|| i_version || ',' || i_destino) || '">Aquí</a>' 
               ||' para poder ver y aceptar dichas responsabilidades.<br><br>'||
                'Cordialmente,<br><br>' || 
                '<b>Enel</b><br><b>Codensa - Emgesa</b><br>Carrera 11 No. 82-76<br>Bogotá, D.C. - Colombia';


            -- Se puede usar este procedimiento para el envio DIRECTO, en caso de requerir pruebas
            -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje,sysdate);    
            CODENSA_GTH.NOTIFICACION_PCK.enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    

        END IF;         

    ELSE
        l_asunto := 'No existe el correo del usuario'; 
        CODENSA_GTH.NOTIFICACION_PCK.INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'El trabajador no cuenta con un correo válido para ser notificado. (Correo nulo)');
    END IF;             

  EXCEPTION 
      WHEN OTHERS THEN
        CODENSA_GTH.NOTIFICACION_PCK.INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'La información para notificar las responsabilidades al trabajador no esta completa');    


  END ENVIO_CORREO_ELECTRONICO;

-------------------------------------------------------------------------
  PROCEDURE NOTIFICACION_RESPONSABILIDADES ( i_version IN NUMBER ) AS
  
    l_tipo       NUMBER;
    l_conteo     NUMBER;

    BEGIN
    
    -- Primero se valida si la version es de tipo general o especifica
    select tipo into l_tipo from SGI_VERSION where id = i_version;

    if l_tipo = SNW_CONSTANTES.CONSTANTE_TIPO('GENERALES') THEN

        -- Se itera sobre los trabajadores cuyo subgrupo profesional,
        -- este asociado con el nivel de la version recibida
        for trabajador in (
            select id from MV_TRABAJADOR where ID_SUBGRUPO_PROFESIONAL in ( 
            (select Id_subgrupo from SGI_SUBGRUPO_NIVEL WHERE ID_NIVEL = 
            (select nombre from sgi_version where id = i_version))))
        loop

            -- Se verifica primero si el registro no ha sido creado anteriormente
            select count(*) into l_conteo from SGI_ACEPTACION where id_trabajador = trabajador.id and id_version = i_version;

            if l_conteo = 0 then

                -- Se prepara el registro donde posteriormente se actualizará la aceptación de acuerdo vayan aceptando    
                INSERT INTO SGI_ACEPTACION VALUES (null,trabajador.id,i_version,null,sysdate,null,null,null,null,null,null,null);
                -- Se notifica por correo electrónico
                ENVIO_CORREO_ELECTRONICO(trabajador.id,i_version,l_tipo);

            end if;

        end loop;  

    ELSE

        -- Se itera sobre los trabajadores cuyo rol es el asociado a la version.
        for trabajador in (
            select id_trabajador from SGI_TRABAJADOR_ROL WHERE ID_ROL in ( 
            (select nombre from sgi_version where id = i_version)))
        loop

            -- Se verifica primero si el registro no ha sido creado anteriormente
            select count(*) into l_conteo from SGI_ACEPTACION where id_trabajador = trabajador.id_trabajador and id_version = i_version;

            if l_conteo = 0 then

                -- Se prepara el registro donde posteriormente se actualizará la aceptación de acuerdo vayan aceptando                
                INSERT INTO SGI_ACEPTACION VALUES (null,trabajador.id_trabajador,i_version,null,sysdate,null,null,null,null,null,null,null);
                -- Se notifica por correo electrónico            
                ENVIO_CORREO_ELECTRONICO(trabajador.id_trabajador,i_version,l_tipo);

            end if;

        end loop;  


    END IF;


    END NOTIFICACION_RESPONSABILIDADES;


-------------------------------------------------------------------------
  PROCEDURE ENVIAR_RECORDATORIO ( i_version IN NUMBER ) AS

    l_tipo       NUMBER;
    l_conteo     NUMBER;
    l_diferencia NUMBER;
    l_dias       NUMBER := 7; -- EL numero minimo de dias de retraso para que se envie el recordatorio

    BEGIN

        -- Se itera sobre los registros de la tabla SGI_ACEPTACION, para la version dada
        for aceptacion in (
            select * from SGI_ACEPTACION where ID_VERSION = i_version )
        loop

            l_diferencia := to_date(sysdate) - to_date(aceptacion.fecha_notificacion);                    

            if l_diferencia >= l_dias and aceptacion.aceptacion is null then

                select tipo into l_tipo from SGI_VERSION where id = i_version;

                -- Se actualiza la fecha de notificación
                UPDATE SGI_ACEPTACION SET FECHA_NOTIFICACION = SYSDATE WHERE ID_TRABAJADOR = ACEPTACION.ID_TRABAJADOR AND ID_VERSION = I_VERSION;

                -- Se notifica por correo electrónico
                ENVIO_CORREO_ELECTRONICO(aceptacion.id_trabajador,aceptacion.id_version,l_tipo);
            end if;
        end loop;          
    END ENVIAR_RECORDATORIO;    

----------------------------------------------------------------------------------------------------
FUNCTION  DETERMINAR_SUBGRUPO(I_SG VARCHAR2) RETURN NUMBER AS
    l_id_subgrupo      NUMBER;
    l_trimed_sg VARCHAR2(100);

BEGIN

    l_trimed_sg := TRIM(I_SG);

    CASE
        WHEN l_trimed_sg = 'P1' THEN   SELECT ID into l_id_subgrupo FROM subgrupo_profesional WHERE NOMBRE LIKE '%P1%';
        WHEN l_trimed_sg = 'P2' THEN   SELECT ID into l_id_subgrupo FROM subgrupo_profesional WHERE NOMBRE LIKE '%P2%';
    ELSE
    SELECT ID into l_id_subgrupo
    FROM subgrupo_profesional
    WHERE NOMBRE LIKE l_trimed_sg||'%' ;
    END CASE;

    RETURN  l_id_subgrupo;

    EXCEPTION
    WHEN others THEN  l_id_subgrupo:= 15;

    RETURN  l_id_subgrupo;



END DETERMINAR_SUBGRUPO;
------------------------------------------------------------------------------
FUNCTION  DETERMINAR_ROLES(I_TRABAJADOR NUMBER) RETURN VARCHAR AS
    l_todos_roles       VARCHAR2(4000) :='';
    aux VARCHAR(200);

BEGIN

    for i in (SELECT ID_ROL FROM SGI_TRABAJADOR_ROL WHERE ID_TRABAJADOR = i_trabajador)
    loop
        SELECT NOMBRE INTO aux
        FROM SGI_NOMBRE
        WHERE ID = i.ID_ROL;
        l_todos_roles := l_todos_roles  || ', ' ||  aux;
    end loop;
        l_todos_roles:= SUBSTR( l_todos_roles, 2);

    RETURN  l_todos_roles;

END DETERMINAR_ROLES;
------------------------------------------------------------------------------
FUNCTION  DETERMINAR_CLASES(I_SGI_VERSION NUMBER) RETURN VARCHAR AS
    
    l_responsabilidades VARCHAR2(4000) := '';    

BEGIN
    
    -- RESPONSABILIDAD
    l_responsabilidades := 'RESPONSABILIDAD <br>';
    for responsabilidad in (select * from sgi_responsabilidad  where id_version = i_sgi_version and
    CLASE_RESPONSABILIDADES = SNW_CONSTANTES.CONSTANTE_TIPO('CATEGORIA_RESPONSABILIDAD'))
    loop        
        l_responsabilidades := l_responsabilidades || '&nbsp;&nbsp;&nbsp;' ||  responsabilidad.descripcion || ' <br>';
    end loop;
    
    -- AUTORIDADES
    l_responsabilidades := l_responsabilidades || '<br>AUTORIDAD <br>';
    for responsabilidad in (select * from sgi_responsabilidad  where id_version = i_sgi_version and
    CLASE_RESPONSABILIDADES = SNW_CONSTANTES.CONSTANTE_TIPO('CATEGORIA_AUTORIDADES'))
    loop        
        l_responsabilidades := l_responsabilidades || '&nbsp;&nbsp;&nbsp;' ||  responsabilidad.descripcion || ' <br>';
    end loop;
    
    --l_responsabilidades  := SUBSTR( l_responsabilidades , 2);
    
    RETURN  l_responsabilidades ;

END DETERMINAR_CLASES;

------------------------------------------------------------------------------
FUNCTION  DETERMINAR_NIVELES_SUBGRUPO(I_SUBGRUPO NUMBER) RETURN VARCHAR AS
    l_todos_niveles      VARCHAR2(4000) :='';
    aux VARCHAR(200);

BEGIN

    for i in (SELECT ID_NIVEL FROM SGI_SUBGRUPO_NIVEL WHERE ID_SUBGRUPO = i_subgrupo AND ID_NIVEL != 
        (SELECT ID FROM SGI_NOMBRE WHERE NOMBRE = '-'))
    loop 
        begin 
        SELECT NOMBRE INTO aux
        FROM SGI_NOMBRE
        WHERE ID = i.ID_NIVEL;
         l_todos_niveles := l_todos_niveles  || ', ' ||  aux;
        exception
        when no_data_found then
        aux := null;        
        end;

    end loop;
        l_todos_niveles:= SUBSTR(l_todos_niveles,3);

    RETURN  l_todos_niveles;

END DETERMINAR_NIVELES_SUBGRUPO;
------------------------------------------------------------------------------
---Funtion to present "Niveles" like a pivot
------------------------------------------------------------------------------
FUNCTION  DETERMINAR_SUBGRUPO_NIVELES(I_NIVEL NUMBER) RETURN VARCHAR AS
    l_todos_subgrupos      VARCHAR2(4000) :='';
    aux VARCHAR(2000);

BEGIN

    for i in (SELECT ID_SUBGRUPO FROM SGI_SUBGRUPO_NIVEL WHERE ID_NIVEL = i_nivel AND ID_SUBGRUPO != 
        (SELECT ID FROM SUBGRUPO_PROFESIONAL WHERE NOMBRE = '-'))
    loop 
        begin 
        SELECT NOMBRE INTO aux
        FROM SUBGRUPO_PROFESIONAL
        WHERE ID = i.ID_SUBGRUPO;
         l_todos_subgrupos := l_todos_subgrupos  || ', ' ||  aux;
        exception
        when no_data_found then
        aux := null;        
        end;

    end loop;
        l_todos_subgrupos:= SUBSTR(l_todos_subgrupos,3);

    RETURN  l_todos_subgrupos;

END DETERMINAR_SUBGRUPO_NIVELES;
------------------------------------------------------------------------------
FUNCTION VALIDAR_USUARIO_TRABAJADOR(I_USER VARCHAR2,I_TRABAJADOR NUMBER) RETURN BOOLEAN AS
    l_identificacion1 NUMBER;
    l_identificacion2 NUMBER;
    
BEGIN

    select numero_identificacion into l_identificacion1 from usuario where USER_NAME = I_USER;
    select numero_identificacion into l_identificacion2 from trabajador where id = I_TRABAJADOR;
    
    return l_identificacion1 = l_identificacion2;

END VALIDAR_USUARIO_TRABAJADOR;

END SGI;
/