CREATE OR REPLACE PROCEDURE snw_cargos."CARGOS_INICIALES" IS
i_cargo number;
l_user_elaboro VARCHAR2(100);
l_user_reviso VARCHAR2(100);
l_user_aprobo VARCHAR2(100);
l_funcion_transversal1 VARCHAR2(400) := 'Desarrollar las demás actividades relacionadas e inherentes al cargo y aquellas que le sean asignadas';
l_funcion_transversal2 VARCHAR2(400) := 'Apropiar y dar cumplimiento a las políticas, documentación y estándares establecidos en los Sistemas Integrados de Gestión de la organización';
l_codigos_funcion NUMBER;
CURSOR cargos_iniciales IS (SELECT * FROM TEMP_CARGOS_INICIALES);
l_id NUMBER; --Id de la tabla temp para el log en la excepción general
l_error VARCHAR2(4000);
BEGIN
  FOR i IN cargos_iniciales
    LOOP
        l_error := '';
        l_id := i.id;
		--Determinar si el código de función es único para poder continuar
		SELECT count(*) into l_codigos_funcion
			FROM CARGO
			WHERE codigo_funcion = i.codigo_funcion;
		if (l_codigos_funcion != 0) then --Ya existe un cargo con ese código de función por tanto no se crea
			l_error := l_error || ' Otro cargo con el mismo código DC ya existe.';
            UPDATE TEMP_CARGOS_INICIALES
				set log = 'Cargo NO creado.'||l_error
				where id = i.id;
			CONTINUE;
		end if;
		--Determinar los user_name de los responsables
		BEGIN
		  select user_name into l_user_elaboro from USUARIO where numero_identificacion = i.elaboro;
		EXCEPTION
		  when others then 
            l_user_elaboro := null;
            l_error := l_error || ' No se encuentra el usuario elaboró.';
		END;
		BEGIN
		  select user_name into l_user_reviso from USUARIO where numero_identificacion = i.reviso;
		EXCEPTION
		  when others then 
            l_user_reviso := null;
            l_error := l_error || ' No se encuentra el usuario revisó.';
		END;
		BEGIN
		  select user_name into l_user_aprobo from USUARIO where numero_identificacion = i.aprobo;
		EXCEPTION
		  when others then 
            l_user_aprobo := null;
            l_error := l_error || ' No se encuentra el usuario aprobó.';
		END;

		--Insert de los campos univalorados
        INSERT INTO CARGO (DENOMINACION,
                       VERSION_CARGO,
                       CODIGO_FUNCION,
                       SUBGRUPO_PROFESIONAL,	
                       CATEGORY_ENEL,	
                       ROLES,	
                       FUNCTIONAL_AREA,
                       VIGENCIA,
                       HOMOLOGACION,
                       CODIGO_HOMOLOGACION,
                       FECHA_INICIO,
                       PERSONAS_DIRECTAS,	
                       PERSONAS_INDIRECTAS,	
                       RECURSOS_EMPRESAS,	
                       RECURSOS_PERSONAS,
                       ESTADO,
                       MISION,	
                       BASICO_ESPECIFICACION/*Detalle educación básica. El nivel es calculado sin persistir*/,
					   COMPLEMENTARIO_ESPECIFICACION/*Detalle educación complementaria. El nivel es calculado sin persistir*/,
                       COMPLEMENTARIO_NIVEL/*Nivel idioma complementario*/,
					   IDIOMA_EXTRA_ESPECIFICACION/*Detalle idioma complementario*/,
                       EXPERIENCIA,
                       DIGITAL_ESPECIFICA,                       
                       OTROS,
                       ID_UNIDAD_ORGANIZATIVA,
					   TIPO_UBICACION/*tipo de DC según elección de UO*/,
                       CARGO_SUPERIOR_JERARQUICO,
					   ELABORO/*Elaboró es el user_name*/,
					   NOMBRE_ELABORO/*Denominación del cargo o de la posición*/,
					   REVISO/*Revisó es el user_name*/,
					   NOMBRE_REVISO/*Denominación del cargo o de la posición*/,
					   APROBO/*Aprobó es el user_name*/,
					   NOMBRE_APROBO/*Denominación del cargo o de la posición*/
                       )
        VALUES (i.cargo, 
                i.version,
                i.codigo_funcion,
                (SELECT ID FROM SUBGRUPO_PROFESIONAL WHERE upper(NOMBRE) = upper(i.subgrupo) ), 
                SNW_CONSTANTES.get_id_tipo(i.clase_enel, 'CATEGORY_ENEL'),
				SNW_CONSTANTES.get_id_tipo(i.roles_descriptions, 'ROLES_DESCRIPTIONS'),
                SNW_CONSTANTES.get_id_tipo(i.funtional_area, 'ESPECIFICO'),
                'S',
                i.homologacion,
                i.CODIGO_HOMOLOGA,
                i.FECHA_CREACION, 
                i.PERS_DIRECTO, 
                i.PERS_INDIR, 
                i.OTROS_EMPR, 
                i.OTROS_PERS,
                SNW_CONSTANTES.CONSTANTE_TIPO('CARGO_VIGENTE'),
                i.MISION, 
                i.ESPECIFICA_EDUCACION_BASICA,
				i.ESPECIFICA_EDUCACION_COMPL,
                SNW_CONSTANTES.get_id_tipo(i.nivel_idiom,'NIVEL_IDIOMA'),
				i.tipo_idioma,
                i.EXP_DESCIP,
                i.DIGITAL_ESP,                
                i.OTROS_DATOS,
                ( SELECT ID FROM UNIDAD_ORGANIZATIVA WHERE i.CODIGO_UNIDAD in (codigo_codensa,codigo_emgesa) ),
				(CASE WHEN i.codigo_unidad is null then SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') else SNW_CONSTANTES.CONSTANTE_TIPO('ESPECIFICO') END),
                i.CARGO_SUPERIOR_JERARQUICO,
				l_user_elaboro,
				FORMULARIO.denominacion_cargo_trabajador(i.elaboro),
				l_user_reviso,
				FORMULARIO.denominacion_cargo_trabajador(i.reviso),
				l_user_aprobo,
				FORMULARIO.denominacion_cargo_trabajador(i.aprobo)
                )
		RETURNING id INTO i_cargo;

		-------ACTUALIZAR EL ID_CARGO PADRE Y REDUNDAR NOMBRES DE LAS UOS, CARGOS QUE REPORTAN Y REQUISITOS DERIVADOS DE SUBGRUPO PROF
	    UPDATE CARGO
			SET id_cargo_padre = i_cargo
			WHERE id = i_cargo;
            
		VERSIONADOR_PCK.REDUNDAR_UBICACION_JERARQUICA(i_cargo);
        -- Ahora se configuran al final
    	-- ADMIN_UNIDAD_ORGANIZATIVA.cargos_reportan(i_cargo);
        VERSIONADOR_PCK.redundar_requisitos(i_cargo);
		------------------------------------insertar tabla SOFTWARE----------------------------------------------------------------------
		if (i.SOFTWARE_1 is not null) then
			INSERT INTO SOFTWARE(DESCRIPCION, ID_CARGO) VALUES (i.SOFTWARE_1, i_cargo); end if;
		if (i.SOFTWARE_2 is not null) then
			INSERT INTO SOFTWARE(DESCRIPCION, ID_CARGO) VALUES (i.SOFTWARE_2, i_cargo); end if;
		if (i.SOFTWARE_3 is not null) then
			INSERT INTO SOFTWARE(DESCRIPCION, ID_CARGO) VALUES (i.SOFTWARE_3, i_cargo); end if;
		if (i.SOFTWARE_4 is not null) then
			INSERT INTO SOFTWARE(DESCRIPCION, ID_CARGO) VALUES (i.SOFTWARE_4, i_cargo); end if;
		if (i.SOFTWARE_5 is not null) then
			INSERT INTO SOFTWARE(DESCRIPCION, ID_CARGO) VALUES (i.SOFTWARE_5, i_cargo); end if;
		if (i.SOFTWARE_6 is not null) then
			INSERT INTO SOFTWARE(DESCRIPCION, ID_CARGO) VALUES (i.SOFTWARE_6, i_cargo); end if;

		----------------------------------INSERTAR EN TABLA licencia------------------------------------------------------------------
        if (i.ESPECI_LICENCIA_1 is not null) then
			INSERT INTO LICENCIA_MATRICULA(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.ESPECI_LICENCIA_1, i_cargo, SNW_CONSTANTES.get_id_tipo(i.tipo_licenc_1,'LICENCIAS_MATRICULAS_CERT'));
		end if;
		if (i.ESPECI_LICENCIA_2 is not null) then
			INSERT INTO LICENCIA_MATRICULA(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.ESPECI_LICENCIA_2, i_cargo, SNW_CONSTANTES.get_id_tipo(i.tipo_licenc_2,'LICENCIAS_MATRICULAS_CERT'));
		end if;
		if (i.ESPECI_LICENCIA_3 is not null) then
			INSERT INTO LICENCIA_MATRICULA(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.ESPECI_LICENCIA_3, i_cargo, SNW_CONSTANTES.get_id_tipo(i.tipo_licenc_3,'LICENCIAS_MATRICULAS_CERT'));
		end if;

		-------------------insertar en tabla RESPONSABILIDAD_PRINCIPAL----------------------------------------------------------------------
        if (i.FUN_1 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_1, i_cargo, 'Específica', 1); end if;
		if (i.FUN_2 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_2, i_cargo, 'Específica', 2); end if;
		if (i.FUN_3 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_3, i_cargo, 'Específica', 3); end if;
		if (i.FUN_4 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_4, i_cargo, 'Específica', 4); end if;
		if (i.FUN_5 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_5, i_cargo, 'Específica', 5); end if;
		if (i.FUN_6 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_6, i_cargo, 'Específica', 6); end if;
		if (i.FUN_7 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_7, i_cargo, 'Específica', 7); end if;
        if (i.FUN_8 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_8, i_cargo, 'Específica', 8); end if;
		if (i.FUN_9 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_9, i_cargo, 'Específica', 9); end if;
		if (i.FUN_10 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_10, i_cargo, 'Específica', 10); end if;		
		if (i.FUN_11 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_11, i_cargo, 'Específica', 11); end if;		
        if (i.FUN_12 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_12, i_cargo, 'Específica', 12); end if;
		if (i.FUN_13 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_13, i_cargo, 'Específica', 13); end if;
		if (i.FUN_14 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_14, i_cargo, 'Específica', 14); end if;
		if (i.FUN_15 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_15, i_cargo, 'Específica', 15); end if;
		if (i.FUN_16 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_16, i_cargo, 'Específica', 16); end if;
		if (i.FUN_17 is not null) then
			INSERT INTO RESPONSABILIDAD_PRINCIPAL(DESCRIPCION, ID_CARGO, TIPO, NUMERO)
				VALUES (i.FUN_17, i_cargo, 'Específica', 17); end if;
        --Actualizar las que son transversales que no se sabe en que ubicación venían
		UPDATE RESPONSABILIDAD_PRINCIPAL set tipo = 'Transversal'
			where id_cargo = i_cargo and ( descripcion like '%'||l_funcion_transversal1||'%' 
				or descripcion like '%'||l_funcion_transversal2||'%');

		---------------------------INSERTAR EN TABLA decision---------------------------------------------------------------------------
		if (i.TOMAR_1 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.TOMAR_1, i_cargo, SNW_CONSTANTES.constante_tipo('TOMAR')); end if; 
        if (i.TOMAR_2 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.TOMAR_2, i_cargo, SNW_CONSTANTES.constante_tipo('TOMAR')); end if; 
		if (i.TOMAR_3 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.TOMAR_3, i_cargo, SNW_CONSTANTES.constante_tipo('TOMAR')); end if; 		
		if (i.TOMAR_4 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.TOMAR_4, i_cargo, SNW_CONSTANTES.constante_tipo('TOMAR')); end if; 		
		if (i.PROPONER_1 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.PROPONER_1, i_cargo, SNW_CONSTANTES.constante_tipo('PROPONER')); end if; 		
		if (i.PROPONER_2 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.PROPONER_2, i_cargo, SNW_CONSTANTES.constante_tipo('PROPONER')); end if;
		if (i.PROPONER_3 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.PROPONER_3, i_cargo, SNW_CONSTANTES.constante_tipo('PROPONER')); end if;
		if (i.PROPONER_4 is not null) then
			INSERT INTO DECISION(DESCRIPCION, ID_CARGO, TIPO)
                VALUES (i.PROPONER_4, i_cargo, SNW_CONSTANTES.constante_tipo('PROPONER')); end if;

		-----------------------insertar en tabla DIMENSION---------------------------------------------------------------------------------------
		if (i.VARBLE_1 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, META_ANUAL, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('ECONOMICA'), i.VARBLE_1,
                         SNW_CONSTANTES.get_id_tipo(i.impacto_1, 'IMPACTO'), i.DESCRIP_1,
						 SNW_CONSTANTES.get_id_tipo(i.META_1, 'META_VALOR_COP'), i_cargo );  end if;
        if (i.VARBLE_2 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, META_ANUAL, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('ECONOMICA'), i.VARBLE_2,
                         SNW_CONSTANTES.get_id_tipo(i.impacto_2, 'IMPACTO'), i.DESCRIP_2,
						 SNW_CONSTANTES.get_id_tipo(i.META_2, 'META_VALOR_COP'), i_cargo );  end if;
		if (i.VARBLE_3 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, META_ANUAL, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('ECONOMICA'), i.VARBLE_3,
                         SNW_CONSTANTES.get_id_tipo(i.impacto_3, 'IMPACTO'), i.DESCRIP_3,
						 SNW_CONSTANTES.get_id_tipo(i.META_3, 'META_VALOR_COP'), i_cargo );  end if;	
		if (i.VARBLE_4 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, META_ANUAL, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('ECONOMICA'), i.VARBLE_4,
                         SNW_CONSTANTES.get_id_tipo(i.impacto_4, 'IMPACTO'), i.DESCRIP_4,
						 SNW_CONSTANTES.get_id_tipo(i.META_4, 'META_VALOR_COP'), i_cargo );  end if;
		if (i.RESCUA_VARIAB_1 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('CUALITATIVA'), i.RESCUA_VARIAB_1,
                         SNW_CONSTANTES.get_id_tipo(i.RESCUA_IMPACTO_1, 'IMPACTO'), i.RESCUA_DESCRIP_1,
						 i_cargo );  end if;
		if (i.RESCUA_VARIAB_2 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('CUALITATIVA'), i.RESCUA_VARIAB_2,
                         SNW_CONSTANTES.get_id_tipo(i.RESCUA_IMPACTO_2, 'IMPACTO'), i.RESCUA_DESCRIP_2,
						 i_cargo );  end if;
		if (i.RESCUA_VARIAB_3 is not null) then
			INSERT INTO DIMENSION(TIPO_VARIABLE, VARIABLE, IMPACTO, DESCRIPCION, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('CUALITATIVA'), i.RESCUA_VARIAB_3,
                         SNW_CONSTANTES.get_id_tipo(i.RESCUA_IMPACTO_3, 'IMPACTO'), i.RESCUA_DESCRIP_3,
						 i_cargo );  end if;

		--------------------------insertar en tabla RESPONSABILIDAD_CONTACTOS-----------------------------------------------------------
		if (i.REL_INTER_1 is not null) then
			INSERT INTO RESPONSABILIDAD_CONTACTOS(TIPO, INTERACCION, PROPOSITO, FRECUENCIA, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('INTERNAS'), i.REL_INTER_1,
                         i.PROPOSITO_1, SNW_CONSTANTES.get_id_tipo(i.FRECUEN_1, 'FRECUENCIA'), i_cargo );  end if;
		if (i.REL_INTER_2 is not null) then
			INSERT INTO RESPONSABILIDAD_CONTACTOS(TIPO, INTERACCION, PROPOSITO, FRECUENCIA, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('INTERNAS'), i.REL_INTER_2,
                         i.PROPOSITO_2, SNW_CONSTANTES.get_id_tipo(i.FRECUEN_2, 'FRECUENCIA'), i_cargo );  end if;
		if (i.REL_INTER_3 is not null) then
			INSERT INTO RESPONSABILIDAD_CONTACTOS(TIPO, INTERACCION, PROPOSITO, FRECUENCIA, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('INTERNAS'), i.REL_INTER_3,
                         i.PROPOSITO_3, SNW_CONSTANTES.get_id_tipo(i.FRECUEN_3, 'FRECUENCIA'), i_cargo );  end if;
		if (i.REL_EXT_1 is not null) then
			INSERT INTO RESPONSABILIDAD_CONTACTOS(TIPO, INTERACCION, PROPOSITO, FRECUENCIA, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('EXTERNAS'), i.REL_EXT_1,
                         i.PROPOSITO_EXT_1, SNW_CONSTANTES.get_id_tipo(i.FRECUEN_EXT_1, 'FRECUENCIA'), i_cargo );  end if;
		if (i.REL_EXT_2 is not null) then
			INSERT INTO RESPONSABILIDAD_CONTACTOS(TIPO, INTERACCION, PROPOSITO, FRECUENCIA, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('EXTERNAS'), i.REL_EXT_2,
                         i.PROPOSITO_EXT_2, SNW_CONSTANTES.get_id_tipo(i.FRECUEN_EXT_2, 'FRECUENCIA'), i_cargo );  end if;
		if (i.REL_EXT_3 is not null) then
			INSERT INTO RESPONSABILIDAD_CONTACTOS(TIPO, INTERACCION, PROPOSITO, FRECUENCIA, ID_CARGO)
                VALUES ( SNW_CONSTANTES.constante_tipo('EXTERNAS'), i.REL_EXT_3,
                         i.PROPOSITO_EXT_3, SNW_CONSTANTES.get_id_tipo(i.FRECUEN_EXT_3, 'FRECUENCIA'), i_cargo );  end if;

		UPDATE TEMP_CARGOS_INICIALES set log = 'Cargo creado.'||l_error	where id = i.id;

    END LOOP;
    
    
    -- Se configuran los cargos de plantilla que reportan
    FOR i IN cargos_iniciales
    LOOP
        ADMIN_UNIDAD_ORGANIZATIVA.cargos_reportan(i_cargo);
    END LOOP;
    
    
EXCEPTION
	WHEN OTHERS THEN
        l_error := SQLCODE||' -ERROR- '||SQLERRM||' -STACK- '||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
		UPDATE TEMP_CARGOS_INICIALES
			set log = 'Cargo NO creado.'||l_error
			where id = l_id;
END CARGOS_INICIALES;
/