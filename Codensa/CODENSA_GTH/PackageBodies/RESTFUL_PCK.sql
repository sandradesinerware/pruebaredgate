CREATE OR REPLACE package body codensa_gth.restful_pck as
--------------------------------------------------------
 function new_dict ( i_name in varchar2, i_value in varchar2 ) return dict_row is
 o_row restful_pck.dict_row;
 begin
 o_row.name := i_name;
 o_row.value := i_value;
 return o_row;
 end;
--------------------------------------------------------
 procedure print_clob ( p_clob in clob ) is
 v_offset number default 1;
 v_chunk_size number := 10000;
 begin
 loop
 exit when v_offset > dbms_lob.getlength(p_clob);
 dbms_output.put_line(dbms_lob.substr(p_clob, v_chunk_size, v_offset));
 v_offset := v_offset + v_chunk_size;
 end loop;
 end print_clob;
-------------------------------------------------------- 
 function response_as_clob( i_response in out utl_http.resp) return clob is
 v_buffer varchar2(20000);
 o_raw_body clob;
 begin
 dbms_lob.createtemporary(o_raw_body, false);
 loop
 begin
 utl_http.read_text(i_response, v_buffer, 15000);
 dbms_lob.writeappend (o_raw_body, length(v_buffer), v_buffer);
 exception
 when utl_http.end_of_body then
 utl_http.end_response(i_response);
 exit;
 when others then
 dbms_output.put_line(dbms_utility.format_error_backtrace);
 dbms_output.put_line(sqlerrm);
 exit;
 end;
 end loop;
 return o_raw_body;
 end;
-------------------------------------------------------- 
 function response_as_blob( i_response in out utl_http.resp) return blob is
 v_buffer raw(15000);
 o_raw_body blob;
 begin
 dbms_lob.createtemporary(o_raw_body, false);
 loop
 begin
 utl_http.read_raw(i_response, v_buffer, 15000);
 dbms_lob.writeappend (o_raw_body, utl_raw.length(v_buffer), v_buffer);
 exception
 when utl_http.end_of_body then
 utl_http.end_response(i_response);
 exit;
 when others then
 dbms_output.put_line(dbms_utility.format_error_backtrace);
 dbms_output.put_line(sqlerrm);
 exit;
 end;
 end loop;
 return o_raw_body;
 end;
--------------------------------------------------------
 function load_response( i_request in out utl_http.req, i_fetch_blob in boolean default false ) return response as
 o_response response;
 l_headers dict_table;
 l_raw_body clob;
 v_response utl_http.resp;
 begin
 l_headers := dict_table();
 declare
 v_value varchar2(1024);
 v_name varchar2(1000);
 v_buffer varchar2(20000);
 l_header dict_row;
 begin
 v_response := utl_http.get_response(i_request);
 o_response.status_code := v_response.status_code;
 l_headers.extend(utl_http.get_header_count(v_response));
 for i in 1..utl_http.get_header_count(v_response) loop
 utl_http.get_header(v_response, i, v_name, v_value);
 l_header.name := v_name;
 l_header.value := v_value;
 l_headers(i) := l_header;
 end loop;
 o_response.headers := l_headers;
 if not i_fetch_blob then
 o_response.body := response_as_clob(v_response);
 else
 o_response.blob_body := response_as_blob(v_response);
 end if;
 end;
 return o_response;
 end;
--------------------------------------------------------
 procedure send_binary ( i_request in out utl_http.req, i_blob in blob ) as
 req_length binary_integer;
 buffer varchar2(2000);
 amount pls_integer := 2000;
 offset pls_integer := 1;
 l_body clob;
 begin
 l_body := apex_web_service.blob2clobbase64(i_blob);
 req_length := dbms_lob.getlength(l_body);
 utl_http.set_header(i_request, 'Content-Length', req_length);
 utl_http.set_header(i_request, 'Content-Transfer-Encoding', 'base64');
 while ( offset < req_length ) loop
 dbms_lob.read(l_body, amount, offset, buffer);
 utl_http.write_text(i_request, buffer);
 offset := offset + amount;
 end loop;
 end;
--------------------------------------------------------
 procedure send_text ( i_request in out utl_http.req, i_clob in clob ) as
 req_length binary_integer;
 buffer varchar2(2000);
 amount pls_integer := 2000;
 offset pls_integer := 1;
 l_body clob;
 begin
 l_body := i_clob;
 req_length := dbms_lob.getlength(l_body);
 utl_http.set_header(i_request, 'Content-Length', req_length);
 while ( offset < req_length ) loop
 dbms_lob.read(l_body, amount, offset, buffer);
 utl_http.write_text(i_request, buffer);
 offset := offset + amount;
 end loop;
 end;
--------------------------------------------------------
 function gen_query_string ( i_parameters in dict_table ) return varchar2 is
 o_query varchar2(8000);
 begin
 o_query := '?';
 for i in i_parameters.first..i_parameters.last loop 
 o_query := o_query || i_parameters(i).name||'='||i_parameters(i).value;
 if i < i_parameters.last then
 o_query := o_query ||chr(38);
 end if;
 end loop;
 return utl_url.escape(o_query);
 end;
-------------------------------------------------------- 
 procedure close_request(i_request in out utl_http.req) is 
 begin
 declare
 l_response utl_http.resp;
 begin
 l_response := utl_http.get_response(i_request);
 utl_http.end_response(l_response); 
 end;
 end;
--------------------------------------------------------
 function make_request (
 i_url in varchar2, 
 i_http_method in varchar2 default 'GET', 
 i_clob_body in clob default null,
 i_blob_body in blob default null,
 i_parameters in dict_table default dict_table(),
 i_headers in dict_table default dict_table(),
 i_fetch_blob in boolean default false,
 i_wallet in varchar2 default null,
 i_wallet_pwd in varchar2 default null
 ) return response as
 l_request utl_http.req;
 l_raw_response clob;
 l_url varchar2(20000);
 l_query varchar2(8000);
 begin
 if i_wallet is not null and i_wallet_pwd is not null then
 utl_http.set_wallet(i_wallet, i_wallet_pwd); 
 end if;
 if i_clob_body is not null and i_blob_body is not null then
 raise_application_error(-20001, 'only a type of body is allowed');
 end if;
 if i_parameters.count = 0 then
 l_url := i_url;
 else
 l_query := gen_query_string(i_parameters);
 l_url := i_url || l_query;
 end if;
 l_request := utl_http.begin_request(l_url, i_http_method, 'HTTP/1.1');
 if i_headers.count > 0 then
 for i in i_headers.first..i_headers.last loop
 utl_http.set_header(l_request, i_headers(i).name, i_headers(i).value);
 end loop;
 end if;
 if i_clob_body is not null then
 send_text(l_request, i_clob_body);
 elsif i_blob_body is not null then
 send_binary(l_request, i_blob_body);
 end if;
 return load_response(l_request, i_fetch_blob);
 exception
 when others then
 close_request(l_request);
 raise; 
 end;
--------------------------------------------------------
end restful_pck;
/