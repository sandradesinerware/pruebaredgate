CREATE OR REPLACE PACKAGE BODY codensa_gth."EVALUACION_FEEDBACK_PCK" AS

 
  

  Procedure Determinar_formulario_feed     (i_id_colaborador    in number,
                                            i_tipo_formulario       in number,
                                            i_periodo               in number,
                                            o_id_formulario         out number) AS
  l_id_gestor number;
  l_contador_form number;
  BEGIN

--buscar el gestor
select jefe into  l_id_gestor
from trabajador 
where NUMERO_IDENTIFICACION=i_id_colaborador;


--se busca el formulario
select count(*) into l_contador_form
from FEED_FORMULARIO
where ID_COLABORADOR=i_id_colaborador
and ID_GESTOR=l_id_gestor
and PERIODO= i_periodo
and TIPO_FORMULARIO=i_tipo_formulario;


if l_contador_form=0 then 
    --si no existe se crea
    Insert into FEED_FORMULARIO (ID_PLANTILLA_EVALUACION,TIPO_FORMULARIO,PERIODO,ID_COLABORADOR,ID_GESTOR,ESTADO)
    values (Determinar_plantilla_feed(i_tipo_formulario),i_tipo_formulario,i_periodo,i_id_colaborador,l_id_gestor,SNW_CONSTANTES.CONSTANTE_TIPO('ESTADO_FEED_EVALUACION_NUEVO'))
    RETURNING ID INTO o_id_formulario;

elsif l_contador_form=1 then
    --si existe retornarlo
    select id into o_id_formulario
    from FEED_FORMULARIO
    where ID_COLABORADOR=i_id_colaborador
    and ID_GESTOR=l_id_gestor
    and PERIODO= i_periodo
    and TIPO_FORMULARIO=i_tipo_formulario;
end if;

    NULL;
  END Determinar_formulario_feed;

  ---------------------------------------------------------------------------------------------------------

  Procedure Administrar_pregunta_feed     (i_id_formulario        in number,
                                        i_id_pregunta           in number,
                                        i_respuesta_cerrada     in number,
                                        i_respuesta_abierta     in varchar2) AS
  l_contador_respuesta number;
  BEGIN

--existe respuesta
select count(*) into l_contador_respuesta
from FEED_RESPUESTA  
where ID_FORMULARIO=i_id_formulario 
and ID_PREGUNTA_EVALUACION=i_id_pregunta;


if l_contador_respuesta=1 then 
--si, actualizar valor
    update FEED_RESPUESTA 
    set RESPUESTA_CERRADA= i_respuesta_cerrada, 
    RESPUESTA_ABIERTA= i_respuesta_abierta
    where ID_FORMULARIO=i_id_formulario 
    and ID_PREGUNTA_EVALUACION=i_id_pregunta;

elsif l_contador_respuesta=0 then 
--no, crear respuesta
    insert into FEED_RESPUESTA (ID_FORMULARIO,ID_PREGUNTA_EVALUACION,RESPUESTA_CERRADA,RESPUESTA_ABIERTA)
    values (i_id_formulario,i_id_pregunta,i_respuesta_cerrada,i_respuesta_abierta);
end if;

  END Administrar_pregunta_feed;

  ---------------------------------------------------------------------------------------------------------

  Procedure Determinar_estado_form_feed   (i_id_formulario        in number,
                                        i_id_estado             in number,
                                        o_return                out number,
                                        o_mensaje               out varchar2) AS

 l_estado_form NUMBER;
  l_nombre_estado varchar2(200);
  l_conteo_filas NUMBER;
  BEGIN

  select count(*) into l_conteo_filas 
  from FEED_FORMULARIO where id=i_id_formulario;

  select nombre into l_nombre_estado
  from tipo where id=i_id_estado;

  if l_conteo_filas>0 then 

     select estado into l_estado_form 
     from FEED_FORMULARIO where id=i_id_formulario;

     if l_estado_form=i_id_estado then
     o_return := 1;
   o_mensaje := 'El formulario está en el estado: ' || l_nombre_estado || '. ';
     else 
        select nombre into l_nombre_estado
        from tipo where id=l_estado_form;
        o_return := 0;
        o_mensaje := 'El formulario  está en el estado: ' || l_nombre_estado || '. ';
     end if;
  else 
  o_return := 0;
   o_mensaje := 'El formulario de evaluación no existe' ;
  end if;

 exception
 when no_data_found then 

   RAISE_APPLICATION_ERROR (-20000, 'El estado no existe');

  END Determinar_estado_form_feed;

  ---------------------------------------------------------------------------------------------------------

  Procedure Existe_formulario_feed        (i_id_gestor            in number,
                                        i_id_colaborador        in number,
                                        i_tipo_formulario       in number,
                                        i_periodo               in number,
                                        o_return                out number,
                                        o_mensaje               out varchar2) AS
  l_conteo_filas NUMBER;
  l_nombre_tipo_form VARCHAR2(300);
  l_nombre_periodo VARCHAR2(300);

BEGIN
 select count(*) into l_conteo_filas
     from FEED_FORMULARIO
     where ID_COLABORADOR=i_id_colaborador
    and ID_GESTOR=i_id_gestor
    and PERIODO= i_periodo
    and TIPO_FORMULARIO=i_tipo_formulario;

    SELECT nombre into l_nombre_tipo_form 
    from tipo where id=i_tipo_formulario;

      SELECT ano into l_nombre_periodo 
    from FEED_PERIODO where id=i_periodo;


   if l_conteo_filas = 0 then --La evaluación no existe
     o_return := 0;
     o_mensaje := 'El trabajador identificado con el número ' || i_id_colaborador || ' no tiene una evaluación de tipo ' || l_nombre_tipo_form || ' en el periodo '
       || l_nombre_periodo || '. ';
   else
     o_return := 1;
     o_mensaje := 'El trabajador identificado con el número ' || i_id_colaborador || ' tiene una evaluación de tipo ' || l_nombre_tipo_form || ' en el periodo '
       || l_nombre_periodo || '. ';
   end if;    NULL;
  END Existe_formulario_feed;

  ---------------------------------------------------------------------------------------------------------

  Function Determinar_pag_form_feed       (i_id_formulario        in number)
                                        return number AS
  l_tipo_formulario number;
  BEGIN
  select TIPO_FORMULARIO into  l_tipo_formulario
  from FEED_FORMULARIO 
  where id =i_id_formulario;

  --FALTA COLOCAR LAS PAGINAS

  IF l_tipo_formulario= SNW_CONSTANTES.CONSTANTE_TIPO('TIPO_FORMULARIO_FEED_GESTOR') THEN 

    RETURN 140;

  ELSIF l_tipo_formulario= SNW_CONSTANTES.CONSTANTE_TIPO('TIPO_FORMULARIO_FEED_COLABORADOR') THEN

    RETURN 140;

  END IF;

  END Determinar_pag_form_feed;

  ---------------------------------------------------------------------------------------------------------

  Procedure Cambiar_estado_form_feed     (i_id_formulario         in number,
                                        i_id_estado             in number) AS
    l_contador_form number;
  BEGIN
  --VERIFICAR SI EXISTE
  select count(*) into  l_contador_form
  from FEED_FORMULARIO 
  where id =i_id_formulario;

if l_contador_form=1 then 
--SI EXISTE CAMBIAR DE ESTADO

update FEED_FORMULARIO 
set estado=i_id_estado   
where id =i_id_formulario;

else 
--SI NO MENSAJE 

 RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en CAMBIO DE ESTADO FORMULARIO FEEDBACK. Contacte al administrador');

end if;


  END Cambiar_estado_form_feed;

  ---------------------------------------------------------------------------------------------------------

  Function Determinar_plantilla_feed      (i_tipo_formulario        in number)
                                        return number AS
  l_id_plantilla number;
  BEGIN
if i_tipo_formulario=SNW_CONSTANTES.CONSTANTE_TIPO('TIPO_FORMULARIO_FEED_GESTOR') then 


    select MAX(ID) INTO l_id_plantilla 
    from GEN_PLANTILLA_EVALUACION 
    WHERE TIPO_PLANTILLA=SNW_CONSTANTES.CONSTANTE_TIPO('PLANTILLA_FEEDBACK_GESTOR');

elsif i_tipo_formulario=SNW_CONSTANTES.CONSTANTE_TIPO('TIPO_FORMULARIO_FEED_COLABORADOR') then 

    select MAX(ID) INTO l_id_plantilla 
    from GEN_PLANTILLA_EVALUACION 
    WHERE TIPO_PLANTILLA= SNW_CONSTANTES.CONSTANTE_TIPO('PLANTILLA_FEEDBACK_COLABORADOR'); 

end if;


RETURN l_id_plantilla;
  END Determinar_plantilla_feed;

END EVALUACION_FEEDBACK_PCK;

/