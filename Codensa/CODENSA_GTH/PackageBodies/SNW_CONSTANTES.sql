CREATE OR REPLACE PACKAGE BODY codensa_gth."SNW_CONSTANTES" as
   function constante_tipo(i_nombre in varchar2) return number IS
    L_AUX NUMBER;
    BEGIN
       SELECT ID into L_AUX FROM TIPO WHERE CONSTANTE = i_nombre;
       RETURN L_AUX;    
       EXCEPTION WHEN NO_DATA_FOUND THEN
         RETURN 0;   
    end constante_tipo;

   function generar_constante_tipo(i_tipo_nombre in varchar2) return varchar2 is 
   begin
    return trim(substr(REGEXP_REPLACE(TRANSLATE(
                      REGEXP_REPLACE(
                            REGEXP_REPLACE(
                                REGEXP_REPLACE(
                                        upper(i_tipo_nombre),'[.,-/:+() ]+','_'
                                        ),'[_]+[(Y)|(DE)|(O)|(LA)|(LOS)|(QUE)|(EN)|(CON)|(POR)]+[_]+','_'
                                ),'[_]+[(Y)|(DE)|(O)|(LA)|(LOS)|(QUE)|(EN)|(CON)|(POR)]+[_]+','_'),'ÁÉÍÓÚÑ','AEIOUN'
                      ),'[_]$',''),0,64)
                      );
   end generar_constante_tipo;
   function constante_dependencia(i_nombre in varchar2) return number is
    L_AUX NUMBER;
    BEGIN
       SELECT ID into L_AUX FROM DEPENDENCIA WHERE UPPER(CONSTANTE) LIKE UPPER(i_nombre);
       RETURN L_AUX;    
       EXCEPTION WHEN NO_DATA_FOUND THEN
         RETURN 0; 
   end constante_dependencia;

   
function get_id_tipo(i_nombre in varchar2, i_tipo_constante in varchar2) return number AS
  l_id_tipo number;
BEGIN
  select id into l_id_tipo
    from tipo
    where tipo = constante_tipo(i_tipo_constante) and upper(nombre) = upper(i_nombre);
  return l_id_tipo;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    return null;
END get_id_tipo;

end "SNW_CONSTANTES";
/