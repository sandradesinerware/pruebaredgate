CREATE OR REPLACE PACKAGE BODY codensa_gth."PERIODO_PCK" AS

  function periodo_activo_default return number is
    id_periodo_activo number;
  begin
    select id into id_periodo_activo from (select id from pdi_periodo where estado = 1 order by fecha_fin desc) where rownum = 1;
    return id_periodo_activo;
    exception when NO_DATA_FOUND then 
         return NULL;    
  end periodo_activo_default;
  
  
  function periodo_maximo_default return number is
    id_periodo_maximo number;
  begin
    select id into id_periodo_maximo from (select id from pdi_periodo order by ano desc) where rownum = 1;
    return id_periodo_maximo;
    exception when NO_DATA_FOUND then 
         return NULL;    
  end periodo_maximo_default; 
  
  Procedure val_estado_periodo (i_ano      IN PERIODO_BARS.ano%type,
                                i_estado   IN ESTADO_BARS.NOMBRE%type,
                                o_return   OUT NUMBER, 
                                o_mensaje  OUT VARCHAR2) AS
 l_conteo_filas NUMBER;
 l_tipo_estado NUMBER;
 l_estado NUMBER;
 l_nombre_tipo_estado CONSTANT VARCHAR2(20) := 'PERIODO_BARS';
BEGIN
 --Validar que exista tipo estado para PERIODO_BARS
 SELECT count(*) into l_conteo_filas
   from TIPO_ESTADO_BARS where upper(nombre) = l_nombre_tipo_estado;
 if l_conteo_filas = 0 then --El tipo estado no existe
   SELECT nvl( max(id), 0 ) + 1 into l_tipo_estado
     from TIPO_ESTADO_BARS;
   INSERT INTO TIPO_ESTADO_BARS (id, nombre) VALUES (l_tipo_estado, l_nombre_tipo_estado);
 else --El tipo estado está una o más veces
   SELECT id into l_tipo_estado
     from TIPO_ESTADO_BARS
     where upper(nombre) = l_nombre_tipo_estado and rownum = 1;
 end if;

 --Validar que exista el estado solicitado para PERIODO_BARS
 SELECT count(*) into l_conteo_filas
   from ESTADO_BARS where upper(nombre) = upper(i_estado) and tipo_estado = l_tipo_estado;
 if l_conteo_filas = 0 then --El estado no existe
   SELECT nvl( max(id), 0 ) + 1 into l_estado
     from ESTADO_BARS;
   INSERT INTO ESTADO_BARS (id, nombre, tipo_estado) VALUES (l_estado, upper(i_estado), l_tipo_estado);
 else --El estado existe una o más veces
   SELECT id into l_estado
     from ESTADO_BARS
     where upper(nombre) = upper(i_estado) and tipo_estado = l_tipo_estado and rownum = 1;
 end if;

 --Validar que el periodo esté en el estado solicitado
 SELECT count(*) into l_conteo_filas
   from PERIODO_BARS
   where ano = i_ano and estado = l_estado;
 if l_conteo_filas = 0 then --El periodo no está en el estado suministrado
   o_return := 0;
   o_mensaje := 'El periodo de evaluación no está en el estado: ' || i_estado || '. ';
 else
   o_return := 1;
   o_mensaje := 'El periodo de evaluación está en el estado: ' || i_estado || '. ';
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_PERIODO. ' || SQLCODE || ' - '
     || SQLERRM);
  END val_estado_periodo;
------------------------------------------------------------------------------------------------------------
  Procedure val_estado_periodo_feed (i_id_periodo      IN NUMBER,
                                i_estado   IN NUMBER,
                                o_return   OUT NUMBER, 
                                o_mensaje  OUT VARCHAR2) AS
  l_estado_periodo NUMBER;
  l_nombre_estado varchar2(200);
  l_conteo_filas NUMBER;
  BEGIN

  select count(*) into l_conteo_filas 
  from feed_periodo where id=i_id_periodo;

  select nombre into l_nombre_estado
  from tipo where id=i_estado;

  if l_conteo_filas>0 then 

     select estado into l_estado_periodo 
     from feed_periodo where id=i_id_periodo;

     if l_estado_periodo=i_estado then
        o_return := 1;
        o_mensaje := 'El periodo de feedback está en el estado: ' || l_nombre_estado || '. ';
     else 
        o_return := 0;
        o_mensaje := 'El periodo de feedback no está en el estado: ' || l_nombre_estado || '. ';
     end if;
  else 
  o_return := 0;
   o_mensaje := 'El periodo de evaluación no existe' ;
  end if;

 exception
 when no_data_found then 

   RAISE_APPLICATION_ERROR (-20000, 'El estado no existe');

  END val_estado_periodo_feed;

END PERIODO_PCK;
/