CREATE OR REPLACE PACKAGE BODY codensa_gth."SNW_GESTION_AGENTES" AS

  PROCEDURE INSERTAR_AGENTE(i_documento in number) AS
  l_conteo number;
  BEGIN
    select count(*) into l_conteo from agente where numero_documento = i_documento;
    if l_conteo = 0 then
        insert into agente (numero_documento) values (i_documento);
    end if;
  END INSERTAR_AGENTE;

END SNW_GESTION_AGENTES;

/