CREATE OR REPLACE PACKAGE BODY codensa_gth."NOTIFICACION_PCK" AS

  PROCEDURE  INSERTAR_LOG(  I_REMITENTE IN VARCHAR, 
                            I_DESTINATARIO IN VARCHAR,
                            I_CUERPO IN VARCHAR, 
                            I_ASUNTO IN VARCHAR, 
                            I_FECHA IN DATE,
                            I_ERRORES IN VARCHAR) AS
  BEGIN
        INSERT INTO CORREO_LOG (DESTINATARIO,REMITENTE,ASUNTO,CUERPO,FECHA,OBSERVACION) 
        VALUES(I_DESTINATARIO,I_REMITENTE,I_ASUNTO,I_CUERPO,I_FECHA,I_ERRORES);
        EXCEPTION WHEN OTHERS THEN
            INSERT INTO CORREO_LOG (DESTINATARIO,REMITENTE,ASUNTO,CUERPO,FECHA,OBSERVACION) 
        VALUES(I_DESTINATARIO,I_REMITENTE,I_ASUNTO,I_CUERPO,I_FECHA,I_ERRORES||' - Error generado en el log.');
  END INSERTAR_LOG;

  PROCEDURE ENVIAR_CORRREO( I_DESTINATARIO IN VARCHAR,
                            I_ASUNTO IN VARCHAR,
                            I_CUERPO IN VARCHAR,I_FECHA DATE) AS
  --req utl_http.req;
  --res utl_http.resp;
  --url varchar2(4000) := ADMIN_EMAIL_SERVER||'/simple_email'; --url para consumir el servicio
  --name varchar2(4000);
  --buffer varchar2(4000); 
  --id_servicio varchar2(2000):='45645gfdgffdgfgdfgdfg';
  l_remitente varchar2(2000):= 'gth.grupoenel@gmail.com';
  --JSON donde se encuentran los parámetros

  --content varchar2(4000) := '{"datos_adicionales":[ {   "correo_origen":"'||l_remitente||'",   "id_servicio": "'||id_servicio||'" }],                              "mensaje": [ {   "to":"'||I_DESTINATARIO||'",   "from": "'||l_remitente||'",   "subject": "'||I_ASUNTO||'",   "message_text": "'||replace(I_CUERPO,chr(34),'\"')||'" }]}';
  BEGIN
      /*apex_json.initialize_clob_output;
      apex_json.open_object;
      apex_json.open_array('datos_adicionales');
      apex_json.open_object;
      apex_json.write('correo_origen', l_remitente);
      apex_json.write('id_servicio', id_servicio);
      apex_json.close_object;
      apex_json.close_array();
      apex_json.open_array('mensaje');
      apex_json.open_object;
      apex_json.write('to', I_DESTINATARIO);
      apex_json.write('from', l_remitente);
      apex_json.write('subject', I_ASUNTO);
      apex_json.write('message_text', I_CUERPO);      
      apex_json.close_object;
      apex_json.close_array();
      apex_json.close_object;
      req := utl_http.begin_request(url, 'POST');
      UTL_HTTP.SET_BODY_CHARSET('UTF-8');
      UTL_HTTP.SET_HEADER(req, 'User-Agent', 'Mozilla/4.0');
      utl_http.set_header(req, 'content-type', 'application/json;'); 
      utl_http.set_header(req, 'Content-Length', length(apex_json.get_clob_output));
      utl_http.write_text(req, apex_json.get_clob_output);
      res := utl_http.get_response(req);
      utl_http.end_response(res);
      apex_json.free_output;*/
      email_pck.enviar_correo(I_DESTINATARIO,I_CUERPO,I_ASUNTO);
      INSERTAR_LOG(l_remitente,I_DESTINATARIO,I_CUERPO,I_ASUNTO,SYSDATE,'Enviado');
      exception when others then
        INSERTAR_LOG(l_remitente,I_DESTINATARIO,I_CUERPO,I_ASUNTO,SYSDATE,SQLERRM);
  END ENVIAR_CORRREO;

-----------------------------------------------------------------------------------------

  PROCEDURE ENVIAR_MENSAJE_INSCRITO_RRHH(I_ID_BARS_PDI IN NUMBER) AS
    l_id_accion number;
    l_id_grupo number;
    l_cuerpo varchar2(4000);
    l_asunto varchar2(4000);
    l_facultad varchar2(4000);
    l_negocio varchar2(4000);
    l_curso varchar2(4000);
    l_grupo varchar2(4000);
    l_encargado varchar2(4000);
    l_fecha_inicio date;
    l_fecha_fin date;
    l_correo varchar2(4000);
    l_id_encabezado number;
    l_concepto varchar2(4000) := 'SIN CONCEPTO';
    l_conteo number;
  BEGIN
    select id_accion,id_pdi_grupo,id_encabezado into l_id_accion,l_id_grupo,l_id_encabezado from bars_pdi where id = I_ID_BARS_PDI;
    if SNW_CONSTANTES.constante_tipo('FORMACION')  = l_id_accion and l_id_grupo is not null then

            select email into l_correo from trabajador 
            where numero_identificacion = (select evaluado from bars_encpdi where id = l_id_encabezado);

            select nvl(facultad,''),nvl(negocio,''),nvl(curso,''),nvl(grupo,''),nvl(encargado,''),nvl(to_char(fecha_inicio),''),nvl(to_char(fecha_fin),'') into
            l_facultad,l_negocio,l_curso,l_grupo,l_encargado,l_fecha_inicio,l_fecha_fin from v_grupos_curso
            where id = l_id_grupo;

            select count(*) into l_conteo 
            from pdi_concepto where tipo_concepto = SNW_CONSTANTES.constante_tipo('CONCEPTO_RRHH') and id_pdi = I_ID_BARS_PDI;
            if l_conteo > 0 and l_correo is not null then
                select decode(id_estado_pdi,30,'APROBADA','NO APROBADA') into l_concepto 
                from pdi_concepto where tipo_concepto = SNW_CONSTANTES.constante_tipo('CONCEPTO_RRHH') and id_pdi = I_ID_BARS_PDI;
                l_cuerpo := 
                    'Estimado usuario,<br>'||
                    'Su siguiente acción de formación fue '||l_concepto||' por el área de talento humano:'||
                    '<br>'||
                    '<br><b>Facultad: </b>'||l_facultad||
                    '<br><b>Negocio: </b>'||l_negocio||
                    '<br><b>Curso: </b>'||l_curso||
                    '<br><b>Grupo: </b>'||l_grupo||
                    '<br><b>Encargado: </b>'||l_encargado||
                    '<br><b>Fecha inicio: </b>'||l_fecha_inicio||
                    '<br><b>Fecha fin: </b>'||l_fecha_fin||
                    '<br><br><br>'||
                    'Favor no responder este mensaje, esta cuenta solo realiza actividades de envió.';
                l_asunto := 'Notificación acción de formación';     
                ENVIAR_CORRREO(l_correo,l_asunto,l_cuerpo);   
            end if;
    end if;
    exception when others then
         Raise_application_error(-20001,'La información no esta completa');
  END ENVIAR_MENSAJE_INSCRITO_RRHH;
  -----------------------------------------------------------------------------------------

  PROCEDURE ENVIAR_MENSAJE_INSCRITO_GESTOR(I_ID_BARS_PDI IN NUMBER) AS
    l_id_accion number;
    l_id_grupo number;
    l_cuerpo varchar2(4000);
    l_asunto varchar2(4000);
    l_facultad varchar2(4000);
    l_negocio varchar2(4000);
    l_curso varchar2(4000);
    l_grupo varchar2(4000);
    l_encargado varchar2(4000);
    l_fecha_inicio date;
    l_fecha_fin date;
    l_correo varchar2(4000);
    l_id_encabezado number;
    l_concepto varchar2(4000) := 'SIN CONCEPTO';
    l_conteo number;
  BEGIN
    select id_accion,id_pdi_grupo,id_encabezado into l_id_accion,l_id_grupo,l_id_encabezado from bars_pdi where id = I_ID_BARS_PDI;
    if SNW_CONSTANTES.constante_tipo('FORMACION')  = l_id_accion and l_id_grupo is not null then
            select email into l_correo from trabajador 
            where numero_identificacion = (select evaluado from bars_encpdi where id = l_id_encabezado);

            select nvl(facultad,''),nvl(negocio,''),nvl(curso,''),nvl(grupo,''),nvl(encargado,''),nvl(to_char(fecha_inicio),''),nvl(to_char(fecha_fin),'') into
            l_facultad,l_negocio,l_curso,l_grupo,l_encargado,l_fecha_inicio,l_fecha_fin from v_grupos_curso
            where id = l_id_grupo;

            select count(*) into l_conteo 
            from pdi_concepto where tipo_concepto = SNW_CONSTANTES.constante_tipo('CONCEPTO_GESTOR') and id_pdi = I_ID_BARS_PDI;
            if l_conteo > 0 and l_correo is not null then
                select decode(id_estado_pdi,30,'APROBADA','NO APROBADA') into l_concepto 
                from pdi_concepto where tipo_concepto = SNW_CONSTANTES.constante_tipo('CONCEPTO_GESTOR') and id_pdi = I_ID_BARS_PDI;
                l_cuerpo := 
                    'Estimado usuario,<br>'||
                    'Su siguiente acción de formación fue '||l_concepto||' por el gestor:'||
                    '<br>'||
                    '<br><b>Facultad: </b>'||l_facultad||
                    '<br><b>Negocio: </b>'||l_negocio||
                    '<br><b>Curso: </b>'||l_curso||
                    '<br><b>Grupo: </b>'||l_grupo||
                    '<br><b>Encargado: </b>'||l_encargado||
                    '<br><b>Fecha inicio: </b>'||l_fecha_inicio||
                    '<br><b>Fecha fin: </b>'||l_fecha_fin||
                    '<br><br><br>'||
                    'Favor no responder este mensaje, esta cuenta solo realiza actividades de envió.';
                l_asunto := 'Notificación acción de formación';
                ENVIAR_CORRREO(l_correo,l_asunto,l_cuerpo);   
            end if;
    end if;
    exception when others then
         Raise_application_error(-20001,'La información no esta completa');
  END ENVIAR_MENSAJE_INSCRITO_GESTOR;

  --------------------------------------------------------------------------------------
  PROCEDURE NOTIFICAR_CAMBIO_GRUPO(I_ID_BARS_PDI IN NUMBER,I_NUEVO_ID_PDI_GRUPO IN NUMBER) AS
    l_id_accion number;
    l_id_grupo_nuevo number;
    l_id_grupo_viejo number;
    l_cuerpo varchar2(4000);
    l_asunto varchar2(4000);
    l_facultad varchar2(4000);
    l_negocio varchar2(4000);
    l_curso varchar2(4000);
    l_grupo_nuevo varchar2(4000);
    l_grupo_viejo varchar2(4000);
    l_encargado varchar2(4000);
    l_fecha_inicio_nuevo date;
    l_fecha_fin_nuevo date;
    l_fecha_inicio_viejo date;
    l_fecha_fin_viejo date;
    l_correo varchar2(4000);
    l_id_encabezado number;
    l_nombre_inscrito varchar2(4000);
  BEGIN
    select id_accion,id_pdi_grupo,id_encabezado into l_id_accion,l_id_grupo_viejo,l_id_encabezado from bars_pdi where id = I_ID_BARS_PDI;
    if SNW_CONSTANTES.constante_tipo('FORMACION')  = l_id_accion and l_id_grupo_viejo is not null then

        select upper(nombres||' '||apellidos) into l_nombre_inscrito from TRABAJADOR where numero_identificacion =
        (select evaluado from bars_encpdi where id = l_id_encabezado);

        select email into l_correo from trabajador 
            where numero_identificacion = (select id_encargado from pdi_curso 
            where id = (select id_pdi_curso from pdi_grupo where id =  l_id_grupo_viejo));

        select nvl(facultad,''),nvl(negocio,''),nvl(curso,''),nvl(grupo,''),nvl(encargado,''),nvl(to_char(fecha_inicio),''),nvl(to_char(fecha_fin),'') into
            l_facultad,l_negocio,l_curso,l_grupo_viejo,l_encargado,l_fecha_inicio_viejo,l_fecha_fin_viejo from v_grupos_curso
            where id = l_id_grupo_viejo;

        select nvl(grupo,''),nvl(to_char(fecha_inicio),''),nvl(to_char(fecha_fin),'') into
            l_grupo_nuevo,l_fecha_inicio_nuevo,l_fecha_fin_nuevo from v_grupos_curso
            where id = I_NUEVO_ID_PDI_GRUPO;

        l_cuerpo := 
                'Estimado usuario,<br>'||
                'El colaborador '||l_nombre_inscrito||' se ha cambiado de grupo.'||
                '<br><br><br>'||
                '<h3>Grupo anterior</h3>'||
                '<br><b>Facultad: </b>'||l_facultad||
                '<br><b>Negocio: </b>'||l_negocio||
                '<br><b>Curso: </b>'||l_curso||
                '<br><b>Grupo: </b>'||l_grupo_viejo||
                '<br><b>Encargado: </b>'||l_encargado||
                '<br><b>Fecha inicio: </b>'||l_fecha_inicio_viejo||
                '<br><b>Fecha fin: </b>'||l_fecha_fin_viejo||
                '<br><br>'||
                '<h3>Grupo nuevo</h3>'||
                '<br><b>Facultad: </b>'||l_facultad||
                '<br><b>Negocio: </b>'||l_negocio||
                '<br><b>Curso: </b>'||l_curso||
                '<br><b>Grupo: </b>'||l_grupo_nuevo||
                '<br><b>Encargado: </b>'||l_encargado||
                '<br><b>Fecha inicio: </b>'||l_fecha_inicio_nuevo||
                '<br><b>Fecha fin: </b>'||l_fecha_fin_nuevo||
                '<br><br><br>'||
                'Favor no responder este mensaje, esta cuenta solo realiza actividades de envió.';
            l_asunto := 'Notificación acción de formación';
            ENVIAR_CORRREO(l_correo,l_asunto,l_cuerpo);   
    end if;
    exception when others then
         Raise_application_error(-20001,'La información no esta completa');

  END;
  --------------------------------------------------------------------------------------
  PROCEDURE NOTIFICAR_CAMBIO_CLAVE(i_token in varchar2) as
  l_correo varchar2(4000);
  l_mensaje varchar2(4000);
  l_asunto varchar2(4000);
  l_nombre varchar2(4000);
  begin
  select EMAIL,nombres||' '||apellidos into l_correo,l_nombre from trabajador 
  where NUMERO_IDENTIFICACION in (select NUMERO_IDENTIFICACION from usuario where CAMBIO=i_token);
  l_mensaje := 'Estimado usuario,<br>'||l_nombre||'<br>'||
                'Por favor de click <a href="'||
                APEX_UTIL.PREPARE_URL(deploy_pck.server||deploy_pck.SERVER_ORDS_CONTEXT||'f?p=101:210:::NO:RP,210:P210_TOKEN:'||i_token)||
                '">aquí</a> para restablecer su contraseña.<br><br>'||
                'Favor no responder este mensaje, esta cuenta solo realiza actividades de envió.';
  l_asunto := 'Cambio de contraseña Performance Management';
  Enviar_Corrreo(l_correo,l_asunto,l_mensaje);
    exception when others then
        Raise_application_error(-20001,'La información no esta completa');
  end;


-------------------------------------------------------------------------------------- 
  PROCEDURE Notificar_Trabajador_PEC(i_id_pec IN NUMBER, i_o_mensaje in out varchar2) AS  
  l_correo      VARCHAR2(4000);
  l_mensaje     VARCHAR2(4000);
  l_asunto      VARCHAR2(4000);  
  l_id_trab     VARCHAR2(4000);
  l_trab_nombre VARCHAR2(4000);
  l_conteo number;
  BEGIN

  SELECT id_trabajador into l_id_trab FROM pec WHERE id = i_id_pec;
  
  SELECT count(*) into l_conteo from trabajador WHERE numero_identificacion = l_id_trab and email is not null;
  
  if l_conteo > 0 then   
 
    SELECT email, nombres||' '||apellidos 
    INTO l_correo, l_trab_nombre
    FROM trabajador  
    WHERE numero_identificacion = l_id_trab;

    l_mensaje := 'Estimado usuario,<br>'||l_trab_nombre||'<br>'||
                 'Ha sido creado su <b>Plan de Entrenamiento en el Cargo (PEC) </b>
                 por favor ingrese al sistema Performance Management.
                 <br>
                 Este plan de entrenamiento te permitirá recibir formaciones y herramientas
                 específicas para el desempeño de tu cargo, facilitando la adaptación al mismo.';

    l_asunto := 'Plan de Entrenamiento en el Cargo PEC';

    Enviar_Corrreo(l_correo,l_asunto,l_mensaje);    
    
    i_o_mensaje := 'Se ha enviado notificación al correo registrado para el trabajador.';
    
    -- No es obligatorio que el correo exista para continuar con la planeación del PEC.
    --Raise_Application_Error(-20000,'El trabajador no cuenta con un correo válido para ser notificado!, por favor comuníquese con el administrador');
  END IF;

  EXCEPTION 
    WHEN OTHERS THEN
      Raise_Application_Error(-20000,'Ha ocurrido un error inesperado en la notificación del PEC al trabajador, por favor comuníquese con el administrador.');

  END Notificar_Trabajador_PEC;

-------------------------------------------------------------------------------------- 
  PROCEDURE Notificar_Evaluacion_PEC(i_id_pec IN NUMBER, i_o_mensaje in out varchar2) AS  
  l_correo      VARCHAR2(4000);
  l_mensaje     VARCHAR2(4000);
  l_asunto      VARCHAR2(4000);  
  l_id_trab     VARCHAR2(4000);
  l_trab_nombre VARCHAR2(4000);
  l_conteo NUMBER;
  l_fecha_pec   DATE;
  BEGIN

  SELECT id_trabajador, fecha_creacion into l_id_trab, l_fecha_pec FROM pec WHERE id = i_id_pec;

  SELECT COUNT(*) Into l_conteo from trabajador where numero_identificacion = l_id_trab and email is not null;
  
  if l_conteo > 0 and l_fecha_pec >= to_date('01/01/2020','DD/MM/YYYY') then   

      SELECT email, nombres||' '||apellidos 
      INTO l_correo, l_trab_nombre
      FROM trabajador  
      WHERE numero_identificacion = l_id_trab;
          
    
      l_mensaje := 'Estimado usuario '||l_trab_nombre||'<br><br>'||
                 'Es muy importante conocer su percepción sobre el PEC desarrollado. Por favor ingrese al sistema Performance Management,
                 para realizar la evaluación de las actividades de su Plan de Entrenamiento en el Cargo.<br><br>'||
                 'Para ir a la aplicación y evaluar el PEC, puede dar clic en el siguiente enlace: '||
                 '<a href="' || codensa_gth.DEPLOY_PCK.OBTENER_SERVER || codensa_gth.DEPLOY_PCK.SERVER_ORDS_CONTEXT || APEX_UTIL.PREPARE_URL('f?p='||'101'||':198:'||V('SESSION')||'::NO::P198_ID_PEC:'|| i_id_pec) || '">Ir a evaluar el PEC</a>' ||
                 '<br><br>' ||
                 'Gracias de antemano por su colaboración,<br><br>
                 División de Formación y Desarrollo';

      l_asunto := 'Evaluación Plan de Entrenamiento en el Cargo PEC';

      Enviar_Corrreo(l_correo,l_asunto,l_mensaje);    
      
      i_o_mensaje := 'Se ha enviado notificación al correo registrado para el trabajador.';            
  
  end if;

  EXCEPTION 
    WHEN OTHERS THEN
      Raise_Application_Error(-20000,'Ha ocurrido un error inesperado en la notificación de la evaluación del PEC al trabajador, por favor comuníquese con el administrador.');

  END Notificar_Evaluacion_PEC;

-------------------------------------------------------------------------------------- 
PROCEDURE Alertar_PEC_No_Planeado(i_pec IN PEC%ROWTYPE) AS  
  l_destinatario  VARCHAR2(4000);
  l_correo        VARCHAR2(4000);
  l_trabajador    VARCHAR2(4000);
  l_trab_nombre   VARCHAR2(4000);
  l_mensaje       VARCHAR2(4000);
  l_asunto        VARCHAR2(4000);
  BEGIN

  SELECT nombres||' '||apellidos, email into l_destinatario, l_correo
    from TRABAJADOR
    where numero_identificacion = i_pec.id_gestor;

  SELECT nombres||' '||apellidos, numero_identificacion  into l_trab_nombre, l_trabajador
    from TRABAJADOR
    where numero_identificacion = i_pec.id_trabajador;

  IF l_correo IS NOT NULL THEN

    l_mensaje := 
    'Estimado(a) ' || l_destinatario || '<br><br>' ||
    'Damos la bienvenida a un nuevo colaborador en su equipo: <br><br>' ||
	'Número asignado: ' || l_trabajador || '<br>' ||
    'Nombre completo: ' || l_trab_nombre || '<br><br>' ||
	'Por favor, ingrese al sistema Performance Management y cree las actividades para su Plan de ' ||
    'Entrenamiento en el Cargo (PEC), con los contenidos que considere prioritarios para su adaptación '||
    'y lograr el mejor desempeño de su labor.  Por ejemplo, en el Plan de Entrenamiento deben ir '||
    'incluidas formaciones específicas como procedimientos, instructivos, políticas, sistemas específicos, '||
    'entre otros, para el buen desarrollo de su labor.' || '<br><br>' ||
    'Es muy importante que usted como Líder, tenga garantizado el cumplimiento del Plan de Entrenamiento en el '||
    'Cargo ya que esta acción genera sentido de pertenencia y motivación.' || '<br><br>' ||
    'La planeación de este Plan de Entrenamiento debe estar a partir del primer día de trabajo de su colaborador, '||
    'y tiene 45 días para finalizar su ejecución en el sistema.' || '<br><br>' ||
    'Para ir a la aplicación y crear el PEC, puede dar clic en el siguiente enlace: ' ||
    '<a href="' || codensa_gth.DEPLOY_PCK.OBTENER_SERVER || codensa_gth.DEPLOY_PCK.SERVER_ORDS_CONTEXT || APEX_UTIL.PREPARE_URL('f?p='||'101'||':187:'||V('SESSION')||'::NO::P187_ID_PEC:'|| i_pec.id) || '">Ir a planear el PEC</a>' ||
    '<br><br>' ||
    'Gracias de antemano por su colaboración,' || '<br><br>' ||
    'División de Formación y Desarrollo';

    l_asunto := 'Se requiere creación de Plan de Entrenamiento en el Cargo PEC';

    Enviar_Corrreo(l_correo, l_asunto, l_mensaje);          
  END IF;

EXCEPTION 
    WHEN OTHERS THEN
        null; --Lineas comentadas por que el campo email ya no será obligatorio.
      --Raise_Application_Error(-20000,'La información para notificar al gestor del nuevo PEC no esta completa, 
      --debe completar el correo del gestor y crear el usuario nuevamente!');      

END Alertar_PEC_No_Planeado;

-------------------------------------------------------------------------------------- 
  PROCEDURE NOTIFICAR_BP_DESDE_GESTOR(i_id_cargo IN NUMBER) AS  

    l_correo_destino        VARCHAR2(4000);
    l_nombres               VARCHAR2(4000);
    l_asunto                VARCHAR2(4000);
    l_mensaje               VARCHAR2(4000);
    l_tipo                  VARCHAR2(4000);    

    l_unidad                VARCHAR2(4000);
    l_denominacion          VARCHAR2(4000);
    l_cargo_elaborador      VARCHAR2(4000);
    l_usuario               VARCHAR2(4000);
    l_justificacion         VARCHAR2(4000);

    l_count                 NUMBER;
    l_reviso                NUMBER;

    BEGIN
    l_asunto := 'Nuevo formulario en su bandeja de CARGOS'; 
    select count(*) into l_count from SNW_CARGOS.cargo where id = i_id_cargo and id = ID_CARGO_PADRE;

    -- Se trata de un caso de CREACIÓN
    IF l_count > 0 THEN
        l_tipo := '<b>CREACIÓN</b>';
        -- l_fecha := 'Fecha de creación'; ??
    -- Se trata de un caso de ACTUALIZACIÓN
    ELSE 
        l_tipo := '<b>ACTUALIZACIÓN</b>';
    END IF;

    -- Obtenemos la U.O del cargo
    select nombre into l_unidad from SNW_CARGOS.UNIDAD_ORGANIZATIVA where id = (select id_unidad_organizativa from SNW_CARGOS.cargo where id = i_id_cargo);
    -- Obtenemos la Denominación del cargo
    select denominacion into l_denominacion from SNW_CARGOS.cargo where id = i_id_cargo;
    -- Obtenemos la Denominación del cargo que elaboró el cargo enviado a la bandeja BP
    select elaboro into l_usuario from SNW_CARGOS.cargo where id = i_id_cargo;
    l_cargo_elaborador := SNW_CARGOS.FORMULARIO.DENOMINACION_DE_USUARIO(l_usuario);
    -- Obtenemos la justificación del cargo
    select justificacion into l_justificacion from SNW_CARGOS.cargo where id = i_id_cargo;


    -- Obtenemos los destinatarios
    -- 1) Si ya lo revisó anteriormente un BP, se envía solamente a el.
    select count(*) into l_reviso from SNW_CARGOS.cargo where id = i_id_cargo and reviso is not null;

    IF l_reviso > 0 THEN

        select count(*) into l_count from trabajador where email is not null and NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where user_name = 
        (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));

        -- SI el correo existe, procedemos con el envio
        IF l_count > 0 THEN

            -- Obtenemos el email del que revisó   
            select email into l_correo_destino from trabajador where NUMERO_IDENTIFICACION = 
            (select numero_identificacion from usuario where user_name = 
            (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));

            -- Obtenemos los nombres del que revisó
            select nombres into l_nombres from trabajador where NUMERO_IDENTIFICACION = 
            (select numero_identificacion from usuario where user_name = 
            (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));

            l_asunto := 'Nuevo formulario en su bandeja de CARGOS'; 

            l_mensaje := 'Estimado(a) ' || l_nombres || '<br>' ||
                     '<br>Ha llegado un formulario de solicitud de ' || l_tipo || ' de una descripción de cargo de su perímetro a su bandeja de <b>BUSINESS PARTNER</b>.' || '<br>' ||

                     '<br><b>FORMULARIO</b>' || '<br>' ||

                     '<br><b>Unidad Organizativa:</b> ' || l_unidad || 
                     '<br><b>Denominación del cargo:</b> ' || l_denominacion || 
                     '<br><b>Elaboró:</b> ' || l_cargo_elaborador || 
                     '<br><b>Fecha de ' || LOWER(l_tipo) || ':</b> ' || sysdate || 
                     '<br><b>Justificación:</b> ' || l_justificacion || '<br>' ||                                   
                     '<br>Para validar y dar su concepto dar clic en <a href="' || DEPLOY_PCK.SERVER || DEPLOY_PCK.SERVER_ORDS_CONTEXT || 'f?p=101:209:::NO::P209_BANDEJA:7">Tareas Pendientes</a>' || '<br>' || 
                     '<br>Cordialmente,' || '<br>' || 
                     '<br>GESTIÓN DE DESCRIPCIONES DE CARGOS';

            -- Este es el que debe usarse para ENCOLAR y enviarse con el disparador       
            -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    
            enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);

        ELSE
            INSERTAR_LOG('gth.grupoenel@gmail.com',l_correo_destino,to_clob(l_mensaje),l_asunto,SYSDATE,'El trabajador no cuenta con un correo válido para ser notificado. (Correo nulo)');
        END IF;     

    ELSE
        -- 2) De lo contrario, se envía a todos los BPs según la consulta definida
        FOR destino IN ( 
                select email,nombres from trabajador where id in (
                (SELECT Id_trabajador FROM SNW_CARGOS.BUSINESS_PARTNER WHERE id_unidad_organizativa = 
                (select SNW_CARGOS.admin_unidad_organizativa.buscar_unidad (id_unidad_organizativa,SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('LINEA_BP')) 
                from SNW_CARGOS.posicion where id =  
                (select id_posicion from SNW_CARGOS.posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
                (select id from trabajador where NUMERO_IDENTIFICACION = 
                (select numero_identificacion from usuario where user_name = 
                (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo)))))))
        ) LOOP

         -- SI el correo existe, procedemos con el envio
         IF destino.email IS NOT NULL THEN

            l_correo_destino := destino.email;  


            l_mensaje := 'Estimado(a) ' || destino.nombres || '<br>' ||
                     '<br>Ha llegado un formulario de solicitud de ' || l_tipo || ' de una descripción de cargo de su perímetro a su bandeja de <b>BUSINESS PARTNER</b>.' || '<br>' ||

                     '<br><b>FORMULARIO</b>' || '<br>' ||

                     '<br><b>Unidad Organizativa:</b> ' || l_unidad || 
                     '<br><b>Denominación del cargo:</b> ' || l_denominacion || 
                     '<br><b>Elaboró:</b> ' || l_cargo_elaborador || 
                     '<br><b>Fecha de ' || LOWER(l_tipo) || ':</b> ' || sysdate || 
                     '<br><b>Justificación:</b> ' || l_justificacion || '<br>' ||
                     '<br>Para validar y dar su concepto dar clic en <a href="' || DEPLOY_PCK.SERVER || DEPLOY_PCK.SERVER_ORDS_CONTEXT || 'f?p=101:209:::NO::P209_BANDEJA:7">Tareas Pendientes</a>' || '<br>' || 
                     '<br>Cordialmente,' || '<br>' || 
                     '<br>GESTIÓN DE DESCRIPCIONES DE CARGOS';

            -- Este es el que debe usarse para ENCOLAR y enviarse con el disparador       
            -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    
            enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    

        ELSE
            INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario '||l_usuario||' sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'El trabajador no cuenta con un correo válido para ser notificado. (Correo nulo)');
        END IF;      

      END LOOP;

  END IF;

  EXCEPTION 
      WHEN OTHERS THEN
        INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario '||l_usuario||' sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'La información para notificar al BP del cargo para validar no esta completa');    

  END NOTIFICAR_BP_DESDE_GESTOR;

  -------------------------------------------------------------------------------------- 
  PROCEDURE NOTIFICAR_POC_DESDE_BP(i_id_cargo IN NUMBER) AS  

    l_correo_destino        VARCHAR2(4000);
    l_nombres               VARCHAR2(4000);
    l_asunto                VARCHAR2(4000);
    l_mensaje               VARCHAR2(4000);
    l_tipo                  VARCHAR2(4000);    

    l_unidad                VARCHAR2(4000);
    l_denominacion          VARCHAR2(4000);    
    l_comentario_bp         VARCHAR2(4000);
    -- l_fecha                 VARCHAR2(4000);    

    l_count                 NUMBER;
    l_reviso                NUMBER;

    BEGIN
    l_asunto := 'Nuevo formulario en su bandeja de CARGOS'; 
    select count(*) into l_count from SNW_CARGOS.cargo where id = i_id_cargo and id = ID_CARGO_PADRE;

    -- Se trata de un caso de CREACIÓN
    IF l_count > 0 THEN
        l_tipo := '<b>CREACIÓN</b>';
        -- l_fecha := 'Fecha de creación'; ??
    -- Se trata de un caso de ACTUALIZACIÓN
    ELSE 
        l_tipo := '<b>ACTUALIZACIÓN</b>';
    END IF;

    -- Obtenemos la U.O del cargo
    select nombre into l_unidad from SNW_CARGOS.UNIDAD_ORGANIZATIVA where id = (select id_unidad_organizativa from SNW_CARGOS.cargo where id = i_id_cargo);
    -- Obtenemos la Denominación del cargo
    select denominacion into l_denominacion from SNW_CARGOS.cargo where id = i_id_cargo;
    -- Obtenemos los comentarios de BP del cargo
    select comentario into l_comentario_bp from SNW_CARGOS.concepto_Cargo where tipo = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_BP') and id_Cargo = i_id_cargo;

    -- Obtenemos los destinatarios POC

        FOR destino IN ( 
                select email,nombres from trabajador where NUMERO_IDENTIFICACION in 
                (select numero_identificacion from usuario where id in (
                (select usuario from SNW_AUTH.usu_grupo where grupo = 
                (select id from SNW_AUTH.grupo where nombre = 'POC'))))
        ) LOOP

         -- SI el correo existe, procedemos con el envio
         IF destino.email IS NOT NULL THEN

            l_correo_destino := destino.email;  


            l_mensaje := 'Estimado(a) ' || destino.nombres || '<br>' ||
                     '<br>Ha llegado un formulario de solicitud de ' || l_tipo || ' de una descripción de cargo de su perímetro a su bandeja de <b>POC</b>.' || '<br>' ||

                     '<br><b>FORMULARIO</b>' || '<br>' ||

                     '<br><b>Unidad Organizativa:</b> ' || l_unidad || 
                     '<br><b>Denominación del cargo:</b> ' || l_denominacion ||                      
                     '<br><b>Fecha de ' || LOWER(l_tipo) || ':</b> ' || sysdate || 
                     '<br><b>Comentarios de BP:</b> ' || l_comentario_bp || '<br>' ||
                     '<br>Para validar y dar su concepto dar clic en <a href="' || DEPLOY_PCK.SERVER || DEPLOY_PCK.SERVER_ORDS_CONTEXT || 'f?p=101:209:::NO::P209_BANDEJA:8">Tareas Pendientes</a>' || '<br>' || 
                     '<br>Cordialmente,' || '<br>' || 
                     '<br>GESTIÓN DE DESCRIPCIONES DE CARGOS';

            -- Este es el que debe usarse para ENCOLAR y enviarse con el disparador       
            -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    
            enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    

        ELSE
            INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'El trabajador no cuenta con un correo válido para ser notificado. (Correo nulo)');
        END IF;      

      END LOOP;

  EXCEPTION 
      WHEN OTHERS THEN
        INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'La información para notificar a los POC del cargo para aprobar no esta completa');    


  END NOTIFICAR_POC_DESDE_BP;

  -------------------------------------------------------------------------------------- 
  PROCEDURE NOTIFICAR_BP_DESDE_POC(i_id_cargo IN NUMBER) AS  

    l_correo_destino        VARCHAR2(4000);
    l_nombres               VARCHAR2(4000);
    l_asunto                VARCHAR2(4000);
    l_mensaje               VARCHAR2(4000);
    l_tipo                  VARCHAR2(4000);    

    l_unidad                VARCHAR2(4000);
    l_denominacion          VARCHAR2(4000);    
    l_cargo_elaborador      VARCHAR2(4000);
    l_comentario_poc        VARCHAR2(4000);
    l_usuario               VARCHAR2(4000);
    -- l_fecha                 VARCHAR2(4000);    

    l_count                 NUMBER;
    l_reviso                NUMBER;

    BEGIN
    l_asunto := 'Nuevo formulario en su bandeja de CARGOS'; 
    select count(*) into l_count from SNW_CARGOS.cargo where id = i_id_cargo and id = ID_CARGO_PADRE;

    -- Se trata de un caso de CREACIÓN
    IF l_count > 0 THEN
        l_tipo := '<b>CREACIÓN</b>';
        -- l_fecha := 'Fecha de creación'; ??
    -- Se trata de un caso de ACTUALIZACIÓN
    ELSE 
        l_tipo := '<b>ACTUALIZACIÓN</b>';
    END IF;

    -- Obtenemos la U.O del cargo
    select nombre into l_unidad from SNW_CARGOS.UNIDAD_ORGANIZATIVA where id = (select id_unidad_organizativa from SNW_CARGOS.cargo where id = i_id_cargo);
    -- Obtenemos la Denominación del cargo
    select denominacion into l_denominacion from SNW_CARGOS.cargo where id = i_id_cargo;
    -- Obtenemos la Denominación del cargo que elaboró el cargo enviado a la bandeja BP
    select elaboro into l_usuario from SNW_CARGOS.cargo where id = i_id_cargo;
    l_cargo_elaborador := SNW_CARGOS.FORMULARIO.DENOMINACION_DE_USUARIO(l_usuario);

    -- Obtenemos los comentarios de POC del cargo
    select comentario into l_comentario_poc from SNW_CARGOS.concepto_Cargo where tipo = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_POC') and id_Cargo = i_id_cargo;

    -- Obtenemos el destinatario BP que reviso        
    select count(*) into l_count from trabajador where email is not null and NUMERO_IDENTIFICACION = 
    (select numero_identificacion from usuario where user_name = 
    (select reviso from SNW_CARGOS.cargo where id = i_id_cargo));

    -- SI el correo existe, procedemos con el envio
    IF l_count > 0 THEN

        -- Obtenemos el email del que revisó   
        select email into l_correo_destino from trabajador where NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where user_name = 
        (select reviso from SNW_CARGOS.cargo where id = i_id_cargo));

        -- Obtenemos los nombres del que revisó
        select nombres into l_nombres from trabajador where NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where user_name = 
        (select reviso from SNW_CARGOS.cargo where id = i_id_cargo));


        l_mensaje := 'Estimado(a) ' || l_nombres || '<br>' ||
                 '<br>Se ha regresado un formulario de solicitud de ' || l_tipo || ' de una descripción de cargo de su perímetro a su bandeja de <b>BUSINESS PARTNER</b>.' || '<br>' ||

                 '<br><b>FORMULARIO</b>' || '<br>' ||

                 '<br><b>Unidad Organizativa:</b> ' || l_unidad || 
                 '<br><b>Denominación del cargo:</b> ' || l_denominacion || 
                 '<br><b>Elaboró:</b> ' || l_cargo_elaborador || 
                 '<br><b>Fecha de ' || LOWER(l_tipo) || ':</b> ' || sysdate || 
                 '<br><b>Comentarios de POC:</b> ' || l_comentario_poc || '<br>' ||
                 '<br>Para validar y dar su concepto dar clic en <a href="' || DEPLOY_PCK.SERVER || DEPLOY_PCK.SERVER_ORDS_CONTEXT || 'f?p=101:209:::NO::P209_BANDEJA:7">Tareas Pendientes</a>' || '<br>' || 
                 '<br>Cordialmente,' || '<br>' || 
                 '<br>GESTIÓN DE DESCRIPCIONES DE CARGOS';

        -- Este es el que debe usarse para ENCOLAR y enviarse con el disparador       
        -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    
        enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);


    ELSE
        INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario '||l_usuario||' sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'El trabajador no cuenta con un correo válido para ser notificado. (Correo nulo)');
    END IF;     



  EXCEPTION 
      WHEN OTHERS THEN
        INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario '||l_usuario||' sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'La información para notificar al BP del cargo para validar no esta completa');    


  END NOTIFICAR_BP_DESDE_POC;

  -------------------------------------------------------------------------------------- 
  PROCEDURE NOTIFICAR_GESTOR_DESDE_BP(i_id_cargo IN NUMBER) AS  

    l_correo_destino        VARCHAR2(4000);
    l_nombres               VARCHAR2(4000);
    l_asunto                VARCHAR2(4000);
    l_mensaje               VARCHAR2(4000);
    l_tipo                  VARCHAR2(4000);    

    l_unidad                VARCHAR2(4000);
    l_denominacion          VARCHAR2(4000);    
    l_comentario_poc        VARCHAR2(4000);
    -- l_fecha                 VARCHAR2(4000);    

    l_count                 NUMBER;
    l_reviso                NUMBER;

    BEGIN
    l_asunto := 'Nuevo formulario en su bandeja de CARGOS'; 
    select count(*) into l_count from SNW_CARGOS.cargo where id = i_id_cargo and id = ID_CARGO_PADRE;

    -- Se trata de un caso de CREACIÓN
    IF l_count > 0 THEN
        l_tipo := '<b>CREACIÓN</b>';
        -- l_fecha := 'Fecha de creación'; ??
    -- Se trata de un caso de ACTUALIZACIÓN
    ELSE 
        l_tipo := '<b>ACTUALIZACIÓN</b>';
    END IF;

    -- Obtenemos la U.O del cargo
    select nombre into l_unidad from SNW_CARGOS.UNIDAD_ORGANIZATIVA where id = (select id_unidad_organizativa from SNW_CARGOS.cargo where id = i_id_cargo);
    -- Obtenemos la Denominación del cargo
    select denominacion into l_denominacion from SNW_CARGOS.cargo where id = i_id_cargo;
    -- Obtenemos los comentarios de BP del cargo
    select comentario into l_comentario_poc from SNW_CARGOS.concepto_Cargo where tipo = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_BP') and id_Cargo = i_id_cargo;

    -- Obtenemos el destinatario Gestor/BP que elaboró el cargo
    select count(*) into l_count from trabajador where email is not null and NUMERO_IDENTIFICACION = 
    (select numero_identificacion from usuario where user_name = 
    (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));

    -- SI el correo existe, procedemos con el envio
    IF l_count > 0 THEN

        -- Obtenemos el email del que elaboro   
        select email into l_correo_destino from trabajador where NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where user_name = 
        (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));

        -- Obtenemos los nombres del que elaboro
        select nombres into l_nombres from trabajador where NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where user_name = 
        (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));


        l_mensaje := 'Estimado(a) ' || l_nombres || '<br>' ||
                 '<br>Se ha regresado un formulario de solicitud de ' || l_tipo || ' de una descripción de cargo de su perímetro a su bandeja de <b>GESTOR</b>.' || '<br>' ||

                 '<br><b>FORMULARIO</b>' || '<br>' ||

                 '<br><b>Unidad Organizativa:</b> ' || l_unidad || 
                 '<br><b>Denominación del cargo:</b> ' || l_denominacion ||                  
                 '<br><b>Fecha de ' || LOWER(l_tipo) || ':</b> ' || sysdate || 
                 '<br><b>Comentarios de BP:</b> ' || l_comentario_poc || '<br>' ||
                 '<br>Para validar y dar su concepto dar clic en <a href="' || DEPLOY_PCK.SERVER || DEPLOY_PCK.SERVER_ORDS_CONTEXT || 'f?p=101:209:::NO::P209_BANDEJA:6">Tareas Pendientes</a>' || '<br>' || 
                 '<br>Cordialmente,' || '<br>' || 
                 '<br>GESTIÓN DE DESCRIPCIONES DE CARGOS';

        -- Este es el que debe usarse para ENCOLAR y enviarse con el disparador       
        -- enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);    
        enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);

    ELSE
        INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'El trabajador no cuenta con un correo válido para ser notificado. (Correo nulo)');
    END IF;     



  EXCEPTION 
      WHEN OTHERS THEN
        INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'La información para notificar al Gestor del cargo para corregir no esta completa');    


  END NOTIFICAR_GESTOR_DESDE_BP;
  
  -------------------------------------------------------------------------------------- 
  PROCEDURE NOTIFICAR_ACEPTACION_DC(i_id_cargo IN NUMBER) AS  

    l_correo_destino        VARCHAR2(4000);
    l_nombres               VARCHAR2(4000);
    l_asunto                VARCHAR2(4000);
    l_mensaje               VARCHAR2(4000);
    l_tipo                  VARCHAR2(4000);    

    l_unidad                VARCHAR2(4000);
    l_denominacion          VARCHAR2(4000);    
    l_comentario_poc         VARCHAR2(4000);
    -- l_fecha                 VARCHAR2(4000);    

    l_count                 NUMBER;
    l_reviso                NUMBER;

    BEGIN
        
        l_asunto := 'Una descripción de cargo creada por usted ha sido aceptada';         
        
        -- El destinatario es quien ELABORO la D.C
        select email,nombres into l_correo_destino,l_nombres from trabajador where numero_identificacion = 
            (select numero_identificacion from usuario where user_name = 
            (select elaboro from SNW_CARGOS.cargo where id = i_id_cargo));
            
        -- Obtenemos la U.O del cargo
        select nombre into l_unidad from SNW_CARGOS.UNIDAD_ORGANIZATIVA where id = (select id_unidad_organizativa from SNW_CARGOS.cargo where id = i_id_cargo);
        -- Obtenemos la Denominación del cargo
        select denominacion into l_denominacion from SNW_CARGOS.cargo where id = i_id_cargo;
        -- Obtenemos los comentarios de POC del cargo
        select comentario into l_comentario_poc from SNW_CARGOS.concepto_Cargo where tipo = SNW_CARGOS.SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_POC') and id_Cargo = i_id_cargo;
            
            
        l_mensaje := 'Estimado(a) ' || l_nombres || '<br>' ||
                         '<br>Una descripción de cargo creada por usted ha sido aceptada por POC,
                         a continuación se presenta la información de esta descripción de cargo:' || '<br>' ||
                             
                         '<br><b>Unidad Organizativa:</b> ' || l_unidad || 
                         '<br><b>Denominación del cargo:</b> ' || l_denominacion ||                      
                         '<br><b>Fecha de aceptación POC:</b> ' || sysdate || 
                         '<br><b>Comentarios de POC:</b> ' || l_comentario_poc || '<br>' ||
                         
                         '<br>Puede consultarla en el módulo de consulta de descripciones de cargo haciendo click ' ||
                         '<a href="' || codensa_gth.DEPLOY_PCK.OBTENER_SERVER || codensa_gth.DEPLOY_PCK.SERVER_ORDS_CONTEXT || APEX_UTIL.PREPARE_URL('f?p='||'101'||':230:'||V('SESSION')||'::NO::P230_CARGO:'|| i_id_cargo) || '">Aquí</a> ' || 
                         'y luego en el menú <b>Todas las descripciones de cargo.</b><br>' ||                        
                         '<br>Cordialmente,' || '<br>' || 
                         '<br>GESTIÓN DE DESCRIPCIONES DE CARGOS';
        
         enviar_Corrreo(l_correo_destino,l_asunto,l_mensaje);               
    
      EXCEPTION 
          WHEN OTHERS THEN
            INSERTAR_LOG('gth.grupoenel@gmail.com',nvl(l_correo_destino,'Usuario sin correo'),to_clob(l_mensaje),l_asunto,SYSDATE,'La información para notificar la aceptación del cargo no esta completa');    


  END NOTIFICAR_ACEPTACION_DC;

-------------------------------------------------------------------------------------- 

  PROCEDURE ENVIAR_CORRREO( I_DESTINATARIO IN VARCHAR,
                            I_ASUNTO IN VARCHAR,
                            I_CUERPO IN VARCHAR) AS
    BEGIN
        insert into correo_cola (destinatario,asunto,cuerpo,fecha_ingreso,enviado) values(I_DESTINATARIO,I_ASUNTO,I_CUERPO,sysdate,'N');
        exception when others then
            INSERTAR_LOG('gth.grupoenel@gmail.com',I_DESTINATARIO,to_clob(I_CUERPO),I_ASUNTO,SYSDATE,SQLERRM);
    END ENVIAR_CORRREO;

-------------------------------------------------------------------------------------- 
    PROCEDURE DISPARADOR_CORREO AS
    BEGIN
        if l_enviados_diario < 500 then
            for i in (select * from correo_cola where enviado = 'N' order by id asc) loop
                ENVIAR_CORRREO(i.destinatario,i.asunto,i.cuerpo,sysdate);
                update correo_cola set fecha_salida=sysdate, enviado='S' where id = i.id;
                l_enviados_diario := l_enviados_diario + 1;
            end loop;
        end if;
        if l_dia_vigente+1 > sysdate then
            l_dia_vigente := sysdate;
        end if;
    END DISPARADOR_CORREO;

PROCEDURE Alertar_PEC_Sin_Evaluar(i_pec IN PEC%ROWTYPE) AS
  l_destinatario  TRABAJADOR%ROWTYPE;
  l_mensaje       VARCHAR2(4000);
  l_asunto        VARCHAR2(4000);
BEGIN
  SELECT * into l_destinatario
    from TRABAJADOR
    where numero_identificacion = i_pec.id_trabajador;

   IF l_destinatario.email IS NOT NULL THEN
      l_mensaje := 'Estimado usuario, '||l_destinatario.nombres||' '||l_destinatario.apellidos||'<br><br>'||
                 'Por favor ingrese al sistema Performance Management, para evaluar el Plan de Entrenamiento en el Cargo
                 que realizó recientemente.<br><br>'||
                 'Recuerde que para la empresa es muy importante conocer su percepción sobre el mismo.<br><br>
                 Para ir a la aplicación y evaluar el PEC, puede dar clic en el siguiente enlace: '||
                 '<a href="' || codensa_gth.DEPLOY_PCK.OBTENER_SERVER || codensa_gth.DEPLOY_PCK.SERVER_ORDS_CONTEXT || APEX_UTIL.PREPARE_URL('f?p='||'101'||':198:'||V('SESSION')||'::NO::P198_ID_PEC:'|| i_pec.id) || '">Ir a evaluar el PEC</a>' ||
                 '<br><br>' ||
                 'Gracias de antemano por su colaboración,<br><br>
                 División de Formación y Desarrollo';
      l_asunto := 'Urgente - Evaluación del Plan de Entrenamiento en el Cargo PEC';
      Enviar_Corrreo(l_destinatario.email, l_asunto, l_mensaje);    
   END IF;      

EXCEPTION 
      WHEN OTHERS THEN
        Raise_Application_Error(-20000,'La notificación de alerta por PEC sin evaluar tuvo un error inesperado. Por favor comuníquese con el administrador.');

END Alertar_PEC_Sin_Evaluar;

END NOTIFICACION_PCK;
/