CREATE OR REPLACE PACKAGE BODY codensa_gth."ADMIN_ASISTENCIA_PCK" AS

--------------------------------------------
FUNCTION ASISTENCIAS_SESIONES (i_id_pdi IN NUMBER) RETURN NUMBER IS
    l_conteo number;
    l_grupo number;
BEGIN
    SELECT ID_PDI_GRUPO INTO l_grupo FROM BARS_PDI WHERE ID=i_id_pdi;
    SELECT COUNT(*) INTO l_conteo FROM PDI_ASISTENCIA 
    LEFT JOIN PDI_SESION ON PDI_ASISTENCIA.ID_PDI_SESION=PDI_SESION.ID
    WHERE PDI_SESION.ID_PDI_GRUPO=l_grupo AND 
    PDI_ASISTENCIA.ID_BARS_PDI=i_id_pdi AND 
    PDI_ASISTENCIA.ASISTIO='S';
    RETURN l_conteo;    
END ASISTENCIAS_SESIONES;
--------------------------------------------
PROCEDURE CREAR_ASISTENCIA(i_id_pdi_sesion in number) is
    l_conteo number;
begin
    for i in (select * 
                from v_inscritos 
                where id_pdi_grupo = (select id_pdi_grupo from pdi_sesion where id = i_id_pdi_sesion)) loop
        select count(*) into l_conteo from pdi_asistencia where id_pdi_sesion = i_id_pdi_sesion and id_bars_pdi = i.id;
        if l_conteo = 0 then
            insert into pdi_asistencia (id_pdi_sesion,id_bars_pdi) values(i_id_pdi_sesion,i.id);
        end if;
    end loop;
end ;
--------------------------------------------
PROCEDURE DEFINIR_ASISTENCIA(i_id_bars_pdi in number,i_id_pdi_sesion in number,i_asistio in varchar2) IS
    l_conteo number;
begin
    select count(*) into l_conteo 
    from pdi_asistencia 
    where id_pdi_sesion = i_id_pdi_sesion and id_bars_pdi = i_id_bars_pdi;
    if l_conteo = 1 then
        update pdi_asistencia set asistio=i_asistio
        where id_pdi_sesion = i_id_pdi_sesion and id_bars_pdi = i_id_bars_pdi;
    end if;
end ;
--------------------------------------------

END ADMIN_ASISTENCIA_PCK;

/