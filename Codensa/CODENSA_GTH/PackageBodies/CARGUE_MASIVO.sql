CREATE OR REPLACE PACKAGE BODY codensa_gth."CARGUE_MASIVO" AS

function  procesar_cargue_masivo return varchar2 is
    
l_errors NUMBER;
l_total NUMBER;
l_m_errors VARCHAR2(30000);

begin
l_m_errors :='';
l_errors :=0;
l_total :=0;
  for i in (select distinct evaluado, periodo from cargue_concer_obj where persistido in ('N'))
  loop
  declare 
  l_formevaobj bars_formevaobj%rowtype;
  begin
      l_total :=l_total +1;
  --Determinar el formulario
    begin
      select * into l_formevaobj from bars_formevaobj where evaluado=i.evaluado and periodo=i.periodo;
    exception
      when no_data_found then
        declare
        l_trabajador trabajador%rowtype;
        l_bars_formevaobj_id bars_formevaobj.id%type;
        begin
          select * into l_trabajador from trabajador where numero_identificacion=i.evaluado;
          DETERMINAR_FORMULARIO_OBJ(i.evaluado, i.periodo, l_trabajador.id_cargo, l_bars_formevaobj_id);
          if (l_bars_formevaobj_id is null) then --El formulario no existe o no se pudo crear
            raise_application_error(-20001,'No se pudo identificar o crear el formulario para el trabajador: '||i.evaluado);
          end if;
          select * into l_formevaobj from bars_formevaobj where id=l_bars_formevaobj_id;
        exception
          when no_data_found then
          raise_application_error(-20000,'No existe el trabajador con numero_identificacion: '||i.evaluado);
        end;
     end;
  --Borrar Objetivos Cerrados actuales
  delete bars_objetivo where formevaobj=l_formevaobj.id and id_tipo=1011;
  --Procesar objetivos cerrados del evaluado en el periodo
  for j in (select * from cargue_concer_obj where persistido in ('N') and evaluado=i.evaluado and periodo=i.periodo)
  loop
  declare 
  l_objetivo bars_objetivo%rowtype;
    begin
      l_objetivo.objind:='Obj cerrado';
      l_objetivo.meta:='Obj cerrado';
      l_objetivo.ponderacion:=j.ponderacion;
      l_objetivo.formevaobj:=l_formevaobj.id;
      l_objetivo.id_tipo:=1011;
      select id, resultado into l_objetivo.id_objetivo_cerrado, l_objetivo.pctconsind
        from objetivo_cerrado where codigo=j.codigo_objetivo;
      begin
        select id into l_objetivo.id_tipo_curva from tipo where tipo=11 and trim(upper(nombre)) like (select trim(upper(tipo_curva)) from objetivo_cerrado where id=l_objetivo.id_objetivo_cerrado);
      exception
        when no_data_found then
          null;
      end;
      insert into bars_objetivo values l_objetivo;
      update cargue_concer_obj set persistido='S' where id=j.id;
  end;
  end loop;
  --intentar VALIDAR RESULTADO O INCLUSO FINALIZARLO del formulario del loop
  DECLARE
    l_msj VARCHAR2(4000) := '';
  BEGIN
    BARS_SUBMITOBJ(pitem=>'LISTA_OBJETIVOS_VALIDAR_RESULTADO', pformulario=>l_formevaobj.id, pmsj=>l_msj);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END;
--Finalización intento validar el resultado del formulario del loop
exception
when others then
l_errors :=l_errors +1;
l_m_errors := l_m_errors ||chr(10)||SUBSTR(SQLERRM,1,10);
end;
end loop;

return l_errors ||' fallidos de '||l_total ||chr(10)||'. <-> Errores:'||chr(10)||nvl(l_m_errors,' ninguno ') ;

end procesar_cargue_masivo;

--------------------------------------------

PROCEDURE CREAR_NO_PDI_MASIVO(I_ID_GRUPO IN NUMBER, I_RESPONSABLE IN NUMBER) IS

L_ENCABEZADO NUMBER := 0;
L_ID_ACCION NUMBER;
BEGIN
    FOR NO_PDI IN (SELECT * FROM TEMP_CARGUE_NO_PDI)  
    LOOP

      L_ENCABEZADO := ADMINISTRACION_PDI_PCK.CREAR_ENCABEZADO(NO_PDI.PERIODO, NO_PDI.ID_GESTOR, NO_PDI.ID_COLABORADOR);

      --apex_debug.message('DEBUG MSG = '||L_ENCABEZADO);
      IF (L_ENCABEZADO != 0) THEN
        BEGIN
          L_ID_ACCION  := ADMINISTRACION_PDI_PCK.CREAR_PDI_BASICO (SNW_CONSTANTES.CONSTANTE_TIPO('FORMACION'), L_ENCABEZADO, 30, NO_PDI.JUSTIFICACION, I_ID_GRUPO); -- 30 => ESTADO: Aprobado por RH
        EXCEPTION WHEN OTHERS
          THEN raise_application_error(-20000, 'Se ha generado un error creando la acción, con los siguientes datos: Periodo:'||NO_PDI.PERIODO||', Gestor:'||NO_PDI.ID_GESTOR||', Colaborador:'||NO_PDI.ID_COLABORADOR||', Grupo:'||NO_PDI.ID_COLABORADOR);
        END;
      ELSE
        raise_application_error(-20000, 'Se ha generado un error creando el encabezado, con los siguientes datos: Periodo:'||NO_PDI.PERIODO||', Gestor:'||NO_PDI.ID_GESTOR||', Colaborador:'||NO_PDI.ID_COLABORADOR||', Grupo:'||NO_PDI.ID_COLABORADOR);
      END IF;

      BEGIN
        ADMINISTRACION_PDI_PCK.CAMBIAR_CONCEPTO_PDI(L_ID_ACCION, 30,'PDI creado automáticamente por cargue NO PDI de administrador',I_RESPONSABLE); -- 30 => ESTADO: Aprobado por RH
      EXCEPTION WHEN OTHERS
        THEN raise_application_error(-20000, 'Se ha generado un error cambiando el concepto con los siguientes datos: Responsable='||I_RESPONSABLE||', Acción:'||L_ID_ACCION );
      END;

    END LOOP NO_PDI;

END CREAR_NO_PDI_MASIVO; 

---------------------------------------------------------------------------------------------------------
function  cargue_plantilla_trabajador(i_nombre varchar2) return number is
l_id_tipo number;
begin
    SELECT ID INTO l_id_tipo
    FROM TIPO
    WHERE NOMBRE = i_nombre;
    return l_id_tipo;
end cargue_plantilla_trabajador;

-------------------------------------------------------------------------------

PROCEDURE VALIDACION_EVALUADO_TARGET(I_TARGET IN NUMBER) IS

CURSOR C_EVALUADOS IS SELECT EVALUADO FROM FORMULARIO_BARS;
CURSOR C_TRABAJDORES IS SELECT trabajador.NUMERO_IDENTIFICACION from trabajador where trabajador.ID_TARGET_OPR<>I_TARGET;

EVALUADOS_BCR C_EVALUADOS%ROWTYPE;
TARGETES_BCR C_TRABAJDORES%ROWTYPE;

BEGIN 
    FOR EVALUADOS_BCR IN C_EVALUADOS LOOP
        FOR TARGETES_BCR IN C_TRABAJDORES LOOP
            IF EVALUADOS_BCR.EVALUADO = TARGETES_BCR.NUMERO_IDENTIFICACION THEN
                DELETE FROM COMENTARIO_BARS WHERE FORMULARIO IN (SELECT formulario_bars.ID FROM formulario_bars 
                WHERE formulario_bars.EVALUADO = EVALUADOS_BCR.EVALUADO);
                DELETE FROM FORMULARIO_BARS WHERE formulario_bars.EVALUADO = EVALUADOS_BCR.EVALUADO;  
            END IF;
        END LOOP;
    END LOOP;
END VALIDACION_EVALUADO_TARGET;

----------------------------------------------------------------------
PROCEDURE MIGRAR_DATOS_FORMULARIO is
CURSOR C_EVALUADOS IS SELECT * FROM temp_evaluacion_comportamiento;

EVALUADOS_BCR C_EVALUADOS%ROWTYPE;

BEGIN 
    FOR EVALUADOS_BCR IN C_EVALUADOS LOOP
        insert into formulario_bars(plantilla,evaluador,evaluado,periodo,estado,PCT_CONSECUCION,tipo)
        values ( null,( EVALUADOS_BCR.EVALUADOR ),( EVALUADOS_BCR.evaluado ),EVALUADOS_BCR.periodo,5,
      (EVALUADOS_BCR.pct_consecucion),1163);
    END LOOP;
end MIGRAR_DATOS_FORMULARIO;
------------------------------------------------------------------------------------------------
END CARGUE_MASIVO;
/