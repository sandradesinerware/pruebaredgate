CREATE OR REPLACE PACKAGE BODY codensa_gth."ADMIN_PEC_PCK" AS
--------------------------------------------
PROCEDURE CREAR_PEC_PREDETERMINADO(i_id_trabajador NUMBER, I_FECHA_INGRESO DATE, i_motivo_ingreso NUMBER) AS

  l_gestor      TRABAJADOR.numero_identificacion%type;
  l_id_pec      TRABAJADOR.numero_identificacion%type;
  l_pec         PEC%ROWTYPE;
BEGIN  

-- SE QUITA LA CONDICION DE QUE EL INICIO DE LABORES DEL TRABAJADOR SEA EL DIA 01 DE DICIEMBRE DE 2018, O POSTERIOR
  --IF (I_FECHA_INGRESO >= to_date('2018-12-01','YYYY-MM-DD')) THEN
            
  SELECT jefe into l_gestor
    from TRABAJADOR where numero_identificacion = i_id_trabajador;
    
  --CREANDO EL PEC--
  INSERT INTO PEC (ID_TRABAJADOR, ID_GESTOR, FECHA_CREACION, MOTIVO_CREACION) 
  VALUES (i_id_trabajador, l_gestor, I_FECHA_INGRESO, i_motivo_ingreso)
  RETURNING ID INTO l_id_pec;
  
  SELECT * into l_pec from PEC where id = l_id_pec;

  --CREANDO EL ESTADO PEC--  
  Definir_Estado_PEC( l_id_pec, snw_constantes.constante_tipo('PEC_POR_PLANEAR'), SYSDATE ); 

  --INSERTANDO ACTIVIDADES PREDETERMINADAS--
  Insertar_Actividades_Pred ( l_id_pec );

  -- SE GENERA LA NOTIFICACIÓN AL GESTOR
  NOTIFICACION_PCK.Alertar_PEC_No_Planeado( l_pec );

END;
--------------------------------------------
PROCEDURE Definir_Estado_PEC( i_id_PEC NUMBER, 
                              i_id_PEC_estado NUMBER, 
                              i_fecha DATE ) 
AS
l_estado_pec        NUMBER;
l_trab_responsable  NUMBER;

BEGIN
  
  --El responsable depende del estado. Si va a evaluado, es el trabajador, de lo contrario el gestor
  if ( i_id_pec_estado = SNW_CONSTANTES.constante_tipo('PEC_EVALUADO') ) then
   SELECT id_trabajador INTO l_trab_responsable FROM PEC WHERE id = i_id_PEC;
  else
   SELECT id_gestor INTO l_trab_responsable FROM PEC WHERE id = i_id_PEC; 
  end if;
  
--CREANDO EL ESTADO PEC--  
  INSERT
  INTO pec_estado (id_PEC, id_tipo_estado, fecha, id_trabajador_responsable) 
  VALUES (i_id_PEC, i_id_PEC_estado, i_fecha, l_trab_responsable) 
  RETURNING id_tipo_estado INTO l_estado_pec;

--ACTUALIZANDO EL ESTADO EN TABLA PEC--    
  UPDATE pec SET id_estado = l_estado_pec WHERE pec.id = i_id_PEC;  


END Definir_Estado_PEC;
--------------------------------------------
PROCEDURE ELIMINAR_DUPLICADOS
AS
l_minimo NUMBER;
BEGIN

  -- Se eliminan duplicados de PEC_ESTADO
  for i in (select count(*), ID_TIPO_ESTADO, id_pec from pec_estado group by ID_TIPO_ESTADO, id_pec having count(*) > 1)
  loop
  
    -- Se preserva el primer registro que se guardó
    select min(id) into l_minimo from pec_estado where id_pec = i.id_pec and ID_TIPO_ESTADO = i.id_tipo_estado;
        
    -- Y se elimina el resto que son duplicados.
    delete pec_estado where id_pec = i.id_pec and ID_TIPO_ESTADO = i.id_tipo_estado and id <> l_minimo;    
    
  end loop;
   
  -- Se eliminan duplicados de PEC_EVALUACION
  for i in (select count(*), ID_pec from pec_evaluacion group by id_pec having count(*) > 1)
  loop
  
    -- Se preserva el primer registro que se guardó
    select min(id) into l_minimo from pec_evaluacion where id_pec = i.id_pec;
    
    -- Se eliminan las respuestas sobrantes
    delete pec_evaluacion_respuestas where id_evaluacion in (select id from pec_evaluacion where id_pec = i.id_pec and id <> l_minimo); 
        
    -- Y se elimina el resto que son duplicados.
    delete pec_evaluacion where id_pec = i.id_pec and id <> l_minimo;    
    
  end loop;


END ELIMINAR_DUPLICADOS;
--------------------------------------------
PROCEDURE Insertar_Actividades_Pred ( i_id_PEC NUMBER) AS
l_intensidad NUMBER;
BEGIN
  --Determinar por diferencia de conjuntos cuales textos faltan
  FOR L_ACTIVIDAD_PRED IN (SELECT * FROM PEC_ACTIVIDAD_PREDETERMINADA WHERE ESTADO_ACTIVO = 'A' and proceso not in
                            (select proceso from pec_actividad where id_pec = i_id_PEC and predeterminada = 'S')
                           )
  LOOP
    INSERT
    INTO PEC_ACTIVIDAD (FECHA, INTENSIDAD, PROCESO, CONTENIDO, TIPO_ENTRENAMIENTO,
                        RESPONSABLE, LUGAR, ID_PEC, PREDETERMINADA, REQUIERE_EVIDENCIA)
    VALUES ( SYSDATE, L_ACTIVIDAD_PRED.INTENSIDAD, L_ACTIVIDAD_PRED.PROCESO, 
             L_ACTIVIDAD_PRED.CONTENIDO, L_ACTIVIDAD_PRED.TIPO_ENTRENAMIENTO, 
             L_ACTIVIDAD_PRED.RESPONSABLE, L_ACTIVIDAD_PRED.LUGAR, i_id_PEC, 
             L_ACTIVIDAD_PRED.PREDETERMINADA, L_ACTIVIDAD_PRED.REQUIERE_EVIDENCIA );
  END LOOP;

  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'No fue posible insertar actividades predeterminadas');

END Insertar_Actividades_Pred;
--------------------------------------------
PROCEDURE Crear_Evaluacion_PEC (i_id_PEC NUMBER) AS
l_pec           PEC%ROWTYPE;
l_id_evaluacion PEC_EVALUACION.ID%TYPE;
l_preguntas     PEC_PREGUNTAS%ROWTYPE;
BEGIN

  SELECT * INTO l_pec FROM PEC WHERE id = i_id_PEC;

  INSERT INTO pec_evaluacion 
    ( id_pec, id_trabajador_evaluador, id_gestor_evaluado ) 
  VALUES 
    (l_pec.id, l_pec.id_trabajador, l_pec.id_gestor) 
  RETURNING id INTO l_id_evaluacion;

  FOR pregunta IN ( SELECT * FROM pec_preguntas WHERE estado LIKE 'A')
  LOOP

    INSERT INTO pec_evaluacion_respuestas
      ( id_evaluacion, id_pregunta ) 
    VALUES 
      ( l_id_evaluacion, pregunta.id );

  END LOOP;

  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'No fue posible crear la evaluación del pec actual');

END Crear_Evaluacion_PEC;
--------------------------------------------
PROCEDURE Evaluar_pregunta (i_id_evaluacion NUMBER, i_id_pregunta NUMBER, i_calificacion NUMBER) AS
BEGIN
  UPDATE pec_evaluacion_respuestas 
  SET calificacion_pregunta = i_calificacion 
  WHERE id_evaluacion = i_id_evaluacion 
    AND id_pregunta = i_id_pregunta;
END Evaluar_pregunta;
--------------------------------------------
PROCEDURE Calcular_Promedio_Evaluacion (i_id_evaluacion NUMBER) AS
l_promedio_evaluacion NUMBER;
BEGIN

  SELECT (SUM(calificacion_pregunta)/5) INTO l_promedio_evaluacion FROM pec_evaluacion_respuestas WHERE id_evaluacion = i_id_evaluacion;
  UPDATE pec_evaluacion SET promedio_evaluacion = l_promedio_evaluacion WHERE id = i_id_evaluacion;

  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-2000, 'No ha sido posible calcular el promedio de la evaluación');

END Calcular_Promedio_Evaluacion;
--------------------------------------------

FUNCTION Validar_Actividades_Pred( i_id_PEC NUMBER ) RETURN BOOLEAN AS
l_actividad_pred            number := 0;
l_actividad_pred_existente  number := 0;
l_fecha_creacion            DATE;
BEGIN
  --Solo aplica para PECs de 2020 en adelante (y así no afectar históricos abandonados)
  SELECT fecha_creacion into l_fecha_creacion
    from V_PEC_BASICO
    where id = i_id_PEC;
  if ( l_fecha_creacion < to_date('01/01/2020','dd/mm/yyyy') ) then
    RETURN TRUE;
  
  else 
    --Se valida con base en el número de actividades predeterminadas (en teoría no se pueden editar)
    SELECT COUNT(*) INTO l_actividad_pred_existente
      FROM pec_actividad 
      WHERE id_pec = i_id_PEC AND predeterminada = 'S';

    SELECT COUNT(*) INTO l_actividad_pred
      FROM pec_actividad_predeterminada
      WHERE predeterminada = 'S' and estado_activo = 'A';

    RETURN l_actividad_pred_existente = l_actividad_pred;
  end if;

END Validar_Actividades_Pred;
--------------------------------------------
FUNCTION Validar_Actividad_Adicional( i_id_PEC NUMBER ) RETURN BOOLEAN AS
l_actividades_no_pred   NUMBER := 0;
BEGIN
  select count(*) into l_actividades_no_pred
    from PEC_ACTIVIDAD
    where id_pec = i_id_PEC and predeterminada = 'N';
  RETURN l_actividades_no_pred > 0;
  
END Validar_Actividad_Adicional;
---------------------------------------------------
FUNCTION Actividad_Requiere_Evidencia( i_id_actividad NUMBER ) RETURN NUMBER AS
    l_requiere_evidencia  number;
BEGIN

    SELECT COUNT(*) INTO l_requiere_evidencia 
    FROM pec_actividad
    WHERE pec_actividad.id = i_id_actividad
        AND pec_actividad.requiere_evidencia = 'S';

  RETURN l_requiere_evidencia;

  EXCEPTION WHEN OTHERS THEN
    RETURN 99999;

END Actividad_Requiere_Evidencia;
--------------------------------------------
FUNCTION Numero_Actividades_PEC( i_id_PEC NUMBER ) RETURN NUMBER AS
    l_actividades  number;
BEGIN

  SELECT COUNT(*) 
  INTO l_actividades 
  FROM pec_actividad
  WHERE pec_actividad.id_pec = i_id_pec;

  RETURN l_actividades;

  EXCEPTION WHEN OTHERS THEN
    RETURN 99999;

END Numero_Actividades_PEC;
--------------------------------------------
FUNCTION Validar_Evidencias_Act( i_id_PEC NUMBER ) RETURN BOOLEAN AS
l_act_sin_evidencia NUMBER;

BEGIN
  select count(*) into l_act_sin_evidencia
  from PEC_ACTIVIDAD a left join PEC_EVIDENCIA e on a.id = e.id_actividad 
  where a.id_pec = i_id_PEC and a.requiere_evidencia = 'S' and e.id is null;
        
  RETURN l_act_sin_evidencia = 0;

END Validar_Evidencias_Act;
--------------------------------------------
FUNCTION permite_crear_pec( I_ID_TRABAJADOR NUMBER ) RETURN VARCHAR AS
l_conteo NUMBER;

BEGIN
  select count(*) into l_conteo
  from pec where id_trabajador = I_ID_TRABAJADOR and 
  id_estado in (snw_constantes.constante_tipo('PEC_POR_PLANEAR'));
  
  if l_conteo > 0 then
    RETURN 'POR_PLANEAR';
  end if;
  
  select count(*) into l_conteo
  from pec where id_trabajador = I_ID_TRABAJADOR and 
  id_estado in (snw_constantes.constante_tipo('PEC_PLANEADO'));
    
  if l_conteo > 0 then
    RETURN 'PEC_PLANEADO';
  end if;
  
  RETURN 'S';
  

END permite_crear_pec;
--------------------------------------------
PROCEDURE Guardar_Referencia_Evidencia(i_id_actividad NUMBER, i_id_archivo_evidencia NUMBER)
AS
BEGIN

  INSERT
  INTO pec_evidencia (id_archivo, id_actividad) 
  VALUES (i_id_archivo_evidencia, i_id_actividad);

END Guardar_Referencia_Evidencia;
--------------------------------------------
FUNCTION Obtener_Url_Archivo_Evidencia( i_id_pec NUMBER, i_id_actividad NUMBER) RETURN VARCHAR2
IS
 l_url_file VARCHAR2(2000);
 l_id_evidence_file NUMBER;
 l_expire_date DATE; 
BEGIN

    SELECT max(pec_evidencia.id_archivo)
    INTO l_id_evidence_file
    FROM pec_evidencia
    LEFT JOIN pec_actividad
        ON pec_evidencia.id_actividad = pec_actividad.id
    WHERE pec_actividad.id_pec = i_id_pec and pec_actividad.id = i_id_actividad;

    -- SE PUEDE DEFINIR EL TIEMPO ANTES DE QUE LA URL EXPIRE Y SE BLOQUEE LA VISUALIZACIÓN --
    -- SYSDATE + 1 agrega 1 día => agrega n dias al momento de expiración desde el instante en que se genera la URL.
    -- SYSDATE + 1/24 agrega 1 hora => n/24 agrega n horas al momento de expiración desde el instante en que se genera la URL.
    -- SYSDATE + 1/24/60 agrega 1 minuto => n/24/60 agrega n minutos al momento de expiración desde el instante en que se genera la URL.
    l_expire_date := sysdate+1/24;

    l_url_file := snw_files.public_pck.get_s3_url_file_uploaded( l_id_evidence_file, l_expire_date );

    RETURN l_url_file;

    EXCEPTION WHEN OTHERS THEN
        RETURN '---CAN NOT GET FILE---';

END Obtener_Url_Archivo_Evidencia;
--------------------------------------------
FUNCTION Validar_Evaluacion_Hecha( i_id_PEC NUMBER ) RETURN BOOLEAN
IS
    l_promedio_evaluacion NUMBER := 0;
BEGIN
    SELECT promedio_evaluacion INTO l_promedio_evaluacion FROM pec_evaluacion WHERE id_PEC = i_id_PEC;
    RETURN l_promedio_evaluacion > 0;
END;
--------------------------------------------

PROCEDURE ejecutar_pec ( i_id_pec NUMBER ) AS
BEGIN
    --A partir de 2020 la ejecución de las actividades no se solicita al usuario
    UPDATE PEC_ACTIVIDAD set cumple = 'S'
      where id_pec = i_id_pec;
    
    --Cambiar estado del PEC
    DEFINIR_ESTADO_PEC( i_id_pec, SNW_CONSTANTES.CONSTANTE_TIPO('PEC_EJECUTADO'), SYSDATE );
    
    --Crear la evaluación
    CREAR_EVALUACION_PEC(i_id_pec);
    
    -- CALFONSO 29/09/2020 - Ahora se hace el llamado desde la aplicación
    -- NOTIFICACION_PCK.Notificar_Evaluacion_PEC(i_id_pec);

EXCEPTION
  WHEN OTHERS THEN
    raise_application_error( -20001, 'Un error ocurrió en EJECUTAR_PEC. '||SQLERRM
    ||' '||DBMS_UTILITY.format_error_backtrace );
    
END ejecutar_pec;

PROCEDURE planear_pec ( i_id_pec NUMBER ) AS
BEGIN

    --Cambiar estado del PEC
    DEFINIR_ESTADO_PEC( i_id_pec, SNW_CONSTANTES.CONSTANTE_TIPO('PEC_PLANEADO'), SYSDATE );
    
    -- CALFONSO 29/09/2020 - Ahora se hace el llamado desde la aplicación
    -- NOTIFICACION_PCK.Notificar_Trabajador_PEC( i_id_pec );

EXCEPTION
  WHEN OTHERS THEN
    raise_application_error( -20001, 'Un error ocurrió en PLANEAR_PEC. '||SQLERRM
    ||' '||DBMS_UTILITY.format_error_backtrace );
    
END planear_pec;

PROCEDURE GESTION_AUTO_PEC_POR_PLANEAR ( i_pec IN PEC%ROWTYPE, i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) ) AS
l_constante_tiempo NUMBER := 3; --Días desde la creación para enviar notificaciones
l_dias_transcurridos NUMBER;
BEGIN
  --Se llama este procedimiento para los PECs que superaron cierto tiempo en días desde la 
  --fecha de creación aun se encuentran en estado por planeaar, con un parche debido
  --al historial retrasado: solo para creados a partir de 2020
  l_dias_transcurridos := trunc(i_fecha_ejecutar) - trunc(i_pec.fecha_creacion);
  if ( i_pec.fecha_creacion >= to_date('01/01/2020','dd/mm/yyyy') AND MOD(l_dias_transcurridos,l_constante_tiempo)=0 ) then
    NOTIFICACION_PCK.Alertar_PEC_No_Planeado(i_pec);
  end if;

EXCEPTION
  WHEN OTHERS THEN --Simplemente queremos que el proceso fallido no bloquee la ejecución de los demás
    NULL;
    
END GESTION_AUTO_PEC_POR_PLANEAR;


PROCEDURE GESTION_AUTO_PEC_PLANEADO( i_pec IN PEC%ROWTYPE, i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) ) AS

l_constante_tiempo NUMBER := 15;
l_dias_transcurridos NUMBER;
l_fecha_maxima_actividad DATE;
BEGIN
  --Se llama este procedimiento para los PECs que superaron cierto tiempo en días desde la fecha de creación
  --las fechas de sus actividades han sido superadas, y se encuentran en estado planeado
  l_dias_transcurridos := trunc(i_fecha_ejecutar) - trunc(i_pec.fecha_creacion); 
  SELECT max(fecha) into l_fecha_maxima_actividad
    from PEC_ACTIVIDAD where id_pec = i_pec.id;
 
  --Si han pasado más de 45 días, y apartir de allí un número entero de veces el tiempo establecido (i.e 15), se ejecuta el proceso de cierre auto
  if ( l_dias_transcurridos >= 45 AND MOD(l_dias_transcurridos,l_constante_tiempo)=0 AND i_fecha_ejecutar>l_fecha_maxima_actividad ) then
    EJECUTAR_PEC(i_pec.id); --ejecutar PEC contiene la notificación de evaluación al trabajador
  end if;

EXCEPTION
  WHEN OTHERS THEN --Simplemente queremos que el proceso fallido no bloquee la ejecución de los demás
    NULL;
    
END GESTION_AUTO_PEC_PLANEADO;

PROCEDURE GESTION_AUTOMATICA_PEC( i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) ) AS
CURSOR pecs_pendientes is
  select * from PEC where id_estado != SNW_CONSTANTES.CONSTANTE_TIPO('PEC_EVALUADO');

BEGIN
  FOR i in pecs_pendientes LOOP
    --El proceso a ejecutar depende del estado
    CASE i.id_estado 
      WHEN SNW_CONSTANTES.CONSTANTE_TIPO('PEC_POR_PLANEAR') THEN
        GESTION_AUTO_PEC_POR_PLANEAR(i, i_fecha_ejecutar);
      WHEN SNW_CONSTANTES.CONSTANTE_TIPO('PEC_PLANEADO') THEN
        GESTION_AUTO_PEC_PLANEADO(i, i_fecha_ejecutar);
      WHEN SNW_CONSTANTES.CONSTANTE_TIPO('PEC_EJECUTADO') THEN
        GESTION_AUTO_PEC_EJECUTADO(i, i_fecha_ejecutar);
    END CASE;
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN --Simplemente queremos que el proceso fallido no bloquee la ejecución de los demás
    NULL;
    
END GESTION_AUTOMATICA_PEC;

PROCEDURE GESTION_AUTO_PEC_EJECUTADO( i_pec IN PEC%ROWTYPE, i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) ) AS
l_constante_tiempo NUMBER := 15; --Días desde la creación para enviar notificaciones
l_dias_transcurridos NUMBER;

BEGIN
  --Se llama este procedimiento para los PECs que superaron cierto tiempo en días desde la 
  --fecha de creación, aun se encuentran en estado ejecutado, y fueron creados a partir de 
  --2020, para no generar alertas para históricos pendientes de evaluación.
  l_dias_transcurridos := trunc(i_fecha_ejecutar) - trunc(i_pec.fecha_creacion);
  if ( i_pec.fecha_creacion >= to_date('01/01/2020','dd/mm/yyyy') and MOD(l_dias_transcurridos,l_constante_tiempo)=0 ) then
    NOTIFICACION_PCK.Alertar_PEC_Sin_Evaluar(i_pec);
  end if;

EXCEPTION
  WHEN OTHERS THEN --Simplemente queremos que el proceso fallido no bloquee la ejecución de los demás
    NULL;

END GESTION_AUTO_PEC_EJECUTADO;

FUNCTION Eliminar_PEC ( i_id_pec number ) RETURN VARCHAR AS
 l_estado_id number;
 l_estado_nombre varchar2(30);
  BEGIN
    BEGIN
        --Validar que no esté planeado para realizar la evaluación
        select pec_estado_id, pec_estado_nombre into l_estado_id, l_estado_nombre
          from v_pec_basico
          where id = i_id_pec;
        if ( l_estado_id != snw_constantes.constante_tipo('PEC_POR_PLANEAR') ) then --Solo se eliminar en por_planear
          return 'PEC no eliminado. Está ' || l_estado_nombre;
        end if;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN return 'PEC no eliminado. No existe';
    END;
    
    --Eliminar el PEC
    delete from pec_estado where id_pec = i_id_pec;    
    delete from pec_actividad where id_pec = i_id_pec;        
    delete from pec_evaluacion where id_pec = i_id_pec;    
    delete from pec where id = i_id_pec;    
    
    RETURN 'PEC eliminado';
    
  END Eliminar_PEC;

END ADMIN_PEC_PCK;
/