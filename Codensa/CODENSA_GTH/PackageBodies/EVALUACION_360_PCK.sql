CREATE OR REPLACE PACKAGE BODY codensa_gth."EVALUACION_360_PCK" is


----------------
    
FUNCTION DETERMINAR_PAG_FORM_PLANTILLA (pformulario IN FORMULARIO_360.ID%type,
ptipo IN VARCHAR2)
RETURN NUMBER
IS
vplantilla PLANTILLA_FORM_360.ID%TYPE;
ktipo_seccion CONSTANT VARCHAR2(10) := 'SECCION';
ktipo_resultado CONSTANT VARCHAR2(10) := 'RESULTADO';
BEGIN
--Determinación de la plantilla
SELECT PLANTILLA_FORM_360.ID into vplantilla
from FORMULARIO_360 inner join PLANTILLA_FORM_360 on (FORMULARIO_360.plantilla =
PLANTILLA_FORM_360.id)
where FORMULARIO_360.id = pformulario;
if upper(ptipo) = ktipo_seccion then --Se busca la primera página de la sección
RETURN 42;
elsif upper(ptipo) = ktipo_resultado then --Se busca la página del resultado
RETURN 51;
end if;
EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_PAG_FORM_PLANTILLA.' || SQLCODE || ' - ' || SQLERRM);
END DETERMINAR_PAG_FORM_PLANTILLA;

---------------------

PROCEDURE               DETERMINAR_FORMULARIO
(pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_360.TIPO%type, pformulario OUT FORMULARIO_360.ID%type)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vplantilla PLANTILLA_FORM_360.ID%TYPE;
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'FORMULARIO_360';

 l_id_cargo TRABAJADOR.ID_CARGO%TYPE;
 l_id_gerencia TRABAJADOR.ID_GERENCIA%TYPE;
 l_id_unidad_organizativa TRABAJADOR.ID_UNIDAD_ORGANIZATIVA%TYPE;
BEGIN
  VAL_ESTADO_PERIODO(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   VAL_ENCUESTA_EXISTE (pevaluado, pano, ptipo, vreturn, vmensaje);

   if (vreturn = 0) then --La evaluación no existe. Crear formulario de acuerdo a la plantilla
     --Escogencia de la plantilla suponiendo que existe solo una para el colectivo (REQUIERE MEJORA EN SIGUIENTE VERSION)
     SELECT PLANTILLA_FORM_360.id into vplantilla
       from TRABAJADOR inner join tipo on (TRABAJADOR.ID_TARGET_BPR = tipo.id)
       inner join PLANTILLA_FORM_360 on (tipo.id = PLANTILLA_FORM_360.TARGET_BPR)
       where TRABAJADOR.numero_identificacion = pevaluado;
     --Elección del evaluador
     SELECT TRABAJADOR.jefe into vevaluador
       from TRABAJADOR
       where TRABAJADOR.NUMERO_IDENTIFICACION = pevaluado;
     --Verificación existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = 'FORMULARIO_360'
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_formulario;
     if vconteo_filas >= 1 then --El tipo_estado_bars sí existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_formulario and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_formulario );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     --Llave primaria formulario a crear
     SELECT NVL( max(id),0 )+1 into pformulario
       from FORMULARIO_360;

       select id_cargo, id_gerencia, id_unidad_organizativa into l_id_cargo, l_id_gerencia, l_id_unidad_organizativa 
       from trabajador where numero_identificacion in (pevaluado);

     --Creación formulario en estado nuevo
     INSERT INTO FORMULARIO_360 (id, plantilla, evaluador, evaluado, periodo, estado, fecha_evaluacion, tipo, id_cargo, id_gerencia, id_unidad_organizativa)
       values (pformulario, vplantilla, vevaluador, pevaluado, pano, vestado, trunc(sysdate), ptipo,  l_id_cargo, l_id_gerencia, l_id_unidad_organizativa);
   else --La encuesta existe, retornar formulario
     SELECT id into pformulario
       from FORMULARIO_360
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;

 else --El periodo no es el activo
   VAL_ENCUESTA_EXISTE (pevaluado, pano, ptipo, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluación no existe. Cómo no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluación existe
     SELECT id into pformulario
       from FORMULARIO_360
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;
 end if;
/*EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);*/
END;

---------------------
PROCEDURE               VAL_ENCUESTA_EXISTE (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_360.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 --Validar que el periodo no sea superior al año actual (REVISAR SI ES NECESARIO EN FASE II)
 if ( pano > extract( YEAR from sysdate ) ) then
   preturn := 0;
   pmensaje := 'Aun no existen encuestas para el periodo ' || pano || '. ';
 else --El periodo es igual o inferior al año actual
   select count(*) into vconteo_filas
     from FORMULARIO_360
     where evaluado = pevaluado and periodo = pano and ptipo = tipo;
   if vconteo_filas = 0 then --La evaluación no existe
     preturn := 0;
     pmensaje := 'El trabajador identificado con el número ' || pevaluado || ' no tiene una evaluación de tipo ' || ptipo || ' en el periodo '
       || pano || '. ';
   else
     preturn := 1;
     pmensaje := 'El trabajador identificado con el número ' || pevaluado || ' tiene una evaluación de tipo ' || ptipo || ' en el periodo '
       || pano || '. ';
   end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_ENCUESTA_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;

--------------------

PROCEDURE               VAL_CORRESP_EVALUADO (pevaluador IN TRABAJADOR.numero_identificacion%type,
  pevaluado IN TRABAJADOR.numero_identificacion%type, pano IN PERIODO_BARS.ano%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
vconteo_filas NUMBER;
vmensaje VARCHAR2(4000);
BEGIN
 /*Ana determinó que para formularios antiguos, la edición la debe hacer el que actualmente figura como jefe
 --Determinación si el periodo es el activo (o vigente)
 VAL_ESTADO_PERIODO (pano, 'ACTIVO', vconteo_filas, vmensaje);
 if (vconteo_filas = 0) then --El año no es el activo. Por tanto, la relación debe buscarse    
                             --en el formulario del año suministrado.
   SELECT count(*) into vconteo_filas
     from FORMULARIO_360
     where periodo = pano and evaluador = pevaluador and evaluado = pevaluado;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
 else --El año es el activo. Por tanto, la relación debe buscarse entre jefes y empleados.
 */
   SELECT count(*) into vconteo_filas
     from TRABAJADOR EMP left join TRABAJADOR JEFE on (EMP.jefe = JEFE.NUMERO_IDENTIFICACION)
     where EMP.NUMERO_IDENTIFICACION = pevaluado and JEFE.NUMERO_IDENTIFICACION = pevaluador;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
-- end if;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   preturn := 0;
   pmensaje := 'Faltan parámetros en la base de datos para verificar la correspondencia entre el evaluado y el evaluador. ';
 WHEN OTHERS THEN
   preturn := 0;
   pmensaje := 'Error inesperado. No se pudo verificar la correspondencia entre el evaluado y el evaluador. ' || SQLCODE
     || ' - ' || SQLERRM || ' ';
END;

---------------------
PROCEDURE               VAL_ESTADO_PERIODO (pano IN PERIODO_BARS.ano%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 vnombre_tipo_estado CONSTANT VARCHAR2(20) := 'PERIODO_BARS';
BEGIN
 --Validar que exista tipo estado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from TIPO_ESTADO_BARS where upper(nombre) = vnombre_tipo_estado;
 if vconteo_filas = 0 then --El tipo estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vtipo_estado
     from TIPO_ESTADO_BARS;
   INSERT INTO TIPO_ESTADO_BARS (id, nombre) VALUES (vtipo_estado, vnombre_tipo_estado);
 else --El tipo estado está una o más veces
   SELECT id into vtipo_estado
     from TIPO_ESTADO_BARS
     where upper(nombre) = vnombre_tipo_estado and rownum = 1;
 end if;

 --Validar que exista el estado solicitado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from ESTADO_BARS where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado;
 if vconteo_filas = 0 then --El estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vestado
     from ESTADO_BARS;
   INSERT INTO ESTADO_BARS (id, nombre, tipo_estado) VALUES (vestado, upper(pestado), vtipo_estado);
 else --El estado existe una o más veces
   SELECT id into vestado
     from ESTADO_BARS
     where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado and rownum = 1;
 end if;

 --Validar que el periodo esté en el estado solicitado
 SELECT count(*) into vconteo_filas
   from PERIODO_BARS
   where ano = pano and estado = vestado;
 if vconteo_filas = 0 then --El periodo no está en el estado suministrado
   preturn := 0;
   pmensaje := 'El periodo de evaluación no está en el estado: ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El periodo de evaluación está en el estado: ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_PERIODO. ' || SQLCODE || ' - '
     || SQLERRM);
END;


-------------------

PROCEDURE               VAL_ESTADO_FORM_360 (pformulario IN FORMULARIO_360.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
BEGIN
 --Validar que el formulario esté en el estado solicitado
 SELECT count(*) into vconteo_filas
   from FORMULARIO_360
   where id = pformulario and estado = ( SELECT id from ESTADO_BARS where upper(nombre) = upper(pestado)
     and tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = 'FORMULARIO_360') );
 if vconteo_filas = 0 then
   preturn := 0;
   pmensaje := 'El formulario no se encuentra en el estado ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El formulario se encuentra en el estado ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar si el formulario se encuentra en el estado ' || pestado || '.
     Hay un problema de parametrización de estados. Contacte al administrador de la aplicación. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_FORM_BARS. Contacte al administrador
     de la aplicación. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;

------------------------

PROCEDURE               CAMBIAR_ESTADO_FORM_360 (pformulario IN FORMULARIO_360.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from FORMULARIO_360
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    null;
  else
    --Verificar tipo_estado_bars = 'FORMULARIO_360'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = 'ESTADO_360';
    if vconteo_filas >= 1 then --El tipo_estado_360 sí existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = 'ESTADO_360' and rownum = 1;
    else --El tipo_estado_bars no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vtipo_estado
        from TIPO_ESTADO_BARS;
      INSERT INTO TIPO_ESTADO_BARS (id, nombre)
           values ( vtipo_estado, 'ESTADO_360' );
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vestado
        from ESTADO_BARS;
      INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
        values ( vestado, upper(pestado), vtipo_estado );
    end if;
    --Actualizar el estado del formulario  
    UPDATE FORMULARIO_360 SET estado = vestado, fecha_evaluacion = trunc(sysdate) where id = pformulario;
  end if;
EXCEPTION
  when others then
    raise_application_error(-20000,'Un error inesperado ocurrió en CAMBIO_ESTADO_FORMS_BARS. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;

----------------------

procedure crear_fomularios_eva_360 (nodo_hijo IN NUMBER, i_periodo IN number)
IS
nodo_padre NUMBER;
nodo_abuelo NUMBER;
existe_form NUMBER;

BEGIN

-- SE OBTIENE EL NODO PADRE O NODO CENTRAL
select JEFE into nodo_padre from TRABAJADOR where NUMERO_IDENTIFICACION = nodo_hijo;
-- SE OBTIENE EL NODO ABUELO O GESTOR
select JEFE into nodo_abuelo from TRABAJADOR where NUMERO_IDENTIFICACION = nodo_padre;

-- SE CREA LA EVALUACION DEL COLABORADOR SI NO EXISTE
select count(ID) into existe_form from FORMULARIO_360 WHERE TIPO = 'EVA_COLABO' and EVALUADOR = nodo_hijo and EVALUADO = nodo_padre and periodo=i_periodo;
IF existe_form = 0 THEN
insert into FORMULARIO_360 (ID, PLANTILLA, EVALUADOR, EVALUADO, PERIODO, ESTADO, TIPO) values ((SELECT NVL(MAX(ID),0) + 1 from FORMULARIO_360), '3',nodo_hijo,nodo_padre,i_periodo,31,'EVA_COLABO');
END IF;

-- SE CREA LA AUTOEVALUACION SI NO EXISTE
select count(ID) into existe_form from FORMULARIO_360 WHERE TIPO = 'AUTOEVALUACION' and EVALUADOR = nodo_padre and EVALUADO = nodo_padre and periodo=i_periodo;
IF existe_form = 0 THEN
insert into FORMULARIO_360 (ID, PLANTILLA, EVALUADOR, EVALUADO, PERIODO, ESTADO, TIPO) values ((SELECT NVL(MAX(ID),0) + 1 from FORMULARIO_360), '1',nodo_padre,nodo_padre,i_periodo,31,'AUTOEVALUACION');
END IF;

--SE CREA LA EVALUACION DEL GESTOR SI NO EXISTE
select count(ID) into existe_form from FORMULARIO_360 WHERE TIPO = 'EVA_GESTOR' and EVALUADOR = nodo_abuelo and EVALUADO = nodo_padre and  periodo=i_periodo;
IF existe_form = 0 THEN
insert into FORMULARIO_360 (ID, PLANTILLA, EVALUADOR, EVALUADO, PERIODO, ESTADO, TIPO) values ((SELECT NVL(MAX(ID),0) + 1 from FORMULARIO_360), '2',nodo_abuelo,nodo_padre,i_periodo,31,'EVA_GESTOR');
END IF;
END;

------------------

procedure generar_formularioMasivo_360 (i_periodo number) as
BEGIN
 FOR c in (SELECT TRABAJADOR FROM TRAEVABARS_360 WHERE PERIODO = i_periodo) loop
  crear_fomularios_eva_360 (c.trabajador, i_periodo);
 end loop;
END;

------------------

procedure DML_SECCION_FORMULARIO (pformulario IN
    FORMULARIO_360.ID%type, pseccion IN SECCION_360.ID%TYPE,
  pnum_rtasx IN NUMBER, prta_1 IN RESPUESTA_360.valor%type DEFAULT NULL,
  prta_2 IN RESPUESTA_360.valor%type DEFAULT NULL, prta_3 IN RESPUESTA_360.valor%type DEFAULT NULL,
  prta_4 IN RESPUESTA_360.valor%type DEFAULT NULL, prta_5 IN RESPUESTA_360.valor%type DEFAULT NULL)
IS
 vplantilla_pregunta PLANTILLA_PREGUNTAS.ID%TYPE;
 vnombre_parametro VARCHAR2(10);
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vconteo_filas NUMBER;
 vpromedio NUMBER;
 pnum_rtas NUMBER;
 v_pregunta NUMBER;
BEGIN



    select count(*) into pnum_rtas 
    from plantilla_preg_360 
    where plantilla_form_360 in (select plantilla from formulario_360 where id = pformulario) 
    and seccion=pseccion;



    FOR i IN 1..pnum_rtas LOOP
    Dbms_Output.Put_Line('Loop '||i||' de '||pnum_rtas);
    --Determinar la plantilla de la pregunta
    SELECT ID into vplantilla_pregunta 
        FROM PLANTILLA_PREG_360
        WHERE PLANTILLA_FORM_360 = (select plantilla from formulario_360 where id = pformulario)  AND SECCION = pseccion and CODIGO_PREGUNTA = i;

    vnombre_parametro := 'prta_' || to_char(i);
    --Determinar si ya existe la pregunta para el formulario especificado
    SELECT COUNT(ID) INTO v_pregunta  
    FROM RESPUESTA_360 WHERE PLANTILLA_PREGUNTA = vplantilla_pregunta and SECCION_360 = pseccion and FORMULARIO = pformulario;  
    --Determinar si hacer INSERT 
    IF v_pregunta = 0 THEN  
    --Insertar las respuestas
    INSERT INTO RESPUESTA_360 (id, seccion_360, plantilla_pregunta, valor, formulario)
        values ( respuesta_seq.nextval, pseccion, vplantilla_pregunta,
        CASE vnombre_parametro WHEN 'prta_1' then prta_1 WHEN 'prta_2' then prta_2 WHEN 'prta_3' then prta_3
        when 'prta_4' then prta_4 when 'prta_5' then prta_5 END, pformulario);
    END IF;
    --Determinar si hacer UPDATE 
    IF v_pregunta <> 0 THEN
    --Actualizar las respuestas
    UPDATE RESPUESTA_360
        SET valor = CASE vnombre_parametro WHEN 'prta_1' then prta_1 WHEN 'prta_2' then prta_2 WHEN 'prta_3' then prta_3
        when 'prta_4' then prta_4 when 'prta_5' then prta_5 END
        where seccion_360 = pseccion and plantilla_pregunta = vplantilla_pregunta and formulario = pformulario;
    END IF;
    END LOOP;

  --Actualizar el promedio de la sección del formulario
  --vpromedio := DETERMINAR_PROMEDIO_SECCION(vseccion_formulario);
  --UPDATE SECCION_FORMULARIO
    --set resultado = round(vpromedio,4)
    --where id = vseccion_formulario;

-- ATRONCOSO 20150203 Se integra el cambio de estado en el dml

    if pseccion = 1 then
      CAMBIAR_ESTADO_FORM_360(pformulario, 'EN_TRATAMIENTO');
    end If;
    if pseccion = 5 then    
      CAMBIAR_ESTADO_FORM_360(pformulario, 'CERRADO');
    end If;

 EXCEPTION WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DML_SECCION_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);
END;

------------------

procedure CALCU_PROM_EVA_360(i_evaluado IN NUMBER, i_periodo IN NUMBER)
IS
cursor colaboradores 
    is SELECT ID FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND TIPO = 'EVA_COLABO';
l_form_id_gestor NUMBER;
l_form_id_auto NUMBER;
l_num_colaboradores NUMBER;
--l_ponderacion_gestor NUMBER;
--l_ponderacion_auto NUMBER;
--l_ponderacion_colabo NUMBER;
k_preguntas_totales NUMBER;
k_secciones_totales NUMBER;
l_preguntas_seccion NUMBER;
l_total_gestor NUMBER;
l_total_auto NUMBER;
l_total_colaboradores NUMBER;
l_total_prom_preg NUMBER;
l_total_prom_sec NUMBER;
l_existe_resultado NUMBER;
l_total_evaluaciones NUMBER;
l_total_eva_cerradas NUMBER;
l_total_eva_sec_auto NUMBER;
l_total_eva_sec_gestor NUMBER;
l_total_eva_sec_colabo NUMBER;
l_numero_principios NUMBER;
l_total_eva_prin_auto NUMBER;
l_total_eva_prin_gestor NUMBER;
l_total_eva_prin_colabo NUMBER;
l_preguntas_principio NUMBER;
l_seccion_principio NUMBER;

BEGIN

-- atroncosos Ya no es necesario que todas la evaluaciones estén cerradas
--Se determina si el evaluado tiene todas sus evaluaciones respectivas en estado cerrado 
--SELECT COUNT(ID) into l_total_evaluaciones FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND PERIODO = i_periodo;
begin    
SELECT COUNT(ID) into l_total_eva_cerradas FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND PERIODO = i_periodo AND ESTADO = '33'; --CERRADO
exception when NO_DATA_FOUND  then
    l_total_eva_cerradas := -1;
end;
--Este proceso solo corre para evaluados con todas sus evaluaciones 360 cerradas
--IF l_total_evaluaciones = l_total_eva_cerradas THEN

--Se obtiene el numero de formulario de la evaluación del gestor
begin    
SELECT ID INTO l_form_id_gestor from FORMULARIO_360  WHERE EVALUADO = i_evaluado AND TIPO = 'EVA_GESTOR' AND PERIODO = i_periodo AND ESTADO = '33'; --CERRADO
exception when NO_DATA_FOUND  then
    l_form_id_gestor := null;
end;
--Se obtiene el numero de formulario de la autoevaluacion
begin
SELECT ID INTO l_form_id_auto FROM FORMULARIO_360  WHERE EVALUADO = i_evaluado AND TIPO = 'AUTOEVALUACION' AND PERIODO = i_periodo AND ESTADO = '33'; --CERRADO
exception when NO_DATA_FOUND  then
    l_form_id_auto := null;
end;
--Se obtiene el numero de colaboradores
SELECT COUNT(ID) INTO l_num_colaboradores FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND TIPO = 'EVA_COLABO' AND PERIODO = i_periodo AND ESTADO = '33'; --CERRADO


--Ponderaciones
--l_ponderacion_gestor := 0.4;
--l_ponderacion_auto := 0.1;
--if l_num_colaboradores <> 0 then
-- l_ponderacion_colabo := 0.5 / l_num_colaboradores;
--end if;

--Constantes
k_preguntas_totales := 19;
k_secciones_totales := 5;

-----------------CALCULO DEL PROMEDIO PONDERADO POR PREGUNTA-------------
FOR i in 1..k_preguntas_totales
LOOP

--Total gestor
--begin
--SELECT VALOR INTO l_total_gestor FROM RESPUESTA_360 WHERE FORMULARIO = l_form_id_gestor and PLANTILLA_PREGUNTA = k_preguntas_totales+i;
--exception when NO_DATA_FOUND then
--    l_total_gestor := 0;
--end;

--Total auto
--begin
--SELECT VALOR INTO l_total_auto FROM RESPUESTA_360 WHERE FORMULARIO = l_form_id_auto AND PLANTILLA_PREGUNTA = i;
--exception when NO_DATA_FOUND  then
--    l_total_auto := 0;
--end;

--Total colaboradores
--begin
--SELECT SUM(VALOR) INTO l_total_colaboradores FROM RESPUESTA_360 WHERE 
--    FORMULARIO in (SELECT ID FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND TIPO = 'EVA_COLABO' AND PERIODO = i_periodo) and PLANTILLA_PREGUNTA = (2*k_preguntas_totales)+i;
--exception when NO_DATA_FOUND then
--    l_total_colaboradores := 0;
--end;*/

--Total promedio todos los TIPOS
begin
 SELECT AVG(VALOR) INTO l_total_prom_preg FROM RESPUESTA_360 WHERE 
    FORMULARIO in (SELECT ID FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND TIPO = 'EVA_COLABO' AND PERIODO = i_periodo) 
    and PLANTILLA_PREGUNTA in (i,i+k_preguntas_totales,i+(2*k_preguntas_totales));
exception when NO_DATA_FOUND then
    l_total_prom_preg := 0;
end;


--Total promedio ponderado de la pregunta
--l_total_prom_preg := l_total_gestor + l_total_auto + l_total_colaboradores / l_total_eva_cerradas;
--l_total_prom_preg := nvl(l_total_prom_preg,0);

--Se hace el insert o update correspondiente
SELECT COUNT(ID) INTO l_existe_resultado FROM PONDERACION_PREG_360 WHERE EVALUADO = i_evaluado AND 
    PLANTILLA_PREG_360 = i AND PERIODO = i_periodo;

IF l_existe_resultado = 0 THEN
    INSERT INTO PONDERACION_PREG_360 (EVALUADO, PLANTILLA_PREG_360, PONDERADO_RESPUESTA, PERIODO) VALUES (i_evaluado, i, nvl(l_total_prom_preg,0), i_periodo);
ELSE
    UPDATE PONDERACION_PREG_360 SET PONDERADO_RESPUESTA = nvl(l_total_prom_preg,0) WHERE EVALUADO = i_evaluado 
    AND PLANTILLA_PREG_360 = i AND PERIODO = i_periodo;    
END IF;

END LOOP;

-----------------CALCULO DEL PROMEDIO POR SECCION Y TIPO DE EVALUACION-------------
FOR i in 1..k_secciones_totales
LOOP

--Se encuentran las preguntas por seccion
SELECT COUNT(DISTINCT(CODIGO_PREGUNTA)) into l_preguntas_seccion FROM PLANTILLA_PREG_360 WHERE SECCION = i;

--TODO: GACARRILLOC Ajuste sujeto a validación. Se garantiza que la suma de al menos 0 para l_total_prom_sec, para que no falle el insert
--Total promedio por sección
SELECT ( ( NVL(SUM(VALOR),0)/l_preguntas_seccion )/( 2+l_num_colaboradores )  ) INTO l_total_prom_sec FROM (SELECT * FROM (SELECT VALOR
    FROM RESPUESTA_360 
    WHERE FORMULARIO IN (l_form_id_gestor,l_form_id_auto) 
    AND SECCION_360 = i
UNION ALL
SELECT VALOR
    FROM RESPUESTA_360 
    WHERE FORMULARIO IN (SELECT ID FROM FORMULARIO_360 WHERE EVALUADO = i_evaluado AND TIPO = 'EVA_COLABO' 
        AND PERIODO = i_periodo)
    AND SECCION_360 = i));

--Total promedio seccion autoevaluacion
select sum(RESPUESTA_360.valor)/l_preguntas_seccion into l_total_eva_sec_auto 
    FROM FORMULARIO_360
    INNER JOIN RESPUESTA_360 ON (FORMULARIO_360.ID = RESPUESTA_360.FORMULARIO)
    WHERE EVALUADO = i_evaluado  AND TIPO = 'AUTOEVALUACION' AND SECCION_360 = i AND PERIODO = i_periodo;

--Total promedio seccion gestor
select sum(RESPUESTA_360.valor)/l_preguntas_seccion into l_total_eva_sec_gestor
    FROM FORMULARIO_360
    INNER JOIN RESPUESTA_360 ON (FORMULARIO_360.ID = RESPUESTA_360.FORMULARIO)
    WHERE EVALUADO = i_evaluado  AND TIPO = 'EVA_GESTOR' AND SECCION_360 = i AND PERIODO = i_periodo;

--Total promedio seccion colaboradores
select (sum(RESPUESTA_360.valor)/l_preguntas_seccion)/l_num_colaboradores into l_total_eva_sec_colabo
    FROM FORMULARIO_360
    INNER JOIN RESPUESTA_360 ON (FORMULARIO_360.ID = RESPUESTA_360.FORMULARIO)
    WHERE EVALUADO = i_evaluado  AND TIPO = 'EVA_COLABO' AND SECCION_360 = i AND PERIODO = i_periodo;


--Se hace el insert o update correspondiente
SELECT COUNT(ID) INTO l_existe_resultado FROM PONDERACION_SECCION_360 WHERE EVALUADO = i_evaluado 
    AND SECCION_360 = i AND PERIODO = i_periodo;
IF l_existe_resultado = 0 THEN
    INSERT INTO PONDERACION_SECCION_360 
    (EVALUADO, SECCION_360, PONDERACION_SECCION, PONDERACION_SEC_AUTO, 
        PONDERACION_SEC_GESTOR, PONDERACION_SEC_COLABO, PERIODO) 
    VALUES (i_evaluado, i, l_total_prom_sec, l_total_eva_sec_auto, 
        l_total_eva_sec_gestor, l_total_eva_sec_colabo, i_periodo);
ELSE
    UPDATE PONDERACION_SECCION_360 SET PONDERACION_SECCION = l_total_prom_sec, 
    PONDERACION_SEC_AUTO = l_total_eva_sec_auto, PONDERACION_SEC_GESTOR = l_total_eva_sec_gestor, 
    PONDERACION_SEC_COLABO = l_total_eva_sec_colabo
    WHERE EVALUADO = i_evaluado AND SECCION_360 = i AND PERIODO = i_periodo;    

END IF;
END LOOP;

-----------------CALCULO DEL PROMEDIO POR PRINCIPIO-------------

--Se calcula el numero de principios
select count(distinct(DECODE(PRINCIPIO,0,NULL,PRINCIPIO))) INTO l_numero_principios FROM PLANTILLA_PREG_360;

FOR i in 1..l_numero_principios 
LOOP

--Se calculan el numero de preguntas por principio
select count(distinct(CODIGO_PREGUNTA)) into l_preguntas_principio
    FROM PLANTILLA_PREG_360 where PLANTILLA_FORM_360 = 1 AND PRINCIPIO = i;

--Se calcula la sección del principio
select distinct(SECCION) into l_seccion_principio
    FROM PLANTILLA_PREG_360 where PLANTILLA_FORM_360 = 1 AND PRINCIPIO = i;


--Total promedio principio autoevaluacion
select sum(RESPUESTA_360.valor)/l_preguntas_principio into l_total_eva_prin_auto
    FROM RESPUESTA_360
    INNER JOIN SECCION_360 ON(SECCION_360.ID = RESPUESTA_360.SECCION_360)
    INNER JOIN FORMULARIO_360 ON (RESPUESTA_360.FORMULARIO = FORMULARIO_360.ID)
    INNER JOIN PLANTILLA_PREG_360 ON (RESPUESTA_360.PLANTILLA_PREGUNTA = PLANTILLA_PREG_360.ID)
    WHERE FORMULARIO_360.EVALUADO = i_evaluado  AND FORMULARIO_360.TIPO = 'AUTOEVALUACION' 
    AND PLANTILLA_PREG_360.PRINCIPIO = i AND FORMULARIO_360.PERIODO = i_periodo;

--Total promedio principio gestor
select sum(RESPUESTA_360.valor)/l_preguntas_principio into l_total_eva_prin_gestor
    FROM RESPUESTA_360
    INNER JOIN SECCION_360 ON(SECCION_360.ID = RESPUESTA_360.SECCION_360)
    INNER JOIN FORMULARIO_360 ON (RESPUESTA_360.FORMULARIO = FORMULARIO_360.ID)
    INNER JOIN PLANTILLA_PREG_360 ON (RESPUESTA_360.PLANTILLA_PREGUNTA = PLANTILLA_PREG_360.ID)
    WHERE FORMULARIO_360.EVALUADO = i_evaluado  AND FORMULARIO_360.TIPO = 'EVA_GESTOR'
    AND PLANTILLA_PREG_360.PRINCIPIO = i AND FORMULARIO_360.PERIODO = i_periodo;


--Total promedio seccion colaboradores
select (sum(RESPUESTA_360.valor)/l_preguntas_principio)/l_num_colaboradores into l_total_eva_prin_colabo
    FROM RESPUESTA_360
    INNER JOIN SECCION_360 ON(SECCION_360.ID = RESPUESTA_360.SECCION_360)
    INNER JOIN FORMULARIO_360 ON (RESPUESTA_360.FORMULARIO = FORMULARIO_360.ID)
    INNER JOIN PLANTILLA_PREG_360 ON (RESPUESTA_360.PLANTILLA_PREGUNTA = PLANTILLA_PREG_360.ID)
    WHERE FORMULARIO_360.EVALUADO = i_evaluado  AND FORMULARIO_360.TIPO = 'EVA_COLABO'
    AND PLANTILLA_PREG_360.PRINCIPIO = i AND FORMULARIO_360.PERIODO = i_periodo;

--Se hace el insert o update correspondiente
SELECT COUNT(ID) INTO l_existe_resultado FROM PROMEDIO_PRINCIPIO_360 WHERE EVALUADO = i_evaluado 
    AND PRINCIPIO = i AND PERIODO = i_periodo;
IF l_existe_resultado = 0 THEN

    INSERT INTO PROMEDIO_PRINCIPIO_360
    (EVALUADO, TIPO_EVALUACION, PRINCIPIO, VALOR, SECCION, PERIODO)
    VALUES (i_evaluado, 'AUTOEVALUACION', i, l_total_eva_prin_auto,
        l_seccion_principio, i_periodo);

    INSERT INTO PROMEDIO_PRINCIPIO_360
    (EVALUADO, TIPO_EVALUACION, PRINCIPIO, VALOR, SECCION, PERIODO)
    VALUES (i_evaluado, 'EVA_GESTOR', i, l_total_eva_prin_gestor,
        l_seccion_principio, i_periodo);

    INSERT INTO PROMEDIO_PRINCIPIO_360
    (EVALUADO, TIPO_EVALUACION, PRINCIPIO, VALOR, SECCION, PERIODO)
    VALUES (i_evaluado, 'EVA_COLABO', i, l_total_eva_prin_colabo,
        l_seccion_principio, i_periodo);

ELSE

    UPDATE PROMEDIO_PRINCIPIO_360 SET VALOR = l_total_eva_prin_auto
    WHERE EVALUADO = i_evaluado AND TIPO_EVALUACION = 'AUTOEVALUACION' AND PRINCIPIO = i 
    AND SECCION = l_seccion_principio AND PERIODO = i_periodo; 

    UPDATE PROMEDIO_PRINCIPIO_360 SET VALOR = l_total_eva_prin_gestor
    WHERE EVALUADO = i_evaluado AND TIPO_EVALUACION = 'EVA_GESTOR' AND PRINCIPIO = i
    AND SECCION = l_seccion_principio AND PERIODO = i_periodo; 

    UPDATE PROMEDIO_PRINCIPIO_360 SET VALOR = l_total_eva_prin_colabo
    WHERE EVALUADO = i_evaluado AND TIPO_EVALUACION = 'EVA_COLABO' AND PRINCIPIO = i
    AND SECCION = l_seccion_principio AND PERIODO = i_periodo; 

END IF;

END LOOP;



--END IF;

 EXCEPTION WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en CALCU_PROM_EVA_360.' 
       || ' Evaluado: ' ||i_evaluado||', seccion: '|| 'i'||', l_total_prom_sec: '|| l_total_prom_sec||', total_eva_sec_auto: '|| l_total_eva_sec_auto 
       || ' Total_Eva_sec_gestor: ' || l_total_eva_sec_gestor || ' Total_Eva_sec_colab: '|| l_total_eva_sec_colabo ||' periodo: '|| i_periodo
       || SQLCODE || ' - ' || SQLERRM || ' *** ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE());

END;

---------------------
procedure CALC_PONDERACION_MASIVA_360 (i_periodo number) as
BEGIN
 FOR c in (SELECT DISTINCT(EVALUADO) FROM FORMULARIO_360 WHERE PERIODO = i_periodo) loop
  CALCU_PROM_EVA_360 (c.evaluado, i_periodo);
 end loop;
 UPDATE PONDERACION_SECCION_360 SET PONDERACION_SECCION = TRUNC(PONDERACION_SECCION,1),
 PONDERACION_SEC_AUTO = TRUNC(PONDERACION_SEC_AUTO,1), PONDERACION_SEC_GESTOR = TRUNC(PONDERACION_SEC_GESTOR,1),
 PONDERACION_SEC_COLABO = TRUNC(PONDERACION_SEC_COLABO,1) WHERE periodo = i_periodo;
 UPDATE PROMEDIO_PRINCIPIO_360 SET VALOR = TRUNC(VALOR,1) WHERE periodo = i_periodo;

END;
----------------------------------------------------------------------------------

function OBTENER_URL_360(I_EVALUADO IN VARCHAR2 , I_PERIODO IN VARCHAR2) 
RETURN VARCHAR2
AS 
/*El ampersand '&' es el caracter por omisión para ingresar una variable de substitución en PL/SQL. Para no requerir configuraciones
como SET DEFINE OFF, o cambiar el caracter por omisión, se concatena usando su ASCII CHR(38)
TODO: GCARRILLO 20171210 Reemplazar v('AI_JNDI_CONNECTION') por una variable global dentro de un paquete y así evitar que en PL/SQL
no se visualice el resultado correcto de esta función*/
L_DOMAIN VARCHAR2(500) :='https://grupoenelcolombia.com:8181';
L_APLICATION_REPORT VARCHAR2(500) :='/SnwAutoRep/ReportsServlet?subfolder=codensa' || CHR(38) || 
  'reportName=eva360' || CHR(38) || 'jndi=' || v('AI_JNDI_CONNECTION');
BEGIN
RETURN L_DOMAIN||L_APLICATION_REPORT||CHR(38)||'EVALUADO='||I_EVALUADO||CHR(38)||'PERIODO='||I_PERIODO;
END OBTENER_URL_360;



end "EVALUACION_360_PCK";

/