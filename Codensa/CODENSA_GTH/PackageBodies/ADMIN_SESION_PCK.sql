CREATE OR REPLACE PACKAGE BODY codensa_gth."ADMIN_SESION_PCK" AS

--------------------------------------------
FUNCTION SESIONES_GRUPO (i_id_grupo IN NUMBER) RETURN NUMBER IS
    l_conteo number;
BEGIN
    SELECT COUNT(*) INTO l_conteo FROM PDI_SESION 
    WHERE PDI_SESION.ID_PDI_GRUPO=i_id_grupo; 
    RETURN l_conteo;
END SESIONES_GRUPO;
--------------------------------------------
FUNCTION SESIONES_CON_ASISTENCIA (i_id_pdi IN NUMBER) RETURN NUMBER IS
    l_grupo number;
    l_conteo number;
BEGIN
    SELECT ID_PDI_GRUPO INTO l_grupo FROM BARS_PDI WHERE ID=i_id_pdi;
    SELECT COUNT(*) INTO l_conteo FROM PDI_SESION 
    RIGHT JOIN PDI_ASISTENCIA ON PDI_ASISTENCIA.ID_PDI_SESION=PDI_SESION.ID
    WHERE ID_PDI_GRUPO=l_grupo AND ID_BARS_PDI=i_id_pdi;
    RETURN l_conteo;
END SESIONES_CON_ASISTENCIA;
--------------------------------------------
PROCEDURE ACTUALIZAR_ASISTENTES_SESION (i_id_sesion NUMBER) IS
    l_asistentes number;    
BEGIN
    update pdi_sesion set asistio='N' where id=i_id_sesion;
    select count(*) into l_asistentes from pdi_asistencia where id_pdi_sesion=i_id_sesion and asistio='S';
    update pdi_sesion set asistentes=l_asistentes where id=i_id_sesion;
    if (l_asistentes > 0) then
        update pdi_sesion set asistio='S' where id=i_id_sesion;
    end if;
END ACTUALIZAR_ASISTENTES_SESION;
--------------------------------------------
FUNCTION SESIONES_COMPLETAS_GRUPO (i_id_grupo NUMBER) RETURN BOOLEAN IS
    l_sesiones_actuales number;
    l_sesiones_grupo number;
BEGIN
    select count(*) into l_sesiones_actuales from pdi_sesion where id_pdi_grupo= i_id_grupo and asistio is not null;
    l_sesiones_grupo:=SESIONES_GRUPO(i_id_grupo);
    if (l_sesiones_actuales<l_sesiones_grupo) then
        return false;
    end if;
    return true;
END SESIONES_COMPLETAS_GRUPO;
--------------------------------------------

END ADMIN_SESION_PCK;

/