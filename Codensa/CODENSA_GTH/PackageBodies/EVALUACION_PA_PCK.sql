CREATE OR REPLACE PACKAGE BODY codensa_gth."EVALUACION_PA_PCK" AS

 Procedure Determinar_formulario(i_evaluado   IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
                                i_ano        IN PERIODO_BARS.ANO%type, 
                                i_tipo       IN FORMULARIO_BARS.TIPO%type, 
                                o_formulario OUT FORMULARIO_BARS.ID%type) AS
  
l_conteo_filas NUMBER;
 l_return NUMBER;
 l_mensaje VARCHAR2(4000);
 l_plantilla PLANTILLA_FORMULARIO.ID%TYPE;
 l_evaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 l_tipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 l_estado ESTADO_BARS.ID%TYPE;
 l_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 l_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 l_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'FORMULARIO_BARS';

 l_id_cargo TRABAJADOR.ID_CARGO%TYPE;
 l_id_gerencia TRABAJADOR.ID_GERENCIA%TYPE;
 l_id_unidad_organizativa TRABAJADOR.ID_UNIDAD_ORGANIZATIVA%TYPE;
BEGIN
  PERIODO_PCK.VAL_ESTADO_PERIODO(i_ano, l_estado_activo, l_conteo_filas, l_mensaje);
 if (l_conteo_filas = 1) then --El periodo es el activo.
   VAL_ENCUESTA_EXISTE (i_evaluado, i_ano, i_tipo, l_return, l_mensaje);

   if (l_return = 0) then --La evaluación no existe. Crear formulario de acuerdo a la plantilla
     --Escogencia de la plantilla suponiendo que existe solo una para el colectivo (REQUIERE MEJORA EN SIGUIENTE VERSION)

     SELECT max(PLANTILLA_FORMULARIO.id) into l_plantilla
       from TRABAJADOR inner join tipo on (TRABAJADOR.ID_TARGET_BPR = tipo.id)
       inner join PLANTILLA_FORMULARIO on (tipo.id = PLANTILLA_FORMULARIO.TARGET_BPR)
       where TRABAJADOR.numero_identificacion = i_evaluado
       and EXTRACT(YEAR FROM PLANTILLA_FORMULARIO.VIGENTE_DESDE)<=i_ano;

     --Elección del evaluador
     SELECT TRABAJADOR.jefe into l_evaluador
       from TRABAJADOR
       where TRABAJADOR.NUMERO_IDENTIFICACION = i_evaluado;
     --Verificación existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = 'FORMULARIO_BARS'
     select count(*) into l_conteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = l_tipo_estado_formulario;
     if l_conteo_filas >= 1 then --El tipo_estado_bars sí existe
       SELECT id into l_tipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = l_tipo_estado_formulario and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into l_tipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( l_tipo_estado, l_tipo_estado_formulario );
     end if;
     --Verificar estado deseado
     SELECT count(*) into l_conteo_filas
       from ESTADO_BARS
       where tipo_estado = l_tipo_estado and upper(nombre) = l_estado_nuevo;
     if l_conteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into l_estado
         from ESTADO_BARS
         where tipo_estado = l_tipo_estado and upper(nombre) = l_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into l_estado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( l_estado, l_estado_nuevo, l_tipo_estado );
     end if;
     --Llave primaria formulario a crear
     SELECT NVL( max(id),0 )+1 into o_formulario
       from FORMULARIO_BARS;

       select id_cargo, id_gerencia, id_unidad_organizativa into l_id_cargo, l_id_gerencia, l_id_unidad_organizativa 
       from trabajador where numero_identificacion in (i_evaluado);

     --Creación formulario en estado nuevo
     INSERT INTO FORMULARIO_BARS (id, plantilla, evaluador, evaluado, periodo, estado, fecha_evaluacion, tipo, id_cargo, id_gerencia, id_unidad_organizativa)
       values (o_formulario, l_plantilla, l_evaluador, i_evaluado, i_ano, l_estado, trunc(sysdate), i_tipo,  l_id_cargo, l_id_gerencia, l_id_unidad_organizativa);
   else --La encuesta existe, retornar formulario
     SELECT id into o_formulario
       from FORMULARIO_BARS
       where evaluado = i_evaluado and periodo = i_ano and tipo = i_tipo;
   end if;

 else --El periodo no es el activo
   VAL_ENCUESTA_EXISTE (i_evaluado, i_ano, i_tipo, l_return, l_mensaje);
   if (l_return = 0) then --La evaluación no existe. Cómo no es periodo activo no se puede crear.
     o_formulario := NULL;
   else --La evaluación existe
     SELECT id into o_formulario
       from FORMULARIO_BARS
       where evaluado = i_evaluado and periodo = i_ano and tipo = i_tipo;
   end if;
 end if;
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);
  END Determinar_formulario;

 -----------------------------------------------------------------------------------------------------------------

  Procedure Dml_seccion_formulario (i_formulario IN  FORMULARIO_BARS.ID%type,
                                    i_seccion IN SECCION_BARS.ID%TYPE,
                                    i_tipo IN SECCION_FORMULARIO.TIPO%TYPE,
                                    i_num_rtasx IN NUMBER, 
                                    i_rta_1 IN RESPUESTA.valor%type DEFAULT NULL,
                                    i_rta_2 IN RESPUESTA.valor%type DEFAULT NULL, 
                                    i_rta_3 IN RESPUESTA.valor%type DEFAULT NULL,
                                    i_rta_4 IN RESPUESTA.valor%type DEFAULT NULL, 
                                    i_rta_5 IN RESPUESTA.valor%type DEFAULT NULL) AS
  l_seccion_formulario SECCION_FORMULARIO.ID%TYPE;
 l_plantilla_pregunta PLANTILLA_PREGUNTAS.ID%TYPE;
 l_nombre_parametro VARCHAR2(10);
 l_return NUMBER;
 l_mensaje VARCHAR2(4000);
 l_conteo_filas NUMBER;
 l_promedio NUMBER;
 l_num_rtas NUMBER;
BEGIN
  --Determinar si la sección existe
  SELECT count(*) into l_conteo_filas
    from SECCION_FORMULARIO
    where formulario = i_formulario and seccion = i_seccion and tipo = i_tipo;


    select count(*) into l_num_rtas 
    from plantilla_preguntas 
    where plantilla_formulario in (select plantilla from formulario_bars where id=i_formulario) 
    and seccion=i_seccion;

  if l_conteo_filas = 0 then --La sección no existe
  Dbms_Output.Put_Line('--La sección no existe');
    --Insertar la sección formulario
    SELECT NVL( max(id),0 )+1 into l_seccion_formulario
      from SECCION_FORMULARIO;
    INSERT INTO SECCION_FORMULARIO (formulario, id, seccion, tipo)
      values (i_formulario, l_seccion_formulario, i_seccion, i_tipo);
    --Insertar las respuestas
    FOR i IN 1..l_num_rtas LOOP
    Dbms_Output.Put_Line('Loop '||i||' de '||l_num_rtas);
      --Determinar la plantilla de la pregunta
      SELECT PLANTILLA_PREGUNTAS.id into l_plantilla_pregunta
        from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.plantilla = PLANTILLA_FORMULARIO.id)
        inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.id = PLANTILLA_PREGUNTAS.plantilla_formulario)
        where FORMULARIO_BARS.id = i_formulario and PLANTILLA_PREGUNTAS.seccion = i_seccion and
        PLANTILLA_PREGUNTAS.codigo_pregunta = i;
      l_nombre_parametro := 'i_rta_' || to_char(i);
      INSERT INTO RESPUESTA (id, seccion_formulario, plantilla_pregunta, valor)
        values ( respuesta_seq.nextval, l_seccion_formulario, l_plantilla_pregunta,
        CASE l_nombre_parametro WHEN 'i_rta_1' then i_rta_1 WHEN 'i_rta_2' then i_rta_2 WHEN 'i_rta_3' then i_rta_3
        when 'i_rta_4' then i_rta_4 when 'i_rta_5' then i_rta_5 END);
    END LOOP;
  else --La sección ya existe
  Dbms_Output.Put_Line('La seccion ya existe ');
    --Obtener la sección formulario
    SELECT id into l_seccion_formulario
      from SECCION_FORMULARIO
      where formulario = i_formulario and seccion = i_seccion and tipo = i_tipo;
    --Actualizar las respuestas
     FOR i IN 1..l_num_rtas LOOP
      --Determinar la plantilla de la pregunta
      SELECT PLANTILLA_PREGUNTAS.id into l_plantilla_pregunta
        from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.plantilla = PLANTILLA_FORMULARIO.id)
        inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.id = PLANTILLA_PREGUNTAS.plantilla_formulario)
        where FORMULARIO_BARS.id = i_formulario and PLANTILLA_PREGUNTAS.seccion = i_seccion and
        PLANTILLA_PREGUNTAS.codigo_pregunta = i;
      l_nombre_parametro := 'i_rta_' || to_char(i);
      UPDATE RESPUESTA
        SET valor = CASE l_nombre_parametro WHEN 'i_rta_1' then i_rta_1 WHEN 'i_rta_2' then i_rta_2 WHEN 'i_rta_3' then i_rta_3
        when 'i_rta_4' then i_rta_4 when 'i_rta_5' then i_rta_5 END
        where seccion_formulario = l_seccion_formulario and plantilla_pregunta = l_plantilla_pregunta;
    END LOOP;
  end if;
  --Actualizar el promedio de la sección del formulario
  l_promedio := DETERMINAR_PROMEDIO_SECCION(l_seccion_formulario);
  UPDATE SECCION_FORMULARIO
    set resultado = round(l_promedio,4)
    where id = l_seccion_formulario;

-- ATRONCOSO 20150203 Se integra el cambio de estado en el dml

    if i_seccion = 1 or i_seccion=8 then
      CAMBIAR_ESTADO_FORM_BARS(i_formulario, 'EN TRATAMIENTO');
    end If;
    if i_seccion = 7 or i_seccion=9 then    
      CAMBIAR_ESTADO_FORM_BARS(i_formulario, 'FACTORES FINALIZADOS');
    end If;

 EXCEPTION WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DML_SECCION_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);

  END Dml_seccion_formulario;

 -----------------------------------------------------------------------------------------------------------------


  function  Determinar_pct_consecucion (i_formulario IN FORMULARIO_BARS.ID%TYPE, 
                                    i_tipo IN SECCION_FORMULARIO.TIPO%TYPE) 
                                    RETURN PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE AS
  l_pct_consecucion PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE;
  l_resultado_encuesta NUMBER;
  l_conteo_filas NUMBER;
  l_periodo PERIODO_BARS.ANO%TYPE;
BEGIN
  --Determinar promedio encuesta y normalizar
  l_resultado_encuesta := NORMALIZAR_BPR(EVALUACION_PA_PCK.DETERMINAR_PROMEDIO_ENCUESTA(i_formulario));


  --Persiste la informacion en FORMULARIO_BARS
  update formulario_bars set media_normalizada = l_resultado_encuesta where id = i_formulario;

    --Determinar pct_consecucion
  l_pct_consecucion := OBTENER_PORCENTAJE_CONSECUCION(l_resultado_encuesta);

  
  /* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
   --Determinar periodo
  select periodo into l_periodo from FORMULARIO_BARS where id = i_formulario; 
  --Determinar pct_consecucion
  SELECT count(*) into l_conteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 61 and periodo = l_periodo AND l_resultado_encuesta BETWEEN media_comp_inf AND media_comp_sup;
  if l_conteo_filas = 1 then --Se determinó el porcentaje de consecución  
    SELECT pct_consecucion into l_pct_consecucion
      from PCT_CONSECUCION_BARS
      where tipo_id = 61 and  periodo = l_periodo AND l_resultado_encuesta BETWEEN media_comp_inf AND media_comp_sup;
  else --No se determinó un porcentaje de consecución
    l_pct_consecucion := NULL;
  end if;
    */
  
  
  RETURN l_pct_consecucion;
  
  
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PCT_CONSECUCION. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
  END Determinar_pct_consecucion;

 -----------------------------------------------------------------------------------------------------------------


  Function Determinar_pag_formulario (i_formulario IN FORMULARIO_BARS.ID%type,
                                    i_tipo IN VARCHAR2) RETURN NUMBER AS

l_plantilla PLANTILLA_FORMULARIO%ROWTYPE;
l_tipo_seccion CONSTANT VARCHAR2(10) := 'SECCION';
l_tipo_resultado CONSTANT VARCHAR2(10) := 'RESULTADO';
BEGIN
--Determinación de la plantilla
SELECT PLANTILLA_FORMULARIO.* into l_plantilla
from PLANTILLA_FORMULARIO inner join FORMULARIO_BARS on (FORMULARIO_BARS.plantilla =
PLANTILLA_FORMULARIO.id)
where FORMULARIO_BARS.id = i_formulario;
if upper(i_tipo) = l_tipo_seccion and l_plantilla.VIGENTE_DESDE>=TO_DATE('01/01/2017','DD/MM/YYYY') then --Se busca la primera página de la sección
RETURN 132;
elsif upper(i_tipo) = l_tipo_resultado and l_plantilla.VIGENTE_DESDE>=TO_DATE('01/01/2017','DD/MM/YYYY') then  --Se busca la página del resultado
RETURN 135;
elsif upper(i_tipo) = l_tipo_seccion and l_plantilla.VIGENTE_DESDE<TO_DATE('01/01/2017','DD/MM/YYYY') then  --SSe busca la primera página de la sección OLD VERSION
RETURN 2;
elsif upper(i_tipo) = l_tipo_resultado and l_plantilla.VIGENTE_DESDE<TO_DATE('01/01/2017','DD/MM/YYYY') then  --Se busca la página del resultado OLD VERSION
RETURN 9;
end if;
EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR (-20000,'Error inesperado en determinar la pagina.' || SQLCODE || ' - ' || SQLERRM);
END Determinar_pag_formulario;

 -----------------------------------------------------------------------------------------------------------------



  PROCEDURE Val_corresp_evaluado       (i_evaluador IN TRABAJADOR.numero_identificacion%type,
                                          i_evaluado IN TRABAJADOR.numero_identificacion%type, 
                                          i_ano IN PERIODO_BARS.ano%type, 
                                          o_return OUT NUMBER, 
                                          o_mensaje OUT VARCHAR2) AS
 l_conteo_filas NUMBER;
BEGIN
   SELECT count(*) into l_conteo_filas
     from TRABAJADOR EMP left join TRABAJADOR JEFE on (EMP.jefe = JEFE.NUMERO_IDENTIFICACION)
     where EMP.NUMERO_IDENTIFICACION = i_evaluado and JEFE.NUMERO_IDENTIFICACION = i_evaluador;
   if l_conteo_filas = 0 then --No hay correspondencia
     o_return := 0;
     o_mensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     o_return := 1;
     o_mensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
-- end if;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   o_return := 0;
   o_mensaje := 'Faltan parámetros en la base de datos para verificar la correspondencia entre el evaluado y el evaluador. ';
 WHEN OTHERS THEN
   o_return := 0;
   o_mensaje := 'Error inesperado. No se pudo verificar la correspondencia entre el evaluado y el evaluador. ' || SQLCODE
     || ' - ' || SQLERRM || ' ';
    NULL;
  END Val_corresp_evaluado;

 -----------------------------------------------------------------------------------------------------------------



  PROCEDURE Val_estado_form_bars  (i_formulario IN FORMULARIO_BARS.id%type,
                                    i_estado IN ESTADO_BARS.NOMBRE%type, 
                                    o_return OUT NUMBER, 
                                    o_mensaje OUT VARCHAR2) AS

 l_conteo_filas NUMBER;
 l_tipo_estado NUMBER;
 l_estado NUMBER;
BEGIN
 --Validar que el formulario esté en el estado solicitado
 SELECT count(*) into l_conteo_filas
   from FORMULARIO_BARS
   where id = i_formulario and estado = ( SELECT id from ESTADO_BARS where upper(nombre) = upper(i_estado)
     and tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = 'FORMULARIO_BARS') );
 if l_conteo_filas = 0 then
   o_return := 0;
   o_mensaje := 'El formulario no se encuentra en el estado ' || i_estado || '. ';
 else
   o_return := 1;
   o_mensaje := 'El formulario se encuentra en el estado ' || i_estado || '. ';
 end if;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   o_return := 0;
   o_mensaje := 'No se pudo validar si el formulario se encuentra en el estado ' || i_estado || '.
     Hay un problema de parametrización de estados. Contacte al administrador de la aplicación. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_FORM_BARS. Contacte al administrador
     de la aplicación. ' || SQLCODE || ' - ' || SQLERRM || ' ');
  END Val_estado_form_bars;


 -----------------------------------------------------------------------------------------------------------------


  PROCEDURE Val_encuesta_existe   (i_evaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
                                    i_ano IN PERIODO_BARS.ANO%type,
                                    i_tipo IN FORMULARIO_BARS.TIPO%type, 
                                    o_return OUT NUMBER,
                                    o_mensaje OUT VARCHAR2) AS

 l_conteo_filas NUMBER;
 l_mensaje VARCHAR2(4000);
BEGIN
 --Validar que el periodo no sea superior al año actual (REVISAR SI ES NECESARIO EN FASE II)
 if ( i_ano > extract( YEAR from sysdate ) ) then
   o_return := 0;
   o_mensaje := 'Aun no existen encuestas para el periodo ' || i_ano || '. ';
 else --El periodo es igual o inferior al año actual
   select count(*) into l_conteo_filas
     from FORMULARIO_BARS
     where evaluado = i_evaluado and periodo = i_ano and i_tipo = tipo;
   if l_conteo_filas = 0 then --La evaluación no existe
     o_return := 0;
     o_mensaje := 'El trabajador identificado con el número ' || i_evaluado || ' no tiene una evaluación de tipo ' || i_tipo || ' en el periodo '
       || i_ano || '. ';
   else
     o_return := 1;
     o_mensaje := 'El trabajador identificado con el número ' || i_evaluado || ' tiene una evaluación de tipo ' || i_tipo || ' en el periodo '
       || i_ano || '. ';
   end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    o_return := 0;
    o_mensaje := 'Error inesperado en VAL_ENCUESTA_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
  END Val_encuesta_existe;


 -----------------------------------------------------------------------------------------------------------------


  PROCEDURE Cambiar_estado_form_bars (i_formulario IN FORMULARIO_BARS.ID%TYPE, 
                                    i_estado IN ESTADO_BARS.NOMBRE%TYPE) AS
 l_conteo_filas NUMBER;
 l_tipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 l_estado ESTADO_BARS.ID%TYPE;
BEGIN
  --Verificar que el formulario exista
  select count(*) into l_conteo_filas
  from FORMULARIO_BARS
  where id = i_formulario;
  if l_conteo_filas = 0 then --formulario inexistente
    null;
  else
    --Verificar tipo_estado_bars = 'FORMULARIO_BARS'
    select count(*) into l_conteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = 'FORMULARIO_BARS';
    if l_conteo_filas >= 1 then --El tipo_estado_bars sí existe
      SELECT id into l_tipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = 'FORMULARIO_BARS' and rownum = 1;
    else --El tipo_estado_bars no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into l_tipo_estado
        from TIPO_ESTADO_BARS;
      INSERT INTO TIPO_ESTADO_BARS (id, nombre)
           values ( l_tipo_estado, 'FORMULARIO_BARS' );
    end if;
    --Verificar estado deseado
    SELECT count(*) into l_conteo_filas
      from ESTADO_BARS
      where tipo_estado = l_tipo_estado and upper(nombre) = upper(i_estado);
    if l_conteo_filas >= 1 then --El estado deseado existe
      SELECT id into l_estado
        from ESTADO_BARS
        where tipo_estado = l_tipo_estado and upper(nombre) = upper(i_estado) and rownum = 1;
    else --El estado deseado no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into l_estado
        from ESTADO_BARS;
      INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
        values ( l_estado, upper(i_estado), l_tipo_estado );
    end if;
    --Actualizar el estado del formulario  
    UPDATE FORMULARIO_BARS SET estado = l_estado, fecha_evaluacion = trunc(sysdate) where id = i_formulario;
  end if;
EXCEPTION
  when others then
    raise_application_error(-20000,'Un error inesperado ocurrió en CAMBIO_ESTADO_FORMS_BARS. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
  END Cambiar_estado_form_bars;

  -----------------------------------------------------------------------------------------------------------------


  FUNCTION Determinar_promedio_encuesta (i_formulario IN FORMULARIO_BARS.ID%TYPE) RETURN NUMBER AS
  l_promedio NUMBER;
BEGIN

 SELECT NVL( AVG(valor),0 ) into l_promedio
   from RESPUESTA
   where seccion_formulario IN (select id from seccion_formulario where formulario = i_formulario)
   and valor != 0;
   --Persiste la informacion en FORMULARIO_BARS
   update formulario_bars set media_comportamientos= round(l_promedio,4) where id=i_formulario;

 RETURN round(l_promedio,4);
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PROMEDIO_ENCUESTA. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
  END Determinar_promedio_encuesta;

    -----------------------------------------------------------------------------------------------------------------

  FUNCTION Val_editar_encuesta    (i_formulario IN FORMULARIO_BARS.ID%TYPE) 
                                    RETURN BOOLEAN  AS
  l_editar_form varchar2(2); 
  l_periodo number;
  BEGIN

  Select PERIODO into l_periodo
  from FORMULARIO_BARS 
  where id=i_formulario;

  select EDITAR_FORM_BARS into l_editar_form 
  from PERIODO_BARS 
  where ANO=l_periodo;

  IF l_editar_form= 'S' THEN 
  RETURN TRUE;
  END IF;
    RETURN FALSE;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN 
    RETURN FALSE;
  END Val_editar_encuesta;

END EVALUACION_PA_PCK;
/