CREATE OR REPLACE PACKAGE body codensa_gth.EMAIL_PCK IS
   procedure enviar_correo(
      receiver    IN VARCHAR2,
      msg         IN VARCHAR2,
      subject     IN VARCHAR2 )  AS
  l_connection utl_smtp.connection;
  smtp_server constant varchar2(45) := 'smtp.gmail.com';
 smtp_port constant number := 587;
 local_wallet_dir constant varchar2(200):= 'file:/rdsdbdata/userdirs/01/';
 local_wallet_pass constant varchar2(45) :='S1nerware';
 local_sender constant varchar2(45) :='gth.grupoenel@gmail.com';
 local_sender_pass constant varchar2(45) :='S1nerware';
   l_boundary    VARCHAR2(50) := '----=*#abc1234321cba#*=';
    BEGIN
       l_connection := utl_smtp.open_connection(
 host=>smtp_server,port=>smtp_port,wallet_path=>local_wallet_dir,wallet_password=>local_wallet_pass,secure_connection_before_smtp=>false);
 utl_smtp.ehlo( l_connection,smtp_server);
      utl_smtp.starttls(l_connection);
      utl_smtp.auth(l_connection, local_sender, local_sender_pass, 'PLAIN');
      utl_smtp.mail(l_connection , local_sender);
      utl_smtp.rcpt(l_connection , receiver);
      utl_smtp.open_data(l_connection);
      utl_smtp.write_data(l_connection, 'Date: ' || TO_CHAR(sysdate, 'DD-MON-YYYY HH24:MI:SS') || Chr(13));
      utl_smtp.write_data(l_connection, 'From: ' || local_sender || Chr(13));
      utl_smtp.write_data(l_connection, 'Subject: ' || subject || Chr(13));
      utl_smtp.write_data(l_connection, 'To: ' || receiver || Chr(13));
      --utl_smtp.write_data(l_connection, '' || Chr(13));
      utl_smtp.write_data(l_connection, 'MIME-Version: 1.0' || UTL_TCP.crlf);
      utl_smtp.write_data(l_connection, 'Content-Type: multipart/alternative; boundary="' || l_boundary || '"' || UTL_TCP.crlf || UTL_TCP.crlf);
      utl_smtp.write_data(l_connection, '--' || l_boundary || UTL_TCP.crlf);
      utl_smtp.write_data(l_connection, 'Content-Type: text/html; charset="UTF-8"' || UTL_TCP.crlf || UTL_TCP.crlf);
      utl_smtp.write_data(l_connection, msg || Chr(13));
      utl_smtp.write_data(l_connection, '--' || l_boundary || '--' || UTL_TCP.crlf);
      utl_smtp.close_data(l_connection);
      utl_smtp.quit(l_connection);
    EXCEPTION  
      WHEN UTL_SMTP.TRANSIENT_ERROR THEN
      htp.p('Transient');   
      htp.p(DBMS_UTILITY.format_error_stack);   
      htp.p(DBMS_UTILITY.format_error_backtrace);    
     WHEN OTHERS THEN
      htp.p('Error backtrace at top level:');   
      htp.p(DBMS_UTILITY.format_error_stack);   
      htp.p(DBMS_UTILITY.format_error_backtrace); 
  END enviar_correo;
--------------------------------------------------------------------------------  
END EMAIL_PCK;
/