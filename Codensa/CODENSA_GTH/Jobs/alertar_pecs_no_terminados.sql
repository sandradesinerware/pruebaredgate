BEGIN DBMS_SCHEDULER.CREATE_JOB(
   job_name		=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,job_action		=> 'begin
  ADMIN_PEC_PCK.GESTION_AUTOMATICA_PEC;
end;'
   ,job_type		=> 'PLSQL_BLOCK'
   ,repeat_interval	=> 'FREQ=DAILY;BYHOUR=6;BYMINUTE=0;BYSECOND=0'
   ,start_date		=> TO_DATE('2020-4-13 6:0:0', 'yyyy-mm-dd hh24:mi:ss')
   ,job_class		=> 'DEFAULT_JOB_CLASS'
   ,comments		=> 'Ejecuta el orquestador de tareas automáticas para PEC como notificaciones o ejecución automática, dependiendo el número de días transcurridos y el estado de cada PEC.'
   ,auto_drop		=> FALSE
   ,enabled		=> TRUE
);
-- Altering

DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'job_priority'
   ,value		=> 3);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'job_weight'
   ,value		=> 1);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'schedule_limit'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'max_run_duration'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'max_runs'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'max_failures'
);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'logging_level'
   ,value		=> DBMS_SCHEDULER.LOGGING_OFF);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'restartable'
   ,value		=> FALSE);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'stop_on_window_close'
   ,value		=> FALSE);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'raise_events'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'instance_id'
);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth."alertar_pecs_no_terminados"'
   ,attribute		=> 'allow_runs_in_restricted_mode'
   ,value		=> FALSE);

END;
/