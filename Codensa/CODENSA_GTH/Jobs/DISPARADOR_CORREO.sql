BEGIN DBMS_SCHEDULER.CREATE_JOB(
   job_name		=> 'codensa_gth.disparador_correo'
   ,job_action		=> 'begin
NOTIFICACION_PCK.DISPARADOR_CORREO;
end;'
   ,job_type		=> 'PLSQL_BLOCK'
   ,repeat_interval	=> 'FREQ=SECONDLY;BYSECOND=10'
   ,start_date		=> TO_DATE('2019-7-22 22:35:33', 'yyyy-mm-dd hh24:mi:ss')
   ,job_class		=> 'DEFAULT_JOB_CLASS'
   ,auto_drop		=> FALSE
   ,enabled		=> TRUE
);
-- Altering

DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'job_priority'
   ,value		=> 3);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'job_weight'
   ,value		=> 1);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'schedule_limit'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'max_run_duration'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'max_runs'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'max_failures'
);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'logging_level'
   ,value		=> DBMS_SCHEDULER.LOGGING_OFF);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'restartable'
   ,value		=> FALSE);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'stop_on_window_close'
   ,value		=> FALSE);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'raise_events'
);


DBMS_SCHEDULER.SET_ATTRIBUTE_NULL(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'instance_id'
);


DBMS_SCHEDULER.SET_ATTRIBUTE(
   name			=> 'codensa_gth.disparador_correo'
   ,attribute		=> 'allow_runs_in_restricted_mode'
   ,value		=> FALSE);

END;
/