CREATE TABLE codensa_gth.bars_resmespdi (
  "ID" NUMBER NOT NULL,
  periodo NUMBER(4) NOT NULL,
  numero_asignado NUMBER NOT NULL,
  nombre_completo VARCHAR2(200 BYTE),
  estancias VARCHAR2(4000 BYTE),
  mentoring VARCHAR2(4000 BYTE),
  proyectos_jefe VARCHAR2(4000 BYTE),
  proyectos_miembro VARCHAR2(4000 BYTE),
  formacion VARCHAR2(4000 BYTE),
  autodesarrollo VARCHAR2(4000 BYTE),
  libre_generica VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS'
);