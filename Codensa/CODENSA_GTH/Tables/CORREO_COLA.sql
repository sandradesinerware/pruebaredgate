CREATE TABLE codensa_gth.correo_cola (
  "ID" NUMBER NOT NULL,
  destinatario VARCHAR2(4000 BYTE),
  asunto VARCHAR2(4000 BYTE),
  cuerpo CLOB,
  fecha_ingreso DATE,
  fecha_salida DATE,
  enviado CHAR,
  CONSTRAINT correo_cola_pk PRIMARY KEY ("ID")
);