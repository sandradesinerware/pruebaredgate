CREATE TABLE codensa_gth.respuesta_360 (
  "ID" NUMBER NOT NULL,
  seccion_360 NUMBER,
  plantilla_pregunta NUMBER,
  valor NUMBER(1),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  formulario NUMBER,
  CONSTRAINT respuesta_360_pk PRIMARY KEY ("ID"),
  CONSTRAINT respuesta_360_formulario_fk FOREIGN KEY (formulario) REFERENCES codensa_gth.formulario_360 ("ID"),
  CONSTRAINT respuesta_360_plan_preg_fk FOREIGN KEY (plantilla_pregunta) REFERENCES codensa_gth.plantilla_preg_360 ("ID"),
  CONSTRAINT respuesta_360_seccion_fk FOREIGN KEY (seccion_360) REFERENCES codensa_gth.seccion_360 ("ID")
);