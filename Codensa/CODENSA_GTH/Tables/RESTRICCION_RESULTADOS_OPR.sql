CREATE TABLE codensa_gth.restriccion_resultados_opr (
  "ID" NUMBER NOT NULL,
  periodo NUMBER,
  tipo_target NUMBER,
  resultado_minimo NUMBER,
  resultado_maximo NUMBER,
  tipo_curva NUMBER,
  CONSTRAINT restriccion_resultados_opr_pk PRIMARY KEY ("ID"),
  CONSTRAINT restriccion_resultados_op_fk1 FOREIGN KEY (periodo) REFERENCES codensa_gth.bars_perobj (ano),
  CONSTRAINT restriccion_resultados_op_fk2 FOREIGN KEY (tipo_target) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT restriccion_resultados_op_fk3 FOREIGN KEY (tipo_curva) REFERENCES codensa_gth.tipo ("ID")
);