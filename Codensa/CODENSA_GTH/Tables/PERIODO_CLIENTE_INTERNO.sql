CREATE TABLE codensa_gth.periodo_cliente_interno (
  "ID" NUMBER NOT NULL,
  ano NUMBER,
  estado VARCHAR2(2 BYTE),
  aud_fecha_creacion DATE,
  aud_creado_por VARCHAR2(4000 BYTE),
  aud_fecha_actualizacion DATE,
  aud_actualizado_por VARCHAR2(4000 BYTE),
  aud_terminal_actualizacion VARCHAR2(4000 BYTE),
  periodo VARCHAR2(4000 BYTE),
  CONSTRAINT periodo_cliente_interno_pk PRIMARY KEY ("ID")
);