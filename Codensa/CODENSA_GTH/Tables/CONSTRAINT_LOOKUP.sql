CREATE TABLE codensa_gth.constraint_lookup (
  constraint_lookup_id NUMBER NOT NULL,
  constraint_name VARCHAR2(30 BYTE),
  error_message VARCHAR2(600 BYTE),
  page_item_name VARCHAR2(255 BYTE),
  tab_form_col_alias VARCHAR2(255 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT constraint_lookup_pk PRIMARY KEY (constraint_lookup_id),
  CONSTRAINT constraint_lookup_uk UNIQUE (constraint_name)
);