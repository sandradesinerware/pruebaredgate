CREATE TABLE codensa_gth.temp_trabajador_jurisdiccion (
  asignado NUMBER NOT NULL,
  nombre_completo VARCHAR2(4000 BYTE),
  centro_trabajo VARCHAR2(4000 BYTE),
  jurisdiccion VARCHAR2(4000 BYTE),
  CONSTRAINT temp_trabajador_jurisdicci_pk PRIMARY KEY (asignado) USING INDEX codensa_gth.asignado_pk
);