CREATE TABLE codensa_gth.pec_preguntas (
  "ID" NUMBER NOT NULL,
  enunciado VARCHAR2(4000 BYTE) NOT NULL,
  numero NUMBER,
  estado CHAR DEFAULT 'A' NOT NULL,
  CONSTRAINT pec_preguntas_evaluacion_pk PRIMARY KEY ("ID")
);
COMMENT ON COLUMN codensa_gth.pec_preguntas.estado IS 'A o I ';