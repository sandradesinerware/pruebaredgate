CREATE TABLE codensa_gth.bars_traevaobj (
  "ID" NUMBER NOT NULL,
  trabajador NUMBER,
  bars_perobj NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_traevaobj_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_traevaobj_uk UNIQUE (trabajador,bars_perobj),
  CONSTRAINT bars_traevaobj_fk2 FOREIGN KEY (bars_perobj) REFERENCES codensa_gth.bars_perobj (ano)
);