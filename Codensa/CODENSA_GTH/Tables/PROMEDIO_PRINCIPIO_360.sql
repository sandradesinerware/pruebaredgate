CREATE TABLE codensa_gth.promedio_principio_360 (
  "ID" NUMBER NOT NULL,
  tipo_evaluacion VARCHAR2(15 BYTE),
  evaluado NUMBER,
  principio NUMBER,
  valor NUMBER,
  periodo NUMBER,
  seccion NUMBER,
  CONSTRAINT promedio_principio_360_pk PRIMARY KEY ("ID")
);