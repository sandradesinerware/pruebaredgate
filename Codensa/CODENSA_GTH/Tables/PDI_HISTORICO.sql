CREATE TABLE codensa_gth.pdi_historico (
  "ID" NUMBER NOT NULL,
  numero_identificacion NUMBER,
  nombre_trabajador VARCHAR2(500 BYTE),
  curso VARCHAR2(500 BYTE),
  periodo NUMBER,
  CONSTRAINT pdi_historico_pk PRIMARY KEY ("ID")
);