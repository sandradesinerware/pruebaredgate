CREATE TABLE codensa_gth.gen_seccion_evaluacion (
  "ID" NUMBER NOT NULL,
  id_plantilla_evaluacion NUMBER NOT NULL,
  nro_seccion VARCHAR2(50 BYTE),
  nombre VARCHAR2(200 BYTE),
  descripcion VARCHAR2(1000 BYTE),
  CONSTRAINT gen_seccion_evaluacion_pk PRIMARY KEY ("ID"),
  CONSTRAINT gen_seccion_eval_plantilla_fk FOREIGN KEY (id_plantilla_evaluacion) REFERENCES codensa_gth.gen_plantilla_evaluacion ("ID")
);