CREATE TABLE codensa_gth.bars_pospre (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(50 BYTE) NOT NULL,
  periodo NUMBER NOT NULL,
  ubicacion NUMBER(1) NOT NULL,
  restrmin NUMBER,
  restrmax NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_pospre_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_pospre_perpre_fk FOREIGN KEY (periodo) REFERENCES codensa_gth.bars_perpre (ano)
);