CREATE TABLE codensa_gth.bars_fortevaobj (
  formevaobj NUMBER NOT NULL,
  fortact VARCHAR2(1000 BYTE),
  fortdes VARCHAR2(1000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_fortevaobj_pk PRIMARY KEY (formevaobj),
  CONSTRAINT bars_fortevaobj_formevaobj_fk FOREIGN KEY (formevaobj) REFERENCES codensa_gth.bars_formevaobj ("ID")
);