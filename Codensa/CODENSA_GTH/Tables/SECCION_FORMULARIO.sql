CREATE TABLE codensa_gth.seccion_formulario (
  formulario NUMBER NOT NULL,
  "ID" NUMBER NOT NULL,
  seccion NUMBER(2) NOT NULL,
  tipo VARCHAR2(1 BYTE) NOT NULL CONSTRAINT seccion_formulario_tipo_chk1 CHECK (TIPO IN ('I', 'C')),
  resultado NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT seccion_formulario_pk PRIMARY KEY ("ID"),
  CONSTRAINT seccion_formulario_seccion_fk FOREIGN KEY (seccion) REFERENCES codensa_gth.seccion_bars ("ID")
);