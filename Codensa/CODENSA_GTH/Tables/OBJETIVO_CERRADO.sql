CREATE TABLE codensa_gth.objetivo_cerrado (
  "ID" NUMBER NOT NULL,
  codigo VARCHAR2(50 BYTE) NOT NULL,
  descripcion VARCHAR2(4000 BYTE) NOT NULL,
  periodo NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  aud_version NUMBER NOT NULL,
  tipo_curva VARCHAR2(200 BYTE),
  escala_6 VARCHAR2(4000 BYTE),
  escala_8 VARCHAR2(4000 BYTE),
  escala_10 VARCHAR2(4000 BYTE),
  meta VARCHAR2(4000 BYTE),
  resultado NUMBER,
  CONSTRAINT objetivo_cerrado_pk PRIMARY KEY ("ID"),
  CONSTRAINT objetivo_cerrado_uk UNIQUE (codigo)
);