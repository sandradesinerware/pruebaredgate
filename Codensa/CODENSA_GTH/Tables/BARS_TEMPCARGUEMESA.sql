CREATE TABLE codensa_gth.bars_tempcarguemesa (
  consecutivo NUMBER NOT NULL,
  id_trabajador NUMBER,
  nombres VARCHAR2(30 BYTE),
  apellidos VARCHAR2(30 BYTE),
  cargo VARCHAR2(255 BYTE),
  tipo_trabajador VARCHAR2(30 BYTE),
  area VARCHAR2(255 BYTE),
  id_gestor NUMBER,
  mesa VARCHAR2(100 BYTE),
  resultado VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_tempcarguemesa_pk PRIMARY KEY (consecutivo)
);