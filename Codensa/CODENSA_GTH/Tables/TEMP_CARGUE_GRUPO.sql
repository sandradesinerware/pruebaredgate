CREATE TABLE codensa_gth.temp_cargue_grupo (
  nombre_curso VARCHAR2(4000 BYTE),
  negocio VARCHAR2(4000 BYTE),
  encargado NUMBER(26),
  nombre_visible VARCHAR2(4000 BYTE),
  objetivo VARCHAR2(4000 BYTE),
  tipo VARCHAR2(4000 BYTE),
  nombre_grupo VARCHAR2(4000 BYTE),
  "VISIBLE" VARCHAR2(26 BYTE),
  estado VARCHAR2(12 BYTE),
  tipo_grupo VARCHAR2(4000 BYTE),
  periodo NUMBER(26),
  fecha_inicio VARCHAR2(4000 BYTE),
  fecha_fin VARCHAR2(4000 BYTE),
  cupo_maximo NUMBER(26),
  cupo_minimo NUMBER(26),
  cupo_analisis NUMBER(26),
  id_negocio NUMBER(26),
  foco_holding VARCHAR2(4000 BYTE),
  foco_interno VARCHAR2(4000 BYTE),
  proveedor VARCHAR2(4000 BYTE),
  horas NUMBER(12),
  no_pdi VARCHAR2(2 BYTE),
  jurisdiccion VARCHAR2(4000 BYTE),
  rol VARCHAR2(4000 BYTE)
);