CREATE TABLE codensa_gth.pdi_foco (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(4000 BYTE),
  tipo NUMBER,
  competencia NUMBER,
  CONSTRAINT pdi_foco_pk PRIMARY KEY ("ID"),
  CONSTRAINT competencia_foco_fk FOREIGN KEY (competencia) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT tipo_foco_fk FOREIGN KEY (tipo) REFERENCES codensa_gth.tipo ("ID")
);