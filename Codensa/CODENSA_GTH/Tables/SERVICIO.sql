CREATE TABLE codensa_gth.servicio (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(4000 BYTE),
  descripcion VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE,
  aud_creado_por VARCHAR2(4000 BYTE),
  aud_fecha_actualizacion DATE,
  aud_actualizado_por VARCHAR2(4000 BYTE),
  aud_terminal_actualizacion VARCHAR2(4000 BYTE),
  division NUMBER,
  codigo VARCHAR2(4000 BYTE),
  CONSTRAINT servicio_pk PRIMARY KEY ("ID"),
  CONSTRAINT servicio_dependencia_fk FOREIGN KEY (division) REFERENCES codensa_gth.dependencia ("ID")
);