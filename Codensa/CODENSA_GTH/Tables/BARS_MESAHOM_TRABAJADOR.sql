CREATE TABLE codensa_gth.bars_mesahom_trabajador (
  trabajador NUMBER NOT NULL,
  mesa NUMBER NOT NULL,
  ajuste NUMBER,
  por_desarrollar VARCHAR2(500 BYTE),
  fortaleza VARCHAR2(500 BYTE),
  comentarios VARCHAR2(500 BYTE),
  pct_calibrado NUMBER,
  pdi_sugerido VARCHAR2(500 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_mesahom_trabajador_pk PRIMARY KEY (trabajador,mesa),
  CONSTRAINT bars_mesahom_trabajador_b_fk FOREIGN KEY (mesa) REFERENCES codensa_gth.bars_mesahom ("ID")
);