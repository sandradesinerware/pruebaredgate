CREATE TABLE codensa_gth.pregunta (
  "ID" NUMBER NOT NULL,
  numero NUMBER,
  enunciado VARCHAR2(4000 BYTE),
  estado NUMBER,
  aud_fecha_creacion DATE,
  aud_creado_por VARCHAR2(4000 BYTE),
  aud_fecha_actualizacion DATE,
  aud_actualizado_por VARCHAR2(4000 BYTE),
  aud_terminal_actualizacion DATE,
  CONSTRAINT pregunta_pk PRIMARY KEY ("ID")
);