CREATE TABLE codensa_gth.pct_consecucion_bars (
  pct_consecucion NUMBER NOT NULL,
  media_comp_inf NUMBER NOT NULL,
  media_comp_sup NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  "ID" NUMBER NOT NULL,
  tipo_id NUMBER DEFAULT 61 NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT pct_consecucion_bars_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_pct_periodo_fk FOREIGN KEY (periodo) REFERENCES codensa_gth.bars_perobj (ano)
);