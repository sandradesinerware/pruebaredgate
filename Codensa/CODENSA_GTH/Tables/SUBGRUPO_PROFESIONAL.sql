CREATE TABLE codensa_gth.subgrupo_profesional (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(4000 BYTE),
  jefe CHAR,
  CONSTRAINT subgrupo_profesional_pk PRIMARY KEY ("ID")
);