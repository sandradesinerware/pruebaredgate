CREATE TABLE codensa_gth.bars_mesahom (
  "ID" NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  descripcion VARCHAR2(100 BYTE) NOT NULL,
  ajuste_superior NUMBER,
  ajuste_inferior NUMBER,
  comentarios VARCHAR2(500 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_mesahom_pk PRIMARY KEY ("ID")
);