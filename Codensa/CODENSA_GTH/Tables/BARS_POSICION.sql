CREATE TABLE codensa_gth.bars_posicion (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(20 BYTE) NOT NULL,
  pct_inf NUMBER NOT NULL,
  pct_sup NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  tipo_id NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_posicion_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_posicion_periodo_fk FOREIGN KEY (periodo) REFERENCES codensa_gth.bars_perobj (ano)
);