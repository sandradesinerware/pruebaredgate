CREATE TABLE codensa_gth.pec_evaluacion (
  "ID" NUMBER NOT NULL,
  id_pec NUMBER,
  id_trabajador_evaluador NUMBER,
  id_gestor_evaluado NUMBER,
  promedio_evaluacion NUMBER DEFAULT 0,
  CONSTRAINT pec_evaluacion_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_evaluacion_fk1 FOREIGN KEY (id_pec) REFERENCES codensa_gth.pec ("ID")
);
COMMENT ON COLUMN codensa_gth.pec_evaluacion.promedio_evaluacion IS 'Promedio aritmético de la calificación de todas las preguntas';