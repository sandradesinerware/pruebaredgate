CREATE TABLE codensa_gth.bars_tiporesp_opcion (
  "ID" NUMBER NOT NULL,
  etiqueta VARCHAR2(50 BYTE) NOT NULL,
  valor NUMBER,
  tiporesp NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_tiporesp_detalle_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_tiporesp_opcion_fk FOREIGN KEY (tiporesp) REFERENCES codensa_gth.bars_tiporesp ("ID")
);