CREATE TABLE codensa_gth.pec_evaluacion_respuestas (
  "ID" NUMBER NOT NULL,
  id_evaluacion NUMBER,
  id_pregunta NUMBER,
  calificacion_pregunta NUMBER,
  comentarios VARCHAR2(4000 BYTE),
  CONSTRAINT pec_evaluacion_preguntas_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_evaluacion_pregunta_fk1 FOREIGN KEY (id_evaluacion) REFERENCES codensa_gth.pec_evaluacion ("ID"),
  CONSTRAINT pec_evaluacion_pregunta_fk2 FOREIGN KEY (id_pregunta) REFERENCES codensa_gth.pec_preguntas ("ID")
);