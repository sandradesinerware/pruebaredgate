CREATE TABLE codensa_gth.pdi_periodo (
  "ID" NUMBER NOT NULL,
  ano NUMBER,
  nombre VARCHAR2(500 BYTE),
  estado NUMBER,
  fecha_inicio DATE,
  fecha_fin DATE,
  asistencia NUMBER,
  CONSTRAINT pdi_periodo_pk PRIMARY KEY ("ID"),
  CONSTRAINT pdi_periodo_estado_fk FOREIGN KEY (estado) REFERENCES codensa_gth.estado_bars ("ID")
);