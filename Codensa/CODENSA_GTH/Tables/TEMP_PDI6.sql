CREATE TABLE codensa_gth.temp_pdi6 (
  "ID" NUMBER NOT NULL,
  justificacion VARCHAR2(4000 BYTE),
  curso VARCHAR2(4000 BYTE),
  evaluado NUMBER,
  comportamiento VARCHAR2(4000 BYTE),
  estado VARCHAR2(4000 BYTE),
  comentarios VARCHAR2(4000 BYTE),
  fecha_inicio VARCHAR2(300 BYTE),
  fecha_fin VARCHAR2(300 BYTE),
  CONSTRAINT temp_pdi6_pk PRIMARY KEY ("ID")
);