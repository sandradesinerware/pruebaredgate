CREATE TABLE codensa_gth.plantilla_preguntas (
  "ID" NUMBER NOT NULL,
  plantilla_formulario NUMBER,
  seccion NUMBER(2),
  codigo_pregunta NUMBER(2),
  fortaleza VARCHAR2(500 BYTE),
  solidez VARCHAR2(500 BYTE),
  requiere_mejorar VARCHAR2(500 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  nombre_pregunta VARCHAR2(500 BYTE),
  CONSTRAINT plantilla_preguntas_pk PRIMARY KEY ("ID"),
  CONSTRAINT plantilla_preguntas_plant_fk FOREIGN KEY (plantilla_formulario) REFERENCES codensa_gth.plantilla_formulario ("ID"),
  CONSTRAINT plantilla_pregunt_seccion_fk FOREIGN KEY (seccion) REFERENCES codensa_gth.seccion_bars ("ID")
);