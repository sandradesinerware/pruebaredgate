CREATE TABLE codensa_gth.bars_grupo_pdi (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(100 BYTE) NOT NULL,
  aud_fecha_creacion VARCHAR2(100 BYTE),
  aud_fecha_actualizacion VARCHAR2(100 BYTE),
  aud_creado_por VARCHAR2(100 BYTE),
  aud_actualizado_por VARCHAR2(100 BYTE),
  permisos NUMBER,
  aud_terminal_actualizacion VARCHAR2(100 BYTE),
  tipo_grupo NUMBER
);