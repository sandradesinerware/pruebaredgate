CREATE TABLE codensa_gth.bars_facpre (
  "ID" NUMBER NOT NULL,
  numero NUMBER NOT NULL,
  descripcion VARCHAR2(200 BYTE) NOT NULL,
  nivel NUMBER(1) NOT NULL,
  padre NUMBER NOT NULL,
  tiporesp NUMBER,
  etiqueta_numero VARCHAR2(20 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_facpre_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_facpre_facpre_fk FOREIGN KEY (padre) REFERENCES codensa_gth.bars_facpre ("ID"),
  CONSTRAINT bars_facpre_tiporesp_fk FOREIGN KEY (tiporesp) REFERENCES codensa_gth.bars_tiporesp ("ID")
);