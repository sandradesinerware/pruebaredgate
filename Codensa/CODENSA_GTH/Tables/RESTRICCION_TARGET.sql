CREATE TABLE codensa_gth.restriccion_target (
  "ID" NUMBER NOT NULL,
  id_target NUMBER NOT NULL,
  id_tipo_evaluacion NUMBER NOT NULL,
  porcentaje_total NUMBER NOT NULL,
  pct_ind_min NUMBER NOT NULL,
  pct_ind_max NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  CONSTRAINT restriccion_target_pk PRIMARY KEY ("ID"),
  CONSTRAINT restriccion_targt_te_fk FOREIGN KEY (id_tipo_evaluacion) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT restriccion_targt_t_fk FOREIGN KEY (id_target) REFERENCES codensa_gth.tipo ("ID")
);