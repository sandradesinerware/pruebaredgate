CREATE TABLE codensa_gth.feed_periodo (
  "ID" NUMBER NOT NULL,
  ano NUMBER,
  estado NUMBER,
  CONSTRAINT feed_periodo_pk PRIMARY KEY ("ID"),
  CONSTRAINT feed_periodo_estado_fk FOREIGN KEY (estado) REFERENCES codensa_gth.tipo ("ID")
);