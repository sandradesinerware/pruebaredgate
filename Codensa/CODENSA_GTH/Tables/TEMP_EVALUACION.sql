CREATE TABLE codensa_gth.temp_evaluacion (
  codigo VARCHAR2(20 BYTE),
  identificacion NUMBER,
  "LOG" VARCHAR2(4000 BYTE),
  "ID" NUMBER NOT NULL,
  CONSTRAINT temp_evaluacion_pk PRIMARY KEY ("ID")
);