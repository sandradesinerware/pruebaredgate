CREATE TABLE codensa_gth.pdi_concepto (
  "ID" NUMBER NOT NULL,
  id_pdi NUMBER NOT NULL,
  id_estado_pdi NUMBER NOT NULL,
  comentario VARCHAR2(4000 BYTE),
  tipo_concepto NUMBER NOT NULL,
  responsable_aprobacion NUMBER NOT NULL,
  CONSTRAINT pdi_concepto_pk PRIMARY KEY ("ID"),
  CONSTRAINT pdi_concepto_pdi_fk FOREIGN KEY (id_pdi) REFERENCES codensa_gth.bars_pdi ("ID"),
  CONSTRAINT pdi_concepto_tipo_est_fk FOREIGN KEY (id_estado_pdi) REFERENCES codensa_gth.estado_bars ("ID"),
  CONSTRAINT pdi_concepto_tipo_fk FOREIGN KEY (tipo_concepto) REFERENCES codensa_gth.tipo ("ID")
);