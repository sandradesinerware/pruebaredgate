CREATE TABLE codensa_gth.estado_bars (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(50 BYTE) NOT NULL,
  tipo_estado NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  tipo_concepto NUMBER,
  CONSTRAINT estado_bars_pk PRIMARY KEY ("ID"),
  CONSTRAINT estado_bars_tipo_concepto_fk FOREIGN KEY (tipo_concepto) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT estado_bars_tipo_estado_b_fk FOREIGN KEY (tipo_estado) REFERENCES codensa_gth.tipo_estado_bars ("ID")
);