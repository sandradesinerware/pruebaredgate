CREATE TABLE codensa_gth.feed_traeva_periodo (
  "ID" NUMBER NOT NULL,
  id_periodo NUMBER,
  id_trabajador NUMBER,
  CONSTRAINT feed_traeva_periodo_pk PRIMARY KEY ("ID"),
  CONSTRAINT feed_traeva_periodo_periodo_fk FOREIGN KEY (id_periodo) REFERENCES codensa_gth.feed_periodo ("ID")
);