CREATE TABLE codensa_gth.bars_respformevapre (
  formevapre NUMBER NOT NULL,
  facpre NUMBER NOT NULL,
  tiporesp_opcion NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_respformevapre_pk PRIMARY KEY (formevapre,facpre),
  CONSTRAINT bars_respformevapre_facpre_fk FOREIGN KEY (facpre) REFERENCES codensa_gth.bars_facpre ("ID"),
  CONSTRAINT bars_respformevapre_form_fk FOREIGN KEY (formevapre) REFERENCES codensa_gth.bars_formevapre ("ID"),
  CONSTRAINT bars_respformevapre_tiporesp FOREIGN KEY (tiporesp_opcion) REFERENCES codensa_gth.bars_tiporesp_opcion ("ID")
);