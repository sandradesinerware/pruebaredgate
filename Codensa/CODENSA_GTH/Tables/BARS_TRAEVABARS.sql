CREATE TABLE codensa_gth.bars_traevabars (
  "ID" NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  trabajador NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_traevabars_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_traevabars_con UNIQUE (periodo,trabajador),
  CONSTRAINT bars_traevabars_fk FOREIGN KEY (periodo) REFERENCES codensa_gth.periodo_bars (ano)
);