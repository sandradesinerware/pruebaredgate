CREATE TABLE codensa_gth.respuesta (
  "ID" NUMBER NOT NULL,
  seccion_formulario NUMBER,
  plantilla_pregunta NUMBER,
  valor NUMBER(1),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT respuesta_pk PRIMARY KEY ("ID"),
  CONSTRAINT respuesta_plantilla_pregu_fk FOREIGN KEY (plantilla_pregunta) REFERENCES codensa_gth.plantilla_preguntas ("ID"),
  CONSTRAINT respuesta_seccion_formula_fk FOREIGN KEY (seccion_formulario) REFERENCES codensa_gth.seccion_formulario ("ID")
);