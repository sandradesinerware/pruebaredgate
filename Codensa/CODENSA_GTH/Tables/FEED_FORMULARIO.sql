CREATE TABLE codensa_gth.feed_formulario (
  "ID" NUMBER NOT NULL,
  id_plantilla_evaluacion NUMBER NOT NULL,
  tipo_formulario NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  id_colaborador NUMBER NOT NULL,
  id_gestor NUMBER NOT NULL,
  estado NUMBER NOT NULL,
  CONSTRAINT feed_formulario_pk PRIMARY KEY ("ID"),
  CONSTRAINT feed_formulario_estado_fk FOREIGN KEY (estado) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT feed_formulario_periodo_fk FOREIGN KEY (periodo) REFERENCES codensa_gth.feed_periodo ("ID"),
  CONSTRAINT feed_formulario_plantilla_fk FOREIGN KEY (id_plantilla_evaluacion) REFERENCES codensa_gth.gen_plantilla_evaluacion ("ID"),
  CONSTRAINT feed_formulario_tipo_form_fk FOREIGN KEY (tipo_formulario) REFERENCES codensa_gth.tipo ("ID")
);