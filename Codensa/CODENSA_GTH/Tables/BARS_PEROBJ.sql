CREATE TABLE codensa_gth.bars_perobj (
  ano NUMBER NOT NULL,
  estado NUMBER NOT NULL,
  fecha_pago_bono DATE,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  fecha_aprobacion DATE,
  fecha_concertacion DATE,
  nombre_visible VARCHAR2(400 BYTE),
  fecha_inicio DATE,
  fecha_fin DATE,
  CONSTRAINT bars_perobj_pk PRIMARY KEY (ano),
  CONSTRAINT bars_perobj_fk FOREIGN KEY (estado) REFERENCES codensa_gth.estado_bars ("ID")
);