CREATE TABLE codensa_gth.gen_pregunta_evaluacion (
  "ID" NUMBER NOT NULL,
  "ID_SECCION_EVALUACIÓN" NUMBER NOT NULL,
  nro_pregunta VARCHAR2(50 BYTE),
  nombre_pregunta VARCHAR2(1000 BYTE),
  tipo_respuesta NUMBER NOT NULL,
  CONSTRAINT gen_pregunta_evaluacion_pk PRIMARY KEY ("ID"),
  CONSTRAINT gen_pregunta_evaltipo_res_fk FOREIGN KEY (tipo_respuesta) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT gen_pregunta_eval_seccion_fk FOREIGN KEY ("ID_SECCION_EVALUACIÓN") REFERENCES codensa_gth.gen_seccion_evaluacion ("ID")
);