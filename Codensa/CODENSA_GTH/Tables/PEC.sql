CREATE TABLE codensa_gth.pec (
  "ID" NUMBER NOT NULL,
  id_trabajador NUMBER NOT NULL,
  id_gestor NUMBER NOT NULL,
  id_estado NUMBER DEFAULT 5857 NOT NULL,
  fecha_creacion DATE,
  intensidad_horas NUMBER,
  observaciones VARCHAR2(4000 BYTE),
  motivo_creacion NUMBER,
  CONSTRAINT pec_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_fk1 FOREIGN KEY (id_gestor) REFERENCES codensa_gth.trabajador (numero_identificacion),
  CONSTRAINT pec_fk2 FOREIGN KEY (motivo_creacion) REFERENCES codensa_gth.tipo ("ID")
);