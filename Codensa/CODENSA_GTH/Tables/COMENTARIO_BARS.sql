CREATE TABLE codensa_gth.comentario_bars (
  formulario NUMBER NOT NULL,
  fuente VARCHAR2(1 BYTE) NOT NULL CONSTRAINT comentario_bars_chk1 CHECK (FUENTE IN ('O', 'R')),
  comentario VARCHAR2(4000 BYTE) NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT comentario_bars_pk PRIMARY KEY (formulario,fuente),
  CONSTRAINT comentario_bars_formulari_fk FOREIGN KEY (formulario) REFERENCES codensa_gth.formulario_bars ("ID")
);