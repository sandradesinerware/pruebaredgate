CREATE TABLE codensa_gth.opcion (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(4000 BYTE),
  valor NUMBER,
  CONSTRAINT opcion_pk PRIMARY KEY ("ID")
);