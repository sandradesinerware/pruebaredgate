CREATE TABLE codensa_gth.pec_estado (
  "ID" NUMBER NOT NULL,
  id_pec NUMBER,
  id_tipo_estado NUMBER,
  fecha DATE,
  id_trabajador_responsable NUMBER,
  CONSTRAINT pec_estado_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_estado_pec_fk FOREIGN KEY (id_pec) REFERENCES codensa_gth.pec ("ID"),
  CONSTRAINT pec_estado_tipo_fk FOREIGN KEY (id_tipo_estado) REFERENCES codensa_gth.tipo ("ID")
);