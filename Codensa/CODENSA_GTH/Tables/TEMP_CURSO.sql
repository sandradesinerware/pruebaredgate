CREATE TABLE codensa_gth.temp_curso (
  "ID" NUMBER NOT NULL,
  numero_asignado NUMBER,
  comportamiento VARCHAR2(255 BYTE),
  curso VARCHAR2(255 BYTE),
  justificacion VARCHAR2(255 BYTE),
  id_comportamiento NUMBER,
  id_curso NUMBER,
  CONSTRAINT temp_curso_pk PRIMARY KEY ("ID")
);