CREATE TABLE codensa_gth.temp_nuevo_tipo_regimen (
  "ID" NUMBER NOT NULL,
  identificacion NUMBER NOT NULL,
  tipo_regimen VARCHAR2(100 BYTE),
  CONSTRAINT temp_nuevo_tipo_regimen_pk PRIMARY KEY ("ID")
);