CREATE TABLE codensa_gth.plantilla_formulario (
  "ID" NUMBER NOT NULL,
  target_bpr NUMBER,
  vigente_desde DATE,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT plantilla_formulario_pk PRIMARY KEY ("ID")
);