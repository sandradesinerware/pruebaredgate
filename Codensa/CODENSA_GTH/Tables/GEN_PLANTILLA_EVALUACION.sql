CREATE TABLE codensa_gth.gen_plantilla_evaluacion (
  "ID" NUMBER NOT NULL,
  tipo_plantilla NUMBER NOT NULL,
  vigencia_inicio DATE NOT NULL,
  vigencia_fin DATE,
  CONSTRAINT gen_plantilla_evaluacion_pk PRIMARY KEY ("ID"),
  CONSTRAINT gen_plantilla_eval_tipo_fk FOREIGN KEY (tipo_plantilla) REFERENCES codensa_gth.tipo ("ID")
);