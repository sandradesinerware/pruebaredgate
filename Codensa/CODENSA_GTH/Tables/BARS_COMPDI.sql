CREATE TABLE codensa_gth.bars_compdi (
  "ID" NUMBER NOT NULL,
  activo VARCHAR2(1 BYTE) NOT NULL CONSTRAINT bars_compdi_activo_ck CHECK (ACTIVO IN ('S','N')),
  comportamiento VARCHAR2(255 BYTE),
  id_comportamiento NUMBER NOT NULL,
  id_negocio NUMBER NOT NULL,
  negocio VARCHAR2(500 BYTE),
  nivel VARCHAR2(255 BYTE),
  objetivo VARCHAR2(4000 BYTE),
  tema VARCHAR2(255 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_compdi_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_compdi_uk UNIQUE (tema,id_comportamiento,id_negocio) DISABLE NOVALIDATE
);