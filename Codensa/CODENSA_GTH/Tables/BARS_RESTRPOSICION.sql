CREATE TABLE codensa_gth.bars_restrposicion (
  posicion NUMBER NOT NULL,
  restrmin NUMBER,
  restrmax NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_restrposicion_pk PRIMARY KEY (posicion),
  CONSTRAINT bars_restrposicion_posicion_fk FOREIGN KEY (posicion) REFERENCES codensa_gth.bars_posicion ("ID")
);