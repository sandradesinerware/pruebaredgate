CREATE TABLE codensa_gth.ponderacion_preg_360 (
  "ID" NUMBER NOT NULL,
  evaluado NUMBER NOT NULL,
  plantilla_preg_360 NUMBER NOT NULL,
  ponderado_respuesta NUMBER NOT NULL,
  periodo NUMBER,
  CONSTRAINT table1_pk PRIMARY KEY ("ID")
);