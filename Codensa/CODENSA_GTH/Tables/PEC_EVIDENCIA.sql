CREATE TABLE codensa_gth.pec_evidencia (
  "ID" NUMBER NOT NULL,
  id_archivo NUMBER,
  id_actividad NUMBER NOT NULL,
  CONSTRAINT pec_evidencia_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_evidencia_fk1 FOREIGN KEY (id_actividad) REFERENCES codensa_gth.pec_actividad ("ID")
);