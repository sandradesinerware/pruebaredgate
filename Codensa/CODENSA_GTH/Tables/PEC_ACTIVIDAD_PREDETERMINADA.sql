CREATE TABLE codensa_gth.pec_actividad_predeterminada (
  "ID" NUMBER NOT NULL,
  fecha DATE,
  intensidad VARCHAR2(50 BYTE),
  proceso VARCHAR2(4000 BYTE),
  contenido VARCHAR2(4000 BYTE),
  tipo_entrenamiento NUMBER,
  responsable VARCHAR2(4000 BYTE),
  lugar VARCHAR2(4000 BYTE),
  estado_activo CHAR,
  predeterminada CHAR DEFAULT 'S',
  requiere_evidencia CHAR DEFAULT 'N',
  CONSTRAINT pec_actividad_predetermina_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_actividad_pred_tipo_fk1 FOREIGN KEY (tipo_entrenamiento) REFERENCES codensa_gth.tipo ("ID")
);
COMMENT ON COLUMN codensa_gth.pec_actividad_predeterminada.estado_activo IS 'A o I';
COMMENT ON COLUMN codensa_gth.pec_actividad_predeterminada.predeterminada IS 'S o N';
COMMENT ON COLUMN codensa_gth.pec_actividad_predeterminada.requiere_evidencia IS 'Define si la actividad requiere evidencia para terminar el pec';