CREATE TABLE codensa_gth.ponderacion_seccion_360 (
  "ID" NUMBER NOT NULL,
  evaluado NUMBER NOT NULL,
  seccion_360 NUMBER NOT NULL,
  ponderacion_seccion NUMBER NOT NULL,
  ponderacion_sec_auto NUMBER,
  ponderacion_sec_gestor NUMBER,
  ponderacion_sec_colabo NUMBER,
  periodo NUMBER,
  CONSTRAINT ponderacion_seccion_360_pk PRIMARY KEY ("ID")
);