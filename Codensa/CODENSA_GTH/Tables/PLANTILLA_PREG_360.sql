CREATE TABLE codensa_gth.plantilla_preg_360 (
  "ID" NUMBER NOT NULL,
  plantilla_form_360 NUMBER,
  seccion NUMBER(2),
  codigo_pregunta NUMBER(2),
  texto_pregunta VARCHAR2(500 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  secuencia_pregunta NUMBER,
  principio NUMBER,
  CONSTRAINT plantilla_preg_360_pk PRIMARY KEY ("ID"),
  CONSTRAINT plantilla_preg_360_fk2 FOREIGN KEY (seccion) REFERENCES codensa_gth.seccion_360 ("ID"),
  CONSTRAINT plantilla_preg_360_plant_fk FOREIGN KEY (plantilla_form_360) REFERENCES codensa_gth.plantilla_form_360 ("ID")
);