CREATE TABLE codensa_gth.bars_objetivo (
  "ID" NUMBER NOT NULL,
  objind VARCHAR2(4000 BYTE),
  meta VARCHAR2(4000 BYTE),
  ponderacion NUMBER(*,2) NOT NULL,
  pctconsind NUMBER(*,2),
  pctconspond NUMBER(*,2),
  formevaobj NUMBER NOT NULL,
  objnum NUMBER(*,0) NOT NULL,
  id_categoria NUMBER,
  unidad_medida NUMBER,
  id_tipo NUMBER,
  nombre_objetivo VARCHAR2(4000 BYTE),
  id_tipo_curva NUMBER,
  escala_6 VARCHAR2(4000 BYTE),
  escala_8 VARCHAR2(4000 BYTE),
  escala_10 VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  id_objetivo_cerrado NUMBER,
  CONSTRAINT bars_objetivo_pk PRIMARY KEY ("ID"),
  CONSTRAINT bars_objetivo_bars_formev_fk1 FOREIGN KEY (formevaobj) REFERENCES codensa_gth.bars_formevaobj ("ID"),
  CONSTRAINT bars_objetivo_fk2 FOREIGN KEY (id_objetivo_cerrado) REFERENCES codensa_gth.objetivo_cerrado ("ID")
);