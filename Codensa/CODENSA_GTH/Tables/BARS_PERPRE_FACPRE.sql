CREATE TABLE codensa_gth.bars_perpre_facpre (
  periodo NUMBER NOT NULL,
  "FACTOR" NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_perfac_pk PRIMARY KEY (periodo,"FACTOR"),
  CONSTRAINT bars_perpre_facpre_fac_fk FOREIGN KEY ("FACTOR") REFERENCES codensa_gth.bars_facpre ("ID"),
  CONSTRAINT bars_perpre_facpre_per_fk FOREIGN KEY (periodo) REFERENCES codensa_gth.bars_perpre (ano)
);