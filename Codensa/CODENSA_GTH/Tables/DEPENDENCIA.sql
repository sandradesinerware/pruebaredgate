CREATE TABLE codensa_gth.dependencia (
  "ID" NUMBER NOT NULL,
  nombre VARCHAR2(500 BYTE) NOT NULL,
  estado CHAR(2 BYTE) NOT NULL,
  tipo NUMBER NOT NULL DISABLE,
  aud_fecha_creacion DATE,
  aud_fecha_actualizacion DATE,
  aud_creado_por VARCHAR2(500 BYTE),
  aud_actualizado_por VARCHAR2(500 BYTE),
  aud_terminal_actualizacion VARCHAR2(500 BYTE),
  constante VARCHAR2(4000 BYTE),
  nivel NUMBER,
  CONSTRAINT dependencia_pk PRIMARY KEY ("ID"),
  CONSTRAINT dependencia_fk FOREIGN KEY (tipo) REFERENCES codensa_gth.dependencia ("ID"),
  CONSTRAINT dependencia_nivel_fk FOREIGN KEY (nivel) REFERENCES codensa_gth.tipo ("ID")
);