CREATE TABLE codensa_gth.feed_respuesta (
  "ID" NUMBER NOT NULL,
  id_formulario NUMBER NOT NULL,
  id_pregunta_evaluacion NUMBER NOT NULL,
  respuesta_cerrada NUMBER,
  respuesta_abierta VARCHAR2(4000 BYTE),
  CONSTRAINT feed_respuesta_pk PRIMARY KEY ("ID"),
  CONSTRAINT feed_respuesta_cerrada_fk FOREIGN KEY (respuesta_cerrada) REFERENCES codensa_gth.tipo ("ID"),
  CONSTRAINT feed_respuesta_formulario_fk FOREIGN KEY (id_formulario) REFERENCES codensa_gth.feed_formulario ("ID"),
  CONSTRAINT feed_respuesta_pregunta_fk FOREIGN KEY (id_pregunta_evaluacion) REFERENCES codensa_gth.gen_pregunta_evaluacion ("ID")
);