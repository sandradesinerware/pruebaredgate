CREATE TABLE codensa_gth.temp_servicio (
  gerencia VARCHAR2(4000 BYTE),
  subgerencia VARCHAR2(4000 BYTE),
  division VARCHAR2(4000 BYTE),
  servicio VARCHAR2(4000 BYTE),
  codigo VARCHAR2(4000 BYTE),
  "LOG" VARCHAR2(4000 BYTE),
  "ID" NUMBER NOT NULL,
  CONSTRAINT temp_servicio_pk PRIMARY KEY ("ID")
);