CREATE TABLE codensa_gth.cargue_concer_obj (
  "ID" NUMBER NOT NULL,
  codigo_objetivo VARCHAR2(50 BYTE) NOT NULL,
  evaluado NUMBER NOT NULL,
  periodo NUMBER NOT NULL,
  aud_version NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  persistido CHAR DEFAULT 'N' NOT NULL,
  ponderacion NUMBER DEFAULT 0 NOT NULL,
  CONSTRAINT cargue_concer_obj_fk2 FOREIGN KEY (codigo_objetivo) REFERENCES codensa_gth.objetivo_cerrado (codigo)
);