CREATE TABLE codensa_gth.comentario_360 (
  formulario NUMBER NOT NULL,
  comentario VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT comentario_360_pk PRIMARY KEY (formulario),
  CONSTRAINT comentario_360_formulari_fk FOREIGN KEY (formulario) REFERENCES codensa_gth.formulario_360 ("ID")
);