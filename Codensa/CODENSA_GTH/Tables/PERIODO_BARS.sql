CREATE TABLE codensa_gth.periodo_bars (
  ano NUMBER NOT NULL,
  estado NUMBER NOT NULL,
  activo_pdi CHAR,
  nombre VARCHAR2(30 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  activo_360 CHAR,
  editar_form_bars VARCHAR2(2 BYTE) DEFAULT 'N',
  CONSTRAINT periodo_bars_pk PRIMARY KEY (ano),
  CONSTRAINT periodo_bars_estado_bars_fk FOREIGN KEY (estado) REFERENCES codensa_gth.estado_bars ("ID")
);