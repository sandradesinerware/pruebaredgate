CREATE TABLE codensa_gth.bars_comevaobj (
  formeva_obj NUMBER NOT NULL,
  comentario_evaluado VARCHAR2(500 BYTE),
  comentario_evaluador VARCHAR2(500 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_comevaobj_pk PRIMARY KEY (formeva_obj),
  CONSTRAINT bars_comevaobj_bars_forme_fk FOREIGN KEY (formeva_obj) REFERENCES codensa_gth.bars_formevaobj ("ID")
);