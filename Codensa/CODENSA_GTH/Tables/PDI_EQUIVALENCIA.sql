CREATE TABLE codensa_gth.pdi_equivalencia (
  "ID" NUMBER NOT NULL,
  nombre_curso_anterior VARCHAR2(500 BYTE),
  nombre_curso_actual VARCHAR2(500 BYTE),
  periodo_anterior NUMBER,
  periodo_actual NUMBER,
  CONSTRAINT pdi_equivalencia_pk PRIMARY KEY ("ID")
);