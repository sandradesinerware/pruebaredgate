CREATE TABLE codensa_gth.pdi_sesion (
  "ID" NUMBER NOT NULL,
  numero NUMBER,
  fecha DATE,
  asistentes NUMBER,
  asistio CHAR,
  id_pdi_grupo NUMBER NOT NULL,
  CONSTRAINT pdi_sesion_pk PRIMARY KEY ("ID"),
  CONSTRAINT pdi_sesion_fk1 FOREIGN KEY (id_pdi_grupo) REFERENCES codensa_gth.pdi_grupo ("ID")
);