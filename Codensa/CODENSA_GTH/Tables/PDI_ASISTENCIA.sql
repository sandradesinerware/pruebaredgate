CREATE TABLE codensa_gth.pdi_asistencia (
  "ID" NUMBER NOT NULL,
  id_pdi_sesion NUMBER NOT NULL,
  id_bars_pdi NUMBER NOT NULL,
  asistio VARCHAR2(2 BYTE),
  aud_fecha_creacion DATE DEFAULT SYSDATE NOT NULL,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE NOT NULL,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT 'HISTORICO' NOT NULL,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS' NOT NULL,
  CONSTRAINT pdi_asistencia_pk PRIMARY KEY ("ID"),
  CONSTRAINT pdi_asistencia_uk1 UNIQUE (id_pdi_sesion,id_bars_pdi),
  CONSTRAINT pdi_asistencia_fk1 FOREIGN KEY (id_pdi_sesion) REFERENCES codensa_gth.pdi_sesion ("ID"),
  CONSTRAINT pdi_asistencia_fk2 FOREIGN KEY (id_bars_pdi) REFERENCES codensa_gth.bars_pdi ("ID")
);