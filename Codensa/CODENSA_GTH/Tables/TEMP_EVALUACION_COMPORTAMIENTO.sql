CREATE TABLE codensa_gth.temp_evaluacion_comportamiento (
  "ID" NUMBER NOT NULL,
  evaluador NUMBER,
  evaluado NUMBER,
  periodo NUMBER,
  pct_consecucion NUMBER,
  validacion NUMBER,
  CONSTRAINT temp_evaluacion_co_id_pk PRIMARY KEY ("ID")
);