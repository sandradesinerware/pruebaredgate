CREATE TABLE codensa_gth.bars_traevapre (
  trabajador NUMBER NOT NULL,
  perpre NUMBER NOT NULL,
  pospre_final NUMBER,
  mesahompre NUMBER,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_traevapre_pk PRIMARY KEY (trabajador,perpre),
  CONSTRAINT bars_traevapre_mesa_fk FOREIGN KEY (mesahompre) REFERENCES codensa_gth.bars_mesahompre ("ID"),
  CONSTRAINT bars_traevapre_per_fk FOREIGN KEY (perpre) REFERENCES codensa_gth.bars_perpre (ano),
  CONSTRAINT bars_traevapre_pospre_fk FOREIGN KEY (pospre_final) REFERENCES codensa_gth.bars_pospre ("ID")
);