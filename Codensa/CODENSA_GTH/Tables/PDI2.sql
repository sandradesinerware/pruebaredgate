CREATE TABLE codensa_gth.pdi2 (
  justificacion VARCHAR2(2000 BYTE),
  curso VARCHAR2(400 BYTE) NOT NULL,
  evaluado VARCHAR2(20 BYTE),
  id_comportamiento VARCHAR2(2 BYTE),
  id_estado VARCHAR2(2 BYTE),
  comentarios VARCHAR2(200 BYTE),
  fecha_inicio VARCHAR2(20 BYTE),
  fecha_fin VARCHAR2(20 BYTE),
  id_encabezado NUMBER,
  id_curso NUMBER,
  dfecha_inicio DATE,
  dfecha_fin DATE,
  "ID" NUMBER(20) NOT NULL,
  CONSTRAINT pdi2_pk PRIMARY KEY ("ID")
);