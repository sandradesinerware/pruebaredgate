CREATE TABLE codensa_gth.pec_actividad (
  "ID" NUMBER NOT NULL,
  fecha DATE,
  intensidad VARCHAR2(50 BYTE),
  proceso VARCHAR2(4000 BYTE),
  contenido VARCHAR2(4000 BYTE),
  tipo_entrenamiento NUMBER,
  responsable VARCHAR2(4000 BYTE),
  lugar VARCHAR2(4000 BYTE),
  cumple CHAR,
  id_pec NUMBER NOT NULL,
  predeterminada CHAR DEFAULT 'N' NOT NULL,
  requiere_evidencia CHAR DEFAULT 'N' NOT NULL,
  CONSTRAINT pec_actividad_pk PRIMARY KEY ("ID"),
  CONSTRAINT pec_actividad_pec_fk FOREIGN KEY (id_pec) REFERENCES codensa_gth.pec ("ID"),
  CONSTRAINT pec_actividad_tipo_fk1 FOREIGN KEY (tipo_entrenamiento) REFERENCES codensa_gth.tipo ("ID")
);
COMMENT ON COLUMN codensa_gth.pec_actividad.cumple IS 'S o N';
COMMENT ON COLUMN codensa_gth.pec_actividad.predeterminada IS 'S o N';
COMMENT ON COLUMN codensa_gth.pec_actividad.requiere_evidencia IS 'Si la actividad requiere evidencia obligatoria';