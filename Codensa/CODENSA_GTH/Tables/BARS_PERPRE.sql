CREATE TABLE codensa_gth.bars_perpre (
  ano NUMBER NOT NULL,
  estado NUMBER NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  CONSTRAINT bars_perpre_pk PRIMARY KEY (ano),
  CONSTRAINT bars_perpre_estado_fk FOREIGN KEY (estado) REFERENCES codensa_gth.estado_bars ("ID")
);