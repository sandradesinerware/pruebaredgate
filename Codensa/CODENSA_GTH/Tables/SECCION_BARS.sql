CREATE TABLE codensa_gth.seccion_bars (
  "ID" NUMBER(2) NOT NULL,
  nombre VARCHAR2(50 BYTE) NOT NULL,
  aud_fecha_creacion DATE DEFAULT SYSDATE,
  aud_fecha_actualizacion DATE DEFAULT SYSDATE,
  aud_creado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_actualizado_por VARCHAR2(30 BYTE) DEFAULT USER,
  aud_terminal_actualizacion VARCHAR2(40 BYTE) DEFAULT 'IP_ADDRESS',
  nombre_360 VARCHAR2(40 BYTE),
  CONSTRAINT seccion_bars_pk PRIMARY KEY ("ID")
);