CREATE TABLE codensa_gth.respuesta_evaluacion (
  "ID" NUMBER NOT NULL,
  valor NUMBER,
  comentario VARCHAR2(4000 BYTE),
  aud_fecha_creacion DATE,
  aud_creado_por VARCHAR2(4000 BYTE),
  aud_fecha_actualizacion DATE,
  aud_actualizado_por VARCHAR2(4000 BYTE),
  aud_terminal_actualizacion VARCHAR2(4000 BYTE),
  evaluacion NUMBER NOT NULL,
  pregunta NUMBER,
  CONSTRAINT respuesta_evaluacion_pk PRIMARY KEY ("ID"),
  CONSTRAINT respuesta_con_pregunta_fk FOREIGN KEY (pregunta) REFERENCES codensa_gth.pregunta ("ID"),
  CONSTRAINT respuesta_evaluacion_con FOREIGN KEY (evaluacion) REFERENCES codensa_gth.evaluacion ("ID")
);