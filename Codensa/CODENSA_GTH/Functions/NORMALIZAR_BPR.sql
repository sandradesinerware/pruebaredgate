CREATE OR REPLACE FUNCTION codensa_gth."NORMALIZAR_BPR" 
(X in NUMBER)
return NUMBER
is
o_result NUMBER;
begin
o_result:=0;
if (X between 1 and 3) then o_result:= (4*X)-4;
elsif (X between 3 and 5) then o_result:= X+5;
end if;
return o_result;
end;

/