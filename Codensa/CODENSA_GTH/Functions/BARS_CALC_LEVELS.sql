CREATE OR REPLACE FUNCTION codensa_gth."BARS_CALC_LEVELS" ( pposicion IN BARS_POSICION.id%type, PERSONAS IN NUMBER, total_mesa IN NUMBER ,pmesa NUMBER ) RETURN VARCHAR2
is
retV    VARCHAR2(200);
n1      NUMBER;
rmin    NUMBER;
rmax    NUMBER;
porcent NUMBER;
asup NUMBER;
ainf NUMBER;
begin
        select NVL(ajuste_superior,0),NVL( ajuste_inferior,0) into asup, ainf from BARS_MESAHOM where id=pmesa;
        select RESTRMIN, RESTRMAX into rmin, rmax from BARS_RESTRPOSICION where POSICION = pposicion;


        porcent := PERSONAS*100/total_mesa;

        if( rmax is not null and porcent > rmax+asup) then
              n1  := ((rmax+asup)*total_mesa)/100;
              if( PERSONAS > n1) then
               retV := 'Remover '||ceil( PERSONAS - n1 )||' persona(s).';
              end if;
        elsif( rmin is not null and porcent < rmin-ainf )  then
          retV := 'Agregar '||ceil((rmin-ainf)*total_mesa/100 - PERSONAS)||' persona(s).';
        end if;


   return retV;
exception
 when NO_DATA_FOUND then
   return '-';
end;

/