CREATE OR REPLACE FUNCTION codensa_gth."BARS_VALIDARPORCENT" ( pposicion IN BARS_POSICION.id%type, porcent IN NUMBER, pmesa IN NUMBER  ) RETURN VARCHAR2 
is
retV  VARCHAR2(200);
rmin  NUMBER;
rmax  NUMBER;
asup NUMBER;
ainf NUMBER;
begin

         retV := 'Correcto.';
         select NVL(ajuste_superior,0),NVL( ajuste_inferior,0) into asup, ainf from BARS_MESAHOM where id=pmesa; 
         select RESTRMIN, RESTRMAX into rmin, rmax from BARS_RESTRPOSICION where POSICION = pposicion;

       if( rmin is not null and porcent < rmin-ainf) then
              retV := 'El porcentaje es inferior al minimo.';
       elsif( rmax is not null and porcent > rmax+asup) then
           retV := 'El porcentaje supera el maximo.';
       end if; 

    return retV;

EXCEPTION
  WHEN NO_DATA_FOUND THEN --La posición no tiene restricción
    return retV;
end;

/