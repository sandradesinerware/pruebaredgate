CREATE OR REPLACE FUNCTION codensa_gth."DETERMINAR_PAG_FORM_PLANTILLA" (pformulario IN FORMULARIO_360.ID%type,
ptipo IN VARCHAR2)
RETURN NUMBER
IS
vplantilla PLANTILLA_FORM_360.ID%TYPE;
ktipo_seccion CONSTANT VARCHAR2(10) := 'SECCION';
ktipo_resultado CONSTANT VARCHAR2(10) := 'RESULTADO';
BEGIN
--Determinación de la plantilla
SELECT PLANTILLA_FORM_360.ID into vplantilla
from FORMULARIO_360 inner join PLANTILLA_FORM_360 on (FORMULARIO_360.plantilla =
PLANTILLA_FORM_360.id)
where FORMULARIO_360.id = pformulario;
if upper(ptipo) = ktipo_seccion then --Se busca la primera página de la sección
RETURN 42;
elsif upper(ptipo) = ktipo_resultado then --Se busca la página del resultado
RETURN 51;
end if;
EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_PAG_FORM_PLANTILLA.' || SQLCODE || ' - ' || SQLERRM);
END DETERMINAR_PAG_FORM_PLANTILLA;

/