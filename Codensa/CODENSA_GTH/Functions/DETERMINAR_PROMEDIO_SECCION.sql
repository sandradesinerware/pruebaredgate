CREATE OR REPLACE FUNCTION codensa_gth."DETERMINAR_PROMEDIO_SECCION" 
    (pseccion_formulario IN SECCION_FORMULARIO.ID%TYPE)
RETURN NUMBER
IS
 vpromedio NUMBER;
BEGIN
 SELECT NVL( AVG(valor),0 ) into vpromedio
   from RESPUESTA
   where seccion_formulario = pseccion_formulario and valor != 0;
 RETURN vpromedio;
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PROMEDIO_SECCION. ' || SQLCODE || ' - '
     || SQLERRM || ' ');
END;

/