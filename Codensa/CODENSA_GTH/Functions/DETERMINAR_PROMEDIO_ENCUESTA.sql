CREATE OR REPLACE FUNCTION codensa_gth."DETERMINAR_PROMEDIO_ENCUESTA" (pformulario IN FORMULARIO_BARS.ID%TYPE,
  ptipo IN SECCION_FORMULARIO.TIPO%TYPE)
RETURN NUMBER
IS
 vpromedio NUMBER;
BEGIN
 /*SELECT NVL( AVG(resultado),0 ) into vpromedio
   from SECCION_FORMULARIO
   where formulario = pformulario and tipo = ptipo;*/
 --Cambio en procedimiento de cálculo de promedio (no es de las secciones sino de las respuestas)
 SELECT NVL( AVG(valor),0 ) into vpromedio
   from RESPUESTA
   where seccion_formulario IN (select id from seccion_formulario where formulario = pformulario)
   and valor != 0;
   --Persiste la informacion en FORMULARIO_BARS
   update formulario_bars set media_comportamientos= round(vpromedio,4) where id=pformulario;

 RETURN round(vpromedio,4);
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PROMEDIO_ENCUESTA. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
END;

/