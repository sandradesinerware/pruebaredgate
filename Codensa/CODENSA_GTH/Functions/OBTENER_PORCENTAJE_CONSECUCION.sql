CREATE OR REPLACE FUNCTION codensa_gth."OBTENER_PORCENTAJE_CONSECUCION" ( i_resultado_obtenido in number) return number is 
    l_porcentaje_consecucion NUMBER;
begin

    l_porcentaje_consecucion := 0;

    if (i_resultado_obtenido >= 6) then    
        l_porcentaje_consecucion := (i_resultado_obtenido * 10) + 20;
    end if;

    return ROUND(l_porcentaje_consecucion, 1);

end OBTENER_PORCENTAJE_CONSECUCION;
/