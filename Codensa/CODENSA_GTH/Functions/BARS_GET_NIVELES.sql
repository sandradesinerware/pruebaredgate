CREATE OR REPLACE FUNCTION codensa_gth."BARS_GET_NIVELES" ( i_porcentaje NUMBER, i_ajuste NUMBER, i_periodo PCT_CONSECUCION_BARS.periodo%type  ) return NUMBER
IS
    iter NUMBER;
    inf  NUMBER;
    sup  NUMBER;
BEGIN

          iter := 1;

          if( i_ajuste < 0) then

              for c in( select PCT_CONSECUCION from PCT_CONSECUCION_BARS where periodo = i_periodo
			    and PCT_CONSECUCION < i_porcentaje order by 1 desc ) loop
               if( iter = abs(i_ajuste) ) then
                     return c.PCT_CONSECUCION;
               end if;
                 iter := iter+1;
              end loop;
          else    
             for c in(  select PCT_CONSECUCION from PCT_CONSECUCION_BARS where periodo = i_periodo
                and PCT_CONSECUCION > i_porcentaje order by 1 asc ) loop
               if( iter = i_ajuste ) then
                     return c.PCT_CONSECUCION;
               end if;
                  iter := iter+1;
             end loop;
          end if;

 return i_porcentaje;
END;
/