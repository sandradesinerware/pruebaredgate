CREATE OR REPLACE FUNCTION codensa_gth."VAL_EXISTE_ENCPDI" 
(
  i_periodo in BARS_ENCPDI.periodo%type,
  i_evaluado in  BARS_ENCPDI.evaluado%type
) return number as
l_conteo NUMBER;
begin

select count(*) into l_conteo from bars_encpdi where periodo=i_periodo and evaluado=i_evaluado;

if (l_conteo=1) then return 1;
elsif (l_conteo>1) then 
raise_application_error(-20000,'Existe más de un regitro de PDI.');
else return 0;
end if;
  return null;
end val_existe_encpdi;

/