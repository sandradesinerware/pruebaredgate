CREATE OR REPLACE FUNCTION codensa_gth."BARS_CALCPERSPOSPRE" ( pposicion IN BARS_POSPRE.id%type, ppersonas IN NUMBER, ptotal_mesa IN NUMBER ) RETURN VARCHAR2
is
retV    VARCHAR2(200);
n1      NUMBER;
rmin    NUMBER;
rmax    NUMBER;
idPos   NUMBER;
porcent NUMBER;
begin
 select RESTRMIN, RESTRMAX into rmin, rmax from BARS_POSPRE where id = pposicion;
 porcent := ROUND((ppersonas/ptotal_mesa)*100,2);

 if( rmax is not null and porcent > rmax ) then
    n1  := (rmax*ptotal_mesa)/100;
    if( ppersonas > n1) then
        retV := 'Remover '||ceil( ppersonas - n1 )||' persona(s).';
    end if;
 elsif( rmin is not null and porcent < rmin )  then
    retV := 'Agregar '||ceil(rmin*ptotal_mesa/100 - ppersonas)||' persona(s).';
 end if;

 return retV;

EXCEPTION
 when NO_DATA_FOUND then
   return '-';
end;

/