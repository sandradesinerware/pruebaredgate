CREATE OR REPLACE FUNCTION codensa_gth."BARS_VALITEMSOBJ" (
    pitem         IN            VARCHAR2,
    pformulario   IN            bars_formevaobj.id%TYPE
) RETURN NUMBER IS

    knuevo               CONSTANT VARCHAR2(30) := 'NUEVO';
    ket                  CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
    kconval              CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
    kconap               CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
    kperaj               CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
    kresval              CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
    kfinalizado          CONSTANT VARCHAR2(30) := 'FINALIZADO';
    kanulado             CONSTANT VARCHAR2(30) := 'ANULADO';
    kadministradores     CONSTANT VARCHAR2(30) := 'ADMINISTRADORES OPR';
    kmodoconsulta        CONSTANT VARCHAR2(30) := 'CONSULTA';
    kmodoedicion         CONSTANT VARCHAR2(30) := 'EDICION';
    l_modo               CONSTANT VARCHAR2(30) := 'EDICION';
    --v('AI_MODO_FORMULARIO');
    l_app_user           VARCHAR2(100); 
    vtipo_estado         NUMBER(2, 0);
    vestado_formulario   VARCHAR2(30);
    vfecha_pago          DATE;
    vusuario             usuario.numero_identificacion%TYPE;
    l_usuario_id         usuario.id%TYPE;
    vevaluado            bars_formevaobj.evaluado%TYPE;
    vevaluador           bars_formevaobj.evaluador%TYPE;
    vgestor              trabajador.jefe%TYPE;
    flag                 BOOLEAN;
    isadmin              BOOLEAN;
    -- l_id_target_opr      NUMBER;
    -- l_periodo   NUMBER;
    l_peso_requerido_obj_cer NUMBER;
    l_peso_planeado_obj_cer BOOLEAN;
-- Colección para almacenar los grupos del usuario
    TYPE grupo_array IS
        TABLE OF grupo.nombre%TYPE;
    vgrupos              grupo_array;
    i                    NUMBER(2) := 0;
BEGIN

--Identificación usuario conectado a la aplicación
l_app_user := v('APP_USER'); -- Esta es una forma de tomar desde PL el valor de APP_USER para la sesión activa

select numero_identificacion into vusuario
  from usuario
  where upper(user_name) = l_app_user;

--Determinar tipo estado OBJETIVOS_BARS
    SELECT
        id
    INTO vtipo_estado
    FROM
        tipo_estado_bars
    WHERE
        upper(nombre) = 'OBJETIVOS_BARS';
--Determinar estado del formulario de OBJETIVOS

    SELECT
        upper(TRIM(e.nombre))
    INTO vestado_formulario
    FROM
        bars_formevaobj   f
        INNER JOIN estado_bars       e ON f.estado = e.id
    WHERE
        f.id = pformulario;
--Determinar evaluado y evaluador del formulario

    SELECT
        evaluado
    INTO vevaluado
    FROM
        bars_formevaobj
    WHERE
        id = pformulario;

    SELECT
        evaluador
    INTO vevaluador
    FROM
        bars_formevaobj
    WHERE
        id = pformulario;

    SELECT
        jefe
    INTO vgestor
    FROM
        trabajador
    WHERE
        numero_identificacion = vevaluado;
-- Determinar fecha de pago bono

    SELECT
        fecha_pago_bono
    INTO vfecha_pago
    FROM
        bars_formevaobj   bf
        JOIN bars_perobj       bp ON ( bp.ano = bf.periodo )
    WHERE
        bf.id = pformulario;

--Determinar id usuario (tener en cuenta que vusuario tiene es el numero de identificacion)
SELECT id INTO l_usuario_id
    FROM usuario
    WHERE upper(user_name) = upper(l_app_user);
--Determinar grupos (i.e. roles) del usuario

    vgrupos := grupo_array();
    FOR rec_grupo IN (
        SELECT
            upper(TRIM(gb.nombre)) nombre
        FROM
            usu_grupo   ubg
            INNER JOIN grupo       gb ON ubg.grupo = gb.id
        WHERE
            ubg.usuario = l_usuario_id
    ) LOOP
        i := i + 1;
        vgrupos.extend(1);
        vgrupos(i) := rec_grupo.nombre;
    END LOOP;

--Validación del display de los items de objetivos de acuerdo al item, al estado del formulario
--y al modo de consulta (EDICION, CONSULTA)

    IF ( l_modo = kmodoconsulta ) THEN
--Está en modo consulta, por tanto ningún botón de edición o cambio de estado debe funcionar
--Solo se verifica si las columnas de objetivos se visualizan dependiendo del estado
        CASE upper(pitem)
            /*WHEN 'OBJETIVOS.ALCANZADO' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;*/
            WHEN 'OBJETIVOS.PCTCONSIND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'OBJETIVOS.PCTPOND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            ELSE
                RETURN 0;
        END CASE;
    ELSIF ( l_modo = kmodoedicion ) THEN
        CASE upper(pitem)
            WHEN 'LISTA_OBJETIVOS_SAVE' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF ( ( ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj
                ) AND ( vevaluado = vusuario ) ) OR ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj,
                    kconval
                ) AND ( vevaluador = vusuario ) ) ) AND ( vfecha_pago >= SYSDATE ) ) OR ( vfecha_pago >= SYSDATE AND isadmin ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'LISTA_OBJETIVOS_ADD' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF ( ( ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj
                ) AND vevaluado = vusuario ) OR ( ( vestado_formulario IN (
                    knuevo,
                    ket,
                    kperaj,
                    kconval
                ) AND ( vevaluador = vusuario ) ) ) AND vfecha_pago >= SYSDATE ) ) OR ( isadmin AND vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'LISTA_OBJETIVOS_DELETE' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF ( ( ( ( vestado_formulario IN (
                    ket,
                    kperaj
                ) AND vevaluado = vusuario ) OR ( vestado_formulario IN (
                    ket,
                    kperaj,
                    kconval
                ) AND vevaluador = vusuario ) ) AND ( vfecha_pago >= SYSDATE ) ) ) OR ( isadmin AND vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

           WHEN 'LISTA_OBJETIVOS_VALIDAR_CONCERTACION' THEN
                IF ( ( vestado_formulario IN (
                    ket,
                    kperaj
                ) AND vevaluado = vusuario ) OR ( vestado_formulario IN (
                    ket,
                    kperaj
                ) AND vevaluador = vusuario ) ) AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'LISTA_OBJETIVOS_APROBAR_CONCERTACION' THEN
                IF ( vestado_formulario IN (
                    kconval
                ) AND ( vusuario IN (
                    vevaluador
                ) ) AND ( vfecha_pago >= SYSDATE ) ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'LISTA_OBJETIVOS_MODIFY' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;

                IF isadmin AND vestado_formulario NOT IN (
                               kfinalizado,
                               kanulado
                ) AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            WHEN 'LISTA_OBJETIVOS_SAVE_RESULTADOS' THEN
                IF vestado_formulario IN (
                    kconap
                ) AND ( vevaluado = vevaluador ) AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'FORTALEZAS_SAVE' THEN
                IF vestado_formulario IN (
                    kresval
                ) AND vevaluador = vusuario AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'LISTA_OBJETIVOS_VALIDAR_RESULTADO' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;
                IF vestado_formulario IN ( kconap, kresval ) 
                  AND ( vgestor = vusuario OR isadmin )
                  AND ( VALIDACIONES_GUI.val_crud_formevaobj_fch_aprob(pformulario,V('APP_USER')) ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'COMENTARIOS_SAVE_O' THEN
                IF vestado_formulario NOT IN (
                   kfinalizado,
                   kanulado
                ) AND vevaluado = vusuario AND ( vfecha_pago >= SYSDATE ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'COMENTARIOS_SAVE_R' THEN
                RETURN 0; --Se decidió en reunión 20/09 que no se mostraría.
            WHEN 'FINALIZAR' THEN
                isadmin := false;
                FOR i IN 1..vgrupos.count LOOP IF vgrupos(i) = kadministradores THEN
                    isadmin := true;
                END IF;
                END LOOP;                        
                
                -- TODO: Este se esta llamando dentro de val_peso_total_objetivos CALFONSO - 4/09/2020 
                -- l_peso_requerido_obj_cer := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_id_target_opr, SNW_CONSTANTES.constante_tipo('OPR_CER') );
                -- select periodo into l_periodo from BARS_FORMEVAOBJ where id = pformulario;
                -- select ID_TARGET_OPR into l_id_target_opr from trabajador where NUMERO_IDENTIFICACION = vevaluado;        
                
                l_peso_planeado_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,SNW_CONSTANTES.constante_tipo('CERRADO'));--Validar Ponderación Requerida de Objetivos Cerrados
                
                -- FALTA validar objetivos abiertos
                -- l_peso_planeado_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,SNW_CONSTANTES.constante_tipo('CERRADO'));--Validar Ponderación Requerida de Objetivos Cerrados
                
                
                IF vestado_formulario IN (kresval) 
                  AND ( l_peso_planeado_obj_cer )
                   AND ( vgestor = vusuario OR isadmin )
                   AND ( VALIDACIONES_GUI.val_crud_formevaobj_fch_aprob(pformulario,V('APP_USER')) ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;

            /*WHEN 'OBJETIVOS.ALCANZADO' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;*/
            WHEN 'OBJETIVOS.PCTCONSIND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            WHEN 'OBJETIVOS.PCTPOND' THEN
                IF vestado_formulario IN (
                    kconap,
                    kresval,
                    kfinalizado
                ) THEN
                    RETURN 1;
                ELSE
                    RETURN 0;
                END IF;
            ELSE
                RETURN 0;
        END CASE;
    ELSE --Caso no contemplado en el modo formulario
        RETURN 0;
    END IF;
    
EXCEPTION
    WHEN OTHERS THEN
    RETURN 0;
        --raise_application_error(-20000, 'Un error inesperado ocurrió en BARS_VALITEMSOBJ. Por favor, infórmelo al administrador de la aplicación. '
          --                              || sqlcode
            --                            || ' - '
              --                          || sqlerrm);
END bars_valitemsobj;
/