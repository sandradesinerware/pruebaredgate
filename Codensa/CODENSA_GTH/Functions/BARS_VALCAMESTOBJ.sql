CREATE OR REPLACE FUNCTION codensa_gth."BARS_VALCAMESTOBJ" ( pestado_nuevo IN ESTADO_BARS.id%type ) RETURN BOOLEAN 
IS
knuevo CONSTANT VARCHAR2(30) := 'NUEVO';
ket CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
kconval CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
kconap CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
kperaj CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
kresval CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
vestado_actual VARCHAR2(30);
vestado_nuevo VARCHAR2(30);

BEGIN
SELECT upper(e.nombre) into vestado_actual
 FROM BARS_FORMEVAOBJ f inner join ESTADO_BARS e on (f.estado = e.id)
 WHERE f.id = v('P85_ID');

SELECT upper(nombre) into vestado_nuevo
 FROM ESTADO_BARS
 WHERE id = pestado_nuevo;

 CASE vestado_nuevo

  WHEN knuevo THEN
   if vestado_actual in (kfinalizado) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN ket THEN
   if vestado_actual in (kfinalizado,kanulado, knuevo) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kconval THEN
   if vestado_actual in (kfinalizado,kanulado,knuevo,ket) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kconap THEN
   if vestado_actual in (kanulado,knuevo,ket,kconval) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kresval THEN
   if vestado_actual in (kanulado,knuevo,ket,kconval,kconap) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  WHEN kfinalizado THEN
   RETURN FALSE;

  WHEN kanulado THEN
   if vestado_actual in (kfinalizado) then
    RETURN FALSE;
   else
    RETURN TRUE;
   end if;

  ELSE
   RETURN FALSE;

 END CASE;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en BARS_VALCAMESTOBJ. Por favor, infórmelo al administrador de la aplicación. ' 
	  || SQLCODE || ' - ' || SQLERRM);

END BARS_VALCAMESTOBJ;

/