CREATE OR REPLACE PROCEDURE codensa_gth."CAL_POSICION_BASE_MEDIA" 
(i_formulario IN formulario_bars.id%type)
is
l_formulario formulario_bars%rowtype;
l_posicion formulario_bars.posicion%type;
begin
select * into l_formulario from formulario_bars where id= I_FORMULARIO;
select nombre into l_posicion from bars_posicion where tipo_id = 61 and l_formulario.media_comportamientos between pct_inf and pct_sup and periodo=l_formulario.periodo;
update formulario_bars set posicion=l_posicion where id=l_formulario.id;
exception
when no_data_found then
    if(l_formulario.id is null) then
        raise_application_error(-20000,'No hay informacion del formulario: '||I_FORMULARIO);
    end if;
    if(l_posicion is null and l_formulario.id is not null) then
        raise_application_error(-20000,'No hay informacion en la tabla BARS_POSICION para el periodo: '||l_formulario.periodo||' y media de comportamientos '||l_formulario.media_comportamientos);
    end if;

end;

/