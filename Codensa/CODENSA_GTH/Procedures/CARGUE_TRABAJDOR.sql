CREATE OR REPLACE PROCEDURE codensa_gth."CARGUE_TRABAJDOR" IS 

CURSOR trabajadores IS SELECT * FROM TEMP_TRABAJADOR;
dummy NUMBER;

l_gerencia NUMBER;
l_cargo NUMBER;
l_unidad_organizativa NUMBER;
l_target_bpr NUMBER;
l_target_opr NUMBER;
l_tipo_regimen NUMBER;
l_empresa NUMBER;
l_nuevo_estado NUMBER;
l_jefe NUMBER;
l_email varchar2(4000);
l_identificacion number;
l_error varchar2(4000);

FUNCTION is_number (p_string IN VARCHAR2) RETURN INT
IS
   v_new_num NUMBER;
BEGIN
   if (p_string is null) then
     RETURN 2;
   else 
     v_new_num := TO_NUMBER(p_string);
   RETURN 1;
   end if;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END;

BEGIN
/*Esta función opera sobre la tabla TEMP_ACTUALIZACIONTRA, la cual contiene cada uno de los trabajadores reportados por Codensa
con sus atributos iniciales, y los nuevos. Permite que la columna con los nuevos valores corresponda a: un nulo (en cuyo caso no se actualiza); 
al id del atributo referenciado (en cuyo caso se actualiza); o sea un texto, en cuyo caso se analiza si ya existe o debe crearse.
Si el trabajador no existe, se crea con los atributos reportados.*/

/*TODO: No maneja el valor nulo como indicador de no actualizar. Debe garantizarse que los valores a actualizar no sean nulos.
Se podría mejorar para que fuera un procedimiento reutilizable el que recorre columna a columna para crearla o identificarla
Uso de constantes para algunas variables.
*/

FOR c in trabajadores LOOP
 --Se empieza consultando el estado porque a los inactivos no vale la pena actualizarle ningún dato.
 if upper(c.nuevo_estado) is not null then
 SELECT DECODE( upper(c.nuevo_estado), 'ACTIVO', 1, 'INACTIVO', 10, null) into l_nuevo_estado FROM DUAL;
 else 
 SELECT DECODE( upper(c.estado), 'ACTIVO', 1, 'INACTIVO', 10, null) into l_nuevo_estado FROM DUAL;
end if;

 if (l_nuevo_estado = 10) then -- Inactivo por tanto solo se actualiza el estado
   BEGIN
   UPDATE TRABAJADOR SET id_estado = l_nuevo_estado
     WHERE numero_identificacion = c.numero_identificacion;
   UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador inactivado' WHERE numero_identificacion = c.numero_identificacion;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN --No existe pero como se va a inactivar no importa
     UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador inactivado' WHERE numero_identificacion = c.numero_identificacion;
   WHEN OTHERS THEN
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualización' WHERE numero_identificacion = c.numero_identificacion;
   END;

 elsif (l_nuevo_estado = 1) then --Los de estado Activo. Por tanto se actualizan o crean. 

 --Se empiezan creando las variables que contendrán los valores de atributos referenciados para crear o actualizar el trabajador

 --gerencia
 if  ( is_number(trim(c.nueva_gerencia)) = 0 ) then --no es número así que: es nulo o el valor referenciado.
   BEGIN
     select id into l_gerencia
		from TIPO
		where tipo = 1 and upper(nombre) = upper(c.nueva_gerencia);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN 
	   l_gerencia := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_gerencia, upper(c.nueva_gerencia), 'A', 1, trunc(sysdate), 2);
	END;
 elsif ( is_number(trim(c.nueva_gerencia)) = 2 ) then
	l_gerencia := c.ID_GERENCIA; 
 else
   l_gerencia := c.nueva_gerencia; 
 end if;

 --cargo
 if ( is_number(trim(c.nuevo_cargo)) = 0 ) then --no es número así que o es el mismo valor o debe crearse
   BEGIN
   select id into l_cargo
		from TIPO
		where tipo = 3 and upper(nombre) = upper(c.nuevo_cargo);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN 
	 --Creación del cargo
	   l_cargo := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_cargo, upper(c.nuevo_cargo), 'A', 3, trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nuevo_cargo)) = 2 ) then
	l_cargo := c.ID_CARGO; 	 
 else
   l_cargo := c.nuevo_cargo; 
 end if;

 --unidad_organizativa
 if ( is_number(trim(c.nueva_unidad_organizativa)) = 0 ) then --no es número así que o es el mismo valor o debe crearse
   BEGIN
   select id into l_unidad_organizativa
		from TIPO
		where tipo = 2 and upper(nombre) = upper(c.nueva_unidad_organizativa);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Creación de la unidad organizativa
	   l_unidad_organizativa := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_unidad_organizativa, upper(c.nueva_unidad_organizativa), 'A', 2, trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nueva_unidad_organizativa)) = 2 ) then
	l_unidad_organizativa := c.ID_UNIDAD_ORGANIZATIVA; 
 else
   l_unidad_organizativa := c.nueva_unidad_organizativa; 
 end if;

 --target_bpr
 if ( is_number(trim(c.nuevo_target_bpr)) = 0 ) then --no es número así que o es el texto referenciado, o un target no manejado
   BEGIN
   select id into l_target_bpr
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TARGET_BPR') and upper(nombre) = upper(c.nuevo_target_bpr);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Es un parámetro sensible, así que no se puede crear sin control
	   l_target_bpr := null;
   END;
elsif ( is_number(trim(c.nuevo_target_bpr)) = 2 ) then
	l_target_bpr := c.ID_TARGET_BPR; 
 else --En la fuente viene el id del target_bpr
   l_target_bpr := c.nuevo_target_bpr; 
 end if;

 --target_opr
 if ( is_number(trim(c.nuevo_target_opr)) = 0 ) then --no es número así que o es el texto referenciado, o un target no manejado
   BEGIN
   select id into l_target_opr
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TARGET_OPR') and upper(nombre) = upper(c.nuevo_target_opr);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Es un parámetro sensible, así que no se puede crear sin control
	   l_target_opr := null;
   END;
elsif ( is_number(trim(c.nuevo_target_opr)) = 2 ) then
	l_target_opr := c.ID_TARGET_OPR; 
 else --En la fuente viene el id del target_opr
   l_target_opr := c.nuevo_target_opr; 
 end if;

 --tipo_régimen
 if ( is_number(trim(c.nuevo_tipo_regimen)) = 0 ) then --no es número así que o es el texto referenciado, o debe crearse (deuda técnica: está creando filas en tipo para tipo_regimen con nombre null
   BEGIN
   select id into l_tipo_regimen
		from TIPO
		where tipo = snw_constantes.constante_tipo ('TIPO_REGIMEN') and upper(nombre) = upper(c.nuevo_tipo_regimen);
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
	   --Creación del tipo régimen
	   l_tipo_regimen := tipo_seq.nextval;
	   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
		 values (l_tipo_regimen, c.nuevo_tipo_regimen, 'A', snw_constantes.constante_tipo ('TIPO_REGIMEN'), trunc(sysdate), 2);
   END;
elsif ( is_number(trim(c.nuevo_tipo_regimen)) = 2 ) then
	BEGIN
	SELECT  id_tipo_regimen INTO l_tipo_regimen FROM TRABAJADOR 
	WHERE numero_identificacion = c.numero_identificacion;
	EXCEPTION
     	WHEN NO_DATA_FOUND THEN
	l_tipo_regimen:=NULL;
	END;


 else --En la fuente viene el id del tipo_regimen
   l_tipo_regimen := c.nuevo_tipo_regimen; 
 end if;

 --nueva empresa
 if ( is_number(trim(c.nueva_empresa)) = 0 ) then --no es número así que o es el mismo valor o debe crearse
	 BEGIN
	   select id into l_empresa
			from TIPO
			where tipo = 8 and upper(nombre) = upper(c.nueva_empresa);
	   EXCEPTION
		 WHEN NO_DATA_FOUND THEN
		   --Creación de la empresa
		   l_empresa := tipo_seq.nextval;
		   insert into TIPO (id, nombre, estado, tipo, fecha_inicio, permisos)
			 values (l_empresa, upper(c.nueva_empresa), 'A', 8, trunc(sysdate), 2);
	   END;
elsif ( is_number(trim(c.nueva_empresa)) = 2 ) then
	l_empresa := c.ID_EMPRESA;
	 else
 l_empresa := c.nueva_empresa;
 end if;

 --jefe
 l_jefe := nvl(c.nuevo_num_asignado_jefe,c.jefe);

 --email
  if  ( c.nuevo_email is not null) then 
    l_email := c.nuevo_email;
  else
    l_email := c.email;
  end if;
--identificación 
  if  ( c.nueva_identificacion is not null) then 
    l_identificacion := c.nueva_identificacion;
  else
    l_identificacion := c.identificacion;
  end if;

  BEGIN
   --Confirmar si el trabajador existe
   SELECT count(*) into dummy from TRABAJADOR where numero_identificacion = c.numero_identificacion;

   if ( dummy >= 1 ) then --El trabajador existe
	   UPDATE TRABAJADOR  
		SET id_gerencia = l_gerencia, id_cargo = l_cargo, id_unidad_organizativa = l_unidad_organizativa, id_target_bpr = l_target_bpr, id_target_opr = l_target_opr,
			 id_empresa = l_empresa, id_estado = l_nuevo_estado, email = l_email, jefe = l_jefe,identificacion = l_identificacion
		 WHERE numero_identificacion = c.numero_identificacion;
	   UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador actualizado' WHERE numero_identificacion = c.numero_identificacion;

	else --El trabajador no existe
       INSERT INTO TRABAJADOR (numero_identificacion, nombres, apellidos, id_cargo, jefe, id_estado, id_gerencia, id_unidad_organizativa, id_target_bpr, id_target_opr,
	     id_tipo_regimen, id_empresa, identificacion, email)
		 VALUES (c.numero_identificacion, c.nombres, c.apellidos, l_cargo, l_jefe, l_nuevo_estado, l_gerencia, l_unidad_organizativa, l_target_bpr, l_target_opr, 
		 l_tipo_regimen, l_empresa, l_identificacion, l_email);
       UPDATE TEMP_TRABAJADOR SET LOG = 'Trabajador creado' WHERE numero_identificacion = c.numero_identificacion;
    end if; 

    EXCEPTION
  WHEN OTHERS THEN
	--dbms_output.put_line(SQLERRM||' identificacion:'||c.numero_identificacion||' error 1');
    l_error := SQLERRM||' identificacion:'||c.numero_identificacion||' error 1';
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualización - '||l_error WHERE numero_identificacion = c.numero_identificacion;
	 dbms_output.put_line( dbms_utility.format_error_backtrace );
  END;

 else --El estado es desconocido por tanto no se puede hacer ni actualización ni creación
	--dbms_output.put_line(SQLERRM||' identificacion:'||c.numero_identificacion||' error '||l_nuevo_estado);
    l_error := SQLERRM||' identificacion:'||c.numero_identificacion||' error 1';
     UPDATE TEMP_TRABAJADOR SET LOG = 'Error en actualización - '||l_error WHERE numero_identificacion = c.numero_identificacion;

 end if;

 END LOOP;

END;
/