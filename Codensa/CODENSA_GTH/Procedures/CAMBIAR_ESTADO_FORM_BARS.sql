CREATE OR REPLACE PROCEDURE codensa_gth."CAMBIAR_ESTADO_FORM_BARS" (pformulario IN FORMULARIO_BARS.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from FORMULARIO_BARS
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    null;
  else
    --Verificar tipo_estado_bars = 'FORMULARIO_BARS'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = 'FORMULARIO_BARS';
    if vconteo_filas >= 1 then --El tipo_estado_bars sí existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = 'FORMULARIO_BARS' and rownum = 1;
    else --El tipo_estado_bars no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vtipo_estado
        from TIPO_ESTADO_BARS;
      INSERT INTO TIPO_ESTADO_BARS (id, nombre)
           values ( vtipo_estado, 'FORMULARIO_BARS' );
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vestado
        from ESTADO_BARS;
      INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
        values ( vestado, upper(pestado), vtipo_estado );
    end if;
    --Actualizar el estado del formulario  
    UPDATE FORMULARIO_BARS SET estado = vestado, fecha_evaluacion = trunc(sysdate) where id = pformulario;
  end if;
EXCEPTION
  when others then
    raise_application_error(-20000,'Un error inesperado ocurrió en CAMBIO_ESTADO_FORMS_BARS. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;

/