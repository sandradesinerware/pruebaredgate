CREATE OR REPLACE PROCEDURE codensa_gth."SNW_CREATE_USER" (
    p_NUMERO_ASIGNADO NUMBER )
IS
  l_trabajador trabajador%rowtype;
  l_password VARCHAR2(60);
BEGIN
   SELECT
    *
     INTO
    l_trabajador
     FROM
    TRABAJADOR
    WHERE
    NUMERO_IDENTIFICACION        = p_NUMERO_ASIGNADO;
  l_password := 1;
  -- ID de USUARIO se llena por el trigger
   INSERT INTO USUARIO (NUMERO_IDENTIFICACION,FECHA_EXPIRACION,USER_NAME,PASSWORD,FIRST_TIME)
    VALUES (l_trabajador.NUMERO_IDENTIFICACION, to_date('2099-12-31','YYYY-MM-DD'), 'CO'||l_trabajador.identificacion , l_password, 'Y');
END;

/