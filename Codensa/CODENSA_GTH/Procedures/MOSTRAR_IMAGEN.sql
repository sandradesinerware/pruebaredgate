CREATE OR REPLACE PROCEDURE codensa_gth."MOSTRAR_IMAGEN" (p_image_id IN NUMBER)
AS
   l_mime        VARCHAR2 (255);
   l_length      NUMBER;
   l_file_name   VARCHAR2 (2000);
   lob_loc       BLOB;
BEGIN
   SELECT mime_type, blob_content, file_name, DBMS_LOB.getlength (blob_content)
     INTO l_mime, lob_loc, l_file_name, l_length
     FROM TRABAJADOR
    WHERE NUMERO_IDENTIFICACION = p_image_id;

   OWA_UTIL.mime_header (NVL (l_mime, 'application/octet'), FALSE);
   HTP.p ('Content-length: ' || l_length);
   OWA_UTIL.http_header_close;
   WPG_DOCLOAD.download_file (lob_loc);
END MOSTRAR_IMAGEN;

/