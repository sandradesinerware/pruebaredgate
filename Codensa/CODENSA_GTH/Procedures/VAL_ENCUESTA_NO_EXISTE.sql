CREATE OR REPLACE PROCEDURE codensa_gth."VAL_ENCUESTA_NO_EXISTE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_BARS.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 select count(*) into vconteo_filas
   from FORMULARIO_BARS
   where evaluado = pevaluado and periodo = pano and ptipo = tipo;
 if vconteo_filas = 0 then --La evaluación no existe
   preturn := 1;
   pmensaje := 'El trabajador identificado con el número ' || pevaluado || ' no tiene una evaluación de tipo ' || ptipo || ' en el periodo '
     || pano || '. ';
 else
   preturn := 0;
   pmensaje := 'El trabajador identificado con el número ' || pevaluado || ' tiene una evaluación de tipo ' || ptipo || ' en el periodo '
     || pano || '. ';
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_ENCUESTA_NO_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;

/