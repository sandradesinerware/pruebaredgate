CREATE OR REPLACE PROCEDURE codensa_gth."BARS_VALCREAPDI" (pperiodo IN PDI_PERIODO.id%type,  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo NUMBER;
 vestado_activo NUMBER;
BEGIN
 --Verificar estado del periodo
 SELECT count(*) into vconteo
  FROM PDI_PERIODO
  WHERE id = pperiodo and ESTADO = 1;

 IF vconteo = 1 then -- El periodo está en estado ACTIVO
  pmensaje := 'Puede crear un nuevo PDI. El periodo está activo. ';
  preturn := 1;
 ELSE
  pmensaje := 'El periodo está inactivo. ';
  preturn := 0;
 END IF;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en BARS_VALCREAPDI. Por favor, infórmelo al administrador de la aplicación. ' 
      || SQLCODE || ' - ' || SQLERRM);

END BARS_VALCREAPDI;

/