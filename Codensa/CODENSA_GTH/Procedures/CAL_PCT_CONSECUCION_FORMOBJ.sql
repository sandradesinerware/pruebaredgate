CREATE OR REPLACE PROCEDURE codensa_gth."CAL_PCT_CONSECUCION_FORMOBJ" ( i_formevaobj in BARS_FORMEVAOBJ.id%type ) as 
/*Calcula el porcentaje de consecución promedio ponderado del formulario, 
transformándolo en base 100%, y actualiza el valor en la tabla*/
l_pctcons_crudo NUMBER(5,2); --Porcentaje de consecución promedio para objetivos totales según el peso original
l_pctcons_base100 NUMBER(5,2); --Porcentaje de consecución promedio para objetivos totales en base 100%
l_peso NUMBER(5,2); --Peso en proporción de los objetivos totales en la evaluación integral
l_periodo BARS_PEROBJ.ano%type;
l_evaluado BARS_FORMEVAOBJ.evaluado%type;
l_target_opr TIPO.id%type;

BEGIN
  --Calcular porcentaje consecución ponderado para objetivos sin interpolación
  select sum( (ponderacion/100)*pctconspond ) into l_pctcons_crudo
    from BARS_OBJETIVO
    where formevaobj = i_formevaobj;
  
  --Calcular peso de objetivos en la evaluación del trabajador con su target actual
  select periodo, evaluado into l_periodo, l_evaluado
    from BARS_FORMEVAOBJ
    where id = i_formevaobj;
  select id_target_opr into l_target_opr
    from TRABAJADOR
    where numero_identificacion = l_evaluado;
  l_peso := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_target_opr,
    SNW_CONSTANTES.get_id_tipo('OPR_ABI', 'TIPO_EVALUACION_TARGET') )
    + VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_target_opr,
    SNW_CONSTANTES.get_id_tipo('OPR_CER', 'TIPO_EVALUACION_TARGET') );

  --Interpolación de la base de peso objetivos abiertos a 100
  l_pctcons_base100 := l_pctcons_crudo * (1/l_peso);
  UPDATE BARS_FORMEVAOBJ set pctconsform = l_pctcons_base100
    where id = i_formevaobj;

/* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
  SELECT count(*) into l_conteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 60 and periodo = l_periodo AND l_Resultado_Ponderado BETWEEN media_comp_inf AND media_comp_sup;

    if l_conteo_filas = 1 then --Se determinó el porcentaje de consecución  
      SELECT pct_consecucion into l_pctconspond
      from PCT_CONSECUCION_BARS
      where tipo_id = 60 and periodo = l_periodo AND l_Resultado_Ponderado BETWEEN media_comp_inf AND media_comp_sup;

      update BARS_FORMEVAOBJ set pctconsform = l_pctconspond where id=i_formulario;

    else --No se determinó un porcentaje de consecución
      l_pctconspond := NULL;
    end if;
*/
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error(-20000,'Error en el cálculo de la consecución total para objetivos del formulario: '||i_formevaobj);
end CAL_PCT_CONSECUCION_FORMOBJ;
/