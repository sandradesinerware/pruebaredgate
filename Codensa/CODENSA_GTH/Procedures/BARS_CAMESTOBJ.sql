CREATE OR REPLACE PROCEDURE codensa_gth."BARS_CAMESTOBJ" (pformulario IN BARS_FORMEVAOBJ.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is

    vconteo_filas NUMBER;
    l_evaluado NUMBER;
    l_evaluador NUMBER;
    l_target_opr NUMBER;
    vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
    vestado ESTADO_BARS.ID%TYPE;
    faltan_parametros EXCEPTION;
    formulario_inexistente EXCEPTION;
 
BEGIN

  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from BARS_FORMEVAOBJ
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    RAISE formulario_inexistente;
  else
    --Verificar tipo_estado_bars = 'OBJETIVOS_BARS'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = 'OBJETIVOS_BARS';
    if vconteo_filas >= 1 then --El tipo_estado_bars sí existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = 'OBJETIVOS_BARS' and rownum = 1;
    else --El tipo_estado_bars no existe. Generar excepción
      RAISE faltan_parametros;
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Generar excepción
      RAISE faltan_parametros;
    end if;
    --Actualizar el estado del formulario
    --atroncoso 20150304 se agrega condición 
    
    if pestado = 'CONCERTACION APROBADA' then
        
        -- Se persiste en el formulario el TARGET del Trabajador
        select EVALUADO into l_evaluado from bars_formevaobj where id = pformulario;
        select ID_TARGET_OPR, JEFE into l_target_opr, l_evaluador from trabajador where NUMERO_IDENTIFICACION = l_evaluado;  
    
        UPDATE BARS_FORMEVAOBJ SET estado = vestado, fecheva = trunc(sysdate), evaluador = l_evaluador,
        PCTCONSFORM = null, RESULTADO_PONDERADO = null,POSICION = null, EVALUADOR_RESULTADO = null, ID_TARGET_OPR = l_target_opr
        where id = pformulario;
    else
        UPDATE BARS_FORMEVAOBJ SET estado = vestado, fecheva = trunc(sysdate) 
        where id = pformulario;
    end if;
  end if;
EXCEPTION
  when faltan_parametros then
    raise_application_error(-20000,'Falta el estado deseado para el cambio de estado en BARS_CAMESTOBJ. 
      Favor contactar al administrador del sistema. ');
  when formulario_inexistente then
    raise_application_error(-20000,'Falta el formulario para el cambio de estado en BARS_CAMESTOBJ. 
      Favor contactar al administrador del sistema. ');
  when others then
    raise_application_error(-20000,'Un error inesperado ocurrió en BARS_CAMESTOBJ. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;
/