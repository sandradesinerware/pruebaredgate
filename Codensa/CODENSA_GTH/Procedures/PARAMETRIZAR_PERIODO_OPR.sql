CREATE OR REPLACE PROCEDURE codensa_gth."PARAMETRIZAR_PERIODO_OPR" (p_periodo IN BARS_PEROBJ.ano%type) IS
--Parametrización de periodos de OPR GTH en CODENSA
--Premisa: los parametros se copian del periodo anterior y luego se editan si es necesario
--El periodo a parametrizar debe existir en BARS_PEROBJ
--Se depende del trigger de creación de la llave primaria de los registros a insertar para RESTRICCION_TARGET y PCT_CONSECUCION_BARS
l_cuenta_bars_posicion NUMBER;

BEGIN
--RESTRICCION POR TARGET
insert into RESTRICCION_TARGET (id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, periodo) 
  ( select id_target, id_tipo_evaluacion, porcentaje_total, pct_ind_min, pct_ind_max, p_periodo
      from RESTRICCION_TARGET
      where periodo = (p_periodo - 1) 
  );

/* ! ! ! Se elimina por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! ! :
    --PCT CONSECUCION BARS
    insert into PCT_CONSECUCION_BARS (pct_consecucion, media_comp_inf, media_comp_sup, periodo, tipo_id)
    (
      select pct_consecucion, media_comp_inf, media_comp_sup, p_periodo, tipo_id 
      from PCT_CONSECUCION_BARS 
      where periodo = (p_periodo - 1)
    );
*/

--BARS_POSICION
select count(*) into l_cuenta_bars_posicion
  from BARS_POSICION
  where periodo = (p_periodo - 1);

insert into BARS_POSICION (id, nombre, pct_inf, pct_sup, periodo, tipo_id)
(
  select (id+l_cuenta_bars_posicion), nombre, pct_inf, pct_sup, p_periodo, tipo_id
  from bars_posicion 
  where periodo = (p_periodo - 1)
);

END;
/