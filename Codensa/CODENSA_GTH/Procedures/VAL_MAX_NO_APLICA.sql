CREATE OR REPLACE PROCEDURE codensa_gth."VAL_MAX_NO_APLICA" (pseccion_formulario IN SECCION_FORMULARIO.ID%TYPE,
  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vsuma NUMBER;
BEGIN
 SELECT NVL(sum(RESPUESTA.valor),0) into vsuma
   from RESPUESTA
   where RESPUESTA.SECCION_FORMULARIO = pseccion_formulario;
 if (vsuma = 0) then --Todas las respuestas fueron 'No aplica'
   preturn := 0;
   pmensaje := 'No se puede almacenar la sección. Al menos una respuesta debe ser diferente a "No Aplica". ';
 else
   preturn := 1;
   pmensaje := 'La sección no supera el máximo número de respuestas "No Aplica". ';
 end if;
EXCEPTION
 WHEN OTHERS THEN 
   preturn := 0;
   pmensaje := 'Error inesperado en VAL_MAX_NO_APLICA. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;

/