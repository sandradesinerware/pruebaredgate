CREATE OR REPLACE PROCEDURE codensa_gth."CARGUE_TRABAJADOR_JURISDICCION" IS 

l_id_jurisdiccion NUMBER;
l_conteo NUMBER;

BEGIN

/* Procedimiento que actualiza las jurisdicciones de los trabajadores segun la tabla TEMP_TRABAJADOR_JURISDICCION */
-- Se itera sobre cada grupo en la tabla temporal

    for i in (SELECT * FROM TEMP_TRABAJADOR_JURISDICCION) loop

        begin


            -- Se busca si el trabajador existe
            select count(*) into l_conteo from trabajador where NUMERO_IDENTIFICACION = i.asignado;

            if l_conteo > 0 then

                -- Obtener el id de tipo de la jurisdiccion que se va a actalizar
                select id into l_id_jurisdiccion from tipo where upper(nombre) = i.jurisdiccion;

                -- Se actualiza la jurisdiccion del trabajdor
                update trabajador set ID_TIPO_JURISDICCION = l_id_jurisdiccion where NUMERO_IDENTIFICACION = i.asignado;

            end if;  

            exception when others then 
                dbms_output.put_line('Error con el trabajador y jurisdiccion: ' || i.asignado||' - '||i.jurisdiccion||' = = = > '||' - '||sqlerrm);             
            end; 
    end loop; 
END;
/