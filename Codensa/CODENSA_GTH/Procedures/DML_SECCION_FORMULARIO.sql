CREATE OR REPLACE PROCEDURE codensa_gth."DML_SECCION_FORMULARIO" (pformulario IN
    FORMULARIO_BARS.ID%type, pseccion IN SECCION_BARS.ID%TYPE,
  ptipo IN SECCION_FORMULARIO.TIPO%TYPE, pnum_rtasx IN NUMBER, prta_1 IN RESPUESTA.valor%type DEFAULT NULL,
  prta_2 IN RESPUESTA.valor%type DEFAULT NULL, prta_3 IN RESPUESTA.valor%type DEFAULT NULL,
  prta_4 IN RESPUESTA.valor%type DEFAULT NULL, prta_5 IN RESPUESTA.valor%type DEFAULT NULL)
IS
 vseccion_formulario SECCION_FORMULARIO.ID%TYPE;
 vplantilla_pregunta PLANTILLA_PREGUNTAS.ID%TYPE;
 vnombre_parametro VARCHAR2(10);
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vconteo_filas NUMBER;
 vpromedio NUMBER;
 pnum_rtas NUMBER;
BEGIN
  --Determinar si la sección existe
  SELECT count(*) into vconteo_filas
    from SECCION_FORMULARIO
    where formulario = pformulario and seccion = pseccion and tipo = ptipo;


    select count(*) into pnum_rtas 
    from plantilla_preguntas 
    where plantilla_formulario in (select plantilla from formulario_bars where id=pformulario) 
    and seccion=pseccion;

  if vconteo_filas = 0 then --La sección no existe
  Dbms_Output.Put_Line('--La sección no existe');
    --Insertar la sección formulario
    SELECT NVL( max(id),0 )+1 into vseccion_formulario
      from SECCION_FORMULARIO;
    INSERT INTO SECCION_FORMULARIO (formulario, id, seccion, tipo)
      values (pformulario, vseccion_formulario, pseccion, ptipo);
    --Insertar las respuestas
    FOR i IN 1..pnum_rtas LOOP
    Dbms_Output.Put_Line('Loop '||i||' de '||pnum_rtas);
      --Determinar la plantilla de la pregunta
      SELECT PLANTILLA_PREGUNTAS.id into vplantilla_pregunta
        from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.plantilla = PLANTILLA_FORMULARIO.id)
        inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.id = PLANTILLA_PREGUNTAS.plantilla_formulario)
        where FORMULARIO_BARS.id = pformulario and PLANTILLA_PREGUNTAS.seccion = pseccion and
        PLANTILLA_PREGUNTAS.codigo_pregunta = i;
      vnombre_parametro := 'prta_' || to_char(i);
      INSERT INTO RESPUESTA (id, seccion_formulario, plantilla_pregunta, valor)
        values ( respuesta_seq.nextval, vseccion_formulario, vplantilla_pregunta,
        CASE vnombre_parametro WHEN 'prta_1' then prta_1 WHEN 'prta_2' then prta_2 WHEN 'prta_3' then prta_3
        when 'prta_4' then prta_4 when 'prta_5' then prta_5 END);
    END LOOP;
  else --La sección ya existe
  Dbms_Output.Put_Line('La seccion ya existe ');
    --Obtener la sección formulario
    SELECT id into vseccion_formulario
      from SECCION_FORMULARIO
      where formulario = pformulario and seccion = pseccion and tipo = ptipo;
    --Actualizar las respuestas
     FOR i IN 1..pnum_rtas LOOP
      --Determinar la plantilla de la pregunta
      SELECT PLANTILLA_PREGUNTAS.id into vplantilla_pregunta
        from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.plantilla = PLANTILLA_FORMULARIO.id)
        inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.id = PLANTILLA_PREGUNTAS.plantilla_formulario)
        where FORMULARIO_BARS.id = pformulario and PLANTILLA_PREGUNTAS.seccion = pseccion and
        PLANTILLA_PREGUNTAS.codigo_pregunta = i;
      vnombre_parametro := 'prta_' || to_char(i);
      UPDATE RESPUESTA
        SET valor = CASE vnombre_parametro WHEN 'prta_1' then prta_1 WHEN 'prta_2' then prta_2 WHEN 'prta_3' then prta_3
        when 'prta_4' then prta_4 when 'prta_5' then prta_5 END
        where seccion_formulario = vseccion_formulario and plantilla_pregunta = vplantilla_pregunta;
    END LOOP;
  end if;
  --Actualizar el promedio de la sección del formulario
  vpromedio := DETERMINAR_PROMEDIO_SECCION(vseccion_formulario);
  UPDATE SECCION_FORMULARIO
    set resultado = round(vpromedio,4)
    where id = vseccion_formulario;

-- ATRONCOSO 20150203 Se integra el cambio de estado en el dml

    if pseccion = 1 then
      CAMBIAR_ESTADO_FORM_BARS(pformulario, 'EN TRATAMIENTO');
    end If;
    if pseccion = 7 then    
      CAMBIAR_ESTADO_FORM_BARS(pformulario, 'FACTORES FINALIZADOS');
    end If;

 EXCEPTION WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DML_SECCION_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);
END;

/