CREATE OR REPLACE PROCEDURE codensa_gth."BARS_SUBMITPDI" (pitem IN VARCHAR2, pformulario IN BARS_PDI.ID%TYPE, pmsj OUT VARCHAR2)
IS
kplaneado CONSTANT VARCHAR2(30) := 'PLANEADO';
kconcertado CONSTANT VARCHAR2(30) := 'CONCERTADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
knoaprobado CONSTANT VARCHAR2(30) := 'NO APROBADO';
kadministradores CONSTANT VARCHAR2(30) := 'ADMINISTRADORES';
kmodoconsulta CONSTANT VARCHAR2(30) := 'CONSULTA';
kmodoedicion CONSTANT VARCHAR2(30) := 'EDICION';
ktipoestadopdi CONSTANT VARCHAR2(30) := 'PDI';
vtipo_estado NUMBER(2,0);
vestado_formulario ESTADO_BARS.nombre%type;
vestado_anterior ESTADO_BARS.nombre%type;
vevaluado BARS_FORMEVAOBJ.EVALUADO%TYPE;
vgestor TRABAJADOR.JEFE%TYPE; --Gestor actual del trabajador
vevaluador BARS_encPDI.EVALUADOR%type; --Gestor registrado en el PDI como concertador
vaccion tipo.nombre%type;
vconteo NUMBER;


BEGIN
/*Para el caso en que se actualiza el formulario (posibilidad única del Trabajador cuando está nuevo o planeado, o del administrador
  se requiere saber en qué estado se almacenó (solo el administrador puede editar manualmente dicho campo). Tener presente que este
  procedimiento se ejecuta después del process row
  */
BEGIN
 SELECT upper(ESTADO_BARS.nombre) into vestado_formulario
 FROM ESTADO_BARS
 WHERE ESTADO_BARS.id = ( SELECT ID_estado FROM BARS_PDI WHERE id = pformulario );
EXCEPTION
 WHEN NO_DATA_FOUND THEN --El formulario está recién creado y sin estado asignado (como cuando se crea el PDI)
      vestado_formulario := 'NUEVO';
END;
--Determinar acción de desarrollo
SELECT upper(nombre) into vaccion
  FROM tipo 
  WHERE id = ( SELECT ID_accion FROM BARS_PDI WHERE id = pformulario );

--Cambios de estado de pdis de acuerdo al item(request) y al estado del formulario
CASE upper(pitem)
WHEN 'CREATE' THEN
  BARS_CAMESTPDI(pformulario, kplaneado);
  pmsj := ' El PDI ha sido planeado. Solicite a su gestor que apruebe la concertación del PDI. ';
WHEN 'SAVE' THEN

  --El comportamiento depende del estado que se asignó al formulario
  BEGIN
    SELECT upper(nombre) into vestado_anterior from ESTADO_BARS where id = v('P78_ESTADO_ANTERIOR');
  EXCEPTION
    WHEN NO_DATA_FOUND THEN --El estado anterior era nulo (como cuando se crea el PDI)
      vestado_anterior := 'NUEVO';
  END;

  CASE vestado_formulario
    WHEN kplaneado THEN --Aplica cuando se hacen modificaciones estando planeado o el administrador lo devuelve a planeado
      UPDATE BARS_ENCPDI SET evaluador = null
        WHERE id = pformulario;
      pmsj := ' El PDI está en estado planeado. Puede ser modificado hasta que solicite al gestor que apruebe la concertación del PDI. ';
    WHEN kconcertado THEN --Aplica cuando el administrador lo modifica a estado concertado
      IF vestado_anterior != kconcertado then --El cambio sí fue de estado
        UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) )
          WHERE id = pformulario;
        pmsj := ' El PDI ha sido concertado. Cuando sea ejecutado, ingrese los datos faltantes y Finalícelo. ';
      ELSE
        pmsj := ' Cambios en el PDI almacenados. ';
      END IF;
    WHEN knoaprobado THEN --Aplica cuando el administrador lo modifica a estado no aprobado
      IF vestado_anterior != knoaprobado then --El cambio sí fue de estado
        UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) )
          WHERE id = pformulario;
        pmsj := ' El PDI no ha sido aprobado.';
      ELSE
        pmsj := ' Cambios en el PDI almacenados. ';
      END IF;
    WHEN kanulado THEN --Aplica cuando el administrador lo modifica a estado anulado
      pmsj := ' El PDI ha sido anulado.';
   -- Esta acción se hace en la fase 2      
    /*WHEN kfinalizado THEN --Aplica cuando el administrador lo modifica a estado finalizado
      SELECT count(*) into vconteo
        FROM BARS_PDI
        WHERE id = pformulario AND NVL(estfinformacion, 0) = 
          CASE WHEN ( vaccion LIKE 'FORMACI_N' ) THEN NVL(estfinformacion, -1) ELSE NVL(estfinformacion, 0) END;
      if (vconteo = 0) then --no tiene los campos requeridos para finalizarse. Se devuelve al estado anterior
        BARS_CAMESTPDI( pformulario, vestado_anterior );
        pmsj := ' El PDI no puede finalizarse. Debe registrar el concepto sobre asistencia a la actividad de formación. ';
      else
        SELECT evaluador into vevaluador
          FROM BARS_ENCPDI
          WHERE id = pformulario;
        if vevaluador IS NULL then --El administrador probablemente lo está pasando de PLANEADO a FINALIZADO
          UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) )
            WHERE id = pformulario;
        end if;
        pmsj := ' El PDI fue finalizado. ';
      end if;*/
    ELSE
      NULL;
    END CASE;

--WHEN 'DELETE' THEN
 -- NULL;
WHEN 'CONCERTAR' THEN
  UPDATE BARS_ENCPDI SET evaluador = ( SELECT numero_identificacion FROM USUARIO WHERE upper(user_name) = upper(v('APP_USER')) ) 
    WHERE id = pformulario;
  BARS_CAMESTPDI(pformulario, kconcertado);
  pmsj := ' El PDI ha sido concertado. Cuando sea ejecutado, ingrese los datos faltantes y Finalícelo. ';
WHEN 'REPROBAR_CONCERTACION' THEN
  UPDATE BARS_ENCPDI SET evaluador = v('APP_USER') WHERE id = pformulario;
  BARS_CAMESTPDI(pformulario, knoaprobado);
  pmsj := ' El PDI no ha sido aprobado.';

-- Esta acción se hace en la fase 2
/*WHEN 'FINALIZAR' THEN --Este botón no llama al proceso de almacenamiento. Solo administrador lo usa.
  if ( vaccion LIKE 'FORMACI_N' AND v('P78_ESTFINFORMACION') is null ) then --no tiene los campos requeridos para finalizarse
    pmsj := ' El PDI no puede finalizarse. Debe registrar el concepto sobre asistencia a la actividad de formación. ';
  else
    UPDATE BARS_PDI 
      SET fecha_inicio = v('P78_FECHA_INICIO'), fecha_fin = v('P78_FECHA_FIN'), estfinformacion = v('P78_ESTFINFORMACION'),
        tutor = v('P78_TUTOR')
      WHERE id = pformulario;
    BARS_CAMESTPDI(pformulario, kfinalizado);
    pmsj := ' El PDI fue finalizado. ';
  end if;*/
ELSE
  pmsj := ' El PDI fue eliminado. ';
END CASE;

EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en BARS_SUBMITPDI. Por favor, infórmelo al administrador de la aplicación. ' 
      || SQLCODE || ' - ' || SQLERRM);
END BARS_SUBMITPDI;

/