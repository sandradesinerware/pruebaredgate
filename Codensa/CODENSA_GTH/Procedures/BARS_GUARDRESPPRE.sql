CREATE OR REPLACE PROCEDURE codensa_gth."BARS_GUARDRESPPRE" (pformulario IN BARS_FORMEVAPRE.ID%type, pfactor IN BARS_FACPRE.ID%TYPE,
  prespuesta IN BARS_TIPORESP_OPCION.ID%TYPE)
IS
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vconteo_filas NUMBER;
BEGIN
  --Verificar que el factor exista y tenga asociado un tipo de respuesta
  SELECT count(*) into vconteo_filas
    from BARS_FACPRE
    where id = pfactor and tiporesp is not null;
  if vconteo_filas = 0 then --El factor no existe o no es válido para tener respuesta asociada
    null;
  else
    --Verificar que la respuesta corresponda a las posibles para el factor
	SELECT count(*) into vconteo_filas
	  from BARS_TIPORESP_OPCION
	  where tiporesp = (select tiporesp from BARS_FACPRE where id = pfactor) and id = prespuesta;
	if vconteo_filas = 0 then --La respuesta no es válida para el factor
	  null;
	else
	  --Verificar si la respuesta está siendo insertada o actualizada
	  SELECT count(*) into vconteo_filas
	    from BARS_RESPFORMEVAPRE
		where formevapre = pformulario and facpre = pfactor;
      if vconteo_filas = 0 then --Insertar la respuesta
	    INSERT INTO BARS_RESPFORMEVAPRE (formevapre, facpre, tiporesp_opcion)
	      values (pformulario, pfactor, prespuesta);
	  else
	    UPDATE BARS_RESPFORMEVAPRE set tiporesp_opcion = prespuesta
		  where formevapre = pformulario and facpre = pfactor;
	  end if;
	end if;  
  end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en BARS_GUARDRESPPRE.' || SQLCODE || ' - ' || SQLERRM);
END;

/