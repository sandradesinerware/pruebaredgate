CREATE OR REPLACE PROCEDURE codensa_gth."VAL_PEROBJ" 
    (i_ano IN BARS_PEROBJ.ano%type,
     i_estado IN BARS_PEROBJ.ESTADO%type, 
     preturn OUT NUMBER, 
     pmensaje OUT VARCHAR2)
IS

 vconteo_filas NUMBER;
 l_estado VARCHAR2(100);
 
BEGIN

    select nombre into l_estado 
    from estado_bars 
    where tipo_estado = 1 and id = i_estado;
    
     SELECT count(*) into vconteo_filas
       from BARS_PEROBJ
       where ano = i_ano and estado = i_estado;
     if vconteo_filas = 0 then 
       preturn := 0;
       pmensaje := 'El periodo de evaluación no está en el estado: ' || l_estado || '. ';
     else
       preturn := 1;
       pmensaje := 'El periodo de evaluación está en el estado: ' || l_estado || '. ';
     end if;
    
    EXCEPTION
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en VAL_ESTADO_PERIODO. ' || SQLCODE || ' - '
         || SQLERRM);
END;
/