CREATE OR REPLACE PROCEDURE codensa_gth."SNW_CREATE_USER_CONTRATISTA" (
    p_cedula IN NUMBER )
IS
  l_contratista contratista%rowtype;
  l_user USUARIO%rowtype;
  l_password VARCHAR2(60);
  l_id_grupo NUMBER;

BEGIN
   SELECT * INTO l_contratista
     FROM
    CONTRATISTA
    WHERE NIT        = p_cedula;

  BEGIN
  -- ID de USUARIO se llena por el trigger
   INSERT INTO USUARIO (NUMERO_IDENTIFICACION,FECHA_EXPIRACION,USER_NAME,PASSWORD,FIRST_TIME)
    VALUES (l_contratista.nit, to_date('2099-12-31','YYYY-MM-DD'), to_char(l_contratista.nit) , '1', 'N');
  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN --El usuario ya existe
   null;
  END;

  SELECT *
  INTO l_user
  FROM usuario
  WHERE user_name = to_char(l_contratista.nit);

  l_password :=  rawtohex(   sys.dbms_crypto.hash (  sys.utl_raw.cast_to_raw( l_contratista.nit||l_user.id||upper(to_char(l_contratista.nit)) ), 2  )   );

  update usuario set password = l_password where user_name = to_char(l_contratista.nit);

  --Por omisión un contratista es usuario de tipo Evaluador de Cliente Interno
  select id into l_id_grupo from grupo where trim(upper(nombre)) = 'EVALUADORES DE SERVICIO';

  BEGIN
    insert into usu_grupo (grupo, usuario) values (l_id_grupo, l_user.numero_identificacion);
  EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN --El usu_grupo ya existe
   null;
  END;

EXCEPTION
WHEN OTHERS THEN  
  dbms_output.put_line( dbms_utility.format_error_backtrace );
  raise_application_error(-20001, 'Error encontrado en crear usuario contratista - ' || SQLCODE || ' - ' || SQLERRM);

END;

/