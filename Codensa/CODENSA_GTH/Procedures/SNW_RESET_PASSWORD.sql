CREATE OR REPLACE PROCEDURE codensa_gth."SNW_RESET_PASSWORD" 
(
  i_numero_asignado in number 
, msg out varchar2 
) as 

l_random_password VARCHAR2(8);
l_username VARCHAR2(60);

Begin

l_random_password := snw_random_password;

select user_name into l_username from usuario where numero_identificacion=i_numero_asignado;

UPDATE usuario
  SET password=rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(l_random_password
    ||id
    ||l_username), 2 )),
    first_time  ='Y'
  WHERE user_name=l_username;

  msg:='Para '||l_username||' la nueva constraseña es: '||l_random_password;

end snw_reset_password;

/