CREATE OR REPLACE PROCEDURE codensa_gth."DETERMINAR_FORMULARIO_OBJ" (
    pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
    pano IN BARS_PEROBJ.ANO%type,     
    pconsecutivo IN NUMBER,   -- Si se recibe 0, es porque no hay evaluaciones o hay solo 1
                              -- Si se recibe mayor a 0, indica el numero del consecutivo para determinar
    pformulario OUT BARS_FORMEVAOBJ.ID%type)

IS

 vconteo_filas NUMBER;
 l_consecutivo NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'OBJETIVOS_BARS';
 l_cargo VARCHAR2(400);
 l_unidad VARCHAR2(400);
 l_id_unidad_organizativa bars_formevaobj.id_unidad_organizativa%TYPE;
 l_id_gerencia bars_formevaobj.id_gerencia%TYPE; 
 l_id_target_opr bars_formevaobj.id_target_opr%type;
 l_fecha_inicio BARS_FORMEVAOBJ.fecha_inicio%type;
 l_fecha_fin BARS_FORMEVAOBJ.fecha_fin%type;

BEGIN

 -- Datos del trabajador que determinan el formulario de objetivos
 SELECT jefe,id_gerencia, id_unidad_organizativa, id_target_opr INTO vevaluador, l_id_gerencia, l_id_unidad_organizativa,l_id_target_opr
   FROM TRABAJADOR
   WHERE numero_identificacion = pevaluado;

 VAL_BARS_PEROBJ(pano, k_estado_activo, vconteo_filas, vmensaje);
 
 -- Si el consecutivo es 0, debe crearse o determinarse el unico existente
 if (pconsecutivo = 0) then

   if (vconteo_filas = 1) then -- El periodo es el activo.

   VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);

   if (vreturn = 0) then -- La evaluación no existe. Crear formulario
     --Verificación existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = 'OBJETIVOS_BARS'
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_formulario;
     if vconteo_filas >= 1 then --El tipo_estado_bars sí existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_formulario and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_formulario );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     
     -- Llave primaria formulario a crear
     SELECT NVL( max(id),0 ) + 1 into pformulario
       from BARS_FORMEVAOBJ;

     -- Cálculo del cargo como nombre de la posicion vigente del evaluado y su Unidad vigente
     BEGIN
     select posicion, unidad_organizativa into l_cargo, l_unidad from snw_cargos.V_POSICION_UO
       where numero_identificacion = pevaluado;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_cargo := 'NO DETERMINADO';
         l_unidad := 'NO DETERMINADA';
     END;
     
     -- EL Numero consecutivo es 1 por ser la primera concertacion
     l_consecutivo := 1;      

     -- Calculo de la fecha inicio y fecha fin
     -- Dado que es la primera concertacion, ocupan todo el rango del periodo en cuestion
     select fecha_inicio into l_fecha_inicio from bars_perobj where ano = pano;
     select fecha_fin into l_fecha_fin from bars_perobj where ano = pano;

       
     -- Creación formulario en estado nuevo
       
         /* Se cambia: (El cargo se desarticula de la creación de objetivos: https://sinerware.atlassian.net/browse/GTH2019-90  
         INSERT INTO BARS_FORMEVAOBJ (id, evaluado, periodo, estado, fecheva, cargo, cencos, zona,id_gerencia, id_unidad_organizativa, evaluador, id_target_opr)
           values (pformulario, pevaluado, pano, vestado, trunc(sysdate), pcargo, null, null,l_id_gerencia, l_id_unidad_organizativa, vevaluador,l_id_target_opr);
         */
         
          -- Creación formulario en estado nuevo
          INSERT INTO BARS_FORMEVAOBJ (id, evaluado, periodo, estado, fecheva, cargo, nombre_cargo, cencos, zona, id_gerencia, id_unidad_organizativa, nombre_unidad_organizativa, id_target_opr, consecutivo, fecha_inicio, fecha_fin)
            values (pformulario, pevaluado, pano, vestado, trunc(sysdate), null, l_cargo, null, null, l_id_gerencia, l_id_unidad_organizativa, l_unidad, l_id_target_opr, l_consecutivo, l_fecha_inicio, l_fecha_fin);
       
    else --La encuesta existe, retornar formulario
      SELECT max(id) into pformulario
        from BARS_FORMEVAOBJ
        where evaluado = pevaluado and periodo = pano;
    end if;

  else -- El periodo no es el activo
    VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);
    if (vreturn = 0) then --La evaluación no existe. Cómo no es periodo activo no se puede crear.
      pformulario := NULL;
    else -- La evaluación existe
      SELECT max(id) into pformulario
        from BARS_FORMEVAOBJ
        where evaluado = pevaluado and periodo = pano;
    end if;
  end if;
 
 else -- Hay mas de una concertación, se retorna la concertacion exacta con: periodo, evaluado, consecutivo
    SELECT max(id) into pformulario
      from BARS_FORMEVAOBJ
      where evaluado = pevaluado and periodo = pano and consecutivo = pconsecutivo;

 end if;

EXCEPTION
when no_data_found then
pformulario := null;
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO_OBJ.' || SQLCODE || ' - ' || SQLERRM);
END;
/