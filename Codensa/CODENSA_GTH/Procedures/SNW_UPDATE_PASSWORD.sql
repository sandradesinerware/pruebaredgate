CREATE OR REPLACE PROCEDURE codensa_gth."SNW_UPDATE_PASSWORD" 
  (
    p_username VARCHAR2,
    p_password VARCHAR2
  )
IS
BEGIN
  UPDATE usuario
  SET password=rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password
    ||id
    ||p_username), 2 )),
    first_time  ='Y'
  WHERE user_name=p_username;
END;

/