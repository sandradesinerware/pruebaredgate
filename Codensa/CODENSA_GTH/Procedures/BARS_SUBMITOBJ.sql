CREATE OR REPLACE PROCEDURE codensa_gth."BARS_SUBMITOBJ" (
    pitem       IN VARCHAR2,
    pformulario IN BARS_FORMEVAOBJ.ID%TYPE,
    pmsj OUT VARCHAR2)
IS
  knuevo             CONSTANT VARCHAR2(30) := 'NUEVO';
  ket                CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
  kconval            CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
  kconap             CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
  kperaj             CONSTANT VARCHAR2(30) := 'CONCERTACION ANULADA';
  kresval            CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
  kfinalizado        CONSTANT VARCHAR2(30) := 'FINALIZADO';
  kanulado           CONSTANT VARCHAR2(30) := 'ANULADO';
  kadministradores   CONSTANT VARCHAR2(30) := 'ADMINISTRADORES OPR';
  vtipo_estado       NUMBER(2,0);
  vestado_formulario VARCHAR2(30);
  vcontador          NUMBER;
  vappuser           NUMBER;
  vevaluado BARS_FORMEVAOBJ.EVALUADO%TYPE;
  vevaluador BARS_FORMEVAOBJ.EVALUADOR%TYPE;
  l_id_target_opr BARS_FORMEVAOBJ.id_target_opr%type;
  vResultadoValFormevaobj NUMBER;
  l_numero_aprobador NUMBER;
  --Colección para almacenar los grupos del usuario
TYPE grupo_array
IS
  TABLE OF GRUPO.NOMBRE%TYPE;
  vgrupos grupo_array;
  i NUMBER(2) := 0;
BEGIN
  --Determinar tipo estado OBJETIVOS_BARS
  SELECT id
  INTO vtipo_estado
  FROM TIPO_ESTADO_BARS
  WHERE upper(nombre) = 'OBJETIVOS_BARS';
  --Determinar estado del formulario de OBJETIVOS, devuelve el estado en mayuscula
  SELECT upper(trim(E.nombre))
  INTO vestado_formulario
  FROM BARS_FORMEVAOBJ F
  INNER JOIN ESTADO_BARS E
  ON F.estado = E.id
  WHERE F.id  = pformulario;
  --Determinar evaluado del formulario y evaluador del trabajador
  SELECT evaluado
  INTO vevaluado
  FROM BARS_FORMEVAOBJ
  WHERE id = pformulario;

  SELECT jefe
  INTO vevaluador
  FROM TRABAJADOR
  WHERE numero_identificacion = vevaluado;
  
  --Si se ejecuta por debajo (ie. desde PL/SQL) por omisión es APEX quien lo ejecuta
  SELECT numero_identificacion
      INTO vappuser
      FROM USUARIO
      WHERE UPPER (user_name)= NVL(UPPER(v('APP_USER')),'APEX');

  --Determinar grupos (i.e. roles) del usuario
  vgrupos := grupo_array();
  FOR rec_grupo IN
  (SELECT upper(trim(gb.nombre)) nombre
  FROM USU_GRUPO ubg
  INNER JOIN GRUPO gb
  ON ubg.grupo = gb.id
  INNER JOIN USUARIO u
  ON (u.id=ubg.usuario)
  WHERE UPPER(u.USER_NAME)   = UPPER(v('APP_USER'))
  )
  LOOP
    i := i + 1;
    vgrupos.EXTEND(1);
    vgrupos(i) := rec_grupo.nombre;
  END LOOP;

  --Cambio de estado del formulario de acuerdo al item de la acción
  CASE upper(pitem)

  WHEN 'LISTA_OBJETIVOS_SAVE' THEN
    SELECT COUNT(*)
    INTO vcontador
    FROM BARS_OBJETIVO
    WHERE formevaobj       = pformulario;
    IF vestado_formulario != kperaj AND vcontador >= 1 THEN
      BARS_CAMESTOBJ (pformulario, ket);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_ADD' THEN
    SELECT COUNT(*)
    INTO vcontador
    FROM BARS_OBJETIVO
    WHERE formevaobj        = pformulario;
    IF (vestado_formulario != kperaj) AND (vcontador >= 1) THEN
      BARS_CAMESTOBJ (pformulario, ket);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_DELETE' THEN
    SELECT COUNT(*)
    INTO vcontador
    FROM BARS_OBJETIVO
    WHERE formevaobj = pformulario;
    IF (vestado_formulario != kperaj) AND (vcontador >= 1) THEN --Se podría pasar a NUEVO un formulario con comentarios o fortalezas
      BARS_CAMESTOBJ (pformulario, ket);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    ELSE
      BARS_CAMESTOBJ (pformulario, knuevo);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_VALIDAR_CONCERTACION' THEN
    DECLARE

      l_obj_cer BOOLEAN;
      l_obj_abi BOOLEAN;
      l_peso_requerido NUMBER;
      l_periodo NUMBER;
      l_target NUMBER;
          
    BEGIN
    
    l_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,1011);--Validar Objetivos Cerrados
    l_obj_abi := validaciones_gui.val_peso_total_objetivos(pformulario,1010);--Validar Objetivos Abiertos

      IF ((l_obj_cer and l_obj_abi) or l_obj_abi) THEN
        BARS_CAMESTOBJ (pformulario, kconval);
        UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate WHERE ID = pformulario;
        pmsj := 'Su concertación de objetivos ha sido validada. ';
        
      ELSE
      if(not l_obj_cer) then
       pmsj := 'Su concertación no es válida. La suma de la ponderacion de objetivos cerrados no cumple el porcentaje para el Target del evaluado.';
      end if;
      if(not l_obj_abi) then
       select periodo, id_target_opr into l_periodo, l_target from BARS_FORMEVAOBJ where id = pformulario;
       l_peso_requerido := (100 * VALIDACIONES_GUI.calc_peso_requerido_evaluacion(l_periodo, l_target, 1162));
       pmsj := 'Su concertación no es válida. La suma de la ponderacion de objetivos abiertos no cumple el porcentaje para el Target del evaluado: ' || l_peso_requerido;
      end if;
      END IF;
    END;

  WHEN 'LISTA_OBJETIVOS_APROBAR_CONCERTACION' THEN
  DECLARE 
   l_obj_cer BOOLEAN;
   l_obj_abi BOOLEAN;
   l_peso_requerido NUMBER;
   l_periodo NUMBER;
   l_target NUMBER;
   
  BEGIN
  
    l_obj_cer := validaciones_gui.val_peso_total_objetivos(pformulario,1011);--Validar Objetivos Cerrados
    l_obj_abi := validaciones_gui.val_peso_total_objetivos(pformulario,1010);--Validar Objetivos Abiertos
    
    IF ((l_obj_cer and l_obj_abi) or l_obj_abi) THEN
        UPDATE BARS_FORMEVAOBJ SET evaluador = vevaluador WHERE id = pformulario;
        BARS_CAMESTOBJ (pformulario, kconap);
        UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate WHERE ID = pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHA_ACU_OBJ = sysdate WHERE ID = pformulario;
        --Se actualiza el evaluador (aprobador de la concertación) para que coincida con quien hace la aprobación de la concertación
        pmsj := 'La concertación de objetivos ha sido aprobada. El proceso continuará cuando el gestor ingrese sus resultados al finalizar el periodo. ';
    else
        select periodo, id_target_opr into l_periodo, l_target from BARS_FORMEVAOBJ where id = pformulario;
        l_peso_requerido := (100 * VALIDACIONES_GUI.calc_peso_requerido_evaluacion(l_periodo, l_target, 1162));
        pmsj := 'La concertacion NO ha sido aprobada. La suma de la ponderacion de objetivos abiertos no cumple el porcentaje para el Target del evaluado: ' || l_peso_requerido;
    END IF;

    declare
    l_counter NUMBER;
    begin
    l_counter := 1;
    for i in (select * from bars_objetivo where FORMEVAOBJ = pformulario)
    loop
        update bars_objetivo set objnum = l_counter where id = i.id;
        l_counter := l_counter + 1;
    end loop;
    end;

  END;
  WHEN 'LISTA_OBJETIVOS_MODIFY' THEN
    DECLARE
      sum_ponderacion  NUMBER;
      vcountresultados NUMBER;
      vcountfortalezas NUMBER;
      vpondnul         NUMBER;
    BEGIN

      SELECT COUNT (*)
      INTO vpondnul
      FROM BARS_OBJETIVO
      WHERE FORMEVAOBJ = pformulario
      AND PONDERACION IS NULL;

      SELECT NVL(SUM(PONDERACION),0)
      INTO sum_ponderacion
      FROM BARS_OBJETIVO
      WHERE FORMEVAOBJ = pformulario;

      SELECT COUNT(*)
      INTO vcountresultados
      FROM BARS_OBJETIVO
      WHERE FORMEVAOBJ=pformulario
      AND PCTCONSIND IS NULL ;

      SELECT COUNT(*)
      INTO vcountfortalezas
      FROM BARS_FORTEVAOBJ
      WHERE FORMEVAOBJ=pformulario;


      --Grabar comentarios evaluado por administrador

      IF vpondnul=0  and (validaciones_gui.val_peso_total_objetivos(pformulario,1010)) THEN
        --Pasar a concertación aprobada
        BARS_CAMESTOBJ (pformulario, kconap);
        UPDATE BARS_FORMEVAOBJ SET EVALUADOR =vappuser WHERE ID=pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHA_ACU_OBJ=sysdate WHERE ID=pformulario;
        -- Pasar a resultado validado
        IF vcountresultados=0 THEN
            BARS_CAMESTOBJ (pformulario, kfinalizado);
            UPDATE BARS_FORMEVAOBJ SET EVALUADOR_RESULTADO=vappuser WHERE ID=pformulario;
            UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
        END IF;
      ELSE
        BARS_CAMESTOBJ (pformulario, ket);
      END IF;

     declare
    l_counter NUMBER;
    begin
    l_counter:=1;
    for i in (select * from bars_objetivo where FORMEVAOBJ=pformulario)
    loop
        update bars_objetivo set objnum=l_counter where id = i.id;
        l_counter:=l_counter+1;
    end loop;
    end;

    END;
  WHEN 'LISTA_OBJETIVOS_SAVE_RESULTADOS' THEN
    IF (vestado_formulario != kconap) THEN
      BARS_CAMESTOBJ (pformulario, kconap);
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;
  WHEN 'FORTALEZAS_SAVE' THEN
    IF v('P44_FORTACT') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_FORTEVAOBJ
      WHERE formevaobj = pformulario;
      IF ( vcontador   = 0 ) THEN --No se ha ingresado fortaleza aun
        INSERT
        INTO BARS_FORTEVAOBJ
          (
            formevaobj,
            fortact
          )
          VALUES
          (
            pformulario,
            v('P44_FORTACT')
          );
      ELSE --Ya se ha ingresado la fortaleza
        UPDATE BARS_FORTEVAOBJ
        SET fortact      = v('P44_FORTACT')
        WHERE formevaobj = pformulario;
      END IF;
      pmsj := 'Fueron almacenados las fortalezas o aspectos por fortalecer del trabajador. ';
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;
    IF v('P44_FORTDES') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_FORTEVAOBJ
      WHERE formevaobj = pformulario;
      IF ( vcontador   = 0 ) THEN --No se ha ingresado fortaleza aun
        INSERT
        INTO BARS_FORTEVAOBJ
          (
            formevaobj,
            fortdes
          )
          VALUES
          (
            pformulario,
            v('P44_FORTDES')
          );
      ELSE --Ya se ha ingresado la fortaleza
        UPDATE BARS_FORTEVAOBJ
        SET fortdes      = v('P44_FORTDES')
        WHERE formevaobj = pformulario;
      END IF;
      pmsj := 'Fueron almacenados las fortalezas o aspectos por fortalecer del trabajador. ';
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;

  WHEN 'LISTA_OBJETIVOS_VALIDAR_RESULTADO' THEN

      DECLARE
      CURSOR cobjetivo
      IS
        SELECT pctconsind
        FROM BARS_OBJETIVO
        WHERE formevaobj = pformulario;
      vcontador NUMBER;
      l_tipo_validacion VARCHAR2(20);
      l_peso_eval NUMBER;
      l_periodo NUMBER;
      l_evaluado NUMBER;
      l_peso_planeado_obj_cer BOOLEAN;
      l_peso_planeado_obj_abi BOOLEAN;
      l_peso_requerido_obj_cer NUMBER;
      l_resultado_obj_cer BOOLEAN;
      
    BEGIN
    
       -- Se obtiene el TARGET_OPR del trabajador (ya no del formulario)
    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = pformulario;
    select ID_TARGET_OPR into l_id_target_opr from trabajador where NUMERO_IDENTIFICACION = l_evaluado;  

      --Determinar si cumple ponderación de objetivos para determinar validación de resultado a realizar (Total que lleva a finalizado o solo Abiertos que lleva a resulltado validado)
    l_peso_requerido_obj_cer := VALIDACIONES_GUI.calc_peso_requerido_evaluacion( l_periodo, l_id_target_opr, SNW_CONSTANTES.constante_tipo('OPR_CER') );
    
    
      SELECT count(*) into vcontador
       FROM BARS_OBJETIVO o 
       WHERE o.formevaobj = pformulario and o.pctconsind is null and o.id_tipo = SNW_CONSTANTES.constante_tipo('CERRADO');
      if vcontador = 0 then l_resultado_obj_cer := TRUE; else l_resultado_obj_cer := FALSE; end if;
      l_peso_planeado_obj_cer:=validaciones_gui.val_peso_total_objetivos(pformulario,1011);--Validar Ponderación Requerida de Objetivos Cerrados
      l_peso_planeado_obj_abi:=validaciones_gui.val_peso_total_objetivos(pformulario,1010);--Validar Ponderación Requerida de Objetivos Abiertos

      CASE WHEN ( l_peso_requerido_obj_cer=0 and l_peso_planeado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'TOTAL';
           WHEN ( l_peso_requerido_obj_cer!=0 and l_peso_planeado_obj_cer and l_resultado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'TOTAL';
           WHEN ( l_peso_requerido_obj_cer!=0 and not l_resultado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'SOLO_ABIERTOS';
           WHEN ( l_peso_requerido_obj_cer!=0 and NOT l_peso_planeado_obj_cer and l_resultado_obj_cer and l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'SOLO_ABIERTOS';
           WHEN ( l_peso_requerido_obj_cer!=0 and not l_peso_planeado_obj_abi ) THEN l_tipo_validacion := 'FALLA_CONCERTACION';
           ELSE l_tipo_validacion := 'FALLA_CONCERTACION';
      END CASE;

      if l_tipo_validacion = 'TOTAL' then --Validación Total

      -- << Ahora se valida paramétricamente mediante la tabla RESTRICCION_RESULTADOS_OPR (Ver el issue de Jira para más detalles): >>
      -- https://sinerware.atlassian.net/browse/GTH2019-53 
      
      
        --val_formevaobj (pformulario, vResultadoValFormevaobj, pmsj, 'TOTAL' ); --Validaciones
        --IF ( vResultadoValFormevaobj = 1 ) then --Pasó las validaciones
          cal_pct_consecucion_obj(pformulario);
          
          if ( ADMIN_OBJETIVOS_PCK.CALCULAR_PCTCONSABI( pformulario ) ) then
            null; --No hay comportamiento especial en caso que falle o no
          end if;
          
          select sum(porcentaje_total)*100 into l_peso_eval from restriccion_target 
              where id_target = l_id_target_opr
              and periodo=l_periodo
              and id_tipo_evaluacion in (1161,1162);                             
          cal_pct_ponderado_form(pformulario,l_peso_eval);       
          cal_posicion_obj(pformulario);
          cal_pct_consecucion_formobj(pformulario);
          BARS_CAMESTOBJ (pformulario, kfinalizado);
          pmsj := 'Formulario diligenciado correctamente, muchas gracias ';
          select numero_identificacion into l_numero_aprobador from usuario where upper(user_name)= upper(v('APP_USER'));
          UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate,EVALUADOR_RESULTADO=l_numero_aprobador  WHERE ID=pformulario;
        --ELSE
        --  pmsj := 'Los resultados ingresados no son válidos. ' || pmsj;
        --END IF;

      elsif l_tipo_validacion = 'SOLO_ABIERTOS' then --Validación solo Abiertos
        --val_formevaobj (pformulario, vResultadoValFormevaobj, pmsj, 'ABIERTO' ); --Validaciones
        --IF ( vResultadoValFormevaobj = 1 ) then --Pasó las validaciones
          cal_pct_consecucion_obj(pformulario);
          if ( ADMIN_OBJETIVOS_PCK.CALCULAR_PCTCONSABI( pformulario ) ) then
            null; --No hay comportamiento especial en caso que falle o no
          end if;
          BARS_CAMESTOBJ (pformulario, kresval);
          pmsj := 'Formulario diligenciado correctamente, muchas gracias ';
          select numero_identificacion into l_numero_aprobador from usuario where upper(user_name)= NVL(upper(v('APP_USER')),'APEX');
          UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate,EVALUADOR_RESULTADO = l_numero_aprobador  WHERE ID = pformulario;
          
        --ELSE
        --  pmsj := 'Los resultados ingresados no son válidos. ' || pmsj;
        --END IF;

      else --Error concertación

        pmsj := 'Debe corregir error en la ponderación de objetivos.';  
        

      end if;

    EXCEPTION
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en la Validación de Resultados. Por favor, infórmelo al administrador de la aplicación.  ' || SQLCODE || ' - ' || SQLERRM);
    END;


  WHEN 'COMENTARIOS_SAVE_O' THEN
    IF v('P44_COMENTARIO_EVALUADO') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_COMEVAOBJ
      WHERE formeva_obj = pformulario;
      IF ( vcontador    = 0 ) THEN --No se ha ingresado comentario aun
        INSERT
        INTO BARS_COMEVAOBJ
          (
            formeva_obj,
            comentario_evaluado
          )
          VALUES
          (
            pformulario,
            v('P44_COMENTARIO_EVALUADO')
          );
      ELSE --Ya se ha ingresado el comentario
        UPDATE BARS_COMEVAOBJ
        SET comentario_evaluado = v('P44_COMENTARIO_EVALUADO')
        WHERE formeva_obj       = pformulario;
      END IF;
      pmsj := 'Sus comentarios fueron almacenados. ';
      UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
    END IF;
  WHEN 'COMENTARIOS_SAVE_R' THEN
    IF v('P44_COMENTARIO_EVALUADOR') IS NOT NULL THEN
      SELECT COUNT(*)
      INTO vcontador
      FROM BARS_COMEVAOBJ
      WHERE formeva_obj = pformulario;
      IF ( vcontador    = 0 ) THEN --No se ha ingresado comentario aun
        INSERT
        INTO BARS_COMEVAOBJ
          (
            formeva_obj,
            comentario_evaluador
          )
          VALUES
          (
            pformulario,
            v('P44_COMENTARIO_EVALUADOR')
          );
        UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
      ELSE --Ya se ha ingresado el comentario
        UPDATE BARS_COMEVAOBJ
        SET comentario_evaluador = v('P44_COMENTARIO_EVALUADOR')
        WHERE formeva_obj        = pformulario;
        UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate WHERE ID=pformulario;
      END IF;
    END IF;
  WHEN 'FINALIZAR' THEN
    cal_pct_consecucion_formobj(pformulario);
    UPDATE BARS_FORMEVAOBJ SET FECHEVA=sysdate,EVALUADOR_RESULTADO=vappuser WHERE ID=pformulario;
    BARS_CAMESTOBJ (pformulario, kfinalizado);
    pmsj := 'El formulario se ha finalizado. Se mantiene disponible solo en modo consulta. ';
  ELSE
    NULL;
  END CASE;
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en BARS_SUBMITOBJ. Por favor, infórmelo al administrador de la aplicación. ' || SQLCODE || ' - ' || SQLERRM);
END BARS_SUBMITOBJ;
/