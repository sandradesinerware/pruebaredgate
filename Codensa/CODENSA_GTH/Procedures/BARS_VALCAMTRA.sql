CREATE OR REPLACE PROCEDURE codensa_gth."BARS_VALCAMTRA" (pevaluado IN BARS_FORMEVAOBJ.evaluado%type, pnuevoestado IN TRABAJADOR.id_estado%type,
  pnuevogestor IN TRABAJADOR.jefe%type, pnuevagerencia IN TRABAJADOR.id_gerencia%type, pnuevocargo IN TRABAJADOR.id_cargo%type,
  pnuevaunidad IN TRABAJADOR.id_unidad_organizativa%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo NUMBER;
 vestado VARCHAR2(30);
 vgestor TRABAJADOR.jefe%TYPE;
 vgerencia TRABAJADOR.id_gerencia%type;
 vcargo TRABAJADOR.id_cargo%type;
 vunidad TRABAJADOR.id_unidad_organizativa%type;
 kinactivo CONSTANT VARCHAR2(30) := 'INACTIVO';

BEGIN
pmensaje := '';
SELECT DECODE(ID_ESTADO, 0, 'INACTIVO', 1 , 'ACTIVO' , 'INACTIVO') into vestado
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

--Obtención de datos actuales previo a la actualización del trabajador
SELECT jefe into vgestor
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

SELECT id_gerencia into vgerencia
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

SELECT id_cargo into vcargo
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

SELECT id_unidad_organizativa into vunidad
 FROM TRABAJADOR
 WHERE numero_identificacion = pevaluado;

CASE 
 WHEN TRUE THEN --AJuste temporal para inactivar la validación en la BD y no en la APP
   preturn := 1;

 WHEN vestado = kinactivo THEN --El trabajador se actualiza con estado Inactivo

    --Cambios relacionados con la existencia de evaluaciones de objetivos
    SELECT count(*) into vconteo
     FROM BARS_FORMEVAOBJ f inner join estado_bars on (f.estado = estado_bars.id) 
     WHERE f.evaluado = pevaluado and upper(estado_bars.nombre) not in ('ANULADO', 'FINALIZADO');
    if (vconteo != 0) then --El trabajador tiene formularios pendientes de anular o finalizar
     preturn := 0;
     pmensaje := pmensaje || 'El trabajador tiene formularios de objetivos sin finalizar. Por favor finalícelos o anúlelos antes de inactivar al trabajador. ';
    else
     preturn := 1;
    end if;

 WHEN ( (vgerencia != pnuevagerencia) OR (vcargo != pnuevocargo) OR (vunidad != pnuevaunidad) ) THEN --Hay cambio en centro_costo (división), cargo o zona

   --Cambios relacionados con la existencia de evaluaciones de objetivos
    SELECT count(*) into vconteo
     FROM BARS_FORMEVAOBJ f inner join estado_bars on (f.estado = estado_bars.id) 
     WHERE f.evaluado = pevaluado and upper(estado_bars.nombre) not in ('ANULADO', 'FINALIZADO');
    if (vconteo != 0) then --El trabajador tiene formularios pendientes de anular o finalizar
     preturn := 0;
     pmensaje := pmensaje || 'El trabajador tiene formularios de objetivos sin finalizar. Por favor finalícelos o anúlelos antes de modificar al trabajador. ';
    else
     preturn := 1;
    end if;

 WHEN (vgestor != pnuevogestor) THEN
   --Validar si el nuevo gestor tiene perfil de gestor en seguridad
   SELECT count(*) into vconteo 
    from FUNCION inner join GRUPO_FUNCION on FUNCION.id = GRUPO_FUNCION.funcion
    inner join GRUPO on GRUPO_FUNCION.grupo = GRUPO.id
    inner join USU_GRUPO on GRUPO.id = USU_GRUPO.grupo
    inner join USUARIO on USU_GRUPO.usuario = USUARIO.numero_identificacion
    where UPPER(FUNCION.nombre) = 'SER GESTOR' AND USUARIO.numero_identificacion = pnuevogestor;
       if (vconteo = 0) then --El nuevo gestor asignado no tiene el perfil gestor de seguridad
     preturn := 0;
     pmensaje := pmensaje || 'El gestor asignado al trabajador no tiene el perfil de autorización requerido para ser gestor. Ingrese a la administración de seguridad y asigne al usuario a un grupo que tenga la función Ser Gestor. ';
    else
     preturn := 1;
    end if;

 ELSE
  preturn := 1;
END CASE;

/*EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en BARS_VALCAMTRA. Por favor, infórmelo al administrador de la aplicación. ' 
      || SQLCODE || ' - ' || SQLERRM);*/

END BARS_VALCAMTRA;

/