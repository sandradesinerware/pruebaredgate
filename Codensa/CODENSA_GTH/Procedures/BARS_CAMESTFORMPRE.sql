CREATE OR REPLACE PROCEDURE codensa_gth."BARS_CAMESTFORMPRE" (pformulario IN BARS_FORMEVAPRE.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 vestado_anterior ESTADO_BARS.NOMBRE%TYPE;
 ktipo_estado_predictores CONSTANT VARCHAR2(30) := 'PREDICTORES_POTENCIAL';
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from BARS_FORMEVAPRE
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    null;
  else
    --Verificar tipo_estado_bars = PREDICTORES_POTENCIAL
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = ktipo_estado_predictores;
    if vconteo_filas >= 1 then --El tipo_estado_bars sí existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) = ktipo_estado_predictores and rownum = 1;
    else --El tipo_estado_bars no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vtipo_estado
        from TIPO_ESTADO_BARS;
      INSERT INTO TIPO_ESTADO_BARS (id, nombre)
           values ( vtipo_estado, ktipo_estado_predictores );
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Debe crearse.
      SELECT NVL( max(id),0 )+1 into vestado
        from ESTADO_BARS;
      INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
        values ( vestado, upper(pestado), vtipo_estado );
    end if;
    --Actualizar el estado del formulario  
    BEGIN
    SELECT nombre into vestado_anterior
      from ESTADO_BARS
      where id = (select estado from BARS_FORMEVAPRE where id = pformulario);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN --El formulario no existe luego no hay estado anterior
        vestado_anterior := '';
    END;
    UPDATE BARS_FORMEVAPRE SET estado = vestado, fecha_actualizacion = trunc(sysdate) where id = pformulario;
  end if;
EXCEPTION
  when others then
    raise_application_error(-20000,'Un error inesperado ocurrió en BARS_CAMESTFORMPRE. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;

/