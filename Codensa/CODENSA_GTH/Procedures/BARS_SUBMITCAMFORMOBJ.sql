CREATE OR REPLACE PROCEDURE codensa_gth."BARS_SUBMITCAMFORMOBJ" ( pid IN BARS_FORMEVAOBJ.id%type, pestado_nuevo IN ESTADO_BARS.id%type, 
 pevaluador IN BARS_FORMEVAOBJ.EVALUADOR%type, pcargo IN BARS_FORMEVAOBJ.cargo%type, 
 pcencos IN BARS_FORMEVAOBJ.cencos%type, pzona IN BARS_FORMEVAOBJ.zona%type, 
 pevaluador_resultado IN BARS_FORMEVAOBJ.evaluador_resultado%type )
IS
knuevo CONSTANT VARCHAR2(30) := 'NUEVO';
ket CONSTANT VARCHAR2(30) := 'EN TRATAMIENTO';
kconval CONSTANT VARCHAR2(30) := 'CONCERTACION VALIDADA';
kconap CONSTANT VARCHAR2(30) := 'CONCERTACION APROBADA';
kperaj CONSTANT VARCHAR2(30) := 'PERIODO AJUSTE';
kresval CONSTANT VARCHAR2(30) := 'RESULTADO VALIDADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
vestado_actual VARCHAR2(30);
vestado_nuevo VARCHAR2(30);

BEGIN
SELECT upper(e.nombre) into vestado_actual
 FROM BARS_FORMEVAOBJ f inner join ESTADO_BARS e on (f.estado = e.id)
 WHERE f.id = pid;

SELECT upper(nombre) into vestado_nuevo
 FROM ESTADO_BARS
 WHERE id = pestado_nuevo;

UPDATE BARS_FORMEVAOBJ SET EVALUADOR = pevaluador, CARGO = pcargo, CENCOS = pcencos, 
  ZONA = pzona, EVALUADOR_RESULTADO = pevaluador_resultado, ESTADO = pestado_nuevo, fecheva = trunc(sysdate)
  WHERE id = pid;

 CASE vestado_nuevo --Se maneja la lógica adicional al cambio del estado

  WHEN knuevo THEN --Si se eliminan los objetivos y comentarios existentes, debe en javascript generarse un mensaje de advertencia
   NULL;

  WHEN ket THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL, evaluador = NULL
    WHERE id = pid;

  WHEN kconval THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL, evaluador = NULL
    WHERE id = pid;

  WHEN kconap THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL
    WHERE id = pid;

  WHEN kresval THEN
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL, evaluador_resultado = NULL
    WHERE id = pid;

  WHEN kfinalizado THEN --Se valida que este cambio sea imposible
   NULL;

  WHEN kanulado THEN --No se elimina su información sino que deja de salir en los resultados. Se elimina la info?
   UPDATE BARS_FORMEVAOBJ SET PCTCONSFORM = NULL
    WHERE id = pid;

  ELSE
   NULL;

 END CASE;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurrió en BARS_SUBMITCAMFORMOBJ. Por favor, infórmelo al administrador de la aplicación. ' 
	  || SQLCODE || ' - ' || SQLERRM);

END BARS_SUBMITCAMFORMOBJ;

/