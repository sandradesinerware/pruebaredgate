CREATE OR REPLACE FORCE VIEW codensa_gth.v_pdi_repeticion_curso ("ID",periodo,asignacion,trabajador,facultad,negocio,curso,grupo,estado,curso_anterior) AS
select 
v_reporte_pdi.id,
v_reporte_pdi.periodo,
v_reporte_pdi.evaluado ASIGNACION,
(select nombres||' '||apellidos from trabajador where numero_identificacion=evaluado) trabajador,
v_grupos_curso.FACULTAD,
v_grupos_curso.NEGOCIO,
v_grupos_curso.CURSO,
v_grupos_curso.GRUPO,
v_reporte_pdi.ESTADO,
ADMINISTRACION_PDI_PCK.CURSO_ANTERIOR(v_reporte_pdi.id) CURSO_ANTERIOR
from v_reporte_pdi
left join v_grupos_curso on v_grupos_curso.id=v_reporte_pdi.id_grupo
where facultad is not null
AND v_reporte_pdi.ID_ESTADO NOT IN (25,26)
and ADMINISTRACION_PDI_PCK.HIZO_CURSO(v_reporte_pdi.id) = 1;