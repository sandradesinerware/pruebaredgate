CREATE OR REPLACE FORCE VIEW codensa_gth.v_reporte_acciones_cruzadas ("ID",evaluado,nombre_evaluado,evaluador,nombre_evaluador,nombre_curso,fecha_inicio,fecha_fin,tipo_curso,id_periodo,periodo) AS
select 
bars_pdi.id,
bars_encpdi.evaluado evaluado,
(select nombres||' '||apellidos from trabajador where numero_identificacion = bars_encpdi.evaluado) nombre_evaluado,
bars_encpdi.evaluador evaluador,
(select nombres||' '||apellidos from trabajador where numero_identificacion = bars_encpdi.evaluador) nombre_evaluador,
pdi_curso.nombre_visible nombre_curso,
pdi_grupo.fecha_inicio fecha_inicio,
pdi_grupo.fecha_fin fecha_fin,
(select nombre from tipo where id = pdi_curso.tipo) tipo_curso,
bars_encpdi.periodo id_periodo,
(select nombre from pdi_periodo where id = bars_encpdi.periodo) periodo
from
bars_pdi 
left join bars_encpdi on (bars_encpdi.id = bars_pdi.id_encabezado)
left join pdi_grupo on (pdi_grupo.id = bars_pdi.id_pdi_grupo)
left join pdi_curso on (pdi_curso.id = pdi_grupo.id_pdi_curso)
where bars_pdi.id in 
(select id_pdi from pdi_concepto where id_estado_pdi = 30 and tipo_concepto= snw_constantes.constante_tipo('CONCEPTO_RRHH'))
and bars_pdi.id_accion = snw_constantes.constante_tipo('FORMACION') and pdi_curso.tipo = snw_constantes.constante_tipo('PRESENCIAL')
and ADMINISTRACION_PDI_PCK.ACCION_FECHA_CRUZADA(bars_pdi.id) = 1;