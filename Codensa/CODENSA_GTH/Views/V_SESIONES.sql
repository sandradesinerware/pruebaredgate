CREATE OR REPLACE FORCE VIEW codensa_gth.v_sesiones ("ID",numero,fecha,asistentes,id_pdi_grupo,grupo,curso) AS
select 
pdi_sesion.id,
pdi_sesion.numero,
pdi_sesion.fecha,
pdi_sesion.asistentes,
pdi_sesion.id_pdi_grupo,
nombre_grupo grupo,
nombre_visible curso
from pdi_sesion
left join pdi_grupo on (pdi_grupo.id = pdi_sesion.id_pdi_grupo)
left join pdi_curso on (pdi_curso.id = pdi_grupo.id_pdi_curso);