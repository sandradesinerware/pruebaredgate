CREATE OR REPLACE FORCE VIEW codensa_gth.vr_reporte_pdi ("ID",id_encabezado,evaluado,evaluador,id_estado,estado,estado_negocio,id_accion,accion,pdi_negocio,id_curso,curso,id_periodo,periodo) AS
SELECT 
ID,
ID_ENCABEZADO,
EVALUADO,
EVALUADOR,
ID_ESTADO,
ESTADO,
(select case count(*) when 0 then 'Pendiente'
    when 1 then 'Realizado'
    else'Pendiente' end from pdi_concepto 
    where id_pdi = V_REPORTE_PDI.id and 
    tipo_concepto = SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_RESULTADO')) ESTADO_NEGOCIO,
ID_ACCION,
decode(id_accion,58,DESCRIPCION,52,AREA,54,NOMBRE_MENTOR,PROYECTO) ACCION,
DECODE(ID_ACCION,57,'FORMACION','DESARROLLO') PDI_NEGOCIO,
ID_CURSO,
CURSO,
ID_PERIODO,
PERIODO
FROM V_REPORTE_PDI
WHERE ID IN (SELECT ID FROM V_EXISTE_CONCEPTO WHERE CONCEPTO_RRHH = 1 AND CONCEPTO_RRHH_RESULTADO = 1);