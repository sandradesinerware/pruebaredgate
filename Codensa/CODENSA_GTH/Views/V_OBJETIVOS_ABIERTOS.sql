CREATE OR REPLACE FORCE VIEW codensa_gth.v_objetivos_abiertos (objnum,objetivo_individual,categoria,unidad_medida,meta,ponderacion,alcanzado,consecucion,resultado,formevaobj,tipo) AS
select BARS_OBJETIVO.OBJNUM as objnum, 
BARS_OBJETIVO.OBJIND as Objetivo_individual,   
TIPO.NOMBRE as CATEGORIA, 
(select nvl(nombre,'-') from tipo where id=BARS_OBJETIVO.UNIDAD_MEDIDA) as UNIDAD_MEDIDA,
BARS_OBJETIVO.META as Meta, BARS_OBJETIVO.PONDERACION as Ponderacion, 
BARS_OBJETIVO.ALCANZADO as Alcanzado, 
BARS_OBJETIVO.PCTCONSIND as Consecucion, 
BARS_OBJETIVO.PCTCONSPOND as Resultado,
BARS_OBJETIVO.FORMEVAOBJ as formevaobj,
(select NOMBRE from tipo where id=BARS_OBJETIVO.id_tipo) as TIPO
from BARS_OBJETIVO left join TIPO on (BARS_OBJETIVO.ID_CATEGORIA=tipo.ID)
where id_tipo = SNW_CONSTANTES.constante_tipo('ABIERTO');