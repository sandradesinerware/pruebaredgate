CREATE OR REPLACE FORCE VIEW codensa_gth.v_objetivo_detalle (formevaobj,evaluado,periodo,pond_1,cons_1,pct_1,pond_2,cons_2,pct_2,pond_3,cons_3,pct_3,pond_4,cons_4,pct_4,pond_5,cons_5,pct_5,pond_6,cons_6,pct_6,pond_7,cons_7,pct_7,pond_8,cons_8,pct_8,pond_9,cons_9,pct_9,pond_10,cons_10,pct_10,pond_11,cons_11,pct_11,pond_12,cons_12,pct_12) AS
select formevaobj,
"EVALUADO",
"PERIODO",
"1_POND" POND_1,
"1_CONS" CONS_1,
"1_PCT" PCT_1,

"2_POND" POND_2,
"2_CONS" CONS_2,
"2_PCT" PCT_2,

"3_POND" POND_3,
"3_CONS" CONS_3,
"3_PCT" PCT_3,

"4_POND" POND_4,
"4_CONS" CONS_4,
"4_PCT" PCT_4,

"5_POND" POND_5,
"5_CONS" CONS_5,
"5_PCT" PCT_5,

"6_POND" POND_6,
"6_CONS" CONS_6,
"6_PCT" PCT_6,

"7_POND" POND_7,
"7_CONS" CONS_7,
"7_PCT" PCT_7,

"8_POND" POND_8,
"8_CONS" CONS_8,
"8_PCT" PCT_8,

"9_POND" POND_9,
"9_CONS" CONS_9,
"9_PCT" PCT_9,

"10_POND" POND_10,
"10_CONS" CONS_10,
"10_PCT" PCT_10,

"11_POND" POND_11,
"11_CONS" CONS_11,
"11_PCT" PCT_11,

"12_POND" POND_12,
"12_CONS" CONS_12,
"12_PCT" PCT_12

from (select * from V_OBJETIVO) 
  pivot (sum(ponderacion) pond, sum(pctconsind) cons, sum(pctconspond) pct for objnum in (1,2,3,4,5,6,7,8,9,10,11,12));