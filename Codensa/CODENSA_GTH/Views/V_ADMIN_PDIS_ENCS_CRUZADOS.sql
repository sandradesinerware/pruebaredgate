CREATE OR REPLACE FORCE VIEW codensa_gth.v_admin_pdis_encs_cruzados (periodo,id_pdi,tipo_pdi,id_encabezado,evaluado,nombre_evaluado,evaluador,nombre_evaluador,evaluador_actual,nombre_evaluador_actual,id_curso,id_pdi_curso,id_grupo,nombre_grupo) AS
select     
        enc.periodo,
        pdi.id id_pdi,
        tipo.nombre tipo_pdi,
        enc.id id_encabezado,
        enc.evaluado,
        t.nombres || ' ' || t.apellidos nombre_evaluado,
        enc.evaluador,
        ( select ( nombres || ' ' || apellidos ) from trabajador where numero_identificacion = enc.evaluador ) nombre_evaluador,
        t.jefe evaluador_actual,
        ( select ( nombres || ' ' || apellidos ) from trabajador where numero_identificacion = t.jefe ) nombre_evaluador_actual,
        pdi.id_curso,
        NVL(to_char(pdi_grupo.id_pdi_curso), 'No PDI Formación') id_pdi_curso,
        NVL(to_char(pdi_grupo.id), 'No PDI Formación') id_grupo,
        NVL(pdi_grupo.nombre_grupo, 'No PDI Formación') nombre_grupo
    from bars_encpdi enc inner join trabajador t on (enc.evaluado = t.numero_identificacion)
    left join bars_pdi pdi on ( pdi.id_encabezado = enc.id )
    left join pdi_grupo on ( pdi_grupo.id = pdi.id_pdi_grupo )
    left join tipo on ( tipo.id = pdi.id_accion );