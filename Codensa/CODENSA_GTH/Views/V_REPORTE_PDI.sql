CREATE OR REPLACE FORCE VIEW codensa_gth.v_reporte_pdi ("ID",id_encabezado,evaluado,evaluador,id_estado,estado,comentarios_gestor,comentarios_rh,justificacion,accion,id_accion,fecha_inicio,comportamiento,fecha_fin,proyecto,area,id_grupo,grupo,id_curso,curso,id_periodo,periodo,descripcion,nombre_mentor,conocimiento) AS
SELECT 
 BARS_PDI.ID,
 BARS_PDI.ID_ENCABEZADO,
 BARS_ENCPDI.EVALUADO,
 BARS_ENCPDI.EVALUADOR,
 BARS_PDI.ID_ESTADO,
 (SELECT NOMBRE FROM ESTADO_BARS WHERE ID=BARS_PDI.ID_ESTADO) ESTADO,
 BARS_PDI.COMENTARIOS_GESTOR,
 BARS_PDI.COMENTARIOS_RH,
 BARS_PDI.JUSTIFICACION,
 (SELECT NOMBRE FROM TIPO WHERE ID=BARS_PDI.ID_ACCION) ACCION,
 BARS_PDI.ID_ACCION ID_ACCION,
 NVL(BARS_PDI.FECHA_INICIO,PDI_GRUPO.FECHA_INICIO) FECHA_INICIO,
 (SELECT NOMBRE FROM TIPO WHERE ID=BARS_PDI.ID_COMPORTAMIENTO) COMPORTAMIENTO,
 NVL(BARS_PDI.FECHA_FIN,PDI_GRUPO.FECHA_FIN) FECHA_FIN,
 BARS_PDI.PROYECTO,
 BARS_PDI.AREA,
 BARS_PDI.ID_PDI_GRUPO ID_GRUPO,
 PDI_GRUPO.NOMBRE_GRUPO GRUPO,
 PDI_CURSO.ID ID_CURSO,
 PDI_CURSO.NOMBRE_VISIBLE CURSO,
 BARS_ENCPDI.PERIODO ID_PERIODO,
 (SELECT ANO FROM PDI_PERIODO WHERE ID=BARS_ENCPDI.PERIODO) PERIODO,
 BARS_PDI.DESCRIPCION,
 BARS_PDI.NOMBRE_MENTOR,
 BARS_PDI.CONOCIMIENTO
 FROM  BARS_PDI
 LEFT JOIN BARS_ENCPDI  ON (BARS_PDI.ID_ENCABEZADO = BARS_ENCPDI.ID)
 LEFT JOIN PDI_GRUPO ON (BARS_PDI.ID_PDI_GRUPO = PDI_GRUPO.ID)
 LEFT JOIN PDI_CURSO ON (PDI_GRUPO.ID_PDI_CURSO = PDI_CURSO.ID);