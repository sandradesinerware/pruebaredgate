CREATE OR REPLACE FORCE VIEW codensa_gth.v_resultado_bpr (numero_asignado,trabajador,gestor,estadotrabajador,cargo,periodo,pct_consecucion,fecha_evaluacion,media_comportamientos,posicion,target_bpr,seccion1,seccion2,seccion3,seccion4,seccion5,seccion6,seccion7,estadoformulario,tipo) AS
select 
       v_trabajador.numero_identificacion numero_asignado,
       (v_trabajador.apellidos||', '|| v_trabajador.nombres) AS trabajador,
       v_trabajador.GESTOR,
       NVL(v_trabajador.estado, 'NO INICIADO Evaluación') EstadoTrabajador, 
       v_trabajador.Cargo,
       BARS_TRAEVABARS.periodo, 
       FORMULARIO_BARS.pct_consecucion, 
       FORMULARIO_BARS.fecha_evaluacion,
       FORMULARIO_BARS.MEDIA_COMPORTAMIENTOS,
       FORMULARIO_BARS.POSICION,
       v_trabajador.TARGET_BPR,
       seccion1.resultado seccion1,
       seccion2.resultado seccion2,
       seccion3.resultado seccion3,
       seccion4.resultado seccion4,
       seccion5.resultado seccion5,
       seccion6.resultado seccion6,
       seccion7.resultado seccion7,
    nvl((select nombre from estado_bars where id = FORMULARIO_BARS.estado), 'No iniciado') EstadoFormulario, 
       'Evaluación' tipo
from BARS_TRAEVABARS 
join v_trabajador on (BARS_TRAEVABARS.TRABAJADOR = v_trabajador.numero_identificacion)
left join FORMULARIO_BARS on (v_trabajador.numero_identificacion = FORMULARIO_BARS.evaluado AND (FORMULARIO_BARS.tipo = 'EVALUACION' or FORMULARIO_BARS.tipo is null))
left join seccion_formulario seccion1 on (FORMULARIO_BARS.ID = seccion1.FORMULARIO AND seccion1.SECCION = 1)
left join seccion_formulario seccion2 on (FORMULARIO_BARS.ID = seccion2.FORMULARIO AND seccion2.SECCION = 2)
left join seccion_formulario seccion3 on (FORMULARIO_BARS.ID = seccion3.FORMULARIO AND seccion3.SECCION = 3)
left join seccion_formulario seccion4 on (FORMULARIO_BARS.ID = seccion4.FORMULARIO AND seccion4.SECCION = 4)
left join seccion_formulario seccion5 on (FORMULARIO_BARS.ID = seccion5.FORMULARIO AND seccion5.SECCION = 5)
left join seccion_formulario seccion6 on (FORMULARIO_BARS.ID = seccion6.FORMULARIO AND seccion6.SECCION = 6)
left join seccion_formulario seccion7 on (FORMULARIO_BARS.ID = seccion7.FORMULARIO AND seccion7.SECCION = 7)
where FORMULARIO_BARS.periodo<2017
UNION ALL
select v_trabajador.numero_identificacion numero_asignado,
       (v_trabajador.apellidos||', '|| v_trabajador.nombres) AS trabajador,
       v_trabajador.GESTOR,
       NVL(v_trabajador.estado, 'NO INICIADO Autoevaluación') EstadoTrabajador, 
       v_trabajador.Cargo,
       BARS_TRAEVABARS.periodo, 
       FORMULARIO_BARS.pct_consecucion, 
       FORMULARIO_BARS.fecha_evaluacion,
       FORMULARIO_BARS.MEDIA_COMPORTAMIENTOS,
       FORMULARIO_BARS.POSICION,
       v_trabajador.TARGET_BPR,
       seccion1.resultado seccion1,
       seccion2.resultado seccion2,
       seccion3.resultado seccion3,
       seccion4.resultado seccion4,
       seccion5.resultado seccion5,
       seccion6.resultado seccion6,
       seccion7.resultado seccion7,
       nvl((select nombre from estado_bars where id = FORMULARIO_BARS.estado), 'No iniciado') EstadoFormulario, 
       'Autoevaluación' tipo
from BARS_TRAEVABARS 
join v_trabajador on (BARS_TRAEVABARS.TRABAJADOR = v_trabajador.numero_identificacion)
left join FORMULARIO_BARS on (v_trabajador.numero_identificacion = FORMULARIO_BARS.evaluado AND (FORMULARIO_BARS.tipo = 'AUTOEVALUACION' or FORMULARIO_BARS.tipo is null))
left join seccion_formulario seccion1 on (FORMULARIO_BARS.ID = seccion1.FORMULARIO AND seccion1.SECCION = 1)
left join seccion_formulario seccion2 on (FORMULARIO_BARS.ID = seccion2.FORMULARIO AND seccion2.SECCION = 2)
left join seccion_formulario seccion3 on (FORMULARIO_BARS.ID = seccion3.FORMULARIO AND seccion3.SECCION = 3)
left join seccion_formulario seccion4 on (FORMULARIO_BARS.ID = seccion4.FORMULARIO AND seccion4.SECCION = 4)
left join seccion_formulario seccion5 on (FORMULARIO_BARS.ID = seccion5.FORMULARIO AND seccion5.SECCION = 5)
left join seccion_formulario seccion6 on (FORMULARIO_BARS.ID = seccion6.FORMULARIO AND seccion6.SECCION = 6)
left join seccion_formulario seccion7 on (FORMULARIO_BARS.ID = seccion7.FORMULARIO AND seccion7.SECCION = 7)
where FORMULARIO_BARS.periodo<2017;