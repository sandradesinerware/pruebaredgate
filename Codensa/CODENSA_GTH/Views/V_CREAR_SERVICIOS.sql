CREATE OR REPLACE FORCE VIEW codensa_gth.v_crear_servicios ("ID",servicio,"codigo servicio",division,subgerencia,gerencia) AS
select servicio.id, servicio.NOMBRE servicio, servicio.CODIGO as "codigo servicio",dep1.NOMBRE division,dep2.nombre subgerencia,dep3.nombre gerencia 
from servicio
join dependencia dep1 on (dep1.id = servicio.division)
join dependencia dep2 on (dep1.tipo = dep2.id)
join dependencia dep3 on (dep2.tipo = dep3.id);