CREATE OR REPLACE FORCE VIEW codensa_gth.v_detalle_pdi ("ID",id_encabezado,justificacion,fecha_inicio,fecha_fin,proyecto,area,nombre_accion_libre,descripcion,comentarios_gestor,comentarios_rh,nuevo_curso,id_estado,"Estado",periodo,evaluador,evaluado,"Acción","Comportamiento","Curso",aud_fecha_creacion,aud_fecha_actualizacion,aud_creado_por,aud_actualizado_por) AS
select
 bars_pdi.id,
 bars_pdi.id_encabezado,
 bars_pdi.justificacion,
 /*nvl(bars_pdi.logro_esperado, '-') logro_esperado,*/
 to_char(bars_pdi.fecha_inicio, 'YYYY-MM-DD') fecha_inicio,
 to_char(bars_pdi.fecha_fin, 'YYYY-MM-DD') fecha_fin,
 nvl(bars_pdi.proyecto, '-') proyecto,
 nvl(bars_pdi.area, '-') area,
 nvl(bars_pdi.nombre_accion_libre, '-') nombre_accion_libre,
 nvl(bars_pdi.descripcion, '-') descripcion,
 bars_pdi.comentarios_gestor,
 bars_pdi.comentarios_rh,
 nvl(bars_pdi.nuevo_curso, '-') nuevo_curso,
 bars_pdi.id_estado,
 estado_bars.nombre as "Estado",
 bars_encpdi.periodo,
 bars_encpdi.evaluador,
 bars_encpdi.evaluado,
 /*gestor.identificacion_nombre gestor,
 trabajador.identificacion_nombre trabajador, */
 t1.nombre as "Acción",
 t2.nombre as "Comportamiento", 
 bars_compdi.tema as "Curso",
 bars_pdi.AUD_FECHA_CREACION,
 bars_pdi.AUD_FECHA_ACTUALIZACION,
 bars_pdi.AUD_CREADO_POR,
 bars_pdi.AUD_ACTUALIZADO_POR
from bars_pdi
 inner join bars_encpdi
 on (bars_pdi.id_encabezado = bars_encpdi.id)
 /*left join v_trabajador gestor
 on (gestor.numero_identificacion = bars_encpdi.evaluador)
 left join v_trabajador trabajador
 on (trabajador.numero_identificacion = bars_encpdi.evaluado)*/
 left join tipo t1
 on (bars_pdi.id_accion = t1.id)
 left join estado_bars
 on (bars_pdi.id_estado = estado_bars.id)
 left join tipo t2
 on (bars_pdi.id_comportamiento = t2.id)
 left join bars_compdi
 on (bars_pdi.id_curso = bars_compdi.id);