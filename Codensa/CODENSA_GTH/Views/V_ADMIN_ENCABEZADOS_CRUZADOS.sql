CREATE OR REPLACE FORCE VIEW codensa_gth.v_admin_encabezados_cruzados (evaluado,nombre_evaluado,evaluador,nombre_evaluador,evaluador_actual,nombre_evaluador_actual,id_encabezado,periodo) AS
select
    t.numero_identificacion evaluado,    
    t.nombres || ' ' || t.apellidos nombre_evaluado,     
    enc.evaluador,
    ( select nombres || ' ' || apellidos from trabajador where numero_identificacion = enc.evaluador ) nombre_evaluador,
    t.jefe evaluador_actual,
    ( select nombres || ' ' || apellidos from trabajador where numero_identificacion = t.jefe ) nombre_evaluador_actual,
    enc.id id_encabezado,
    enc.periodo
    from trabajador t 
    left join bars_encpdi enc 
        on ( enc.evaluado = t.numero_identificacion );