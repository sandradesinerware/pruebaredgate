CREATE OR REPLACE FORCE VIEW codensa_gth.vr_gestor (trabajador,evaluador,periodo) AS
select 
(select nombres||' '||apellidos from trabajador where numero_identificacion = evaluador) trabajador,
evaluador,
periodo
from bars_encpdi
group by periodo,evaluador;