CREATE OR REPLACE FORCE VIEW codensa_gth.v_pec_actividad ("ID",fecha,intensidad,proceso,contenido,tipo_entrenamiento,responsable,lugar,predeterminada,id_pec,allowed_row_operation) AS
select ID,
       FECHA,
       INTENSIDAD,
       PROCESO,
       CONTENIDO,
       TIPO_ENTRENAMIENTO,
       RESPONSABLE,
       LUGAR,
       PREDETERMINADA,
       ID_PEC,
       CASE WHEN upper(predeterminada) = 'S' THEN 'LOCK'
            ELSE 'UD' END allowed_row_operation
from PEC_ACTIVIDAD;