CREATE OR REPLACE FORCE VIEW codensa_gth.bars_rendimientopre (numero_identificacion,link_identificacion,nombres,apellidos,posicion_inicial_id,posicion_inicial_nombre,mesa,posicion_final_id,posicion_final_nombre) AS
(
select T.NUMERO_IDENTIFICACION,
T.NUMERO_IDENTIFICACION as LINK_IDENTIFICACION, 
T.NOMBRES,
T.APELLIDOS,
nvl(BP.id, 0) as posicion_inicial_id,
nvl(BP.nombre,'Sin definir') as posicion_inicial_nombre,
BTP.mesahompre as mesa,
nvl(BTP.pospre_final, 0) as posicion_final_id,
nvl(BPFINAL.nombre,'Sin definir') as posicion_final_nombre
from TRABAJADOR T
 inner join BARS_TRAEVAPRE BTP on (T.numero_identificacion = BTP.trabajador)
 left join BARS_FORMEVAPRE BF on (BTP.trabajador = BF.evaluado  and BTP.perpre = BF.periodo) /*Confirmar, pues al usar left join, un trabajador no evaluado cuenta para el cálculo del total de la mesa, pero su posición sería 'Sin definir' porque no tendría */
 left join BARS_POSPRE BP on (BF.posicion = BP.id)
 left join BARS_POSPRE BPFINAL on (BTP.pospre_final = BPFINAL.id)
where
 BF.tipo = 'EVALUACION'
);