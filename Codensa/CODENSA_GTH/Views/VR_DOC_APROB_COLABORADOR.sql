CREATE OR REPLACE FORCE VIEW codensa_gth.vr_doc_aprob_colaborador (evaluado,"ID",id_periodo,periodo,nombre_pdi,id_accion,tipo_pdi,fecha_inicio) AS
select
v_reporte_pdi.evaluado,
v_reporte_pdi.id,
id_periodo,
(select nombre from pdi_periodo where id=id_periodo) periodo,
nvl(curso,accion) nombre_pdi,
id_accion,
accion tipo_pdi,
fecha_inicio
from v_reporte_pdi
where id_estado=30;