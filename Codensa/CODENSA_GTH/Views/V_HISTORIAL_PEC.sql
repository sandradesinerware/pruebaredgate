CREATE OR REPLACE FORCE VIEW codensa_gth.v_historial_pec ("ID",identificacion_trabajador,nombre_trabajador,identificacion_gestor,nombre_gestor,estado_actual,fecha_creacion,intensidad_horas,pec_estado,fecha_estado) AS
select 
  pec.id,
  pec.id_trabajador identificacion_trabajador,
  (select nombres || ' ' || apellidos from trabajador where numero_identificacion = pec.id_trabajador) nombre_trabajador,
  pec.id_gestor identificacion_gestor,
  (select nombres || ' ' || apellidos from trabajador where numero_identificacion = pec.id_gestor) nombre_gestor,
  (select nombre from tipo where id = pec.id_estado) estado_actual,
  pec.fecha_creacion,
  pec.intensidad_horas,
  (select nombre from tipo where id = pec_estado.id_tipo_estado) pec_estado,
  pec_estado.fecha fecha_estado
from pec_estado
left join pec
  on pec_estado.id_pec = pec.id;