CREATE OR REPLACE FORCE VIEW codensa_gth.vr_colaborador ("ID",id_periodo,periodo,evaluado,evaluador,nombre_evaluado) AS
select
id,
periodo id_periodo,
(select nombre from pdi_periodo where id=periodo) periodo,
evaluado,
evaluador,
(select nombres||' '||apellidos from trabajador where numero_identificacion=evaluado) nombre_evaluado
from bars_encpdi;