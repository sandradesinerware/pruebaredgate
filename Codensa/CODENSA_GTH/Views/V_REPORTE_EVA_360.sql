CREATE OR REPLACE FORCE VIEW codensa_gth.v_reporte_eva_360 (evaluado,s1,s2,s3,s4,s5,evaluado2,numero_evaluaciones,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,evaluado3,nue,trat,cerr,anul,periodo) AS
select "EVALUADO","S1","S2","S3","S4","S5","EVALUADO2","NUMERO_EVALUACIONES","P1","P2","P3","P4","P5","P6","P7","P8","P9","P10","P11","P12","P13","P14","P15","P16","P17","P18","P19","EVALUADO3","NUE","TRAT","CERR","ANUL", "PERIODO" 
from(
select EVALUADO , SECCION_360, PONDERACION_SECCION, PERIODO from PONDERACION_SECCION_360 where periodo = (select MAX(ano) from PERIODO_BARS where activo_360 = 'S')
)
pivot
(
sum(PONDERACION_SECCION) for SECCION_360 in(1 S1,2 S2,3 S3,4 S4,5 S5) 
) T1
JOIN
(select * 
from(
select EVALUADO EVALUADO2, PLANTILLA_PREG_360, PONDERADO_RESPUESTA, ( select count(formulario_360.id) from formulario_360 where formulario_360.evaluado = PONDERACION_PREG_360.EVALUADO
                                                                     and formulario_360.periodo = (select MAX(ano) from PERIODO_BARS where activo_360 = 'S') ) NUMERO_EVALUACIONES
from PONDERACION_PREG_360
where periodo = (select MAX(ano) from PERIODO_BARS where activo_360 = 'S')
)
pivot
(
sum(PONDERADO_RESPUESTA) for PLANTILLA_PREG_360 in(1 P1,2 P2,3 P3,4 P4,5 P5,6 P6,7 P7,8 P8,9 P9,10 P10,11 P11,12 P12,13 P13,14 P14,15 P15,16 P16,17 P17,18 P18,19 P19) 
)) T2
ON (T1.EVALUADO = T2.EVALUADO2)
JOIN
(select * 
from
(
select EVALUADO EVALUADO3, NOMBRE, count(FORMULARIO_360.ID) NUMERO_FORMULARIOS from 
formulario_360 
left join 
estado_bars on(formulario_360.estado = estado_bars.id)
where periodo = (select MAX(ano) from PERIODO_BARS where activo_360 = 'S')
group by evaluado, estado_bars.nombre
)
pivot
(
sum(NUMERO_FORMULARIOS) for nombre in('NUEVO' NUE, 'EN_TRATAMIENTO' TRAT, 'CERRADO' CERR, 'ANULADO' ANUL)
)) T3
ON (T2.EVALUADO2 = T3.EVALUADO3);