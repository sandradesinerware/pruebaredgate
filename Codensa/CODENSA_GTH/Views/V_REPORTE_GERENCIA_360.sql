CREATE OR REPLACE FORCE VIEW codensa_gth.v_reporte_gerencia_360 (periodo,gerencia,s1,s2,s3,s4,s5,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19) AS
select PERIODO, GERENCIA, TRUNC(avg(s1),1) s1, TRUNC(avg(s2),1) s2, TRUNC(avg(s3),1) s3, TRUNC(avg(s4),1) s4, TRUNC(avg(s5),1) s5,
TRUNC(avg(p1),1) p1, TRUNC(avg(p2),1) p2, TRUNC(avg(p3),1) p3, TRUNC(avg(p4),1) p4, TRUNC(avg(p5),1) p5, TRUNC(avg(p6),1) p6, TRUNC(avg(p7),1) p7, TRUNC(avg(p8),1) p8, TRUNC(avg(p9),1) p9, TRUNC(avg(p10),1) p10, TRUNC(avg(p11),1) p11, TRUNC(avg(p12),1) p12, TRUNC(avg(p13),1) p13, TRUNC(avg(p14),1) p14, TRUNC(avg(p15),1) p15, TRUNC(avg(p16),1) p16, TRUNC(avg(p17),1) p17, TRUNC(avg(p18),1) p18, TRUNC(avg(p19),1) p19
from V_REPORTE_EVA_360 
join v_trabajador on (V_REPORTE_EVA_360.evaluado = v_trabajador.NUMERO_IDENTIFICACION) 
group by PERIODO, GERENCIA;