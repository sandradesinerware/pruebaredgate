CREATE OR REPLACE FORCE VIEW codensa_gth.v_asistencia ("ID",id_pdi_sesion,numero,asistio,id_pdi_grupo,nombre_grupo,nombres,apellidos,numero_asignado) AS
SELECT 
PDI_ASISTENCIA.ID,
PDI_SESION.ID ID_PDI_SESION,
PDI_SESION.NUMERO,
PDI_ASISTENCIA.ASISTIO,
PDI_GRUPO.ID ID_PDI_GRUPO,
PDI_GRUPO.NOMBRE_GRUPO,
(select nombres from trabajador where trabajador.numero_identificacion=bars_encpdi.evaluado) nombres,
(select apellidos from trabajador where trabajador.numero_identificacion=bars_encpdi.evaluado) apellidos,
bars_encpdi.evaluado numero_asignado
FROM PDI_ASISTENCIA
LEFT JOIN PDI_SESION ON (PDI_SESION.ID = PDI_ASISTENCIA.ID_PDI_SESION)
LEFT JOIN PDI_GRUPO ON (PDI_GRUPO.ID = PDI_SESION.ID_PDI_GRUPO)
LEFT JOIN BARS_PDI ON (BARS_PDI.ID = PDI_ASISTENCIA.ID_BARS_PDI)
LEFT JOIN BARS_ENCPDI ON (BARS_ENCPDI.ID = BARS_PDI.ID_ENCABEZADO);