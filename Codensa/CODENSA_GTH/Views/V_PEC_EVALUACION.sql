CREATE OR REPLACE FORCE VIEW codensa_gth.v_pec_evaluacion ("ID",enunciado,numero,estado,calificacion_pregunta,id_evaluacion,id_pec,id_trabajador_evaluador,id_gestor_evaluado) AS
select 
  pec_preguntas.id,
  pec_preguntas.enunciado,
  pec_preguntas.numero,
  pec_preguntas.estado,
  pec_evaluacion_respuestas.calificacion_pregunta,
  pec_evaluacion_respuestas.ID_EVALUACION,
  pec_evaluacion.ID_PEC,
  pec_evaluacion.id_trabajador_evaluador,
  pec_evaluacion.id_gestor_evaluado
from pec_preguntas  
left join pec_evaluacion_respuestas
  on pec_preguntas.id = pec_evaluacion_respuestas.id_pregunta
left join pec_evaluacion
  on pec_evaluacion_respuestas.id_evaluacion = pec_evaluacion.id
  where pec_preguntas.estado <> 'I';