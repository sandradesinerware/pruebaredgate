CREATE OR REPLACE FORCE VIEW codensa_gth.v_url_grupo (id_grupo,periodo,nombre_curso,tipo_curso,nombre_grupo,estado_grupo,fecha_inicio,fecha_fin,url_grupo,jurisdiccion) AS
select 
pdi_grupo.ID id_grupo,
(select ano from pdi_periodo where id=pdi_grupo.id_periodo) periodo,
pdi_curso.nombre_visible nombre_curso,
(select nombre from tipo where id = pdi_curso.tipo) tipo_curso,
pdi_grupo.nombre_grupo nombre_grupo,
(select nombre from tipo where id = pdi_grupo.estado) estado_grupo,
pdi_grupo. fecha_inicio,
pdi_grupo.fecha_fin,
(SELECT UTILS_PCK.BUILD_URL_GROUP(pdi_grupo.ID) FROM DUAL) url_grupo,
(select nombre from tipo where id = pdi_grupo.id_tipo_jurisdiccion) jurisdiccion
from pdi_grupo 
left join pdi_curso on (pdi_curso.id = pdi_grupo.id_pdi_curso);