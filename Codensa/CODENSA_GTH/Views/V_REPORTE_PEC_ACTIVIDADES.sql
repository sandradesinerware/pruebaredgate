CREATE OR REPLACE FORCE VIEW codensa_gth.v_reporte_pec_actividades (id_pec,id_actividad,fecha,intensidad,proceso,contenido,tipo_entrenamiento,responsable,lugar,evidencia,cumple,predeterminada) AS
SELECT
  ID_PEC,
  ID ID_ACTIVIDAD,
  FECHA,
  INTENSIDAD,
  PROCESO,
  CONTENIDO,
  (SELECT NOMBRE FROM TIPO WHERE ID = PEC_ACTIVIDAD.TIPO_ENTRENAMIENTO) TIPO_ENTRENAMIENTO,
  RESPONSABLE,
  LUGAR,
  '' EVIDENCIA,
  CASE CUMPLE WHEN 'S' THEN 'Si' WHEN 'N' THEN 'No'ELSE 'Sin definir' END CUMPLE,
  CASE PREDETERMINADA WHEN 'S' THEN 'Si' ELSE 'No' END PREDETERMINADA 
FROM PEC_ACTIVIDAD;