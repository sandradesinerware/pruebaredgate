CREATE OR REPLACE FORCE VIEW codensa_gth.v_objetivo (periodo,objnum,evaluado,ponderacion,pctconsind,pctconspond,formevaobj) AS
select f.periodo,
o.objnum,
f.evaluado,
o.ponderacion,
o.pctconsind,
o.pctconspond,
o.formevaobj
from bars_objetivo o right join bars_formevaobj f /*en 2014 se cargaron formularios sin objetivos*/ on (f.id=o.formevaobj);