CREATE OR REPLACE FORCE VIEW codensa_gth.v_reporte_avance_formacion ("ID",id_periodo,periodo,evaluado,trabajador,grupo,curso,proveedor,tipo,tipo_formacion,foco_holding,foco_interno,no_pdi,horas_curso,horas_asistidas,fecha_inicio,fecha_fin,asistencia,asistencia_ideal,estado_grupo,concepto_gestor,gestor) AS
select 
bars_pdi.ID,
bars_encpdi.periodo ID_PERIODO,
(select nombre from pdi_periodo where id =bars_encpdi.periodo) PERIODO,
bars_encpdi.EVALUADO,
(SELECT NOMBRES||' '||APELLIDOS FROM TRABAJADOR WHERE NUMERO_IDENTIFICACION = bars_encpdi.EVALUADO) TRABAJADOR,
(select nombre_grupo from pdi_grupo where id = bars_pdi.id_pdi_grupo) GRUPO,
(select nombre_visible from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo)) CURSO,
(select provedor from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo)) PROVEEDOR,
(select nombre from tipo where id =  (select tipo from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo))) TIPO,
(select nombre from tipo where id =  (select tipo_formacion from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo))) TIPO_FORMACION,
(select nombre from pdi_foco where id = (select foco_holding from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo))) FOCO_HOLDING,
(select nombre from pdi_foco where id = (select foco_interno from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo))) FOCO_INTERNO,
CASE (select no_pdi from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id= bars_pdi.id_pdi_grupo))
WHEN 'S' 
then 'Si'
ELSE 'No' END NO_PDI,
(select horas from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id = bars_pdi.id_pdi_grupo)) HORAS_CURSO,
round((select horas from pdi_curso where id = (select id_pdi_curso from pdi_grupo where id = bars_pdi.id_pdi_grupo)) * bars_pdi.avance_porcentaje_pdi/100,1) HORAS_ASISTIDAS,
pdi_grupo.FECHA_INICIO,
pdi_grupo.FECHA_FIN,
nvl(round(bars_pdi.avance_porcentaje_pdi),0) ASISTENCIA,
ADMINISTRACION_PDI_PCK.CALCULAR_AVANCE_IDEAL_PDI(bars_pdi.ID) ASISTENCIA_IDEAL,
(select nombre from tipo where id = pdi_grupo.estado) estado_grupo,
CASE 
WHEN (SELECT ID FROM PDI_CONCEPTO WHERE ID_PDI = bars_pdi.ID AND TIPO_CONCEPTO=snw_constantes.constante_tipo('CONCEPTO_RESULTADO')) IS NULL THEN NULL
ELSE (select nombre from estado_bars where id = (SELECT ID_ESTADO_PDI FROM PDI_CONCEPTO WHERE ID_PDI=bars_pdi.ID AND TIPO_CONCEPTO=snw_constantes.constante_tipo('CONCEPTO_RESULTADO')))
END CONCEPTO_GESTOR,
(SELECT NOMBRES||' '||APELLIDOS FROM TRABAJADOR WHERE NUMERO_IDENTIFICACION = bars_encpdi.EVALUADOR) GESTOR
from bars_pdi
LEFT JOIN bars_encpdi on (bars_encpdi.id = bars_pdi.id_encabezado)
left join pdi_grupo on (pdi_grupo.id = bars_pdi.id_pdi_grupo)
where id_accion = snw_constantes.constante_tipo('FORMACION')
and bars_pdi.id_pdi_grupo is not null;