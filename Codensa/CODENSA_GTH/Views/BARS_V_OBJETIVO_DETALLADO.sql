CREATE OR REPLACE FORCE VIEW codensa_gth.bars_v_objetivo_detallado (objnum,objetivo_individual,categoria,unidad_medida,meta,ponderacion,alcanzado,consecucion,resultado,formevaobj,tipo) AS
select "OBJNUM",
"OBJETIVO_INDIVIDUAL",
"CATEGORIA",
"UNIDAD_MEDIDA",
"META",
"PONDERACION",
"ALCANZADO",
"CONSECUCION",
"RESULTADO",
"FORMEVAOBJ",
"TIPO" from 
(select * from V_OBJETIVOS_ABIERTOS union all select * from V_OBJETIVOS_CERRADOS);