CREATE OR REPLACE FORCE VIEW codensa_gth.v_trabajador_con_pec (usuario_trabajador,identificacion_trabajador,nombre_trabajador,identificacion_gestor,nombre_gestor) AS
select  
  v_trabajador.usuario usuario_trabajador,
  v_trabajador.NUMERO_IDENTIFICACION identificacion_trabajador,
  v_trabajador.nombre_completo nombre_trabajador,
  v_trabajador.gestor_nro_identificacion identificacion_gestor,
  v_trabajador.gestor nombre_gestor          
  from v_trabajador
  WHERE numero_identificacion IN (select id_trabajador from pec);