CREATE OR REPLACE FORCE VIEW codensa_gth.v_inscritos_grupo (id_grupo,no_trabajador,trabajador,no_gestor,gestor,estado,id_pdi) AS
select 
id_grupo,
evaluado No_trabajador,
(select nombres||' '||apellidos from trabajador where numero_identificacion = evaluado) Trabajador,
evaluador No_gestor,
(select nombres||' '||apellidos from trabajador where numero_identificacion = evaluador) Gestor,
estado,
id id_pdi
from v_reporte_pdi where id_grupo is not null;