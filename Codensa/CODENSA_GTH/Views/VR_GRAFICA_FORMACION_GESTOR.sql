CREATE OR REPLACE FORCE VIEW codensa_gth.vr_grafica_formacion_gestor (evaluador,id_periodo,conteo,estado_negocio,proporcion) AS
select 
evaluador,
id_periodo,
sum(conteo) conteo,
estado_negocio,
to_char(sum(proporcion))||'%' proporcion
from
(select 
evaluador,
id_periodo,
count(*) conteo,
estado_negocio,
round(count(*)/(select count(*) from vr_reporte_pdi where evaluador = pdi.evaluador and id_periodo = pdi.id_periodo and ID_ACCION = 57)*100) proporcion
from vr_reporte_pdi pdi
WHERE ID_ACCION = 57
group by evaluador,id_periodo,estado_negocio
union all
SELECT 
evaluador,
id_periodo,
0 conteo,
'Pendiente' ESTADO_NEGOCIO,
0 proporcion
FROM VR_REPORTE_PDI PDI
WHERE ID_ACCION = 57
union all
SELECT 
evaluador,
id_periodo,
0 conteo,
'Realizado' ESTADO_NEGOCIO,
0 proporcion
FROM VR_REPORTE_PDI PDI
WHERE ID_ACCION = 57)
group by evaluador,id_periodo,estado_negocio;