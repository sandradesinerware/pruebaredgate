CREATE OR REPLACE PACKAGE codensa_gth."ADMIN_CURSO_PCK" AS 

--------------------------------------------
FUNCTION EXISTE_PDI_GRUPO (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
FUNCTION EXISTE_PDI_VIGENTE_GRUPO (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
FUNCTION INSCRITOS_GRUPO (i_id_grupo IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE CANCELAR_GRUPO (i_id_grupo IN NUMBER, i_comentario IN VARCHAR2, i_responsable in PDI_CONCEPTO.RESPONSABLE_APROBACION%type);
--------------------------------------------
FUNCTION GRUPO_SIN_INSCRITOS_VALIDOS (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
PROCEDURE CAMBIAR_ESTADO_GRUPO (i_id_grupo IN NUMBER, i_estado IN NUMBER);
--------------------------------------------
FUNCTION GRUPO_RESULTADO_COMPLETO (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
PROCEDURE FINALIZAR_GRUPO (i_id_grupo IN NUMBER);
--------------------------------------------
PROCEDURE FINALIZAR_GRUPO_POR_RESULTADO (i_id_grupo IN NUMBER);
--------------------------------------------
FUNCTION GRUPO_PRESENCIAL (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------

END ADMIN_CURSO_PCK;

/