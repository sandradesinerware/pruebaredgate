CREATE OR REPLACE PACKAGE codensa_gth."EVALUACION_FEEDBACK_PCK" AS 

Procedure Determinar_formulario_feed     (i_id_colaborador        in number,
                                        i_tipo_formulario       in number,
                                        i_periodo               in number,
                                        o_id_formulario         out number);

Procedure Administrar_pregunta_feed     (i_id_formulario        in number,
                                        i_id_pregunta           in number,
                                        i_respuesta_cerrada     in number,
                                        i_respuesta_abierta     in varchar2);                                                                               

Procedure Determinar_estado_form_feed   (i_id_formulario        in number,
                                        i_id_estado             in number,
                                        o_return                out number,
                                        o_mensaje               out varchar2);   


Procedure Existe_formulario_feed        (i_id_gestor            in number,
                                        i_id_colaborador        in number,
                                        i_tipo_formulario       in number,
                                        i_periodo               in number,
                                        o_return                out number,
                                        o_mensaje               out varchar2);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        

Function Determinar_pag_form_feed       (i_id_formulario        in number)
                                        return number;


Procedure Cambiar_estado_form_feed     (i_id_formulario         in number,
                                        i_id_estado             in number);                                         


Function Determinar_plantilla_feed      (i_tipo_formulario        in number)
                                        return number;   

END EVALUACION_FEEDBACK_PCK;

/