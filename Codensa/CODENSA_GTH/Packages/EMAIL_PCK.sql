CREATE OR REPLACE PACKAGE codensa_gth.EMAIL_PCK AS 
  
    procedure enviar_correo(
            receiver    IN VARCHAR2,
      msg         IN VARCHAR2,
      subject     IN VARCHAR2 );
     
END EMAIL_PCK;
/