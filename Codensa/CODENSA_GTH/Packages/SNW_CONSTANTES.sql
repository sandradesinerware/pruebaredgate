CREATE OR REPLACE PACKAGE codensa_gth."SNW_CONSTANTES" as
function constante_tipo(i_nombre in varchar2) return number;
function generar_constante_tipo(i_tipo_nombre in varchar2) return varchar2;
function constante_dependencia(i_nombre in varchar2) return number;

--Esta función retorna el id del tipo que corresponde a un nombre de dominio y tipo de dominio particular
function get_id_tipo(i_nombre in varchar2, i_tipo_constante in varchar2) return number;

end;
/