CREATE OR REPLACE PACKAGE codensa_gth."EVALUACION_PA_PCK" AS 

    Procedure Determinar_formulario (i_evaluado  IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
                                    i_ano        IN PERIODO_BARS.ANO%type, 
                                    i_tipo       IN FORMULARIO_BARS.TIPO%type, 
                                    o_formulario OUT FORMULARIO_BARS.ID%type
                                    );


    Procedure Dml_seccion_formulario (i_formulario IN  FORMULARIO_BARS.ID%type,
                                    i_seccion IN SECCION_BARS.ID%TYPE,
                                    i_tipo IN SECCION_FORMULARIO.TIPO%TYPE,
                                    i_num_rtasx IN NUMBER, 
                                    i_rta_1 IN RESPUESTA.valor%type DEFAULT NULL,
                                    i_rta_2 IN RESPUESTA.valor%type DEFAULT NULL, 
                                    i_rta_3 IN RESPUESTA.valor%type DEFAULT NULL,
                                    i_rta_4 IN RESPUESTA.valor%type DEFAULT NULL, 
                                    i_rta_5 IN RESPUESTA.valor%type DEFAULT NULL) 
                                    ;

    function  Determinar_pct_consecucion (i_formulario IN FORMULARIO_BARS.ID%TYPE, 
                                    i_tipo IN SECCION_FORMULARIO.TIPO%TYPE)
                                    RETURN PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE;

    Function Determinar_pag_formulario (i_formulario IN FORMULARIO_BARS.ID%type,
                                    i_tipo IN VARCHAR2) RETURN NUMBER;

    PROCEDURE Val_corresp_evaluado  (i_evaluador IN TRABAJADOR.numero_identificacion%type,
                                    i_evaluado IN TRABAJADOR.numero_identificacion%type, 
                                    i_ano IN PERIODO_BARS.ano%type, 
                                    o_return OUT NUMBER, 
                                    o_mensaje OUT VARCHAR2);

    PROCEDURE Val_estado_form_bars  (i_formulario IN FORMULARIO_BARS.id%type,
                                    i_estado IN ESTADO_BARS.NOMBRE%type, 
                                    o_return OUT NUMBER, 
                                    o_mensaje OUT VARCHAR2);

    PROCEDURE Val_encuesta_existe   (i_evaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
                                    i_ano IN PERIODO_BARS.ANO%type,
                                    i_tipo IN FORMULARIO_BARS.TIPO%type, 
                                    o_return OUT NUMBER,
                                    o_mensaje OUT VARCHAR2);

    PROCEDURE Cambiar_estado_form_bars (i_formulario IN FORMULARIO_BARS.ID%TYPE, 
                                    i_estado IN ESTADO_BARS.NOMBRE%TYPE);


    FUNCTION Determinar_promedio_encuesta (i_formulario IN FORMULARIO_BARS.ID%TYPE) 
                                    RETURN NUMBER ;   

    FUNCTION Val_editar_encuesta    (i_formulario IN FORMULARIO_BARS.ID%TYPE) 
                                    RETURN BOOLEAN ;                            






END EVALUACION_PA_PCK;

/