CREATE OR REPLACE PACKAGE codensa_gth."ADMIN_SESION_PCK" AS 

--------------------------------------------
FUNCTION SESIONES_GRUPO (i_id_grupo IN NUMBER) RETURN NUMBER;
--------------------------------------------
FUNCTION SESIONES_CON_ASISTENCIA (i_id_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE ACTUALIZAR_ASISTENTES_SESION (i_id_sesion NUMBER);
--------------------------------------------
FUNCTION SESIONES_COMPLETAS_GRUPO (i_id_grupo NUMBER) RETURN BOOLEAN;
--------------------------------------------

END ADMIN_SESION_PCK;

/