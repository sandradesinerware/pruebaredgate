CREATE OR REPLACE PACKAGE codensa_gth."ADMINISTRACION_PDI_PCK" AS 

--------------------------------------------
FUNCTION DETERMINAR_ESTADO_PDI
(i_id_pdi IN NUMBER, i_estado IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
PROCEDURE CAMBIAR_ESTADO_PDI
(i_id_pdi IN NUMBER, i_estado IN NUMBER);
--------------------------------------------
PROCEDURE REGISTRAR_PDI_CONCEPTO
(i_id_pdi IN NUMBER, i_estado IN NUMBER, i_comentario IN PDI_CONCEPTO.COMENTARIO%type, i_responsable in PDI_CONCEPTO.RESPONSABLE_APROBACION%type);
--------------------------------------------
PROCEDURE INSERTAR_COMENTARIO
(i_id_pdi IN NUMBER, i_comentario IN BARS_PDI.COMENTARIOS_GESTOR%type, i_tipo_comentario IN VARCHAR2 DEFAULT NULL);
--------------------------------------------
PROCEDURE CAMBIAR_CONCEPTO_PDI
(i_id_pdi IN NUMBER, i_estado IN NUMBER, i_comentario IN PDI_CONCEPTO.COMENTARIO%type DEFAULT NULL, i_responsable in PDI_CONCEPTO.RESPONSABLE_APROBACION%type);
--------------------------------------------
FUNCTION DISPONIBILIDAD_CUPO_GRUPO (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
FUNCTION NRO_CUPOS_DISP_GRUPO (i_id_grupo IN NUMBER) RETURN NUMBER;
--------------------------------------------
FUNCTION TRABAJADOR_GRUPO_CRUZADO (i_id_grupo IN NUMBER,i_id_bars_encpdi IN NUMBER) RETURN VARCHAR2;
--------------------------------------------
FUNCTION GRUPO_CRUZADO_CAMBIO (i_id_grupo IN NUMBER,i_id_bars_pdi IN NUMBER) RETURN VARCHAR2;
--------------------------------------------
FUNCTION TRABAJADOR_DOBLE_GRUPO_CURSO (i_id_grupo IN NUMBER,i_id_bars_encpdi IN NUMBER) RETURN VARCHAR2;
--------------------------------------------
FUNCTION DETERMINAR_ESTADO_GRUPO (i_id_grupo IN NUMBER,i_id_estado IN NUMBER  DEFAULT SNW_CONSTANTES.CONSTANTE_TIPO('ACTIVO_GRUPO')) RETURN BOOLEAN;
--------------------------------------------
FUNCTION GRUPO_ESTA_INICIADO (i_id_grupo IN NUMBER) RETURN BOOLEAN;
--------------------------------------------
FUNCTION VALIDAR_ESTADO_PERIODO_PDI (i_id_periodo_pdi IN NUMBER,i_id_estado_pdi IN NUMBER DEFAULT 1) RETURN BOOLEAN;
--------------------------------------------
FUNCTION MAXIMO_NRO_ACCIONES (i_id_bars_encpdi IN NUMBER, i_id_accion IN NUMBER, i_usuario IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN;
--------------------------------------------
FUNCTION MAX_NRO_ACCI_SINCRONICAS (i_id_bars_encpdi IN NUMBER, i_id_accion IN NUMBER, i_usuario IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN;
-----------------------------------------
FUNCTION LISTA_ESPERA (i_id_grupo IN NUMBER, i_id_periodo IN NUMBER DEFAULT NULL, 
  i_id_jurisdiccion_trabajador IN NUMBER DEFAULT NULL) RETURN VARCHAR2;
--------------------------------------------
FUNCTION CURSO_ANTERIOR(i_id_pdi IN NUMBER) RETURN VARCHAR2;
--------------------------------------------
FUNCTION HIZO_CURSO(i_id_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE REPROBAR_PDI_REPETICION_CURSO
(i_id_pdi IN NUMBER,i_responsable IN NUMBER);
--------------------------------------------
FUNCTION CALCULAR_AVANCE_PDI(i_id_bars_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
FUNCTION CALCULAR_AVANCE_IDEAL_PDI(i_id_bars_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE GUARDAR_AVANCE_PDI (i_id_bars_pdi in number);
--------------------------------------------
FUNCTION PORCENTAJE_REST_PDI (i_id_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE CAMBIAR_RESULTADO_PDI (i_id_pdi IN NUMBER ,i_responsable PDI_CONCEPTO.RESPONSABLE_APROBACION%type);
--------------------------------------------
FUNCTION DETERMINAR_ENCABEZADO_PDI (i_id_periodo IN NUMBER,i_gestor in number,i_colaborador in number) RETURN NUMBER;
--------------------------------------------
FUNCTION CREAR_ENCABEZADO(i_id_periodo IN NUMBER,i_gestor in number,i_colaborador in number) RETURN NUMBER;
-------------------------------------------- 
FUNCTION CONTEO_PDI_GESTOR(i_evaluado IN NUMBER,i_evaluador IN NUMBER,i_id_periodo IN NUMBER,
                            i_pdi_negocio IN VARCHAR2,i_estado_negocio IN VARCHAR2) RETURN NUMBER;
-------------------------------------------- 
FUNCTION PORCENTAJE_PDI_GESTOR(i_evaluado IN NUMBER,i_evaluador IN NUMBER,i_id_periodo IN NUMBER) RETURN NUMBER;
-------------------------------------------- 
FUNCTION OBTENER_RESULTADO_ADMIN(i_id_pdi IN NUMBER) RETURN NUMBER;
-------------------------------------------- 
FUNCTION ACCION_FECHA_CRUZADA (i_id_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------  
FUNCTION CREAR_PDI_BASICO (I_TIPO_ACCION IN NUMBER, I_ID_ENCABEZADO IN NUMBER, 
                            I_ID_ESTADO IN NUMBER, I_JUSTIFICACION IN VARCHAR2, 
                            I_ID_PDI_GRUPO IN NUMBER) 
RETURN NUMBER;
-------------------------------------------- 
FUNCTION PDI_RECHAZADO (i_id_pdi IN NUMBER) RETURN NUMBER;
--------------------------------------------
PROCEDURE MIGRAR_ENCABEZADO (i_evaluado IN NUMBER, i_periodo IN NUMBER);
---------------------------------------------
PROCEDURE MIGRAR_ENCABEZADOS (i_periodo IN NUMBER);
-------------------------------------------- 
FUNCTION VALIDAR_USUARIO (i_id_grupo IN NUMBER,i_numero_identificacion IN NUMBER) RETURN VARCHAR;
-------------------------------------------- 
FUNCTION TRABAJADOR_JURISDICCION (i_id_trabajador IN NUMBER,i_id_jurisdiccion IN NUMBER) RETURN BOOLEAN;
-------------------------------------------- 
FUNCTION TRABAJADOR_ROL (i_numero_identificacion IN NUMBER,i_id_rol IN NUMBER) RETURN BOOLEAN;

END ADMINISTRACION_PDI_PCK;
/