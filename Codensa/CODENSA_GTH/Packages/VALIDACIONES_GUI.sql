CREATE OR REPLACE PACKAGE codensa_gth."VALIDACIONES_GUI" as 

  /* 
 22 PLANEADO                                           
 23 CONCERTADO                                         
 24 FINALIZADO                                         
 25 ANULADO                                            
 26 NO APROBADO                                        
 30 APROBADO                                           
 31 PENDIENTE  */ 



function val_trans_estado_pdi_bool(
i_pdi in NUMBER,
i_estado in NUMBER,
o_msg OUT VARCHAR2 ) 
return boolean;


function val_boton_create_p78 
(
  i_accion in varchar2 ,
  i_usuario in varchar2
) return boolean;


function val_boton_save_p78 
(
  i_accion in varchar2 ,--Cambiar por i_accion
  i_usuario in varchar2
) return boolean;

function identificar_usuario (
i_usuario usuario.user_name%type)
return usuario.numero_identificacion%type;


function cargar_evaluado_accion (
i_accion bars_pdi.id%type) 
return bars_encpdi.evaluado%type;

function val_crud_formevaobj_evaluado(
i_formevaobj in bars_formevaobj.id%type,
i_username in usuario.user_name%type) 
return boolean;

FUNCTION val_crud_formevaobj_evaluador(
    i_formevaobj IN bars_formevaobj.id%type,
    i_username   IN usuario.user_name%type)
  RETURN BOOLEAN;

function val_crud_formevaobj_fch_concer(
    i_formevaobj in bars_formevaobj.id%type,
    i_username   in usuario.user_name%type)
  return BOOLEAN;

function val_crud_formevaobj_fch_aprob(
    i_formevaobj in bars_formevaobj.id%type,
    i_username   in usuario.user_name%type)
  return BOOLEAN;

function calc_peso_requerido_evaluacion(
    i_periodo NUMBER, 
    i_target NUMBER, 
    i_tipo_evaluacion NUMBER)
  return RESTRICCION_TARGET.porcentaje_total%TYPE;

function val_peso_total_objetivos(i_formulario BARS_FORMEVAOBJ.id%type,
i_tipo_objetivo TIPO.ID%type)
 return BOOLEAN;

 function val_peso_objetivo(i_formulario BARS_FORMEVAOBJ.id%type,
i_tipo_objetivo TIPO.ID%type,
i_ponderacion BARS_OBJETIVO.ponderacion%type)
 return BOOLEAN;


end validaciones_gui;
/