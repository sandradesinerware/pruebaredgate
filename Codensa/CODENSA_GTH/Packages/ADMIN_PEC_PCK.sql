CREATE OR REPLACE PACKAGE codensa_gth."ADMIN_PEC_PCK" AS 
-----------------------FLUJO DEL PEC-----------------------------
PROCEDURE Definir_Estado_PEC ( i_id_PEC NUMBER, i_id_PEC_estado NUMBER, i_fecha DATE );
FUNCTION Validar_Actividades_Pred( i_id_PEC NUMBER ) RETURN BOOLEAN;
FUNCTION Validar_Actividad_Adicional( i_id_PEC NUMBER ) RETURN BOOLEAN;
FUNCTION Validar_Evidencias_Act( i_id_PEC NUMBER ) RETURN BOOLEAN;
FUNCTION permite_crear_pec( I_ID_TRABAJADOR NUMBER ) RETURN VARCHAR;
PROCEDURE ejecutar_pec ( i_id_pec NUMBER );
PROCEDURE planear_pec ( i_id_pec NUMBER );

-----------------------DML DEL PEC-------------------------------
PROCEDURE CREAR_PEC_PREDETERMINADO( I_ID_TRABAJADOR NUMBER, I_FECHA_INGRESO DATE, i_motivo_ingreso NUMBER);
PROCEDURE Insertar_Actividades_Pred ( i_id_PEC NUMBER);
FUNCTION Eliminar_PEC ( i_id_pec number ) RETURN VARCHAR;

---------------------GESTIÓN DE ACTIVIDADES----------------------
FUNCTION Actividad_Requiere_Evidencia( i_id_actividad NUMBER ) RETURN NUMBER;
FUNCTION Numero_Actividades_PEC( i_id_PEC NUMBER ) RETURN NUMBER;

------------------EVALUACIÓN DEL PEC-----------------------------
PROCEDURE Crear_Evaluacion_PEC (i_id_PEC NUMBER);
PROCEDURE Evaluar_pregunta (i_id_evaluacion NUMBER, i_id_pregunta NUMBER, i_calificacion NUMBER);
PROCEDURE Calcular_Promedio_Evaluacion (i_id_evaluacion NUMBER);
FUNCTION Validar_Evaluacion_Hecha( i_id_PEC NUMBER ) RETURN BOOLEAN;

-----------------CARGUE DE LA EVIDENCIA--------------------------
PROCEDURE Guardar_Referencia_Evidencia(i_id_actividad NUMBER, i_id_archivo_evidencia NUMBER);
FUNCTION Obtener_Url_Archivo_Evidencia( i_id_pec NUMBER, i_id_actividad NUMBER) RETURN VARCHAR2;

------------------TAREAS AUTOMÁTICAS DEL PEC---------------------
PROCEDURE GESTION_AUTO_PEC_POR_PLANEAR(i_pec IN PEC%ROWTYPE, i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) );
PROCEDURE GESTION_AUTO_PEC_PLANEADO(i_pec IN PEC%ROWTYPE, i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) );
PROCEDURE GESTION_AUTO_PEC_EJECUTADO( i_pec IN PEC%ROWTYPE, i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) );
PROCEDURE GESTION_AUTOMATICA_PEC( i_fecha_ejecutar IN DATE DEFAULT TRUNC(SYSDATE) );
PROCEDURE ELIMINAR_DUPLICADOS;

END ADMIN_PEC_PCK;
/