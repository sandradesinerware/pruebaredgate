CREATE OR REPLACE package codensa_gth.restful_pck as
-------------------------------------------------------- 
 type dict_row is record ( name varchar2(2000) default null, value varchar2(2000) default null );
-------------------------------------------------------- 
 type dict_table is table of dict_row;
-------------------------------------------------------- 
 type response is record ( status_code number, headers dict_table, body clob, blob_body blob );
-------------------------------------------------------- 
 function make_request (
 i_url in varchar2,
 i_http_method in varchar2 default 'GET',
 i_clob_body in clob default null,
 i_blob_body in blob default null,
 i_parameters in dict_table default dict_table(),
 i_headers in dict_table default dict_table(),
 i_fetch_blob in boolean default false,
 i_wallet in varchar2 default null,
 i_wallet_pwd in varchar2 default null ) return response;
--------------------------------------------------------
 procedure print_clob ( p_clob in clob );
--------------------------------------------------------
 function new_dict ( i_name in varchar2, i_value in varchar2 ) return dict_row;
--------------------------------------------------------
end restful_pck;
/