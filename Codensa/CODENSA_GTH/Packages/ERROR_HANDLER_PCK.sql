CREATE OR REPLACE PACKAGE codensa_gth."ERROR_HANDLER_PCK" AS 


FUNCTION fun_catch_error(p_error IN apex_error.t_error)
RETURN apex_error.t_error_result;
END ERROR_HANDLER_PCK;

/