CREATE OR REPLACE TRIGGER codensa_gth."BI_RESPUESTA_EVALUACION" 
  BEFORE INSERT ON codensa_gth."RESPUESTA_EVALUACION"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "RESPUESTA_CLIENTE_INTERNO_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/