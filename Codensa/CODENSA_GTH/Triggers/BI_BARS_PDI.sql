CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_PDI" 
  BEFORE INSERT ON codensa_gth."BARS_PDI"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select BARS_PDI_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/