CREATE OR REPLACE TRIGGER codensa_gth."BI_OPCION" 
  BEFORE INSERT ON codensa_gth."OPCION"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "OPCION_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/