CREATE OR REPLACE TRIGGER codensa_gth."BI_TEMP_TRABAJADOR" 
  BEFORE INSERT ON codensa_gth."TEMP_TRABAJADOR"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "TEMP_TRABAJADOR_SEQ".nextval into :new."ID" from dual;
  end if;
end;
/