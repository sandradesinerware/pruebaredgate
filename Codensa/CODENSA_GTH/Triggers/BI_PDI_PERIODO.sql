CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_PERIODO" 
  BEFORE INSERT ON codensa_gth."PDI_PERIODO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_PERIODO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/