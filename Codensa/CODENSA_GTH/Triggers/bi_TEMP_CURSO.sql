CREATE OR REPLACE TRIGGER codensa_gth."bi_TEMP_CURSO" 
  BEFORE INSERT ON codensa_gth."TEMP_CURSO"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "TEMP_CURSO_SEQ".nextval into :new."ID" from dual;
  end if;
end;
/