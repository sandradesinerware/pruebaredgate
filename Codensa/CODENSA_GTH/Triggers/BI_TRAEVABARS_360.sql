CREATE OR REPLACE TRIGGER codensa_gth."BI_TRAEVABARS_360" 
  BEFORE INSERT ON codensa_gth."TRAEVABARS_360"
  REFERENCING FOR EACH ROW
  begin
if :NEW."ID" is null then
select "TRAEVABARS_360_SEQ".nextval into :NEW."ID" from dual;
end if;
end;
/