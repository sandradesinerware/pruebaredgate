CREATE OR REPLACE TRIGGER codensa_gth."BI_TEMP_CARGUE_NO_PDI" 
  BEFORE INSERT OR UPDATE ON codensa_gth."TEMP_CARGUE_NO_PDI"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select TEMP_CARGUE_NO_PDI_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/