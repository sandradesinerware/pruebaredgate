CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_EQUIVALENCIA" 
  BEFORE INSERT ON codensa_gth."PDI_EQUIVALENCIA"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_EQUIVALENCIA_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/