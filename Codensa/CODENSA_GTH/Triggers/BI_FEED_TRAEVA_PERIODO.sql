CREATE OR REPLACE TRIGGER codensa_gth."BI_FEED_TRAEVA_PERIODO" 
  BEFORE INSERT ON codensa_gth."FEED_TRAEVA_PERIODO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "FEED_TRAEVA_PERIODO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/