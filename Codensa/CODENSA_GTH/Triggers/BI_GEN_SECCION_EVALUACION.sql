CREATE OR REPLACE TRIGGER codensa_gth."BI_GEN_SECCION_EVALUACION" 
  BEFORE INSERT ON codensa_gth."GEN_SECCION_EVALUACION"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "GEN_SECCION_EVALUACION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/