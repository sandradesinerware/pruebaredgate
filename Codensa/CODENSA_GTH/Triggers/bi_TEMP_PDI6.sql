CREATE OR REPLACE TRIGGER codensa_gth."bi_TEMP_PDI6" 
  BEFORE INSERT ON codensa_gth."TEMP_PDI6"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "TEMP_PDI6_SEQ".nextval into :new."ID" from dual;
  end if;
end;
/