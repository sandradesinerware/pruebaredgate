CREATE OR REPLACE TRIGGER codensa_gth."BI_PROMEDIO_PRINCIPIO_360" 
  BEFORE INSERT ON codensa_gth."PROMEDIO_PRINCIPIO_360"
  REFERENCING FOR EACH ROW
  begin

IF (:NEW.ID IS NULL) THEN
SELECT SEQ_PROMEDIO_PRINCIPIO_360.nextval into :NEW.ID FROM dual;
END IF;

end;
/