CREATE OR REPLACE TRIGGER codensa_gth."BI_PERIODO_CLIENTE_INTERNO" 
  BEFORE INSERT ON codensa_gth."PERIODO_CLIENTE_INTERNO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PERIODO_CLIENTE_INTERNO_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/