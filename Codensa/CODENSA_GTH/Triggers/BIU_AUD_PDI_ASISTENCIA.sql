CREATE OR REPLACE TRIGGER codensa_gth.BIU_AUD_PDI_ASISTENCIA 
      BEFORE INSERT OR UPDATE ON codensa_gth.PDI_ASISTENCIA FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;        
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;
/