CREATE OR REPLACE TRIGGER codensa_gth."BI_ESTADO_BARS" 
  BEFORE INSERT ON codensa_gth."ESTADO_BARS"
  REFERENCING FOR EACH ROW
  begin
if :NEW."ID" is null then
select "ESTADO_BARS_SEQ".nextval into :NEW."ID" from dual;
end if;
end;
/