CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_SESION" 
  BEFORE INSERT OR UPDATE ON codensa_gth."PDI_SESION"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select PDI_SESION_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/