CREATE OR REPLACE TRIGGER codensa_gth."BI_FEED_FORMULARIO" 
  BEFORE INSERT ON codensa_gth."FEED_FORMULARIO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "FEED_FORMULARIO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/