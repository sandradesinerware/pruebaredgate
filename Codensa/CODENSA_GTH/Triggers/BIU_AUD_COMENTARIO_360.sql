CREATE OR REPLACE TRIGGER codensa_gth."BIU_AUD_COMENTARIO_360" 
  BEFORE INSERT OR UPDATE ON codensa_gth."COMENTARIO_360"
  REFERENCING FOR EACH ROW
  BEGIN
  IF inserting THEN
   :NEW.AUD_FECHA_CREACION := SYSDATE;
   :NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);
  END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;
/