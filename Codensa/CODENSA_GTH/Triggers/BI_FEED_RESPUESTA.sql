CREATE OR REPLACE TRIGGER codensa_gth."BI_FEED_RESPUESTA" 
  BEFORE INSERT ON codensa_gth."FEED_RESPUESTA"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "FEED_RESPUESTA_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/