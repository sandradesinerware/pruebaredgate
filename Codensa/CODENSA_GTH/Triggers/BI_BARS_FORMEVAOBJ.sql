CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_FORMEVAOBJ" 
  BEFORE INSERT ON codensa_gth."BARS_FORMEVAOBJ"
  REFERENCING FOR EACH ROW
  begin
    if(:NEW.ID is null) then
    SELECT BARS_FORMEVAOBJ_SEQ.nextval into :NEW.ID from dual;
    end if;
end;
/