CREATE OR REPLACE TRIGGER codensa_gth."BI_DEPENDENCIA" 
  BEFORE INSERT ON codensa_gth."DEPENDENCIA"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "DEPENDENCIA_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/