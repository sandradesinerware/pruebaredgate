CREATE OR REPLACE TRIGGER codensa_gth."BIU_TIPO" 
  BEFORE INSERT OR UPDATE ON codensa_gth."TIPO"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "TIPO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
  :NEW.aud_fecha_creacion      := sysdate;
  :NEW.AUD_CREADO_POR          := nvl(v('APP_USER'),USER);

  end if;
  :NEW.aud_fecha_actualizacion := sysdate;
  :NEW.AUD_ACTUALIZADO_POR     := nvl(v('APP_USER'),USER);
  :NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;
/