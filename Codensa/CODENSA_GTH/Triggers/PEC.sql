CREATE OR REPLACE TRIGGER codensa_gth."PEC" 
  BEFORE INSERT OR UPDATE ON codensa_gth."PEC"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select PEC_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/