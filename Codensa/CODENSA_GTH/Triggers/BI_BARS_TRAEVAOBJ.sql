CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_TRAEVAOBJ" 
  BEFORE INSERT ON codensa_gth."BARS_TRAEVAOBJ"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "BARS_TRAEVAOBJ_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/