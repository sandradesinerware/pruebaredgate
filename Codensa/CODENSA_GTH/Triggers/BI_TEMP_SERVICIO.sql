CREATE OR REPLACE TRIGGER codensa_gth."BI_TEMP_SERVICIO" 
  BEFORE INSERT ON codensa_gth."TEMP_SERVICIO"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "TEMP_SERVICIO_SEQ".nextval into :new."ID" from dual;
  end if;
end;
/