CREATE OR REPLACE TRIGGER codensa_gth."BI_GEN_PLANTILLA_EVALUACION" 
  BEFORE INSERT ON codensa_gth."GEN_PLANTILLA_EVALUACION"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "GEN_PLANTILLA_EVALUACION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/