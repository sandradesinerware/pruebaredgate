CREATE OR REPLACE TRIGGER codensa_gth."BI_TEMP_EVALUACION" 
  BEFORE INSERT ON codensa_gth."TEMP_EVALUACION"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "TEMP_EVALUACION_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/