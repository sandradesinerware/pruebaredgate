CREATE OR REPLACE TRIGGER codensa_gth."BARS_TR_BARS_RENDIMIENTOPRE" 
  INSTEAD OF UPDATE ON codensa_gth."BARS_RENDIMIENTOPRE"
  REFERENCING FOR EACH ROW
  begin
update bars_traevapre set pospre_final=:new.posicion_final_id where trabajador=:new.numero_identificacion and mesahompre=:new.mesa;
end;
/