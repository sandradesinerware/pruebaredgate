CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_HISTORICO" 
  BEFORE INSERT ON codensa_gth."PDI_HISTORICO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_HISTORICO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/