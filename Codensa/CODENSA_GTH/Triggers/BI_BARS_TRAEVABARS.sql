CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_TRAEVABARS" 
  BEFORE INSERT ON codensa_gth."BARS_TRAEVABARS"
  REFERENCING FOR EACH ROW
  begin
if :NEW."ID" is null then
select "BARS_TRAEVABARS_SEQ".nextval into :NEW."ID" from dual;
end if;
end;
/