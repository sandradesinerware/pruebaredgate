CREATE OR REPLACE TRIGGER codensa_gth."BI_SERVICIO" 
  BEFORE INSERT ON codensa_gth."SERVICIO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "SERVICIO_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/