CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_FOCO" 
  BEFORE INSERT OR UPDATE ON codensa_gth."PDI_FOCO"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select PDI_FOCO_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/