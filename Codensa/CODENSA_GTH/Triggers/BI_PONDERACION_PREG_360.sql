CREATE OR REPLACE TRIGGER codensa_gth."BI_PONDERACION_PREG_360" 
  BEFORE INSERT ON codensa_gth."PONDERACION_PREG_360"
  REFERENCING FOR EACH ROW
  begin
if :NEW.ID is null then
select PONDERACION_PREG_360_SEQ.nextval into :NEW.ID from dual;
end if;
end;
/