CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_ENCPDI" 
  BEFORE INSERT ON codensa_gth."BARS_ENCPDI"
  REFERENCING FOR EACH ROW
  begin
  if (:NEW.ID is null) then
  select BARS_ENCPDI_SEQ.nextval into :NEW.ID from dual;
  end if;
end;
/