CREATE OR REPLACE TRIGGER codensa_gth."BIU_AUD_CARGUE_CONCER_OBJ" 
  BEFORE INSERT OR UPDATE ON codensa_gth."CARGUE_CONCER_OBJ"
  REFERENCING FOR EACH ROW
  BEGIN
    --  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.
    --
    --    MODIFIED   (YYYY/MM/DD)
    --      jsalamanca 2015-05-05 - Created
    --
    --    VERSION 1.0
    --    Trigger que pobla las columnas de auditoria
    ----------------------------------------------------------------------------------------------------------------
  IF inserting THEN
  if(:NEW.ID is null) then 
  select CARGUE_CONCER_OBJ_SEQ.nextval into :NEW.ID from dual;
  end if;
   :NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);
   :NEW.AUD_FECHA_CREACION := SYSDATE;   
   :NEW.AUD_VERSION        := 1;
  END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
begin
SELECT owa_util.get_cgi_env ('REMOTE_ADDR') into :NEW.AUD_TERMINAL_ACTUALIZACION FROM DUAL;--Capturar Apex
exception
when others then
:NEW.AUD_TERMINAL_ACTUALIZACION:=SYS_CONTEXT('USERENV','IP_ADDRESS');--Capturar SQL developer
end;
:NEW.AUD_VERSION                := NVL(:OLD.AUD_VERSION,0)+1;
END;
/