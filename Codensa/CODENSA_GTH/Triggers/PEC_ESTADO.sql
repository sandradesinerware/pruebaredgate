CREATE OR REPLACE TRIGGER codensa_gth."PEC_ESTADO" 
  BEFORE INSERT OR UPDATE ON codensa_gth."PEC_ESTADO"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select PEC_ESTADO_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/