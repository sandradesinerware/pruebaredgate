CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_GRUPO" 
  BEFORE INSERT ON codensa_gth."PDI_GRUPO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_GRUPO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/