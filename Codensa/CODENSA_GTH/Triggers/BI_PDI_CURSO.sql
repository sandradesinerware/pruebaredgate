CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_CURSO" 
  BEFORE INSERT ON codensa_gth."PDI_CURSO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_CURSO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/