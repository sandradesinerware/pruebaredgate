CREATE OR REPLACE TRIGGER codensa_gth."BI_EVALUACION" 
  BEFORE INSERT ON codensa_gth."EVALUACION"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "EVALUACION_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/