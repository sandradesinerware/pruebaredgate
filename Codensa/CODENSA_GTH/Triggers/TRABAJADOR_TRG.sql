CREATE OR REPLACE TRIGGER codensa_gth.TRABAJADOR_TRG 
BEFORE INSERT ON codensa_gth.TRABAJADOR 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT TRABAJADOR_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/