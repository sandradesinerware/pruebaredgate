CREATE OR REPLACE TRIGGER codensa_gth."BI_PDI_CONCEPTO" 
  BEFORE INSERT ON codensa_gth."PDI_CONCEPTO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_CONCEPTO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/