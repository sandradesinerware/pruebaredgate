CREATE OR REPLACE TRIGGER codensa_gth."bi_OBJETIVOS_TEMP" 
  BEFORE INSERT ON codensa_gth."BARS_CARGUEOPR"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "OBJETIVOS_TEMP_SEQ".nextval into :new."ID" from sys.dual;
  end if;
end;
/