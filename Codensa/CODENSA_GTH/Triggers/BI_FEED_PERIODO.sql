CREATE OR REPLACE TRIGGER codensa_gth."BI_FEED_PERIODO" 
  BEFORE INSERT ON codensa_gth."FEED_PERIODO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "FEED_PERIODO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;
/