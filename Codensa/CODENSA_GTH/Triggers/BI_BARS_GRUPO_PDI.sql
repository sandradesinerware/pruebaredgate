CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_GRUPO_PDI" 
  BEFORE INSERT ON codensa_gth."BARS_GRUPO_PDI"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "BARS_GRUPO_PDI_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/