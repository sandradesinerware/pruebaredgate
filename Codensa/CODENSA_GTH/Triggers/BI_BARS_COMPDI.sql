CREATE OR REPLACE TRIGGER codensa_gth."BI_BARS_COMPDI" 
  BEFORE INSERT OR UPDATE ON codensa_gth."BARS_COMPDI"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select BARS_COMPDI_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;
/