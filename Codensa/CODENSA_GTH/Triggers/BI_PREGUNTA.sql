CREATE OR REPLACE TRIGGER codensa_gth."BI_PREGUNTA" 
  BEFORE INSERT ON codensa_gth."PREGUNTA"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PREGUNTA_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/