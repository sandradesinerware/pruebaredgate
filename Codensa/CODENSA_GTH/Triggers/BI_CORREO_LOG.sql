CREATE OR REPLACE TRIGGER codensa_gth."BI_CORREO_LOG" 
  BEFORE INSERT ON codensa_gth."CORREO_LOG"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "CORREO_LOG_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;
/